/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

  package uk.ac.ed.cyphsem.cli;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;


/**
 * This class regroups the examples structure ({@link Graph}s, {@link PropertyKey},
 *
*/
public class Sample {

  /** Sample empty graph */
  static public final Graph emptyGraph = new Graph();

  /** Sample graph with a single node (and no relation, label, properties). */
  static public final Graph singleNodeGraph = new Graph();


/** Sample graph to be used in the testing of queries.
 * Ascii representation:
<pre>
                +--------[loves]---------+
                |                        |
                |                        |
                +---(cat tabby savvy) &lt;--+
                    (name:"zelia"   )
                       /\
                       |
                       |
                 [has       ]
                 [since:2018]
                       |
                       |                    [knows     ]
      + -------&gt; (human        )------------[since:2011]----------------+
      |          (name:"Nadime")-----+                                  |
      |             |       |        |                                  |
[knows     ]        |       |        |                                  |
[since:2008]    [kept   ]   |    [kept   ]                              v
      |         [in:2015]   |    [in:2017]---&gt; (cat) &lt;---[has]---(name:"Leonid")
      |             |       |                                           |
      +-----+       |       +---[kept   ]------------+                  |
            |       |           [in:2017]            |                  |
            |       |                                |                  |
            |       +-----------------+              |                  |
            |                         |              v                  |
      (human       )                  +--&gt; (cat blue savvy     )        |
 +--&gt; (name:"Victor")---[has       ]-----&gt; (name:"Athanase"    )        |
 |                      [since:2014]       (breed:"RussianBlue")        |
 |                                                                      |
 +------------------------------[knows     ] ---------------------------+
                                [since:2017]
</pre>
*/
  static public final Graph catGraph = new Graph();

  /** The node of {@link #singleNodeGraph}. */
  static public final Node n0 = singleNodeGraph.addNode();

  /** A node of {@link #catGraph}. */
  static public final Node leonid = catGraph.addNode(new HashSet<>(), new HashMap<>());
  /** A node of {@link #catGraph}. */
  static public final Node leonidsCat = catGraph.addNode(new HashSet<>(), new HashMap<>());
  /** A node of {@link #catGraph}. */
  static public final Node victor = catGraph.addNode(new HashSet<>(), new HashMap<>());
  /** A node of {@link #catGraph}. */
  static public final Node athanase = catGraph.addNode(new HashSet<>(), new HashMap<>());
  /** A node of {@link #catGraph}. */
  static public final Node nadime = catGraph.addNode(new HashSet<>(), new HashMap<>());
  /** A node of {@link #catGraph}. */
  static public final Node zelia = catGraph.addNode(new HashSet<>(), new HashMap<>());

  static public final Value leonid_v = new Value(leonid);
  static public final Value leonidsCat_v = new Value(leonidsCat);
  static public final Value victor_v = new Value(victor);
  static public final Value athanase_v = new Value(athanase);
  static public final Value nadime_v = new Value(nadime);
  static public final Value zelia_v = new Value(zelia);

  static public final RelationType knows = new RelationType("knows");
  static public final RelationType kept = new RelationType("kept");
  static public final RelationType has = new RelationType("has");
  static public final RelationType loves = new RelationType("loves");

  static public final Label human = new Label("human");
  static public final Label cat = new Label("cat");
  static public final Label savvy = new Label("savvy");
  static public final Label tabby = new Label("tabby");
  static public final Label blue = new Label("blue");

  static public final PropertyKey breed = new PropertyKey("breed");
  static public final PropertyKey name = new PropertyKey("name");
  static public final PropertyKey since = new PropertyKey("since");
  static public final PropertyKey in = new PropertyKey("in");

  static public final Relation r1 = catGraph.addRelation(has, leonid, leonidsCat);
  static public final Relation r2 = catGraph.addRelation(has, victor, athanase);
  static public final Relation r3 = catGraph.addRelation(has, nadime, zelia);

  static public final Relation r4 = catGraph.addRelation(kept, nadime, leonidsCat);
  static public final Relation r5 = catGraph.addRelation(kept, nadime, athanase);
  static public final Relation r6 = catGraph.addRelation(kept, nadime, athanase);

  static public final Relation r7 = catGraph.addRelation(knows, leonid, victor, new HashMap<>());
  static public final Relation r8 = catGraph.addRelation(knows, victor, nadime, new HashMap<>());
  static public final Relation r9 = catGraph.addRelation(knows, nadime, leonid, new HashMap<>());
  static public final Relation r0 = catGraph.addRelation(loves, zelia, zelia, new HashMap<>());

  static {
    leonid.overwriteProperty(name, new Value("Leonid"));

    leonidsCat.addLabel(cat);

    victor.overwriteProperty(name, new Value("Victor"));
    victor.addLabel(human);
    victor.addLabel(savvy);


    athanase.overwriteProperty(name, new Value("Athanase"));
    athanase.overwriteProperty(breed, new Value("RussianBlue"));
    athanase.addLabel(cat);
    athanase.addLabel(blue);
    athanase.addLabel(savvy);

    nadime.overwriteProperty(name, new Value("Nadime"));
    nadime.addLabel(human);

    zelia.overwriteProperty(name, new Value("Zelia"));
    zelia.addLabel(savvy);
    zelia.addLabel(cat);
    zelia.addLabel(tabby);

    r2.overwriteProperty(since, new Value(2014));
    r3.overwriteProperty(since, new Value(2018));

    r4.overwriteProperty(in, new Value(2017));
    r5.overwriteProperty(in, new Value(2015));
    r6.overwriteProperty(in, new Value(2017));

    r7.overwriteProperty(since, new Value(2017));
    r8.overwriteProperty(since, new Value(2008));
    r9.overwriteProperty(since, new Value(2011));
  }

  static public final Name a = new Name("a");
  static public final Name b = new Name("b");
  static public final Name c = new Name("c");
  static public final Name d = new Name("d");
  static public final Name e = new Name("e");
  static public final Name f = new Name("f");

  static public final Table unitTable = new Table(new Record());
  static public final Table emptyTable = new Table();
  static public final Table aToCats = new Table (
      new Record ( a, athanase_v  ),
      new Record ( a, zelia_v     ),
      new Record ( a,leonidsCat_v )
    );



}
