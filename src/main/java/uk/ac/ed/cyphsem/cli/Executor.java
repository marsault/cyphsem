/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.cli;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.Parameters;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.language.parser.Parser;

import uk.ac.ed.cyphsem.language.query.Query;
import uk.ac.ed.cyphsem.language.query.QueryData;

import uk.ac.ed.cyphsem.table.Table;

import uk.ac.ed.cyphsem.outbridge.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

import java.util.Arrays;
import java.util.List;




public class Executor {

  public static final String usage = "Usage: cyphsem-executor <query> [-F] [--graph=<file_with_input_graph>] [--set=<semantics_for_set>] [--create=<semantics_for_create>] [--merge=<semantics_for_merge>] ";

  public static void main (String[] args) throws Exception {
/*
    if ((args.length != 1) && (args.length != 2)) {
      System.out.println("");
      System.exit(1);
    }*/


    boolean f_flag = false;
    String query_str = null;
    String graph_str = null;
    for (int i = 0 ; i<args.length ; i++) {
      if (args[i].equals("-F")) {
        f_flag = true;
      }
      else if (args[i].startsWith("--")) {
        String[] strs = args[i].split("=");
        if (strs.length != 2)
          throw new RuntimeException(usage+"\n\n"+"Invalid option at position"+i+".");
        String key = strs[0].substring(2);
        String value = strs[1];
        if (key.equals("graph"))
          graph_str = value;
        else
          try { Parameters.global.setParameter(key,value); }
          catch (IllegalArgumentException e) { throw new RuntimeException(usage,e); }
      }
      else if (args[i].startsWith("-")) {
        throw new RuntimeException(usage+"\n\n"+"Unknown option"+args[i]+".");
      }
      else
          query_str = args[i];
    }

    if (query_str == null) {
      System.out.println(usage);
      System.exit(1);
    }

    Graph graph = (graph_str == null) ? Sample.catGraph : Parser.graphOf(Paths.get(graph_str));
    Query query = (f_flag)
                  ? Parser.queryOf(Paths.get(query_str))
                  : Parser.queryOf(args[0]);

    QueryData result = query.execute(graph);
    System.out.println(result.table());
    System.out.println();
    System.out.println(result.graph().toCypher());
  }

}
