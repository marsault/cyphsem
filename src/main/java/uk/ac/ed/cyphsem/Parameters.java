/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem;

import uk.ac.ed.cyphsem.Parameters.*;
import uk.ac.ed.cyphsem.utils.*;

import java.util.NoSuchElementException;
import java.lang.IllegalArgumentException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import java.util.function.Supplier;
import java.util.function.Consumer;
import java.util.function.BiConsumer;
import java.util.function.Function;


/** This class implements the dynamic parameters for semantics.
  * Static field {@link #global} gives the current global instance of parameters
  * to be used during computation.
***/

public class Parameters {

  public final static Parameters global = new  Parameters();
  public static Parameters make() { return new Parameters(); }

  private static final Map<String, ParameterGetterSetter<? extends Enum<?>>> map
    = new HashMap<>();


//   private static interface EnumBuilder<T> {
//     default T
//   }

  private static class ParameterGetterSetter<T extends Enum<T>>
  {
    Class<T> enumClass;
    Function<Parameters,T> supplier;
    BiConsumer<Parameters, T> consumer;

    public T get(Parameters p) { return supplier.apply(p); }

    public void setParameter(Parameters p, String value) {
      String uppercase_value = value.toUpperCase();
      try {  consumer.accept (p, Enum.valueOf(enumClass, uppercase_value));  }
      catch (IllegalArgumentException e) {
        throw new IllegalArgumentException(
            "Value "+ value +", implicitly rewritten as " + uppercase_value
            + ", does not define a proper instance of enum " + enumClass.getCanonicalName() + "."
            +"  Acceptable values are : " + StringOf.iterable(Arrays.asList(enumClass.getEnumConstants()),"","",", ") + ".");
      }
    }

    private ParameterGetterSetter
      (Class<T> c, Function<Parameters, T> supplier,  BiConsumer<Parameters, T> consumer)
    {
      this.enumClass = c;
      this.supplier = supplier;
      this.consumer = consumer;
    }
  }

  public static enum MergeVersion {
    LEGACY,
    ATOMIC,
    GROUPING,
    COLLAPSE,
    AGGRESSIVE;
  }
  private MergeVersion merge = MergeVersion.AGGRESSIVE;
  public MergeVersion merge() { return merge; }
  static {
    map.put("merge",
      new ParameterGetterSetter<MergeVersion>(
        MergeVersion.class,
        ( p -> p.merge ),
        ( (p,a)  -> p.merge = a )
      )
    );
  }

  public static enum EqualityVersion {
    PERMISSIVE,
    STRICT;
  }
  private EqualityVersion equality = EqualityVersion.PERMISSIVE;
  static {
    map.put("equality",
      new ParameterGetterSetter<EqualityVersion>(
        EqualityVersion.class,
        p -> p.equality,
        (p,a) -> p.equality = a));
  }

  public static enum SetVersion {
    LEGACY,
    PERMISSIVE,
    STRICT;
  }
  private SetVersion set = SetVersion.STRICT;
  static {
    map.put("set",
      new ParameterGetterSetter<SetVersion>(
        SetVersion.class,
        ( p -> p.set ),
        (p,a) -> p.set = a
      ));
  }

  public static enum DeleteVersion {
    LEGACY,
    AMNESIC,
    TOMBSTONE;
  }
  private DeleteVersion delete = DeleteVersion.TOMBSTONE;
  static {
    map.put("delete",
      new ParameterGetterSetter<DeleteVersion>(
        DeleteVersion.class,
        p -> p.delete,
        (p,a) -> p.delete = a
      ));
  }







  public void setParameter(String key, String value)
  throws IllegalArgumentException
  {  getGetterSetter(key).setParameter(this,value);  }

  @SuppressWarnings("rawtypes")
  private static ParameterGetterSetter getGetterSetter(String key)
  throws IllegalArgumentException
  {
    if (map.containsKey(key))
      return map.get(key);
    else
      throw new IllegalArgumentException("Parameter "+key+" does not exists");
  }

  private Enum<?> get(String key) {
    return getGetterSetter(key).get(this);
  }

  public EqualityVersion equality() {  return equality;  }
  public SetVersion set() {  return set;  }
  public DeleteVersion delete() {  return delete;  }

  public Set<String> allParams() {  return map.keySet();  }

  public String toString() {
    String res = "{";
    String sep = "";
    for (String param : allParams())
    {
      res += sep + param + "=" + get(param);
      sep = ", ";
    }
    res+= "}";
    return res;
  }

}
