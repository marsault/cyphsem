/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem;

import uk.ac.ed.cyphsem.*;
import uk.ac.ed.cyphsem.Parameters.*;
import uk.ac.ed.cyphsem.Parameters.MergeVersion.*;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import java.io.IOException;


import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.HashMap;

import java.util.function.Predicate;
import java.util.function.Function;
import java.util.function.BiFunction;

import java.util.stream.Collectors;



public class Utils {

  public static enum Policy {
    IGNORE_WITH_TRUE,
    IGNORE_WITH_FALSE,
    PROPAGATE;
  }

  public static class ExceptionPolicy<E extends Exception> {



    private final Policy policy;

    public ExceptionPolicy(Policy policy) { this.policy = policy; }
    public ExceptionPolicy() { policy = Policy.PROPAGATE; }
    public ExceptionPolicy(boolean b) { policy = b ? Policy.IGNORE_WITH_TRUE
                                            : Policy.IGNORE_WITH_FALSE; }

    public boolean handle(E exception) throws E {
      switch (policy) {
        case IGNORE_WITH_TRUE: return true;
        case IGNORE_WITH_FALSE: return false;
        case PROPAGATE: throw exception;
      }
      throw new UnreachableStatementError();
    }

  };

  public static <E,F> String mapToString(Map<E,F> map) {
    Set<E> keys = map.keySet();
    String res = "{";
    String sep = "";
    for (E key : keys) {
      res += sep;
      res += key.toString();
      res += ":";
      res += map.get(key).toString();
      sep = ",";
    }
    return res+"}";
  }




//   public static class Pair<E,F> {
//     public E first;
//     public F second;
//
//     public Pair() {};
//     public Pair(E first, F second) { this.first = first; this.second = second; }
//
//     public String toString() {
//       return ("("+first.toString()+","+second.toString()+")");
//     }
//   }
//
//   static class Triplet<E,F,G> {
//     public E first;
//     public F second;
//     public G third;
//
//     public Triplet(E first, F second, G third) {
//       this.first = first;
//       this.second = second;
//       this.third = third;
//     }
//   }

//  public static String readFile(String path)
//  throws IOException {
//    return readFile(new File(path));
//  }
//
//  public static String readFile(File f)
//  throws IOException {
//    return readFile(f, StandardCharsets.UTF_8);
//  }
//
//  public static String readFile(String path, Charset encoding)
//  throws IOException
//  {
//    return readFile(new File(path), encoding);
//  }
//
//  public static String readFile(File f, Charset encoding)
//  throws IOException
//  {
//    byte[] encoded = Files.readAllBytes(f.toPath());
//    return new String(encoded, encoding);
//  }

  public static <E,F> List<F> map (List<E> input, Function<? super E,F> f) {
    return input.stream().map(e -> f.apply(e)).collect(Collectors.toList());
  }

  public static <E,F> List<F> flatMap (List<E> input, Function<? super E,? extends Stream<? extends F>> mapper) {
    return input.stream().flatMap(mapper).collect(Collectors.toList());
  }

  public static <E,F> Set<F> flatMap (Set<E> input, Function<? super E,? extends Stream<? extends F>> mapper) {
    return input.stream().flatMap(mapper).collect(Collectors.toSet());
  }

  @SafeVarargs
  public static <E> List<E> concat (List<? extends E>... lists) {
    int n = 0;
    for (List<? extends E> l : lists)
      n+= l.size();
    List<E> result = new ArrayList<> (n);
    for (List<? extends E> l : lists)
      result.addAll(l);
    return result;
  }

  public static <E,F> Set<F> map (Set<E> input, Function<? super E,F> f) {
    return input.stream().map(e -> f.apply(e)).collect(Collectors.toSet());
  }

  public static <E> Set<E> filter (Set<E> input, Predicate<? super E> f) {
    return input.stream().filter(f).collect(Collectors.toSet());
  }

  public static <E,F> Set<F> stream (Set<E> input, Function<Stream<E>,Stream<F>> f) {
    return f.apply(input.stream()).collect(Collectors.toSet());
  }

  public static <E> List<E> distinct (List<E> input) {
    return input.stream().distinct().collect(Collectors.toList());
  }

  public static <E,F> F iter(F start, Collection<E> input, BiFunction<F,? super E,F> f)
  {
    F tmp = start;
    for (E e : input)
      tmp = f.apply(tmp,e);
    return tmp;
  }

  public static int hashTuple(Object... objs) {
    return Objects.hash(objs);
  }

  public static int hashColl(Collection<?> list) {
    return Objects.hash(list.toArray());
  }



  public static <E,F> Collection<List<E>> split (Iterable<E> l, Function<? super E,F> function)
  {
    Map<F,List<E>> map = new HashMap<>();
    for (E e : l) {
      F f = function.apply(e);
      if (! map.containsKey(f))
        map.put(f, new ArrayList<>());
      map.get(f).add(e);
    }
    return map.values();
  }

  public static <E> Iterable<E> otfFilter (Iterable<E> start, Predicate<E> predicate) {
    return new Iterable<E>() {
      public Iterator<E> iterator() {
        return new Iterator<E>() {
          Iterator<E> boxedIt = start.iterator();
          Optional<E> next = computeNext();

          public E next() {
            E result = next.orElseThrow(() -> new NoSuchElementException());
            next = computeNext();
            return result;
          }

          public Optional<E> computeNext() {
            Optional<E> result = Optional.empty();
            while ((!result.isPresent()) && (boxedIt.hasNext())) {
              E el = boxedIt.next();
              if (predicate.test(el))
                result = Optional.of(el);
            }
            return result;
          }
          public boolean hasNext() {  return next.isPresent();  }
        };
      }
    };
  }

  /** Indicates if two collections are equals, that is, have the same elements
    * with the same multiplicity, eventually in different order.
    * @param <E> type of the elements in the collection; expected to have a correct {@link Object#hashCode}.
  ***/
  public static <E> boolean equalAsMultiset
    ( Iterable<? extends E> first, Iterable<? extends E> second)
  {
    Map<E,Integer> map = new HashMap<>();
    for (E el : first) {
      int prev = map.getOrDefault(el, 0);
      map.put(el, prev+1);
    }
    for (E el : second) {
      int prev = map.getOrDefault(el, 0);
      map.put(el, prev-1);
    }
    for (E e : map.keySet())
      if (map.get(e) !=  0)
        return false;
    return true;
  }


  /** Functions creating a temporary file with given extension and containing given content 
    * @param content Content of the file
    * @param ext Extension of the created file
    * @return A temporary file containing the given content.
  ***/
  public static Path tmpFileOfString(String content, String ext) {
    Path file = null ;
    try {
      file = Files.createTempFile("CyphSem_",ext);
      Files.write(file, content.getBytes());
     } 
     catch (IOException e) {
       throw new RuntimeException("Problem writing to temporary file "+file+".",e);
     }
     return file;
  }
  
}
