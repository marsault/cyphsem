/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.exception;

import java.lang.RuntimeException;
import java.lang.Throwable;

public class ParsingException extends RuntimeException {

  public ParsingException(String msg) { super(msg); }
  public ParsingException(String msg, Throwable t) { super(msg,t); }
  protected ParsingException
    ( String msg, Throwable c, boolean enableSuppression,
      boolean writableStackTrace )
  { super(msg, c, enableSuppression, writableStackTrace); }

  public ParsingException(Throwable cause) { super(cause); }

}
