/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.exception;

import java.lang.RuntimeException;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import java.util.Arrays;

public class DynamicCastException extends RuntimeException {


    public DynamicCastException(String msg) {
      super(msg);
    }

    public DynamicCastException(Value v, Value.Kind... kinds) {
      super(
        "The kind of value " + v + " is "
        + v.getKind() + " while it should be one of " + Arrays.asList(kinds) + ".");
    }

    public DynamicCastException(Expression e, Value v, Value.Kind... kinds) {
      super(
        "Expression " + e + " evaluates to " + v + " ("
        + v.getKind() + ") while its kind is expected to be one of " + Arrays.asList(kinds) + ".");
    }

}
