/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.exception;

import java.lang.RuntimeException;
import java.lang.Throwable;

public class UnsupportedFeatureException extends ParsingException {

  public enum Reason {
    IMPLEMENTATION("The following feature is not implemented in CyphSem yet: "),
    SEMANTICS("The following feature is not defined by the semantics yet: "),
    FORBIDDEN("The following features is forbidden by the semantics: "),
    RESTRICTED("The following feature is forbidden in Cyphsem for technical reasons: ");
    public final String prefix;
    private Reason(String x) { prefix = x; }
  }

  public UnsupportedFeatureException(String msg, Reason r) {
    super(r.prefix+msg+".");
  }

  public UnsupportedFeatureException(String msg) { super(msg); }
  public UnsupportedFeatureException(String msg, Throwable t) { super(msg,t); }
  protected UnsupportedFeatureException
    ( String msg, Throwable c, boolean enableSuppression,
    boolean writableStackTrace )
  { super(msg, c, enableSuppression, writableStackTrace); }
  public UnsupportedFeatureException(Throwable cause) { super(cause); }

}
