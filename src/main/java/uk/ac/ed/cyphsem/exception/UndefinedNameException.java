/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.exception;

import uk.ac.ed.cyphsem.table.*;

import java.lang.IllegalArgumentException;



public class UndefinedNameException extends IllegalArgumentException {

    public UndefinedNameException() {}

    public UndefinedNameException(String message) { super(message); }

    public UndefinedNameException(Name name, Object cause) {
      super ("Variable name " + name + " is unbound in " + cause + ".");
    }


}
