/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;

import uk.ac.ed.cyphsem.table.*;

import java.util.Iterator;
import java.util.List;

public interface Expression {


  /**
   * Evaluates this {@link Expression} to a {@link Value} in the context of the given {@link Graph} and {@link Record}.
   * @param graph {@link Graph} used for {@link PropertyKey}, {@link Label} or {@link RelationType} lookup.
   * @param record {@link Record} used for {@link Name} lookup.
   * @return The {@link Value} this objects evaluates to.
   * @throws UndefinedNameException If <tt>record</tt> does not define each {@link Name} appearing as a sub-expression of <tt>this</tt>.
   * @throws UnspecifiedBehaviourException If the evaluation is not specified by the formal semantics.
   */
  public Value evaluate(Graph graph, Record record);
//   throws UndefinedNameException, UnspecifiedBehaviourException;

//   @Deprecated
//   public static
//   boolean
//   semEquals
//     ( Graph graph, Record record, Expression e1, Expression e2,
//       boolean falseOnFail )
//   {
//     Value v1 = e1.evaluate(graph, record);
//     Value v2 = e2.evaluate(graph, record);
//     return ( v1.semEquals(v2,  falseOnFail) );
//   }

}
