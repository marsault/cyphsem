/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;

import java.util.List;

/** Implements Cypher queries that have no {@code UNION}/{@code UNION ALL}, that is
  * a sequence of {@link Clause}s ending with a {@link ReturnQueryBis RETURN}.
  * <br>
  *  This interface corresponds to the {@code query°} grammar-token from the
  * formal syntax of Cypher Queries (as described in Francis et al., 2018).
*/
public interface QueryBis extends Query {


//   /** Helper function to build a QueryBis from several clauses and a query bis.
//     *
//     *
//     * @beware Parameters are in non-natural order.
//   **/
//   public static QueryBis make(QueryBis right, Clause... others) {
//     QueryBis previous = right;
//     for( int i = others.length-1; i>= 0; i--)
//       previous = new ClauseQueryBis(others[i], previous);
//     return previous;
//   }
//
//   public static QueryBis make(QueryBis right, List<Clause> l) {
//     return make(right, l.toArray(new Clause[l.size()]));
//   }
//   public static QueryBis make(Clause... others) {
//                                    return make(new ReturnQueryBis(), others);  }
}
