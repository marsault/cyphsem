/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;

/** Implements a Cypher query.
  * In addition to methods in {@link QueryDataFunction}, this interface provides
  * default methods to execute the query.
  * <br><br>
  * This interface corresponds to the {@code query} grammar-token from the
  * formal syntax of Cypher Queries (as described in Francis et al., 2018).
**/
public interface Query extends QueryDataFunction {


  /** Executes this query on given input {@link Graph},
    * or throws a {@link InterruptedException} if computation is interrupted
    * through given {@link Interruptor}.
    * @param graph Input graph
    * @param interruptor Interruptor provided by main thread to possibly
    * signal this thread to interrupt the computation.
    * @return The pair (output {@link Graph}, {@link Table}) after execution
    * @throws InterruptedException If computation was interrupted due to a call
    * to {@link Interruptor#check}.
  **/
  default public QueryData execute(Graph graph, Interruptor interruptor)
  throws InterruptedException
  {
    return apply(new QueryData(graph, new Table(new Record())), interruptor);
  }

  /** Executes this query on given input {@link Graph}.
    * <br>
    * Consists of calling {@link #execute} with a new {@link Interruptor},
    * hence cannot be interrupted.
  **/
  default public QueryData execute(Graph graph) {
    try {
      return execute(graph, new Interruptor());
    } catch (InterruptedException e) { throw new UnreachableStatementError(); }
  }

}
