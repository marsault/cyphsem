/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.Utils.Policy;

import uk.ac.ed.cyphsem.language.expression.Expression;

import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.language.pattern.*;

import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

/** Implements Cypher clauses of the form <tt>MATCH p1,..,pn</tt>,
  * where p1, ..., pn are path patterns.
***/
public class MatchClause 
implements StandardReadingClause, PatternHolder 
{

  /** Path patterns of this {@link MatchClause}. */
  protected List<PathPattern> patterns;

  /** Compiled version of field {@link #patterns}. (Unused in current implementation.) */
  protected CompiledPattern compiledPattern = new CompiledPattern(false);

  @Override
  public List<PathPattern> patterns() {  return patterns; }

  /** Constructs a {@link MatchClause} wrapping given patterns. */
  public MatchClause(List<PathPattern> patterns) {
    this.patterns = patterns;
    compiledPattern.addPatterns(patterns);
    PatternHolder.super.assertPatternTupleIsSuitable();
  }

  /** Constructs a {@link MatchClause} wrapping given patterns.
    * Same as : {@link #MatchClause MatchClause}{@code (Arrays.asList(patterns))} .
  */
  public MatchClause(PathPattern... patterns) {
    this(Arrays.asList(patterns));
  }

  @Override
  public String toString()
  { return PatternHolder.super.formatToString("MATCH"); }


  /** {@inheritDoc}
    * <br><br>
    * Implementation consists in starting from a one-element list containing one
    * empty {@link AssignmentResult}
    * and calling metho {@link PathPattern#computeAssignment computeAssignment}
    * on each {@link PathPattern} in {@link #patterns}
    * @see PathPattern#computeAssignment
  **/
  @Override
  public Table apply (Graph graph, Record record, Interruptor interruptor)
  throws InterruptedException
  {
    List<AssignmentResult> old_records
      = Arrays.asList( new AssignmentResult(
                  null, record, new HashSet<Relation>(), new HashSet<Expression>()) );
    for (PathPattern p : patterns) {
      interruptor.check();
      List<AssignmentResult> new_records = new LinkedList<AssignmentResult>();
      for (AssignmentResult ar : old_records)
        for (Node node : graph.nodes())
          new_records.addAll(
              p.computeAssignment(graph,
                                  new AssignmentResult( new Path(node),
                                                        ar.record,
                                                        ar.relations,
                                                        ar.postConditions),
                                  interruptor));
      old_records = new_records;
    }

    Table result = new Table();
    for (AssignmentResult ar : old_records) {
      if (ar.satisfiesPostConditions(graph))
        result.add(ar.record);
    }
    return result;
  }

}
