/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import java.util.List;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This enum lists the different aggregating functions supported by CyphSem.
 */
public enum AggregatingFunction implements ValueToValueFunction {
  COUNT( x ->
            new Value(x.stream()
                      .filter(value -> !value.isOfKind(Value.Kind.NULL))
                      .count())
  ),
  COUNT_STAR ("COUNT(*)", x -> new Value(x.stream().count()) ),
  COLLECT(x -> new Value(x)),
  MIN(x -> x.stream()
            .filter(value -> !value.isOfKind(Value.Kind.NULL))
            .sorted()
            .findFirst()
            .orElse(new Value())
  );

  final static private Map<String, AggregatingFunction> _ofString
      = new HashMap<String, AggregatingFunction>();
  static {  for(AggregatingFunction foo : AggregatingFunction.values())
              _ofString.put(foo.name,foo); }

  private ValueToValueFunction function;
  private String name;

  private AggregatingFunction(ValueToValueFunction function) {
    this(null, function);
  }

  private AggregatingFunction (String name, ValueToValueFunction function) {
    this.function = function;
    if (name != null)
      this.name = name;
    else
      this.name = super.toString().replaceAll("_"," ");
  }

  @Override
  public Value call(List<Value> operands) { return (function.call(operands)); }

  @Override
  public Value call(Value... operands) { return (function.call(operands)); }

  @Override public String toString() { return name; }

  @Override public
  String toString(List<Expression> exps) {
    if (this == COUNT_STAR)
      return toString();
    else
      return ValueToValueFunction.super.toString(exps);
  }

  public static
  AggregatingFunction of(String str) {
    if (_ofString.containsKey(str))
      return _ofString.get(str);
    throw new IllegalArgumentException("No such aggregating function: "+str+".");
  }
}
