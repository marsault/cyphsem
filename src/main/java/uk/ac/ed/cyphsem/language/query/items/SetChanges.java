/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query.items;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;


/** Models the changes to be applied to the graph by a <tt>SET</tt> clause
  * They have to be accumulated to be sure no two changes are contradictory.
  * @see SetClause
***/
public class SetChanges
extends Pair<  Map< Pair<Id,PropertyKey>, Value >,
                Set< Pair<Node,Label> >  >
{
  /** Indicate whether non-deterministic behaviour should raise an {@link NonDeterminismException} (if {code permissive = true}) or not.
  ***/
  public boolean permissive;

  /** Constructs a {@link SetChanges} with no changes. ***/
  public SetChanges(boolean b) {super(new HashMap<>(), new HashSet<>()); permissive = b;}

  /** Constructs a {@link SetChanges} with changes.
    * <br>
    * Consists in calling {@link #SetChanges SetChanges}(b) and computing the changes induced by given {@code items} in the context of {@link Graph} {@code g} and {@link Table} t.
  ***/
  public SetChanges
    ( boolean b, Collection<? extends SetItem> items, Graph g, Table t )
  {
    this(b);
    for (SetItem s : items)
      s.addChanges(this,g,t);
  }

    /** Adds a change to a property of a Node or Relation.
      * If {@link #permissive} is {@code true}, a previous change affecting the same pair ({@link Id},{@link PropertyKey}) is overwritten silently.
      * @param id {@link Node} or {@link Relation} affected by the change.
      * @param k Key of the property to be changed
      * @param v {@link Value} with which key {@code k} should be associated at the end of the execution of the {@link SetClause}.
      * @throws NonDeterminismException if field {@link #permissive} is {@code false} and computed changes contradicts one of the previously added changes.
    */
  public void addProp(Id id, PropertyKey k, Value v) {
    Pair<Id,PropertyKey> key = new Pair<>(id,k);
    if (permissive || !first.containsKey(key))
      first.put(key,v);
    else if (! first.get(key).equals(v))
      throw new NonDeterminismException (
        "Property " + k + " of node " + id + " is to be set to both " + v +
        " and " + first.get(key)+".");
  }

  /** Adds a change to a label of a Node.
    * @param n {@link Node} affected by the change.
    * @param l New {@link Label} to be added to {@code n} at the end of the execution of the {@link SetClause}.
  */
  public void addLabel(Node n, Label l) {
    second.add(new Pair<>(n,l));
  }

  /** Execute the changes represented by this object.
    * The two given parameters are functions treating the case where the input and output graphs are distinct.
    * @param node_map Function mapping {@link Node}s from the input graph to {@link Node}s of the output graph.
    * @param rel_map Function mapping {@link Relation}s from the input graph to {@link Node}s of the output graph.
  ***/
  public void execute(Function<Node,Node> node_map, Function<Relation,Relation> rel_map) {
    for (Map.Entry<Pair<Id,PropertyKey>, Value> entry :  first.entrySet()) {
      Id i = (entry.getKey().first instanceof Node)
              ? node_map.apply((Node)entry.getKey().first)
              : rel_map.apply((Relation)entry.getKey().first);
      i.overwriteProperty(entry.getKey().second, entry.getValue());
    }
    for (Pair<Node,Label> p : second) {
      node_map.apply(p.first).addLabel(p.second);
    }
  }
}
