/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.*;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.value.*;

import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.*;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of expressions of the form <tt>{</tt><var>k{1}:e{1}</var>,&hellip;,<var>k{n}:e{n}</var>}</tt>, where k{1}</var>,&hellip;,<var>k{n}</var> are {@link PropertyKey}s and e{1}</var>,&hellip;,<var>e{n}</var> may be any kind of {@link Expression}s.
 */
public class ExplicitMapExpression
    extends ArrayList<KeyExpPair>
    implements Expression
{

  /**
   * Constructs a new ExplicitMapExpression combining position-wise {@link PropertyKey}s and {@link Expression}s into {@link KeyExpPair}s.
   * @param keys list of {@link PropertyKey}s that are expected to be distinct (without check).
   * @param subExps list of {@link Expression}s that is expected to have the same size as <tt>keys</tt>
   */
  public
  ExplicitMapExpression
    (List<PropertyKey> keys, List<Expression> subExps)
  {
    super(keys.size());
    if (keys.size() != subExps.size())
      throw (new IllegalArgumentException
          ("ExplicitMapExpression: the two arguments must be of equal length"));
    for(int i = 0; i < keys.size(); i++)
      add(new KeyExpPair(keys.get(i), subExps.get(i)));
  }

  /**
   * Constructs a new ExplicitMapExpression pre-built arguments.
   * @param pairs elements added to the supertype as references.
   */
  public
  ExplicitMapExpression
    (KeyExpPair... pairs)
  {
    super(pairs.length);
    for (KeyExpPair pair : pairs)
      add(pair);
  }

  /**
   * Constructs a new ExplicitMapExpression using pre-built arguments.
   * @param pairs Its elements are added to supertype as references.  List is unmodified by constructor and no reference is kept.
   */
  public
  ExplicitMapExpression
    (List<KeyExpPair> pairs)
  {
    super(pairs);
  }

  /** {@inheritDoc}
    * @return {@inheritDoc} Returned {@link Value} is of {@link Value.Kind Kind}
    * mapped to two different values.
    * {@link Value.Kind#MAP MAP}.
  ***/
  @Override
  public Value evaluate(Graph graph, Record record) {
    Map<PropertyKey,Value> map = new HashMap<PropertyKey,Value>();
    for (KeyExpPair pair : this) {
        map.put(pair.key, pair.exp.evaluate(graph,record));
    }
    return ( new Value(map) );
  }

  @Override
  public String toString() {
    return StringOf.iterable(this,"{","}",",");
  }

}
