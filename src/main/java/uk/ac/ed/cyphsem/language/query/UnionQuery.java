/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.Table;


import java.util.List;
import java.util.LinkedList;

/** Represents queries of the form <tt>q1 UNION q2</tt> and <tt>q1 UNION ALL q2</tt>, where q1 and q2
  * are {@link Query}'s.
*/
public class UnionQuery implements Query {

  /** Query at the left of keyword <tt>UNION</tt>. */
  public Query left;

  /** Query at the right of keyword <tt>UNION</tt>. */
  public Query right;

  /** Indicates whether keyword <tt>ALL</tt>. */
  public boolean hasALL;


  /** Constructs a {@link UnionQuery} that wraps the parameters. */
  public UnionQuery
    ( Query left, Query right, boolean hasALL )
  { this.left = left;
    this.right = right;
    this.hasALL = hasALL;
  }


  /** Shorthand for {@link #UnionQuery UnionQuery}{@code (left,right,false)} */
  public UnionQuery(Query left, Query right)
  { this(left,right,false); }


  @Override
  public String toString() {
    return "(" + left + " UNION "+ (hasALL?"ALL ":"") + right + ")";
  }

  @Override
  public QueryData apply(QueryData qd, Interruptor i)
  throws InterruptedException
  { i.check();
    QueryData tmp_l = left.apply(qd, i);
    QueryData tmp_r = right.apply(new QueryData(tmp_l.graph(),qd.table()), i);
    Table t = tmp_r.table();
    t.addAll(tmp_l.table());
    if (!hasALL)
      t.unique();
    return new QueryData(tmp_r.graph(),t);
  }
}
