/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import java.util.Objects;

/** Simple implementation of a pair ({@link PropertyKey}, {@link Value}).
  * Methods {@link #equals} and {@link #hashCode} are implemented by combining
  * the respective methods of {@link PropertyKey} and {@link Value}.
***/
public class KeyValPair {

  /** The wrapped key. */
  public final PropertyKey key;

  /** The wrapped value. */
  public final Value val;

  /** Constructs a KeyValPair wrapping arguments. */
  public KeyValPair(PropertyKey key, Value val) {
    this.key = key;
    this.val = val;
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, val);
  }

  @Override
  public boolean equals (Object o) {
    if (!(o instanceof KeyValPair))
      return false;
    KeyValPair other = (KeyValPair) o;
    return (key.equals(other.key) && val.equals(other.val));
  }

}
