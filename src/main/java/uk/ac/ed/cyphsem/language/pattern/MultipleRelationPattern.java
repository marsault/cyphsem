/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;

import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.Pair;

import uk.ac.ed.cyphsem.Interruptor;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;



/**
  This class implements multiple relation patterns, that is.
  * It hence has a fields, {@link #it}, containing the information regarding
  * range.
**/
public class MultipleRelationPattern extends RelationPattern {

  /** Range of this relation pattern. */
  protected Iteration it;

  /** Returns the field {@link #it}*/
  public Iteration iteration() { return it; }

  /** Constructs a MultipleRelationPattern wrapping the arguments.
    * Simply set field {@link #it} to given parameter and calls constructor
    * of RelationPattern to remaining parameters.
  **/
  public MultipleRelationPattern
      ( Iteration it, NodePattern previous, UnnamedPathPattern next, Object... others )
  {
    super(previous, next, others);
    this.it = it;
  }

  @Override
  public List<AssignmentResult> computeAssignment
    ( Graph graph, AssignmentResult partialAR, Interruptor i )
    throws InterruptedException
  {
    i.check();
    it.reset();
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();
    for (AssignmentResult ar : before(graph, partialAR,i))
      result.addAll(computeAssignment(graph, ar, i, new LinkedList<Relation>()));
    return result;
  }

  public List<AssignmentResult>
  computeAssignment
    ( Graph graph, AssignmentResult partialAR, Interruptor i, List<Relation> rels)
    throws InterruptedException
  {
    i.check();
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();
    if ( it.mayStop() ) {
      List<Value> vals = new ArrayList<Value>(rels.size());
      for (Relation r : rels)  vals.add(new Value(r));
        try {
          AssignmentResult newAR
              = (name == null) ?  partialAR
                               : partialAR.verifyOrAdd(name, new Value(vals));
          result.addAll(after(graph, newAR,i));
        } catch (RecordInconsistencyException e) { }
//       if (new_partial_u != null) {
//         List<AssignmentResult> part_result = after(path, used, graph, new_partial_u, postConditions);
//         for (List<Expression> le : exps)
//           AssignmentResults.filter(part_result, graph, le);
//         result.addAll(part_result);
//       }
    }

    if ( it.mayContinue() ) {
      for (AssignmentResult ar :  matchingRelations (graph, partialAR)) {
//         Relation rel = pair.first;
//         used.add(rel);
        rels.add(ar.path.lastRelation());
//         exps.add(pair.second);
//         path.add(rel, rel.otherEnd(path.dst()));
        it.next();
//         List<AssignmentResult> part_result =
//             ;
        result.addAll(computeAssignment(graph, ar, i, rels));
        it.previous();
//         path.removeLastLink();
//         exps.remove(exps.size()-1);
        rels.remove(rels.size()-1);
//         used.remove(rel);
      }
    }

//     System.out.println(path.indent()+"</Relation "+path+">"+" : "+result);
    return result;
  }

  /** Returns the iterationString */
  public String iterationString() {
    return (it.toString());
  }


  @Override
  public Map<Name,VarStatus>
  assertSuitable(
      boolean forUpdate,
      Map<Name,VarStatus> varStatus
    )
  throws AssertionFailedException
  {
    previous.assertSuitable(forUpdate,varStatus);
    assertSuitable(forUpdate,varStatus,VarStatus.REL_LIST);
    return next.assertSuitable(forUpdate, varStatus);
  }
}
