/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.table.Record;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation of all expressions with a function on top.  It includes  call to functions,
 such as , and arithmetic/boolean operations such as <tt><var>e{1}</var>+<var>e{2}</var></tt> or
 <tt><var>e{1}</var> OR <var>e{2}</var></tt>.
 */
public class FunctionalExpression implements Expression {

  /**
   * The function to apply.
   */
  ValueToValueFunction function;

  /**
   * The operands of the function.
   */
  List<Expression> operands;

  /**
   * Constructs a FunctionalExpression as a wrapper to the arguments.
   */
  public
  FunctionalExpression
      (ValueToValueFunction function, List<Expression> operands)
  {
    this.function = function;
    this.operands = operands;
  }

  /**
   * Constructs a FunctionalExpression, wrapping <tt>function</tt> and building a new {@link ArrayList} from <tt>exps</tt>.
   */
  public
  FunctionalExpression
      (ValueToValueFunction function, Expression... exps)
  {
    this.function = function;
    this.operands = new ArrayList<Expression>(Arrays.asList(exps));
  }

  @Override
  public Value evaluate(Graph graph, Record record) {
    ArrayList<Value> list = new ArrayList<Value>(operands.size());
    for (Expression e: operands)
      list.add(e.evaluate(graph,record));
    return function.call(list);

  }


  @Override
  public String toString() {
    // FIXME: This works only for infix binary functions or prefix unary functions.
    if (operands.size() == 1)
      return ("(" + function.toString()
                  + " " + operands.get(0).toString() + ")");
    else if (operands.size() == 2)
      return ("(" + operands.get(0).toString()
                  + " " + function.toString()
                  + " " + operands.get(1).toString() + ")");
    else
      throw (new Error("TODO:This case is not treated yet"));
  }


}
