/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.language.query.items.*;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
/** Represents a <tt>RETURN</tt> {@link QueryBis Query°}.
* It essentially consists in a {@link WithClause} possibly followed by an {@link OrderBy}.
*/

public class ReturnQueryBis extends WithClause implements QueryBis {

  /** Optional {@link OrderBy} following this. */
  protected Optional<OrderBy> subclause = Optional.empty();

  /** Constructs a new {@link ReturnQueryBis} with given items and subclause.
    * Implementation passes {@code isStarPresent} and {@code extras} to {@link WithClause#WithClause super(..)}; and holds {@code subclause}.
  ***/
  public ReturnQueryBis(boolean isStarPresent, List<WithItem> extras, Optional<OrderBy> subclause) {
    super(isStarPresent,extras);
    this.subclause = subclause;
  }

  /** Constructs a new {@link ReturnQueryBis} with given items and star status, and without subclause.
    * <br>
    * Shorthand for {@link #ReturnQueryBis ReturnQueryBis}{@code (isStarPresent, extras, Optional.empty())}.
  ***/
  public ReturnQueryBis(boolean isStarPresent, List<WithItem> extras) { this(isStarPresent, extras, Optional.empty()); }


  /** Constructs a new {@link ReturnQueryBis} without {@link OrderBy} subclause nor star.
    * <br>
    * Shorthand for {@link #ReturnQueryBis ReturnQueryBis}{@code (false, extras, Optional.empty())}.
  ***/
  public ReturnQueryBis(List<WithItem> extras) { this(false, extras, Optional.empty()); }

  /** Constructs a new {@link ReturnQueryBis} with star and no extra item.
    * <br>
    * Shorthand for {@link #ReturnQueryBis ReturnQueryBis}{@code this(true, new ArrayList<>(), Optional.empty())}.
  ***/
  public ReturnQueryBis() { this(true, new ArrayList<>(), Optional.empty()); }


  @Override
  public String toString() {
    String result = super.toString();
    return "RETURN"+result.substring(4);
  }

  @Override
  public QueryData apply(QueryData qd, Interruptor i)
  throws InterruptedException
  {
    i.check();
    QueryData res = super.apply(qd,i);
    if (subclause.isPresent())
      res = new QueryData(res.graph(), subclause.get().apply(res.graph(), res.table(), i));
    return res;
  }

  public List<Record> applyKeepingOrder(QueryData qd, Interruptor i)
  throws InterruptedException
  {
    i.check();
    QueryData res = super.apply(qd,i);
    if (subclause.isPresent())
      return subclause.get().applyKeepingOrder(res.graph(), res.table(), i);
    return new ArrayList<>(res.table());
  }


}
