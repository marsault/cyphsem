/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.parser;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

/** This class represents an expression, possibly under an aggregating function.
  * This structure is used heavily during the traversal of AST of expressions:
  {@link Expression Expressions} are usually unboxed and boxed again during the
  treatment of any node * of the AST of an expression.
  * This is due to the fact that specification states that an aggregating
    function may appear only
    on top of a WITH/RETURN expressions, while grammar in fact allows it to
    appear in any expression.
  */
public class BoxedExp extends Pair<Optional<Pair<AggregatingFunction,Boolean>>,Expression> {

  /** Constructs a BoxedExp wrapping the given {\@link Expression}. */
  public BoxedExp(Expression e) { super(Optional.empty(),e); }

  /** Constructs a BoxedExp wrapping the given {@link AggregatingFunction}
      and{\@link Expression}.
  ***/
  public BoxedExp(AggregatingFunction af, Expression e) { this(af, false, e); }

  /** Constructs a BoxedExp wrapping the given {@link AggregatingFunction}
      and{\@link Expression}.
    * @param af
    * @param distinct Whether the keyword <tt>DISTINCT</tt> is present.s
    * @param e
  ***/
  public BoxedExp(AggregatingFunction af, Boolean distinct, Expression e) {
    super(Optional.of(new Pair<AggregatingFunction,Boolean>(af,distinct)),e); }


  /** Unbox the boxed expression, assuming that it no aggregating function is
    * present.
    * @throws ParsingException if this boxed expression had indeed an aggregating function.
    * @return the boxed {@link Expression}
  ***/
  public Expression unbox() {
    if (first.isPresent())
      throw new ParsingException("We don't allow aggregating function anywhere but in WITH/RETURN on top.");
    return second;
  }

  /** Unbox this boxed expression, apply the given function, and box the result again.
    * @throws ParsingException if this boxed expression had indeed an aggregating function.
  ***/
  public BoxedExp map (Function<Expression,Expression> f) {
    return box(f.apply(this.unbox()));
  }

  /** If parameter is present, same as {@link #map}, otherwise returns {@code this}.
    * @throws ParsingException if this boxed expression had indeed an aggregating function and parameter {@code f} {@link Optional#isPresent is present}.
  ***/
  public BoxedExp map (Optional<Function<Expression,Expression>> f) {
    if (f.isPresent())
      return new BoxedExp(f.get().apply(this.unbox()));
    else
      return this;
  }

  public WithItem toWithItem(Optional<Name> name) {
    Optional<WithItem> ne
      = name.map(n -> (first.map( x -> new WithItem(x.first, x.second, n, second))
            .orElse(new WithItem(n,second))));
    return ne.orElse(
      (first.map(x -> new WithItem(x.first, x.second, second))
            .orElse(new WithItem(second)))
    );
  }

  public WithItem toWithItem() {
    return toWithItem(Optional.empty());
  }
  public WithItem toWithItem(Name name) {
    return toWithItem(Optional.of(name));
  }

  public static BoxedExp box(Expression e)  { return new BoxedExp(e);}
}
