/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;

import uk.ac.ed.cyphsem.language.expression.Expression;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.Utils.Policy;
import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Collection;
import java.util.Optional;

/**  Root abstract class for all patterns.  **/
public abstract class PathPattern {


  Name name = null;


  /** Returns the length of the pattern, that is, the number of relation patterns
  **/
  public abstract int length();

  public abstract void addTopLevelVariables(Set<Name> temp_set);

  /** Computes all the assignment results respecting this pattern and the given
    * assignment result.
    * @param graph
    * @param partial_ar Partial assignment result to complete.
    * @param i Interruptor provided by another thread to eventually terminate
    * @return All possible AssignmentResult
    computation early.
    * @throws InterruptedException if {@code }
  **/
  public abstract List<AssignmentResult> computeAssignment
    ( Graph graph, AssignmentResult partial_ar, Interruptor i )
  throws InterruptedException;

  /** Same as {@link  #computeAssignment} with an empty {@link Interruptor}.*/
  public List<AssignmentResult> computeAssignment
    ( Graph graph, AssignmentResult partial_ar)
  {
    try {  return computeAssignment(graph, partial_ar, new Interruptor());  }
    catch (InterruptedException e) {  throw new UnreachableStatementError();  }
  }


  /** Class representing the different status variable may have within the pattern */
  public static enum VarStatus {
    FRESH,
    BOUND,
    PATH, PATH_IF_EMPTY,
    NODE,  NODE_IF_EMPTY,
    REL, REL_IF_EMPTY,  REL_LIST,  REL_LIST_IF_EMPTY;

    //This function expect other to be among {PATH, NODE, REL, REL_LIST}
    public VarStatus
    assertSuitable(boolean forUpdate, Name name, VarStatus other, boolean isEmpty)
    throws AssertionFailedException
    {
//       System.out.println(forUpdate+", "+name+", "+other+", "+isEmpty);
      if ((forUpdate) && (other == REL_LIST))
        throw new AssertionFailedException("Iteration is not allowed in a pattern in an update clause.");
      if (this == BOUND) {
          if (forUpdate) {
            if (other.makeEmpty() != NODE_IF_EMPTY)
              throw new AssertionFailedException("Name "+name+" is bound, hence cannot be used as path or relationship variable in an update clause.");
            if (!isEmpty)
              throw new AssertionFailedException("Name "+name+" is bound, hence cannot be used in a pattern that has constraints in an update clause.");
          }
       }
       else if (this != FRESH) {
        if (this.makeEmpty() != other.makeEmpty())
          throw new AssertionFailedException("Name "+name+" is used with type distinct from the one it was used before.");
        if ((!isEmpty) && (other != this))
          throw new AssertionFailedException("Name "+name+" is not allowed to be in a non-empty pattern here.");
      }
      return isEmpty ? other : other.makeEmpty();
    }

    public VarStatus makeEmpty() {
      switch (this) {
        case FRESH:
        case BOUND: throw new IllegalArgumentException("method makeEmpty() should not be used on BOUND");
        case PATH:
        case PATH_IF_EMPTY: return PATH_IF_EMPTY;
        case NODE:
        case NODE_IF_EMPTY: return NODE_IF_EMPTY;
        case REL:
        case REL_IF_EMPTY: return REL_IF_EMPTY;
        case REL_LIST:
        case REL_LIST_IF_EMPTY: return REL_LIST_IF_EMPTY;
      }
      throw new UnreachableStatementError();
    }
  }

  public Map<Name,VarStatus> assertSuitable(boolean forUpdate, Collection<Name> boundVariables)
  throws AssertionFailedException
  {
    HashMap<Name,VarStatus> map = new HashMap<>(boundVariables.size());
    for (Name name : boundVariables)
      map.put(name, VarStatus.NODE_IF_EMPTY);
    return assertSuitable(forUpdate,map);
  }

  public Map<Name,VarStatus> assertSuitable(boolean forUpdate)
  throws AssertionFailedException
    { return assertSuitable(forUpdate, new ArrayList<>(1)); }


  abstract public Map<Name,VarStatus>
  assertSuitable(
      boolean forUpdate,
      Map<Name,VarStatus> varStatus
    )
  throws AssertionFailedException;

  public Map<Name,VarStatus>
  assertSuitable(
      boolean forUpdate,
      Map<Name,VarStatus> varStatus,
      VarStatus target
    )
  throws AssertionFailedException
  {
    VarStatus previousStatus =
      name == null ? VarStatus.FRESH : varStatus.getOrDefault(name, VarStatus.FRESH);
    try {
      VarStatus newStatus =
        previousStatus.assertSuitable(forUpdate, name, target, isEmpty());
      if (name != null)
        varStatus.put(name, newStatus);
    } catch (AssertionFailedException e) {
      throw new AssertionFailedException ("In pattern part: "+toThisString()+".  "+e.getMessage(),e);
    }
    return varStatus;
  }

  /** Returns the string representation of this pattern only, ignoring
    * the remainder of the path (if applicable).
    * For instance, a relation pattern would return <tt>-[]-&gt;</tt> instead of
    * <tt>-[]-&gt;()-[]-&gt;</tt> .
  ***/
  public String toThisString() {
    return toString();
  }

  /** Indicate whether this pattern is empty, that is has no properties,
    * labels or types.
  ***/
  abstract public boolean isEmpty();
//   public List<AssignmentResult> computeAssignment (Graph graph) {
//     List<AssignmentResult> result = new LinkedList<AssignmentResult>();
//
//     Record r = new Record();
//     Set<Relation> used = new HashSet<Relation>();
//     Set<Expression> postConditions = new HashSet<Expression>();
//     for (Node n : graph.nodes) {
//       List<AssignmentResult> part_result
//         = computeAssignment(graph,
//             new AssignmentResult(new Path(n), r,used,postConditions) );
//       for (AssignmentResult assignment : part_result)
//         if (assignment.filterPostConditions(graph))
//           result.add(assignment);
//     }
//     return result;
//   }

//   public List<AssignmentResult> computeAssignment (Graph g, Record partial_u) {
//     return computeAssignment (, g, partial_u, ); }




//   public List<AssignmentResult> computeAssignment
//     (
//
//     Set<Relation> used, Graph graph, Record partial_u, Set<Expression> postConditions)
//   {
//
// //     Set<Expression>  = new HashSet<Expression>();
//
//       List<AssignmentResult> tmp
//         = computeAssignment(new Path(n), used, graph, partial_u, postConditions );
//
//     }
//
//     return result;
//   }

  /** Computes the free variables in this pattern, that is the name of this pattern
    * and of its successor patterns, successively.
    */
  public Set<Name> freeVariables() {
    Set<Name> s= new HashSet<Name>();
    addTopLevelVariables(s);
    return s;
  }

  abstract public List<UnnamedPathPattern> serialize();
}
