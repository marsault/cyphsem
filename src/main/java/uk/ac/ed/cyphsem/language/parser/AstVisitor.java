/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.parser;

import static uk.ac.ed.cyphsem.language.parser.BoxedExp.box;

import uk.ac.ed.cyphsem.exception.ParsingException;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;
import uk.ac.ed.cyphsem.exception.UnsupportedFeatureException;
import uk.ac.ed.cyphsem.exception.UnsupportedFeatureException.Reason;

import uk.ac.ed.cyphsem.language.expression.AggregatingFunction;
import uk.ac.ed.cyphsem.language.expression.ExplicitListExpression;
import uk.ac.ed.cyphsem.language.expression.ExplicitMapExpression;
import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;
import uk.ac.ed.cyphsem.language.expression.FunctionalExpression;
import uk.ac.ed.cyphsem.language.expression.Functions;
import uk.ac.ed.cyphsem.language.expression.KeyExpPair;
import uk.ac.ed.cyphsem.language.expression.SubListExpression;
import uk.ac.ed.cyphsem.language.expression.UnaryMapExpression;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Label;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.RelationType;

import uk.ac.ed.cyphsem.language.parser.autogen.CypherParser;
import uk.ac.ed.cyphsem.language.parser.autogen.CypherLexer;
import uk.ac.ed.cyphsem.language.parser.autogen.CypherParser.*;

import uk.ac.ed.cyphsem.language.pattern.Iteration;
import uk.ac.ed.cyphsem.language.pattern.MultipleRelationPattern;
import uk.ac.ed.cyphsem.language.pattern.NamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.NodePattern;
import uk.ac.ed.cyphsem.language.pattern.PathPattern;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern;
import uk.ac.ed.cyphsem.language.pattern.SimpleRelationPattern;
import uk.ac.ed.cyphsem.language.pattern.UnnamedPathPattern;

import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.language.query.OrderBy.OrderedExpression;

import uk.ac.ed.cyphsem.table.Name;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.utils.Pair;
import uk.ac.ed.cyphsem.utils.Either;


import java.io.IOException;

import java.math.BigInteger;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

import java.util.function.Function;

import java.util.stream.Collectors;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;


/** This class is a collections of static functions that are used for traversing
  * the AST built by class {@link CypherParser}, autogenerated by ANTLER from
  * grammar Cypher.g4 .
  * Each class extending {@link ParserRuleContext} is treated by the function
  * named "treat"+x if the name of the context is "OC_"+x+"Context".
  * EG, the function to call to treat an {@code OC_IntegerLiteralContext} is
  * named {@code treatIntegerLiteral} and returns a BigInteger.
  * @fordev This function uses heavily static function {@link BoxedExp#box}  of
  * class {@link BoxedExp}
***/
final class AstVisitor {

  /** Do not use : AstVisitor is not a class to be instantiated */
  private AstVisitor() {}

  /** Helper function to return a LinkedList with one element. */
  private static <E> LinkedList<E> listFromOne(E e) {
    LinkedList<E> l = new LinkedList<>();
    l.add(e);
    return l;
  }

  static BigInteger treatIntegerLiteral(OC_IntegerLiteralContext ctx) {
    TerminalNode tn  = ctx.OctalInteger();
    if (tn != null)
      return new BigInteger(tn.getSymbol().getText().substring(2),8);

    tn = ctx.HexInteger();
    if (tn != null)
      return new BigInteger(tn.getSymbol().getText().substring(2),16);

    tn = ctx.DecimalInteger();
    if (tn != null)
      return new BigInteger(tn.getSymbol().getText());

    throw new ParsingException("IntegerLiteral: should never happen.");
  }

  static Boolean treatBooleanLiteral(OC_BooleanLiteralContext ctx) {
    if (ctx.TRUE() !=  null)
      return true;
    if (ctx.FALSE() != null)
      return false;
    throw new ParsingException("IntegerLiteral: should never happen.");
  }


  static BoxedExp treatListLiteral(OC_ListLiteralContext ctx) {
    List<OC_ExpressionContext> exp_ctxs = ctx.oC_Expression();
    if (exp_ctxs != null) {
      List<Expression> exps = new ArrayList<Expression>(exp_ctxs.size());
      for (OC_ExpressionContext ec : exp_ctxs)
        exps.add(treatExpression(ec).unbox());
      return box(new ExplicitListExpression(exps));
    }
    else
      return box(new ExplicitListExpression());
  }

  static BoxedExp treatLiteral(OC_LiteralContext ctx) {
    if (ctx.oC_NumberLiteral() != null) {
      if (ctx.oC_NumberLiteral().oC_IntegerLiteral() == null)
        throw new ParsingException("NumberLiteral: we allow only integers as number literals.");
      return box(new Value(treatIntegerLiteral(ctx.oC_NumberLiteral().oC_IntegerLiteral())));
    }
    if (ctx.StringLiteral() != null)
      return box(new Value(treatStringLiteral(ctx.StringLiteral())));

    if (ctx.oC_BooleanLiteral() != null)
      return box(new Value(treatBooleanLiteral(ctx.oC_BooleanLiteral())));

    if (ctx.NULL() != null)
      return box(new Value());

    if (ctx.oC_MapLiteral() != null)
      return treatMapLiteral(ctx.oC_MapLiteral());

    if (ctx.oC_ListLiteral() != null)
      return treatListLiteral(ctx.oC_ListLiteral());
    throw new ParsingException("IntegerLiteral: should never happen.");
  }

  static BoxedExp treatMapLiteral(OC_MapLiteralContext ctx) {
      List<OC_PropertyKeyNameContext> p_ctxs = ctx.oC_PropertyKeyName();
      List<OC_ExpressionContext> e_ctxs = ctx.oC_Expression();


      if (p_ctxs == null || e_ctxs == null)
        return box(new ExplicitMapExpression());

      List<PropertyKey> keys = new ArrayList<PropertyKey>(p_ctxs.size());
      for (OC_PropertyKeyNameContext p_ctx : p_ctxs)
        keys.add(treatPropertyKeyName(p_ctx));

      List<Expression> exps = new ArrayList<Expression>(e_ctxs.size());
      for (OC_ExpressionContext e_ctx : e_ctxs)
        exps.add(treatExpression(e_ctx).unbox());
      return box(new ExplicitMapExpression(keys, exps));
  }


  static String treatStringLiteral(TerminalNode tn) {
    String res = tn.getSymbol().getText();
    return res.substring(1,res.length()-1);
  }

  static BoxedExp treatExpression(OC_ExpressionContext ctx) {
    List<OC_XorExpressionContext> xor_ctxs = ctx.oC_OrExpression().oC_XorExpression();
    BoxedExp e = null;
    for (OC_XorExpressionContext xor_ctx : xor_ctxs) {
      if (e == null)
        e = treatXorExpression(xor_ctx);
      else
        e = e.map( x -> new FunctionalExpression(Functions.OR, x, treatXorExpression(xor_ctx).unbox()));
    }
    return e;
  }

  static BoxedExp treatXorExpression(OC_XorExpressionContext ctx) {
    List<OC_AndExpressionContext> and_ctxs = ctx.oC_AndExpression();
    BoxedExp e = null;
    for (OC_AndExpressionContext and_ctx : and_ctxs) {
      if (e == null)
        e = treatAndExpression(and_ctx);
      else
        e = box(new FunctionalExpression(Functions.XOR,
                                         e.unbox(),
                                         treatAndExpression(and_ctx).unbox()));
    }
    return e;
  }

  static BoxedExp treatAndExpression(OC_AndExpressionContext ctx) {
    List<OC_NotExpressionContext> not_ctxs = ctx.oC_NotExpression();
    BoxedExp e = null;
    for (OC_NotExpressionContext not_ctx : not_ctxs) {
      if (e == null)
        e = treatNotExpression(not_ctx);
      else
        e = box(new FunctionalExpression(Functions.AND,
                                         e.unbox(),
                                         treatNotExpression(not_ctx).unbox()));
    }
    return e;
  }

  static BoxedExp treatNotExpression(OC_NotExpressionContext ctx) {
    OC_ComparisonExpressionContext comp_ctx = ctx.oC_ComparisonExpression();
    BoxedExp subExp = treatComparisonExpression(comp_ctx);
    if ( ctx.NOT().size() % 2 == 1 )
      return subExp.map(x -> new FunctionalExpression(Functions.NOT, x));
    else
      return subExp;
  }

  static BoxedExp treatComparisonExpression(OC_ComparisonExpressionContext ctx) {
    OC_AddOrSubtractExpressionContext left_ctx = ctx.oC_AddOrSubtractExpression();
    BoxedExp l = treatAddOrSubtractionExpression(left_ctx);
    List<OC_PartialComparisonExpressionContext> right_ctxs = ctx.oC_PartialComparisonExpression();

    if  (right_ctxs == null || right_ctxs.size() == 0)
      return l;
    if  (right_ctxs.size() != 1)
      throw new ParsingException ("ComparisonExpression: we allow only one comparison in a row");
    return l.map(x -> treatPartialComparisonExpression (x, right_ctxs.get(0)));
  }

  static Expression treatPartialComparisonExpression
    (Expression lhs, OC_PartialComparisonExpressionContext ctx)
  {
    String operation = ctx.getStart().getText();
    Functions f;
    switch (operation) {
      case "="  : f = Functions.EQ;  break;
      case "<>" : f = Functions.NEQ; break;
      case "<"  : f = Functions.LE;  break;
      case ">"  : f = Functions.GE;  break;
      case "<=" : f = Functions.LEQ; break;
      case ">=" : f = Functions.GEQ; break;
      default: throw new ParsingException("partialComparisonExpression: unreachable statement.");
    }
    Expression rhs = treatAddOrSubtractionExpression(ctx.oC_AddOrSubtractExpression()).unbox();
    return new FunctionalExpression(f,lhs,rhs);
  }

  static BoxedExp treatAddOrSubtractionExpression
  (OC_AddOrSubtractExpressionContext ctx)
  {
    List<OC_MultiplyDivideModuloExpressionContext> operands =
      ctx.oC_MultiplyDivideModuloExpression();
    List<Functions> operators = new ArrayList<Functions>(operands.size());
    for (ParseTree child : ctx.children) {
      if (child instanceof TerminalNode) {
        String operator = child.getText();
        switch (operator) {
          case "+" : operators.add(Functions.PLUS); break;
          case "-" : operators.add(Functions.MINUS); break;
        }
      }
    }
    if ( (operators.size()+1) != operands.size() )
      throw new ParsingException("AddOrSubtractExpression: impossible case");

    BoxedExp e = treatMultiplyDivideModuloExpression (operands.get(0));
    for (int i = 0;  i < operators.size(); i++ ) {
      final int j = i;
      e = e.map(x -> new FunctionalExpression( operators.get(j), x,
                  treatMultiplyDivideModuloExpression(operands.get(j+1)).unbox()));
    }
    return e;
  }

  static BoxedExp treatMultiplyDivideModuloExpression
  (OC_MultiplyDivideModuloExpressionContext ctx)
  {
    List<OC_PowerOfExpressionContext> pow_ctxs = ctx.oC_PowerOfExpression();
    if (pow_ctxs.size() != 1)
      throw new ParsingException("MultiplyDivideModuloExpression: We do not allow multiplication, division or modulo.");

    return treatPowerOfExpression(pow_ctxs.get(0));
  }

  static BoxedExp treatPowerOfExpression
  (OC_PowerOfExpressionContext ctx)
  {
    List<OC_UnaryAddOrSubtractExpressionContext> una_ctxs = ctx.oC_UnaryAddOrSubtractExpression();
    if (una_ctxs.size() != 1)
      throw new ParsingException("MultiplyDivideModuloExpression: We do not allow multiplication, division or modulo.");
    return treatUnaryAddOrSubtractExpression(una_ctxs.get(0));
  }

  static BoxedExp treatUnaryAddOrSubtractExpression
  (OC_UnaryAddOrSubtractExpressionContext ctx)
  {
    OC_StringListNullOperatorExpressionContext ctx2 = ctx.oC_StringListNullOperatorExpression();
    BoxedExp e = treatStringListNullOperatorExpression(ctx2);
    Boolean isPlus = true;
    for (ParseTree child : ctx.children)
      if ((child instanceof TerminalNode) && (child.getText().equals("-")))
        isPlus = !isPlus;
    if (isPlus)
      return e;
    else
      return e.map( x -> new FunctionalExpression(Functions.MINUS, new Value(0), x));
  }

  static BoxedExp treatStringListNullOperatorExpression
  (OC_StringListNullOperatorExpressionContext ctx)
  {
    List<BoxedExp> exps_ = new ArrayList<BoxedExp>(ctx.children.size());
    List<String> operators = new ArrayList<String>(ctx.children.size());

    int total = 0;

    List<String> possibilities = Arrays.asList(
        "STARTS","ENDS","IN","CONTAINS","[","..","]","IS");

    List<String> adj = Arrays.asList("WITH","NULL", "NOT");

    for (ParseTree child : ctx.children) {
      if(child instanceof OC_ExpressionContext) {
        exps_.add(treatExpression((OC_ExpressionContext) child));
        operators.add(null);
        total++;
      } else if (child instanceof OC_PropertyOrLabelsExpressionContext) {
        exps_.add(treatPropertyOrLabelsExpression((OC_PropertyOrLabelsExpressionContext) child));
        operators.add(null);
        total++;
      } else if (child instanceof TerminalNode) {
        String op = child.getText();
        if (possibilities.contains(op)) {
          exps_.add(box(null));
          operators.add(op);
          total++;
        } else if (adj.contains(op)) {
          int end = operators.size()-1;
          operators.set(end, operators.get(end)+"_"+op);
        }
      }
    }
    if (exps_.size() == 1)
      return exps_.get(0);
    List<Expression> exps = Utils.map(exps_, e -> e.unbox());

    Map<String,Functions> map = new HashMap<String,Functions>();
    /*This maps contains all infix binary functions */
    map.put("STARTS_WITH", Functions.STARTS_WITH);
    map.put("ENDS_WITH", Functions.ENDS_WITH);
    map.put("IN", Functions.IN);
    map.put("CONTAINS", Functions.CONTAINS);
    Expression lhs = exps.get(0);
    for (int i = 1; i< total; i++) {
      String op = operators.get(i);
      if (op == null)
        throw new ParsingException ("StringListNullOperatorExpression: This case should never happen.");

      if ( op.equals("IS_NULL") )
        lhs = new FunctionalExpression(Functions.IS_NULL,lhs);
      else if( op.equals("IS_NOT_NULL"))
        lhs = new FunctionalExpression(Functions.IS_NOT_NULL,lhs);
      else if( map.containsKey(op) ) {
        lhs = new FunctionalExpression(map.get(op), lhs, exps.get(i+1));
        i++;
      } else if ( op.equals("[")) {
        int start = i;
        while ( operators.get(i) == null || !operators.get(i).equals("]") )
          i++;
        if (i == start+2)
          lhs = new SubListExpression(lhs, SubListExpression.Span.UNIQUE, exps.get(start+1));
        else if (i == start+4)
          lhs = new SubListExpression(lhs, exps.get(start+1), exps.get(start+3));
        else { // i == start+3
          if (operators.get(start+1) != null)
            lhs = new SubListExpression(lhs, SubListExpression.Span.UNTIL, exps.get(start+2));
          else
            lhs = new SubListExpression(lhs, SubListExpression.Span.FROM, exps.get(start+1));
        }
      } else
        throw new ParsingException ("StringListNullOperatorExpression: we do not support =~."+op);
    }

    return box(lhs);
  }

  static
  BoxedExp treatPropertyOrLabelsExpression (OC_PropertyOrLabelsExpressionContext ctx)
  {
    BoxedExp lhs = treatAtom(ctx.oC_Atom());
    int total = 0;

    for (ParseTree child : ctx.children) {
      if(child instanceof OC_PropertyLookupContext)
        lhs = treatPropertyLookup(lhs.unbox(), (OC_PropertyLookupContext) child);
      else if (child instanceof OC_NodeLabelsContext)
        throw new ParsingException("PropertyOrLabelsExpression: I don't get what I'm supposed to do with these nodeLabels.");
//         lhs = treatNodeLabels(lhs, (OC_NodeLabelsContext) child);
    }
    return lhs;
  }



//   static
//   BoxedExp box(Expression e) {
// //     if (e == null)
// //       throw new NullPointerException();
//     return new BoxedExp (e);
//   }


  static
  BoxedExp treatAtom (OC_AtomContext ctx)
  {
    if (ctx.oC_Literal() != null)
      return treatLiteral(ctx.oC_Literal());

    if (ctx.oC_Parameter() != null)
      throw new ParsingException("Atom: we don't allow parameters");
    if (ctx.oC_CaseExpression() != null)
      throw new ParsingException("Atom: we don't allow caseExpression");
    if (ctx.COUNT() != null) {
      return new BoxedExp(AggregatingFunction.COUNT_STAR, new Value(true));
    }
    if (ctx.oC_ListComprehension() != null)
      throw new ParsingException("Atom: we don't allow listComprehension");
    if (ctx.oC_PatternComprehension() != null)
      throw new ParsingException("Atom: we don't allow patternComprehension");
    if (ctx.oC_FilterExpression() != null)
      throw new ParsingException("Atom: we don't allow any kind of filter");
    if (ctx.oC_RelationshipsPattern() != null)
      throw new ParsingException("Atom: we don't allow relationshipPattern as atom.");

    if (ctx.oC_ParenthesizedExpression() != null)
      return treatExpression(ctx.oC_ParenthesizedExpression().oC_Expression());

    if (ctx.oC_FunctionInvocation() != null)
      return treatFunctionInvocation(ctx.oC_FunctionInvocation());

    if (ctx.oC_Variable() != null)
      return box(treatVariable(ctx.oC_Variable()));

    throw new ParsingException("Atom: unreachable statement.");
  }

  static
  BoxedExp treatFunctionInvocation
    (OC_FunctionInvocationContext ctx)
  {
    String name = treatSymbolicName(ctx.oC_FunctionName().oC_SymbolicName()).toUpperCase();
    boolean distinct = (ctx.DISTINCT() != null);
    List<Expression> exps = ctx.oC_Expression().stream().map(e -> treatExpression(e).unbox())
                                .collect(Collectors.toList());
    if (exps.size() != 1)
      throw new ParsingException("FunctionInvocation: only aggregating function with one argument are allowed");
    return new BoxedExp(AggregatingFunction.of(name), distinct, exps.get(0));
  }

  static Name treatVariable (OC_VariableContext ctx) {
    return new Name (treatSymbolicName(ctx.oC_SymbolicName()));
  }

  static String treatSchemaName(OC_SchemaNameContext ctx) {
//     if (ctx.oC_SymbolicName() != null)
//       return treatSymbolicName(ctx.oC_SymbolicName());
//     if (ctx.oC_ReservedWord() != null)
//       return (ctx.oC_ReservedWord().getText());
//
//     throw new ParsingException("SchemaName: unreachableStatement.");
    return ctx.getText();
  }

  static
  BoxedExp
  treatPropertyLookup (Expression lhs, OC_PropertyLookupContext ctx)
  {
    return box(new UnaryMapExpression(lhs, treatPropertyKeyName(ctx.oC_PropertyKeyName())));
  }

  static List<Label> treatNodeLabels(OC_NodeLabelsContext ctx)
  {
    List<OC_NodeLabelContext> nl_ctxs = ctx.oC_NodeLabel();
    List<Label> res = new ArrayList<Label>(nl_ctxs.size());
    for (OC_NodeLabelContext nl_ctx : nl_ctxs)
      res.add(treatNodeLabel(nl_ctx));
    return res;
  }

  static Label treatNodeLabel(OC_NodeLabelContext ctx) {
    return new Label(treatSchemaName(ctx.oC_LabelName().oC_SchemaName()));

  }
  static PropertyKey treatPropertyKeyName(OC_PropertyKeyNameContext ctx) {
    return new PropertyKey(treatSchemaName(ctx.oC_SchemaName()));

  }

  static String treatSymbolicName(OC_SymbolicNameContext ctx) {
//     if (ctx.UnescapedSymbolicName() != null)
//       return ctx.UnescapedSymbolicName().getSymbol().getText();
//     if (ctx.EscapedSymbolicName() != null)
//       return ctx.EscapedSymbolicName().getSymbol().getText();
//     if (ctx.HexLetter() != null)
//       return ctx.HexLetter().getSymbol().getText();
//     throw new ParsingException("SymbolicName: we do not allow reserved keyword as symbolic names.");
    return ctx.getText();
  }

  static Query treatRegularQuery(OC_RegularQueryContext ctx) {
    Query lhs = treatSingleQuery(ctx.oC_SingleQuery());
    for (OC_UnionContext uni_ctx : ctx.oC_Union())
      lhs = treatUnion(lhs, uni_ctx);
    return lhs;
  }

  static Query treatUnion(Query lhs, OC_UnionContext ctx)
  { return new UnionQuery(lhs,
                          treatSingleQuery(ctx.oC_SingleQuery()),
                          ctx.ALL() != null );
  }

  static Query treatSingleQuery(OC_SingleQueryContext ctx) {
    if (ctx.oC_SinglePartQuery() != null)
      return treatSinglePartQuery(ctx.oC_SinglePartQuery());
    if (ctx.oC_MultiPartQuery() != null)
      return treatMultiPartQuery(ctx.oC_MultiPartQuery());
    throw new ParsingException("SingleQuery: unreachable statement.");
  }

  static QueryBis treatSinglePartQuery(OC_SinglePartQueryContext ctx) {
    List<Clause> clauses = new LinkedList<Clause>();
    for (ParseTree child : ctx.children) {
      if (child instanceof OC_UpdatingClauseContext)
        clauses.addAll(treatUpdatingClause((OC_UpdatingClauseContext) child));
      else if (child instanceof OC_ReadingClauseContext)
        clauses.addAll(treatReadingClause((OC_ReadingClauseContext) child));
    }

    QueryBis right = (ctx.oC_Return()!= null) ? treatReturn(ctx.oC_Return())
                                              : new ReturnQueryBis();
    return (clauses.size() == 0) ? right : new ClauseQueryBis(clauses, right);
  }

  static QueryBis treatMultiPartQuery(OC_MultiPartQueryContext ctx) {
    List<Clause> clauses = new LinkedList<Clause>();
    for (ParseTree child : ctx.children) {
      if (child instanceof OC_UpdatingClauseContext)
        clauses.addAll(treatUpdatingClause((OC_UpdatingClauseContext) child));
      else if (child instanceof OC_ReadingClauseContext)
        clauses.addAll(treatReadingClause((OC_ReadingClauseContext) child));
      else if (child instanceof OC_WithContext)
        clauses.addAll(treatWith((OC_WithContext) child));
    }
    QueryBis right = treatSinglePartQuery(ctx.oC_SinglePartQuery());
    return new ClauseQueryBis(clauses, right);
  }

  static List<Clause> treatUpdatingClause(OC_UpdatingClauseContext ctx) {
    if (ctx.oC_Create() != null)
      return listFromOne(treatCreate(ctx.oC_Create()));

    if (ctx.oC_Set() != null)
      return listFromOne(treatSet(ctx.oC_Set()));

    if (ctx.oC_Remove() != null)
      return listFromOne(treatRemove(ctx.oC_Remove()));

    if (ctx.oC_Delete() != null)
      return listFromOne(treatDelete(ctx.oC_Delete()));

    if (ctx.oC_Merge() != null)
      return listFromOne(treatMerge(ctx.oC_Merge()));

    throw new UnreachableStatementError();
  }

  static MergeClause treatMerge(OC_MergeContext ctx) {
    if (ctx.oC_MergeAction().size() != 0)
      throw new UnsupportedFeatureException("ON CREATE and ON MATCH subclauses", Reason.SEMANTICS);
    List<PathPattern> list = new ArrayList<>();
    list.add(treatPatternPart(ctx.oC_PatternPart()));
    return (new MergeClause(list));
  }

  static DeleteClause treatDelete(OC_DeleteContext ctx) {
    List<Expression> expressions = Utils.map(ctx.oC_Expression(), x -> treatExpression(x).unbox());
    boolean b = (ctx.DETACH() !=  null);

    return new DeleteClause(b, expressions);
  }

  static RemoveClause treatRemove(OC_RemoveContext ctx) {
    return
      new RemoveClause(
        Utils.map(ctx.oC_RemoveItem(), x -> treatRemoveItem(x)) );
  }

  static
  Pair<Expression,Either<List<Label>,List<PropertyKey>>>
  treatRemoveItem
    ( OC_RemoveItemContext ctx )
  {
    if (ctx.oC_Variable() != null)
      return new Pair<>(
        treatVariable(ctx.oC_Variable()),
        Either.first(treatNodeLabels(ctx.oC_NodeLabels()))
        );
    else {
      Pair<Expression,List<PropertyKey>> pair
        = treatPropertyExpression(ctx.oC_PropertyExpression());
      return new Pair<>(pair.first, Either.second(pair.second));
    }
  }



  static SetClause treatSet(OC_SetContext ctx) {
    List<OC_SetItemContext> subctxs = ctx.oC_SetItem();
    return new SetClause( Utils.map(subctxs, AstVisitor::treatSetItem) );
  }

  static SetItem treatSetItem(OC_SetItemContext ctx) {
    if (ctx.oC_PropertyExpression() != null) { //
      Pair<Expression,List<PropertyKey>> pair
        = treatPropertyExpression(ctx.oC_PropertyExpression());
      Expression exp = treatExpression(ctx.oC_Expression()).unbox();
      return new SetItem.PropertyOverwrite(pair.first, pair.second, exp);
    }
    else if (ctx.oC_NodeLabels() != null) { //LabList
      Expression exp = treatVariable(ctx.oC_Variable());
      List<Label> labels = treatNodeLabels(ctx.oC_NodeLabels());
      return new SetItem.LabelAdd(exp, labels);
    }
    else {
      boolean b = ctx.children.stream().anyMatch( x-> x.getText().equals("+="));
      Expression l = treatVariable(ctx.oC_Variable());
      Expression r = treatExpression(ctx.oC_Expression()).unbox();
      return new SetItem.PropertyCopy(l,r,b);
    }
  }

  static Pair<Expression,List<PropertyKey>>
  treatPropertyExpression(OC_PropertyExpressionContext ctx) {
    return new Pair<>(treatAtom(ctx.oC_Atom()).unbox(),
        Utils.map(ctx.oC_PropertyLookup(),
                  x->treatPropertyKeyName(x.oC_PropertyKeyName())));
  }

  static
  Pair<Boolean,List<WithItem>>
  treatReturnItems
      (OC_ReturnItemsContext ctx)
  {
    List<OC_ReturnItemContext> item_ctxs = ctx.oC_ReturnItem();
    List<WithItem> named_exps = new ArrayList<WithItem> (item_ctxs.size());
    for (OC_ReturnItemContext item_ctx : item_ctxs) {
      named_exps.add(treatReturnItem(item_ctx));
    }
    if (ctx.children.get(0).getText().equals("*"))
      return new Pair<Boolean,List<WithItem>>(true, named_exps);
    else
      return new Pair<Boolean,List<WithItem>>(false, named_exps);
  }


  static QueryBis treatReturn(OC_ReturnContext ctx) {
//     OC_ReturnItemsContext ctx2 = .returnItems();
    Pair<Pair<Boolean,List<WithItem>>,OrderBy> pair= treatReturnBody(ctx.oC_ReturnBody());
    //FIXME: ORDER BY is ignored in RETURN for now.
    return new ReturnQueryBis(pair.first.first, pair.first.second);
  }

  static Pair<Pair<Boolean,List<WithItem>>,OrderBy>
  treatReturnBody(OC_ReturnBodyContext ctx)
  {
    Pair<Boolean,List<WithItem>> items = treatReturnItems(ctx.oC_ReturnItems());
    OrderBy ob = null;
    OC_OrderContext o_ctx = ctx.oC_Order();
    if (o_ctx != null) {
      List<OrderedExpression> order_desc = treatOrder(ctx.oC_Order());
      List<OrderBy.SubClause> subclauses = new ArrayList<OrderBy.SubClause>(2);
      if (ctx.oC_Skip() != null)
        subclauses.add(treatSkip(ctx.oC_Skip()));
      if (ctx.oC_Limit() != null)
        subclauses.add(treatLimit(ctx.oC_Limit()));
      ob = new OrderBy(order_desc,subclauses);
    }
    return (new Pair<Pair<Boolean,List<WithItem>>,OrderBy>(items, ob));
  }

  static OrderBy.Skip treatSkip(OC_SkipContext ctx) {
    return new OrderBy.Skip(treatExpression(ctx.oC_Expression()).unbox());
  }

  static OrderBy.Limit treatLimit(OC_LimitContext ctx) {
    return new OrderBy.Limit(treatExpression(ctx.oC_Expression()).unbox());
  }

  static List<OrderedExpression> treatOrder(OC_OrderContext ctx) {
    List<OC_SortItemContext> si_ctxs = ctx.oC_SortItem();
    List<OrderedExpression> result = new ArrayList<OrderedExpression>(si_ctxs.size());
    for (OC_SortItemContext si_ctx : si_ctxs)
      result.add(treatSortItem(si_ctx));
    return result;
  }


  static OrderedExpression treatSortItem(OC_SortItemContext ctx) {
    return new OrderedExpression((ctx.DESC() == null && ctx.DESCENDING() == null),
                          treatExpression(ctx.oC_Expression()).unbox());
  }


  static WithItem treatReturnItem(OC_ReturnItemContext ctx) {
    BoxedExp exp = treatExpression(ctx.oC_Expression());
    Optional<Name> name =   Optional.ofNullable(ctx.oC_Variable())
                                    .map(x ->treatVariable(x));
    return exp.toWithItem(name);
  }

  static List<Clause> treatReadingClause(OC_ReadingClauseContext ctx) {
    if (ctx.oC_Match() != null)
      return treatMatch(ctx.oC_Match());

    if (ctx.oC_Unwind() != null)
      return Arrays.asList(treatUnwind(ctx.oC_Unwind()));

    if (ctx.oC_InQueryCall() != null)
      throw new ParsingException("ReadingClause: we do not allow in-query calls.");

    throw new ParsingException("ReadingClause: unreachable statement.");
  }

  static List<Clause> treatMatch(OC_MatchContext ctx) {
    boolean optional = false;
    if (ctx.OPTIONAL() != null)
      optional = true;
    List<Clause> result = new ArrayList<Clause>(2);
    List<PathPattern> patterns = treatPattern(ctx.oC_Pattern())
    ;
    WhereClause whereClause = null;
    if (ctx.oC_Where() != null)
      whereClause = treatWhere(ctx.oC_Where());

    if (optional)  {
      if (whereClause == null)
        result.add(new OptionalMatchClause(patterns));
      else
        result.add(new OptionalMatchClause(whereClause, patterns));
    } else {
      result.add(new MatchClause(patterns));
      if (whereClause != null)
        result.add(whereClause);
    }
    return result;
  }

  static Clause treatUnwind(OC_UnwindContext ctx) {
    Expression exp = treatExpression(ctx.oC_Expression()).unbox();
    Name name = treatVariable(ctx.oC_Variable());
    return new UnwindClause(exp, name);
  }

//   static void treatUpdatingPart(OC_UpdatingPartContext ctx) {
//     if (ctx.oC_UpdatingClause().size() != 0)
//       throw new ParsingException("UpdatingPart: we do not allow in-query calls.");
//   }

  static WhereClause treatWhere(OC_WhereContext ctx) {
    return new WhereClause(treatExpression(ctx.oC_Expression()).unbox());
  }

  static List<Clause> treatWith(OC_WithContext ctx) {
    Pair<Pair<Boolean,List<WithItem>>,OrderBy> triplet = treatReturnBody(ctx.oC_ReturnBody());
    Pair<Boolean,List<WithItem>> pair = triplet.first;
    List<Clause> result = new ArrayList<Clause>(4);
    result.add(new WithClause(pair.first, pair.second));
    if (triplet.second != null)
      result.add(triplet.second);
    if (ctx.oC_Where() != null)
      result.add(treatWhere(ctx.oC_Where()));
    return result;
  }

  static List<PathPattern> treatPattern(OC_PatternContext ctx) {
    List<OC_PatternPartContext> part_ctxs = ctx.oC_PatternPart();
    List<PathPattern> res = new ArrayList<PathPattern>(part_ctxs.size());
    for (OC_PatternPartContext  part_ctx : part_ctxs)
      res.add(treatPatternPart(part_ctx));
    return res;
  }

  static PathPattern treatPatternPart(OC_PatternPartContext ctx) {
    UnnamedPathPattern upp =
        treatAnonymousPatternPart( ctx.oC_AnonymousPatternPart() );
    if (ctx.oC_Variable() != null)
      return new NamedPathPattern(treatVariable(ctx.oC_Variable()), upp);
    else
      return upp;
  }

  static
  UnnamedPathPattern
  treatAnonymousPatternPart
      (OC_AnonymousPatternPartContext ctx)
  {
    return treatPatternElement(ctx.oC_PatternElement());
  }

  static UnnamedPathPattern treatPatternElement(OC_PatternElementContext ctx) {
    if (ctx.oC_PatternElement() != null)
      return treatPatternElement(ctx.oC_PatternElement());

    List<OC_PatternElementChainContext> chain_ctxs = ctx.oC_PatternElementChain();
    int length = chain_ctxs.size();
    List<UnnamedPathPattern> path
        = new ArrayList<UnnamedPathPattern>(2*(length+1));
//     List<RelationPattern> unlinked_rels
//       = new ArrayList<RelationPattern>(length);
    path.add(treatNodePattern(ctx.oC_NodePattern()));
    for ( OC_PatternElementChainContext sub_ctx : chain_ctxs ) {
      Pair<RelationPattern,NodePattern> pair
          = treatPatternElementChain(sub_ctx);
      path.add(pair.first);
      path.add(pair.second);
    }
    path.add(path.get(2*length)); //this is to ensure that the successor of the relation at position 2*(length-1) exists.

    for (int i=0; i<length; i++) {
      int j = 2*i;
      RelationPattern rel = (RelationPattern) path.get(j+1);
      rel.setPrevious( (NodePattern) path.get(j) );
      rel.setNext(path.get(j+3));
    }

    return (path.get(1));
  }

  static ExplicitMapExpression treatProperties(OC_PropertiesContext ctx) {
    if (ctx.oC_MapLiteral() != null)
      return (ExplicitMapExpression) treatMapLiteral(ctx.oC_MapLiteral()).unbox();
    if (ctx.oC_Parameter() != null)
      throw new UnsupportedFeatureException("$ parameters", Reason.RESTRICTED);

    throw new UnreachableStatementError();
  }

  static NodePattern treatNodePattern(OC_NodePatternContext ctx) {
//     return new NodePattern();

    List<Label> labels;
    if (ctx.oC_NodeLabels() != null)
      labels = treatNodeLabels(ctx.oC_NodeLabels());
    else
      labels = new ArrayList<Label>();

    Name name;
    if (ctx.oC_Variable() != null)
      name = treatVariable(ctx.oC_Variable());
    else
      name = null;

    ExplicitMapExpression map;
    if (ctx.oC_Properties() != null)
      map = treatProperties(ctx.oC_Properties());
    else
      map = null;

    return new NodePattern(name, labels, map);
  }

  static
  RelationPattern
  treatRelationshipPattern
      (OC_RelationshipPatternContext ctx)
  {
    RelationPattern rp;
    if (ctx.oC_RelationshipDetail() != null)
      rp = treatRelationshipDetail(ctx.oC_RelationshipDetail());
    else
      rp = new SimpleRelationPattern(null,null);
      // fixme : this is awful...


    if (ctx.oC_LeftArrowHead() != null && ctx.oC_RightArrowHead() == null)
      rp.setDirection(RelationPattern.Direction.BACKWARD);
    if (ctx.oC_LeftArrowHead() == null && ctx.oC_RightArrowHead() != null)
      rp.setDirection(RelationPattern.Direction.FORWARD);

    return rp;
  }

  /** This function returns a RelationPattern, the direction of which remains to be set */
  static
  RelationPattern
  treatRelationshipDetail
      (OC_RelationshipDetailContext ctx)
  {
    List<Object> args = new ArrayList<Object>(3);
    if (ctx.oC_Variable() != null)
      args.add(treatVariable(ctx.oC_Variable()));
    if (ctx.oC_RelationshipTypes() != null)
      args.add(treatRelationshipTypes(ctx.oC_RelationshipTypes()));
    if (ctx.oC_Properties() != null)
      args.add(treatProperties(ctx.oC_Properties()));

    if (ctx.oC_RangeLiteral() == null)
      return new SimpleRelationPattern(null, null, args.toArray());
    else {
      Iteration it = treatRangeLiteral(ctx.oC_RangeLiteral());
      return new MultipleRelationPattern
          ( it,
            null, null,
            args.toArray()
          );
    }
  }

  static
  Pair <RelationPattern, NodePattern>
  treatPatternElementChain
      (OC_PatternElementChainContext ctx)
  {
    return new Pair<RelationPattern, NodePattern>
      ( treatRelationshipPattern(ctx.oC_RelationshipPattern()),
        treatNodePattern(ctx.oC_NodePattern())
      );
  }

  static
  List<RelationType>
  treatRelationshipTypes
      ( OC_RelationshipTypesContext ctx )
  {
    List<OC_RelTypeNameContext> rel_ctxs = ctx.oC_RelTypeName();
    List<RelationType> res = new ArrayList<RelationType>(rel_ctxs.size());
    for(OC_RelTypeNameContext rel_ctx : rel_ctxs) {
      res.add(treatRelTypeName(rel_ctx));
    }
    return res;
  }

  static RelationType treatRelTypeName (OC_RelTypeNameContext ctx) {
    return new RelationType(treatSchemaName(ctx.oC_SchemaName()));
  }


  static Iteration treatRangeLiteral(OC_RangeLiteralContext ctx) {
    if (ctx.oC_IntegerLiteral().size() == 0 )
      return new Iteration.Star();
    int n;
    n = treatIntegerLiteral( ctx.oC_IntegerLiteral().get(0) ).intValue();
    if (ctx.oC_IntegerLiteral().size() == 2)
      return new Iteration.Between(
          n,
          treatIntegerLiteral( ctx.oC_IntegerLiteral().get(1) ).intValue()
        );

    boolean metNumber = false;
    for ( ParseTree child : ctx.children ) {
      if (child instanceof OC_IntegerLiteralContext)
        metNumber = true;
      if ( child instanceof TerminalNode && child.getText().equals("..") ) {
        if (metNumber)
          return new Iteration.From(n);
        else
          return new Iteration.Until(n);
      }
    }
    return new Iteration.Fixed(n);
  }

  static CreateClause treatCreate(OC_CreateContext ctx) {
    return new CreateClause(treatPattern(ctx.oC_Pattern()));
  }

  static Clause treatCyphSemClause(OC_CyphSemClauseContext ctx) {
    if (ctx.oC_Match() != null) {
      List<Clause> clauses = treatMatch(ctx.oC_Match());
      if (clauses.size() != 1)
        throw new ParsingException("`MATCH ... WHERE ...` is two CyphSem clauses, not one.");
      return clauses.get(0);
    }

    if (ctx.oC_Unwind() != null)
      return treatUnwind(ctx.oC_Unwind());

    if (ctx.oC_With() != null) {
      List<Clause> clauses = treatWith(ctx.oC_With());
      if (clauses.size() != 1)
        throw new ParsingException("`WITH ... ORDER BY ...` is two CyphSem clauses, not one.");
      return clauses.get(0);
    }


    if (ctx.oC_Where() != null)
      return treatWhere(ctx.oC_Where());

    if (ctx.oC_Order() != null) {
      OC_OrderContext o_ctx = ctx.oC_Order();
      if (o_ctx != null) {
        List<OrderedExpression> order_desc = treatOrder(o_ctx);
        List<OrderBy.SubClause> subclauses = new ArrayList<OrderBy.SubClause>(2);
        if (ctx.oC_Skip() != null)
          subclauses.add(treatSkip(ctx.oC_Skip()));
        if (ctx.oC_Limit() != null)
          subclauses.add(treatLimit(ctx.oC_Limit()));
        return new OrderBy(order_desc,subclauses);
      }
    }

    if (ctx.oC_Create() != null)
      return treatCreate(ctx.oC_Create());

    if (ctx.oC_Merge() != null)
      return treatMerge(ctx.oC_Merge());

    if (ctx.oC_Delete() != null)
      return treatDelete(ctx.oC_Delete());

    if (ctx.oC_Set() != null)
      return treatSet(ctx.oC_Set());

    if (ctx.oC_Remove() != null)
      return treatRemove(ctx.oC_Remove());

    throw new UnreachableStatementError();
  }

}
