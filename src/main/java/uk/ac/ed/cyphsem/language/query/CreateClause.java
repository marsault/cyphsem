/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.value.*;


import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Id;
import uk.ac.ed.cyphsem.datamodel.graph.Label;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.RelationType;

import uk.ac.ed.cyphsem.language.pattern.CompiledPattern;
import uk.ac.ed.cyphsem.language.pattern.NamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.NodePattern;
import uk.ac.ed.cyphsem.language.pattern.UnnamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.PathPattern;
import uk.ac.ed.cyphsem.language.pattern.PathPattern.VarStatus;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern.Direction;
import uk.ac.ed.cyphsem.language.pattern.SimpleRelationPattern;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/** Implements Cypher clauses of the form <tt>CREATE p1,..,pn</tt>,
  * where p1, ..., pn are path patterns.
***/
public class CreateClause implements WritingClause, PatternHolder {

    /** Path patterns of this {@link CreateClause}. */
    List<PathPattern> patterns;

    /** Compiled version of field {@link #patterns} */
    CompiledPattern compiledPattern;

    @Override //from PatternHolder
    public List<PathPattern> patterns()
    { return patterns; }

    /** Constructs a {@link CreateClause} from given patterns. */
    public CreateClause(List<PathPattern> patterns) {
      this.patterns = patterns;
      compiledPattern = new CompiledPattern(true, patterns);
      PatternHolder.super.assertPatternTupleIsSuitable();
    }

    @Override
    public String toString() {
      return PatternHolder.super.formatToString("CREATE");
    }




    @Override
    public QueryData apply(QueryData qd, Interruptor interruptor)
    throws InterruptedException
    {
      interruptor.check();
      try {
        Graph graph_in = qd.graph();
        Graph graph_out = graph_in;
        Table table_out = new Table();
        compiledPattern.assertValidInContextOf(qd.table().domain());

        List<Node> new_nodes = new ArrayList<>();         // collect nodes and
        List<Relation> new_relations = new ArrayList<>(); // relations to add
                                                          // to graph_out

        for (Record record_in : qd.table()) {
          interruptor.check();
          Record record_out = new Record(record_in);

          /* Creation of nodes */
          Map<Name,Node> node_map = new HashMap<Name,Node>();
          for (Name name : compiledPattern.nodeVariables())
            if ( record_in.isBound(name) )
              node_map.put(name, record_in.get(name).asNode());
            else {
              Value map_value = compiledPattern.propertyOf(name).evaluate(graph_in,record_in);
              Values.assertStorable(map_value);
              Node new_node = graph_out.makeNode( compiledPattern.labelsOf(name),
                                                  map_value.asMap() );
              node_map.put(name, new_node);
              new_nodes.add(new_node);
              if (compiledPattern.isGenuine(name))
                record_out.bind(name, new Value(new_node));
            }

          /* Creation of relations */
          Map<Name,Relation> rel_map = new HashMap<Name,Relation>();
          for (Name name : compiledPattern.relationVariables()) {
            // This case should never happen since relation variable are not
            // allowed to be bound in update clauses.
            //if ( record_in.isBound(name) )
              //rel_map.put(name, record_in.get(name).asRelation());
            //else  {
            Value map_value
              = compiledPattern.propertyOf(name).evaluate(graph_in,record_in);
            Relation new_rel = graph_out.makeRelation(
                compiledPattern.anyTypeOf(name),              /* type */
                node_map.get(compiledPattern.sourceOf(name)), /* source */
                node_map.get(compiledPattern.targetOf(name)), /* target */
                map_value.asMap());                           /* properties */
            rel_map.put(name, new_rel);
            new_relations.add(new_rel);
            if (compiledPattern.isGenuine(name))
              record_out.bind(name, new Value(new_rel));
            //}
          }

          /* Adding paths variable to record_out */
          for (Name path_name : compiledPattern.genuinePathVariables()) {
            boolean b = true;
            List<Node> path_nodes = new ArrayList<>();
            List<Relation> path_rels = new ArrayList<>();
            for ( Name id_name : compiledPattern.pathOf(path_name) ){
              if (b)
                path_nodes.add(node_map.get(id_name));
              else
                path_rels.add(rel_map.get(id_name));
              b = !b;
            }
            record_out.bind(
                path_name, new Value(new Path(path_nodes, path_rels)));
          }
          table_out.add(record_out);
        }
        /* Adding nodes and relations to the graph */
        for (Node node : new_nodes)
          graph_out.addNode(node);
        for (Relation relation : new_relations)
          graph_out.addRelation(relation);
        return new QueryData(graph_out,table_out);
      } catch (DynamicCastException|RecordInconsistencyException|AssertionFailedException e)
        { throw new UnspecifiedBehaviourException("In "+toString()+".",e); }
    }
}
