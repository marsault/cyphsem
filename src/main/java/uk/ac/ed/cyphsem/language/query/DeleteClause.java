/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.Parameters;
import uk.ac.ed.cyphsem.Parameters.DeleteVersion;
import uk.ac.ed.cyphsem.Parameters.DeleteVersion.*;
import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.exception.UnreachableStatementError;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;
import uk.ac.ed.cyphsem.exception.UnsupportedFeatureException;

import uk.ac.ed.cyphsem.datamodel.graph.Id;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Node;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;

import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.*;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Implements Cypher clauses of the form <tt>DELETE e1,..,en</tt> or <tt>DETACH DELETE e1,..,en</tt>,
  * where e1,..en are {@link Expression}s.
  * At execution time, it is expected that all these expressions evaluate to
  * nodes or relations.
***/
public class DeleteClause implements WritingClause {

  /** Whether this is a <tt>DETACH DELETE</t>. */
  protected boolean detach = false;

  protected Set<Expression> items = new HashSet<>();

  /** Constructs a {@link DeleteClause} from given boolean and expressions.
    * A new set is built from parameter {@code coll}.
  **/
  public DeleteClause(boolean isDetach, Collection<? extends Expression> coll) {
    detach = isDetach;
    addItems(coll);
  }

  /** Constructs a {@link DeleteClause} that is not <tt>DETACH DELETE</tt> from
  given expression.
    <br>
    * Implementation calls {@link #DeleteClause DeleteClause}{@code (false,coll)}. */
  public DeleteClause(Collection<Expression> coll) {
    this(false,coll);
  }

  /** Returns the set of expressions without copy. */
  public Set<Expression> items() { return items; }

  /** Add an element to {@link #items}. */
  public void addItem(Expression e) { items.add(e); }

  /** Adds elements to {@link #items}. */
  public void addItems(Collection<? extends Expression> x) { items.addAll(x); }


  @Override
  public QueryData apply(QueryData qd, Interruptor interruptor)
  throws InterruptedException
  {
    return apply(qd, interruptor, Parameters.global.delete());
  }

  public QueryData apply(QueryData qd, Interruptor interruptor, Parameters.DeleteVersion version)
  throws InterruptedException
  {
    switch (version) {
      case TOMBSTONE: {
        Set<Id> id_to_delete = new HashSet<>();
        for (Record record : qd.table())
          id_to_delete.addAll(Utils.map(items, x -> Expressions.evaluateToId(x,qd.graph(),record)));
        List<Node> nodes_to_verify =
          id_to_delete.stream().filter(x -> x instanceof Node)
                               .map (x -> (Node) x)
                               .collect(Collectors.toList());

        if (detach) {
          id_to_delete.addAll(Utils.flatMap(nodes_to_verify, x -> x.in.stream()));
          id_to_delete.addAll(Utils.flatMap(nodes_to_verify, x -> x.out.stream()));
          id_to_delete.addAll(Utils.flatMap(nodes_to_verify, x -> x.loop.stream()));
        } else
          for (Node n : nodes_to_verify) {
            for (Relation r : n.loop)
              if (! id_to_delete.contains(r))
                throw new UnspecifiedBehaviourException("In execution of clause "+toString()+".  Node " + n + " is to be deleted, but one loop "+r+" is not");
            for (Relation r : n.out)
              if (! id_to_delete.contains(r))
                throw new UnspecifiedBehaviourException("In execution of clause "+toString()+".  Node " + n + " is to be deleted, but one outgoing relation "+r+" is not");
            for (Relation r : n.in)
              if (! id_to_delete.contains(r))
                throw new UnspecifiedBehaviourException("In execution of clause "+toString()+".  Node " + n + " is to be deleted, but one incoming relation "+r+" is not");
          }
        for (Id id : id_to_delete)
          if (id instanceof Relation)
            qd.graph().remove(id);
        for (Id id : id_to_delete)
          if (id instanceof Node)
            qd.graph().remove(id);
        return qd;
      }
      case AMNESIC :
        throw new UnsupportedFeatureException("The AMNESIC behaviour of clause DELETE is not implemented yet.");
      case LEGACY:
        throw new UnsupportedFeatureException("The LEGACY behaviour of clause DELETE is not implemented yet.");
    }
    throw new UnreachableStatementError();
  }

  @Override
  public String toString() {
    return
      (detach? "DETACH " :"")
      + "DELETE "
      + StringOf.iterable(items, "", "", ", ");
  }
}
