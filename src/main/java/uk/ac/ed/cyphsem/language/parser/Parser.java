/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.parser;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;

import uk.ac.ed.cyphsem.language.query.*;

import uk.ac.ed.cyphsem.table.*;

import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;

import uk.ac.ed.cyphsem.language.parser.autogen.CypherParser;
import uk.ac.ed.cyphsem.language.parser.autogen.CypherLexer;
import uk.ac.ed.cyphsem.language.parser.autogen.CypherParser.*;

import uk.ac.ed.cyphsem.language.pattern.*;

import java.io.IOException;

import java.math.BigInteger;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

public interface Parser {

  public static Query queryOf (CharStream in) {
    //CypherLexer lexer = new CypherLexer(in);
    //lexer.addErrorListener(SyntaxErrorListener.INSTANCE);
    //lexer.removeErrorListener(ConsoleErrorListener.INSTANCE);

    //CommonTokenStream tokens = new CommonTokenStream(lexer);

    //CypherParser parser = new CypherParser(tokens);
    //parser.removeErrorListener(ConsoleErrorListener.INSTANCE);
    //parser.addErrorListener(SyntaxErrorListener.INSTANCE);

    OC_RegularQueryContext ctx
      = cypherParser(in).oC_Cypher()
                        .oC_Statement()
                        .oC_Query()
                        .oC_RegularQuery();

    return (AstVisitor.treatRegularQuery(ctx));
  }

  public static Query queryOf (String input) {
    return queryOf(CharStreams.fromString(input));
  }


  public static Query queryOf(Path path) throws IOException {
    return queryOf (CharStreams.fromPath(path));
  }


  public static Graph graphOf(String input) {
    String[] str_arr = input.split("CREATE ");
    List<String> clauses = new ArrayList<String> (str_arr.length);
    List<PathPattern> patterns = new ArrayList<PathPattern> (str_arr.length);
    for (int i = 1; i<str_arr.length; i++) {
      String clause = str_arr[i];
      CharStream in = CharStreams.fromString(clause);

      //CypherLexer lexer = new CypherLexer(in);
      //lexer.removeErrorListener(ConsoleErrorListener.INSTANCE);
      //lexer.addErrorListener(SyntaxErrorListener.INSTANCE);

      //CommonTokenStream tokens = new CommonTokenStream(lexer);

      //CypherParser parser = new CypherParser(tokens);
      //parser.removeErrorListener(ConsoleErrorListener.INSTANCE);
      //parser.addErrorListener(SyntaxErrorListener.INSTANCE);

      OC_PatternContext ctx = cypherParser(in).oC_Pattern();
      patterns.addAll(AstVisitor.treatPattern(ctx));
    }

    HashMap<Name,Node> name_to_node = new HashMap<Name,Node>();

    Graph g = new Graph();

    for (PathPattern pattern : patterns) {
      if (pattern instanceof NodePattern) {
        NodePattern pattern_as_node = (NodePattern) pattern;
        Node new_node = g.addNode(new ArrayList<>(),new HashMap<>());
        name_to_node.put(pattern_as_node.getName(), new_node);
        ExplicitMapExpression map = pattern_as_node.getMap();
        if (map != null)
          for (KeyExpPair pair : map)
            new_node.overwriteProperty(pair.key,Expressions.staticEvaluation(pair.exp));
        List<Label> labels = pattern_as_node.getLabels();
        if (labels != null)
          for (Label label : labels)
            new_node.addLabel(label);
      } else if (pattern instanceof RelationPattern) {
        RelationPattern pattern_as_rel= (RelationPattern) pattern;
        NodePattern src = pattern_as_rel.getPrevious();
        NodePattern dst = (NodePattern) pattern_as_rel.getNext();

        List<RelationType> types = pattern_as_rel.types().get();
        if ( (types == null) || (types.size() != 1) )
          throw new Error();

        Node src_as_node = name_to_node.get(src.getName());
        Node dst_as_node = name_to_node.get(dst.getName());
        Map<PropertyKey,Value> property_map = new HashMap<>();
        ExplicitMapExpression map = pattern_as_rel.getMap();
        if (map != null)
          for (KeyExpPair pair : map)
            property_map.put(pair.key, Expressions.staticEvaluation(pair.exp));
        Relation new_rel = g.addRelation(types.get(0), src_as_node, dst_as_node, property_map);
      }
    }
    return g;
  }

  public static Graph graphOf(Path path) throws IOException {
      return graphOf (new String(Files.readAllBytes(path), StandardCharsets.UTF_8));
  }

  public static List<PathPattern>patternsOf(String input) {
    return patternsOf(CharStreams.fromString(input));
  }

  public static List<PathPattern>patternsOf(Path path) throws IOException {
    return patternsOf (CharStreams.fromPath(path));
  }

  public static List<PathPattern>patternsOf(CharStream in) {
    OC_Pattern_StandaloneContext ctx
      = cypherParser(in).oC_Pattern_Standalone();
    return (AstVisitor.treatPattern(ctx.oC_Pattern()));
  }

  /** Parse a single {@link Clause} in given {@link CharStream}. */
  public static Clause clauseOf(CharStream in) {
    OC_CyphSemClause_StandaloneContext ctx
      = cypherParser(in).oC_CyphSemClause_Standalone();
    return AstVisitor.treatCyphSemClause(ctx.oC_CyphSemClause());
  }

  /** Convenience function for parse a single clause from a given String.
    * Calls {@code clauseOf(CharStreams.fromString(input))}. */
  public static Clause clauseOf(String input) {
    return clauseOf( CharStreams.fromString(input) );
  }

  /** Convenience function to parse a single clause from a file.
    * Calls {@code clauseOf (CharStreams.fromPath(path))}. */
  public static Clause clauseOf(Path path) throws IOException
  {
    return clauseOf( CharStreams.fromPath(path) );
  }


  private static CypherParser cypherParser(CharStream in)
  {
    CypherLexer lexer = new CypherLexer(in);
    lexer.addErrorListener(SyntaxErrorListener.INSTANCE);
    lexer.removeErrorListener(ConsoleErrorListener.INSTANCE);

    CommonTokenStream tokens = new CommonTokenStream(lexer);

    CypherParser parser = new CypherParser(tokens);
    parser.removeErrorListener(ConsoleErrorListener.INSTANCE);
    parser.addErrorListener(SyntaxErrorListener.INSTANCE);
    return parser;
  }
}
