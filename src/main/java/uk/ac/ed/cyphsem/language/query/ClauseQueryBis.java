/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;

import java.lang.RuntimeException;

import java.util.ArrayList;
import java.util.List;


/**
  *
*/
public class ClauseQueryBis implements QueryBis {

  public Clause left;
  public QueryBis  right;

  public ClauseQueryBis(Clause left, QueryBis right) {
    this.left = left;
    this.right = right;
  }

  public ClauseQueryBis(List<Clause> left, QueryBis right) {
    if (left.size() == 0)
      throw new RuntimeException("ClauseQuery requires at least one clause");
    this.left = left.get(0);
    this.right = right;
    for (int i = left.size(); --i>0; )
      this.right = new ClauseQueryBis(left.get(i), this.right);
  }

  @Override
  public QueryData apply(QueryData qd, Interruptor i)
  throws InterruptedException
  {
    i.check();
    return (right.apply(left.apply(qd,i),i));
  }

  @Override
  public String toString() {
    return left+"\n"+right;
  }


}
