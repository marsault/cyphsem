/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;


import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.Utils.*;
import uk.ac.ed.cyphsem.Utils.Policy.*;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;
import uk.ac.ed.cyphsem.exception.UndefinedNameException;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;

import uk.ac.ed.cyphsem.language.expression.ExplicitMapExpression;
import uk.ac.ed.cyphsem.language.expression.KeyExpPair;
import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.FunctionalExpression;
import uk.ac.ed.cyphsem.language.expression.UnaryMapExpression;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.language.expression.Functions;
import uk.ac.ed.cyphsem.language.expression.Expressions;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.RelationType;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Optional;



public abstract class RelationPattern extends UnnamedPathPattern {

  public static enum Direction {
    FORWARD, BACKWARD, BOTH;
  }


  NodePattern previous;
  UnnamedPathPattern next;

  Optional<List<RelationType>> acceptableTypes = Optional.empty();
  ExplicitMapExpression map = null;
  Direction direction = Direction.BOTH;

  public Direction getDirection() {
    return direction;
  }


  public
  RelationPattern
      ( NodePattern previous, UnnamedPathPattern next, Object... others )
  {
    this.previous = previous;
    this.next = next;
    for (Object o : others) {
      if (o instanceof Name)
        name = (Name) o;
      else if (o instanceof RelationType) {
//         if (acceptableTypes == null)
//           acceptableTypes = new ArrayList<RelationType>();
        addType((RelationType) o);
      } else if (o instanceof KeyExpPair) {
        if (map == null)
          map = new ExplicitMapExpression((KeyExpPair) o);
        else
          map.add((KeyExpPair) o);
      } else if (o instanceof ExplicitMapExpression) {
        if (map == null)
          map = (ExplicitMapExpression) o;
        else
          map.addAll((ExplicitMapExpression) o);
      } else if (o instanceof Direction)
        direction = ((Direction) o);
      else if (o instanceof List) {
//         if (acceptableTypes == null)
//           acceptableTypes = new ArrayList<RelationType>();
        @SuppressWarnings("unchecked")
        List<RelationType> l = (List<RelationType>) o;
        l.stream().forEach(this::addType);
//         acceptableTypes .addAll(l);
      }
      else
        throw new Error ("Argument "+o+" has an unacceptable type for constructor");
    }
  }

  protected void addType(RelationType type) {
    if (!acceptableTypes.isPresent())
      acceptableTypes = Optional.of(new ArrayList<>());
    acceptableTypes.get().add(type);
  }

  public boolean isEmpty() {
    return ( (!acceptableTypes.isPresent())
             && (map == null || map.isEmpty() ));
  }


  @Override
  public abstract List<AssignmentResult>
  computeAssignment (Graph graph, AssignmentResult partialAR, Interruptor i )
      throws InterruptedException;


  protected List<AssignmentResult> before (Graph graph, AssignmentResult partialAR, Interruptor i)
  throws InterruptedException
  {  return previous.computeAssignment(graph, partialAR,i);  }

  protected List<AssignmentResult> after (Graph graph, AssignmentResult partialAR, Interruptor i)
  throws InterruptedException
  {  return next.computeAssignment(graph, partialAR,i);  }

  public List<AssignmentResult>
  matchingRelations
      ( Graph graph, AssignmentResult partialAR )
  {
//     System.out.println(">"+partialAR);
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();

    List<Relation> relations = new LinkedList<Relation>();
    Node node = partialAR.path.dst();
    for(Relation r: node.loop)
        if ( !partialAR.relations.contains(r) )
          relations.add(r);
    if ( direction == Direction.BOTH || direction == Direction.FORWARD )
      for (Relation r : node.out)
        if ( !partialAR.relations.contains(r) )
          relations.add(r);
    if ( direction == Direction.BOTH || direction == Direction.BACKWARD )
      for (Relation r : node.in)
        if ( !partialAR.relations.contains(r) )
          relations.add(r);

    for(Iterator<Relation> it = relations.iterator(); it.hasNext(); ) {
      Relation r = it.next();
      AssignmentResult newAR = partialAR.addRelation(r);
      if (this.acceptsType(r.type())) {
        newAR = null;
      }
      else if (map != null)
        for (KeyExpPair kepair : map) {
          Expression e1 = kepair.exp;
          Expression e2 = new UnaryMapExpression(new Value(r), kepair.key);
          try {
            if (! Expressions.semEquals(e1, e2, graph, partialAR.record,
                  Policy.PROPAGATE) )
            {
              newAR = null;
              break;
            }
          } catch (UndefinedNameException e) {
            newAR = newAR.addPostCondition(new FunctionalExpression(Functions.EQ, e1, e2));
          }
        }
      if (newAR != null)
        result.add(newAR);
    }

//     System.out.println(result);
    return result;
  }

  @Override
  public int length() {
    return (1+next.length());
  }


  public abstract String iterationString();
  public boolean isSimple() {
    return iterationString().equals("");
  }

  @Override
  public String toString() {
    return previous.toString()+toThisString()+next.toString();
  }

  public String toThisString() {
    String left = (direction == Direction.BACKWARD) ? "<-[" : "-[";
    String right = (direction == Direction.FORWARD) ? "]->" : "]-";
    List<Object> parts = new ArrayList<>(4);
    //Name
    if (name != null)
      parts.add(name);
    //Label
    acceptableTypes.ifPresent( x -> parts.add(StringOf.iterable(x,":","","|")));
    //Iteration
    String it = iterationString();
    if (it != "")
      parts.add(it);
    //Map
    if (map != null)
      parts.add(map);
    //Concatenate
    return StringOf.iterable(parts,left,right," ");
  }

  public Optional<List<RelationType>> types()   { return acceptableTypes; }
  public UnnamedPathPattern getNext() {  return next;  }
  public NodePattern getPrevious() {  return previous;  }
  public boolean acceptsType(RelationType type) {
    return acceptableTypes.map( x -> !x.contains(type)).orElse(false);
  }
  public Name getName() { return name; }
  public Name name() { return name; }
  public ExplicitMapExpression getMap() {  return map;  }


  public void setNext(UnnamedPathPattern next) {
    this.next = next;
  }

  public void setPrevious(NodePattern previous) {
    this.previous = previous;
  }

  @Override
  public void addTopLevelVariables(Set<Name> temp_set) {
    previous.addTopLevelVariables(temp_set);
    if (name != null)
      temp_set.add(name);
    next.addTopLevelVariables(temp_set);
  }

  public void setDirection(Direction direction) {
    this.direction = direction;
  }

  @Override
  public List<UnnamedPathPattern> serialize() {
    List<UnnamedPathPattern> res = next.serialize();
    res.add(0,this);
    res.add(0,previous);
    return res;
  }
}
