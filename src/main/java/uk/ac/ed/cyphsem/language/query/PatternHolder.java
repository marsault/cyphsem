/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;

import uk.ac.ed.cyphsem.language.pattern.PathPattern;
import uk.ac.ed.cyphsem.language.pattern.PathPattern.VarStatus;

import uk.ac.ed.cyphsem.table.*;

import uk.ac.ed.cyphsem.utils.*;

import java.lang.Iterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Interface providing helper method for clauses that hold patterns.
**/
public interface PatternHolder {

  /** Returns the patterns held by this {@link PatternHolder} */
  public Iterable<? extends PathPattern> patterns();

  /** Is this Pattern holder a writing clause.
    * Default implementation returns {@code (this instanceof WritingClause)}. */
  default public boolean isWritingClause() { return (this instanceof WritingClause); }

  /**
    * Asserts that held patterns are suitable, in the context of an input {@link Table} the domain of which is given as parameter.
    * @param boundedVariables domain of the input {@link Table}
    * @throws AssertionFailedException if held patterns are not suitable.
  */
  default public void assertPatternTupleIsSuitable(Iterable<? extends Name> boundedVariables)
  throws AssertionFailedException
  {
    Map<Name,VarStatus> map= new HashMap<>();
    for(Name n : boundedVariables)
      map.put(n,VarStatus.BOUND);
    try {
      for (PathPattern p : patterns())
        p.assertSuitable(this.isWritingClause(),map);
    } catch (AssertionFailedException e) {
      throw new AssertionFailedException ("In clause:\n"+toString()+"\n.  "+e.getMessage(),e);
    }
  }

  /** Asserts that held patterns is suitable.
    * Calls {@link #assertPatternTupleIsSuitable} with an empy list.
    * @throws AssertionFailedException if held patterns are not suitable.
  **/
  default public void assertPatternTupleIsSuitable()
  {  assertPatternTupleIsSuitable(Collections.emptyList());  }


  /** Helper function to print a PatternHolder.
    * @param name Printable name of this (eg. "MATCH", "MERGE" )
    * @return The formatted string representation of this pattern holder. One pattern by line; each line except first is prepended by <var>(i+1)</var> space if name is of length <var>i</var>.
  **/
  default public String formatToString(String name) {
    char[] sep = new char[name.length()+3];
    sep[0] = ',';
    sep[1] = '\n';
    for (int i = -1; i<name.length(); i++)
      sep[i+3] = ' ';
    return name+StringOf.iterable(patterns()," ","",new String(sep));
  }
}
