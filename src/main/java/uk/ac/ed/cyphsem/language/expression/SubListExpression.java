/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.table.Record;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/** Implementation of expressions extracting an element or a sublist.
  * That is, expressions of one
  * of the following forms :
  * <ol>
  * <li><var>e1</var>{@code [}<var>e2</var>{@code ]}</li>
  * <li><var>e1</var>{@code [}<var>e2</var>{@code ..]}</li>
  * <li><var>e1</var>{@code [..}<var>e2</var>{@code ]}</li>
  * <li><var>e1</var>{@code [}<var>e2</var>{@code ..}<var>e3</var>{@code ]}</li>
  * </ol>
  * where <var>e1</var>,<var>e2</var> and <var>e3</var> are expressions.
  *
***/
public class SubListExpression implements Expression {

  /** First sub-expression (<var>e1</var> in the general description) */
  private Expression left;

  /** Second sub-expression (<var>e2</var> in the general description) */
  private Expression middle;

  /** Third sub-expression, if necessary (<var>e3</var> in the general description).
    * Fields {@link #right} and {@link #span} are mutually exclusive.
  ***/
  private Optional<Expression> right = Optional.empty();

  /** Case of sublist this sublist represents.
    * Fields {@link #right} and {@link #span} are mutually exclusive.
  ***/
  private Optional<Span> span = Optional.empty();

  /** Different cases when SubListExpression has exactly two subExpressions.
    * {@link Span#UNIQUE} is represent form 2;
    * {@link Span#FROM} is case 2;
    * {@link Span#UNTIL} is case 3;
  ***/
  public enum Span {
    UNIQUE, FROM, UNTIL;
  }


  /** Constructs an expression of the form 4 (see general description).*/
  public
  SubListExpression
    ( Expression left, Expression middle, Expression right )
  {
    this.left = left;
    this.middle = middle;
    this.right = Optional.of(right);
  }

  // if d = FROM    ->  left[middle..]
  // id d = UNTIL   ->  left[..middle]
  // if d = UNIQUE  ->  left[middle]
  /** Constructs an expression of the form 1,2 or 3 (see general description). */
  public
  SubListExpression
    ( Expression left, Span d, Expression middle)
  {
    this.left = left;
    this.middle = middle;
    this.span = Optional.of(d);
  }



  /** Normalises an integer according to a maximum size, in case it does not
    * corresponds to a valid index of the list.
    * @param i the element of the list
    * @param size the size of the list
    * @return <var>i</var> if <var>0 &leq; i &leq; size</var>,
               or <var>i+size</var> if <var>(-size) &leq; i &lt; 0</var>,
               or <var>0</var> if  <var>i &lt; (-size)</var>,
               or <var>size</var> if  <var>i &gt; size</var>,
  ***/
  private static BigInteger renorm(BigInteger i, BigInteger size) {
    BigInteger res = i;
    if (res.signum() == -1) // res < 0
      res = res.add(size);
    if (res.signum() == -1) // res < 0
      return null;
    if (res.compareTo(size) > 0) { // res > size
      return null;
    }
    return res;
  }

  /** {@inheritDoc}
    * @return {@inheritDoc} If sublist expression is of form 2, 3 or 4 (see
    * general description), returned {@link Value} is of
    * {@link Value.Kind Kind} {@link Value.Kind#LIST LIST} or
    * {@link Value.Kind#NULL NULL}.
  ***/
  @Override
  public Value evaluate(Graph graph, Record record) {
    Optional<List<Value>> left_opt_value
      = Expressions.evaluateToListOrNull(left, graph, record);
    Optional<BigInteger> middle_opt_value
      = Expressions.evaluateToIntegerOrNull(middle, graph, record);
    Optional<Optional<BigInteger>> right_value
      = right.map(x->Expressions.evaluateToIntegerOrNull(x, graph, record));

    if ( !left_opt_value.isPresent() || !middle_opt_value.isPresent()
         || !right_value.map(x -> x.isPresent()).orElse(true))
      return new Value();

    List<Value> left_value = left_opt_value.get();

    BigInteger end = BigInteger.valueOf(left_value.size());
    BigInteger mid = renorm(middle_opt_value.get(), end);
    if (mid == null)
      return new Value();
    if (right_value.isPresent()) {
      end = renorm(right_value.get().get(), end);
      if (end == null)
        return new Value();
      return (new Value(left_value.subList(mid.intValue(), end.intValue())));
    }

    switch (span.get())  {
      case UNIQUE: {
        if (mid.compareTo(BigInteger.valueOf(left_value.size()))==0)
          return new Value();
        return left_value.get(mid.intValue());
      }
      case UNTIL:
        return new Value(left_value.subList(0,mid.intValue()));
      case FROM:
        return new Value(left_value.subList(mid.intValue(), end.intValue()));
    }

    throw new UnreachableStatementError();
  }

  @Override
  public String toString() {
    String res = left.toString() + "[" ;
    if (right.isPresent() || span.get() == Span.FROM || span.get() == Span.UNIQUE)
      res += middle.toString();
    if (right.isPresent() || span.get() == Span.FROM || span.get() == Span.UNTIL)
      res += "..";
    if (right.isPresent())
      res += right.get().toString();
    if (span.isPresent() && span.get() == Span.UNTIL)
      res += middle.toString();
    res += "]";
    return (res);
  }


}
