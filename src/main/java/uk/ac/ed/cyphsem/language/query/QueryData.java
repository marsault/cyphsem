/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.Table;

import uk.ac.ed.cyphsem.utils.*;

/** QueryData is a pair (Graph,Table) and is the main type for semantics
  * function (see {@link QueryDataFunction});
  */
public class QueryData extends Pair<Graph,Table> {

  /** Constructs a QueryData holding given graph and table.*/
  public QueryData(Graph g, Table t) { super(g,t); }

  /** Constructs a QueryData holding given graph and a new singleton table built from given record
    * <br> Calls {@link #QueryData QueryData}{@code (g,new Table(r))}.
  ***/
  public QueryData(Graph g, Record r) { this(g, new Table(r)); }

  /** Constructs a QueryData holding given graph and a new empty table.
    * <br> Calls {@link #QueryData QueryData}{@code (g,new Table())}.
  ***/
  public QueryData (Graph g) { this(g, new Table()); };

  /** Returns held {@link Graph}.
    * @beware Graph is not copied.
  **/
  public Graph graph() { return first; }

  /** Returns held {@link Table}.
    * @beware Table is not copied.
  **/
  public Table table() { return second; }

}
