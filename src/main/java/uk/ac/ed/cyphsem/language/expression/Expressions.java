/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;


import uk.ac.ed.cyphsem.exception.DynamicCastException;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;
import uk.ac.ed.cyphsem.exception.UndefinedNameException;

import uk.ac.ed.cyphsem.datamodel.value.Values;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;

import uk.ac.ed.cyphsem.Utils.Policy.*;
import uk.ac.ed.cyphsem.Utils.*;

import uk.ac.ed.cyphsem.utils.Pair;

import uk.ac.ed.cyphsem.datamodel.graph.*;

import uk.ac.ed.cyphsem.table.Record;

import java.util.Map;
import java.util.Optional;
import java.util.List;

import java.math.BigInteger;

/**
 * Container for utility functions about {@link Expression}s.
 */
public interface Expressions {

  /**
   * Evaluates <tt>e</tt> statically.
   * @param e expression to be evaluated; expected not to have any reference to a graph or a
   * @return a static value, that is, a value that has no reference to a graph.
   */
  public static
  Value staticEvaluation(Expression e)
  {  return e.evaluate(new Graph(), new Record());  }



  /**
   * Tests whether two expressions evaluates to values that are semantically equal.
   * Extra arguments specify how to handle expressions
   * @param left_expr
   * @param right_expr
   * @param graph
   * @param record
   * @param evaluationFailurePolicy1 handler in case a {@link UnspecifiedBehaviourException} is thrown during the evaluation of <tt>left_expr</tt> or <tt>right_expr</tt>.
   * @param evaluationFailurePolicy2 same as previous, but for {@link UndefinedNameException}.
   * @param equalityFailurePolicy1 handler in case a {@link UnspecifiedBehaviourException} is thrown during the evaluation of the equality of <tt>left_val</tt> and <tt>right_val</tt>, where <tt>left_val</tt> and <tt>right_val</tt> are the evaluationrespectively
   * @param equalityFailurePolicy2 same as previous, but for {@link UndefinedNameException}.
   * @return <tt>true</tt> if <tt>left_expr</tt> and <tt>right_expr</tt> are semantically equal.
   */
  public static
  boolean
  semEquals
    ( Expression left_expr, Expression right_expr, Graph graph, Record record,
      ExceptionPolicy<UnspecifiedBehaviourException> evaluationFailurePolicy1,
      ExceptionPolicy<UndefinedNameException> evaluationFailurePolicy2,
      ExceptionPolicy<UnspecifiedBehaviourException> equalityFailurePolicy1,
      ExceptionPolicy<UndefinedNameException> equalityFailurePolicy2
    )
  {
    Value l_evaluated, r_evaluated;
    try {
      l_evaluated = left_expr.evaluate(graph, record);
      r_evaluated = right_expr.evaluate(graph, record);
    } catch (UnspecifiedBehaviourException e) {
      return evaluationFailurePolicy1.handle(e);
    } catch (UndefinedNameException e) {
      return evaluationFailurePolicy2.handle(e);
    }

    return ( Values.semEquals(l_evaluated, r_evaluated,
                              equalityFailurePolicy1,
                              equalityFailurePolicy2 ) );
  }

  /**
   * Tests whether two expressions evaluates to values that are semantically equal.
   * Calls fully specified function with the same name; all {@link ExceptionPolicy} are to propagate exception.
   */
  public static
  boolean
  semEquals
    ( Expression l, Expression r, Graph graph, Record record )
  {
    return semEquals(l,r,graph,record,
      new ExceptionPolicy<UnspecifiedBehaviourException>(),
      new ExceptionPolicy<UndefinedNameException>());
  }


  /**
   * Tests whether two expressions evaluates to values that are semantically equal.
   * Calls fully specified function with the same name; {@link ExceptionPolicy}s given are duplicated, hence used both if exceptions are thrown during evaluations or equality.
   */
  public static
  boolean
  semEquals
    ( Expression l, Expression r, Graph graph, Record record,
      ExceptionPolicy<UnspecifiedBehaviourException> policy1,
      ExceptionPolicy<UndefinedNameException> policy2
    )
  {
    return semEquals(l, r, graph,record, policy1, policy2, policy1, policy2);
  }

  /**
   * Tests whether two expressions evaluates to values that are semantically equal.
   * Calls the function with the same name building arguments of type {@link ExceptionPolicy} using the given {@link Policy} position-wise.
   */
  public static
  boolean
  semEquals
    ( Expression l, Expression r, Graph graph, Record record,
      Policy p1,
      Policy p2,
      Policy p3,
      Policy p4
    )
  {
    return semEquals(l, r, graph,record,
      new ExceptionPolicy<UnspecifiedBehaviourException>(p1),
      new ExceptionPolicy<UndefinedNameException>(p2),
      new ExceptionPolicy<UnspecifiedBehaviourException>(p3),
      new ExceptionPolicy<UndefinedNameException>(p4));
  }

  /**
   * Tests whether two expressions evaluates to values that are semantically equal.
   * Calls the function with the same name building arguments of type {@link ExceptionPolicy} using the given {@link Policy} position-wise.
   */
  public static
  boolean
  semEquals
    ( Expression l, Expression r, Graph graph, Record record,
      Policy p1,
      Policy p2
    )
  {
    return semEquals(l, r, graph,record,
      new ExceptionPolicy<UnspecifiedBehaviourException>(p1),
      new ExceptionPolicy<UndefinedNameException>(p2));
  }

  /**
   * Tests whether two expressions evaluates to values that are semantically equal.
   * Uses the same policy for all cases.
   */
  public static
  boolean
  semEquals
    ( Expression l, Expression r, Graph graph, Record record, Policy p )
  {
    return semEquals(l, r, graph,record, p, p);
  }

  /**
   * Evaluates an {@link Expression} against an expected {@link Kind} and an expected return Type.  This function is very fragile and should not be used directly.
   * @param <T> Type one expects as
   * @param e {@link Expression} to evaluate.
   * @param g {@link Graph} used for evaluation.
   * @param r {@link Record} used for evaluation
   * @param acceptable_kinds
   * @return a {@link Pair} containing the value and the Kind.
   * @throws DynamicCastException if
   * @throws ClassCastException if {@link Value} is of the correct {@link Kind} but could not be casted to <tt>T</tt>.
   * @see Expression#evaluate
   * @see Value#castAs
   */
  static <T> Pair<Kind,T> evaluateTo(Expression e, Graph g, Record r,  Kind... acceptable_kinds)
  {
    Value v = e.evaluate(g,r);

    try  {
      return v.<T>castAs(acceptable_kinds);
    } catch (DynamicCastException ex) {
      throw new DynamicCastException (e,v, acceptable_kinds);
    }
  }

  /**
   * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#NODEID NODEID} or {@link Kind#RELID RELID}; raises a {@link DynamicCastException} otherwise.
   * @param e {@link Expression} to evaluate
   * @param g {@link Graph} used for evaluation
   * @param r {@link Record} used for evaluation
   * @return The {@link Id} <tt>e</tt> evaluates to
   * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
   * @throws UndefinedNameException
   * @throws UnspecifiedBehaviourException
   * @see Expression#evaluate
   */
  public static Id evaluateToId(Expression e, Graph g, Record r) {
    return Expressions.<Id>evaluateTo(
              e, g, r, Kind.NODEID, Kind.RELID
            ).second;
  }

  /**
   * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#NODEID NODEID}; raises a {@link DynamicCastException} otherwise.
   * @param e {@link Expression} to evaluate
   * @param g {@link Graph} used for evaluation
   * @param r {@link Record} used for evaluation
   * @return The {@link Node} <tt>e</tt> evaluates to
   * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#NODEID NODEID}
   * @throws UndefinedNameException
   * @throws UnspecifiedBehaviourException
   * @see Expression#evaluate
   */
  public static Node evaluateToNode(Expression e, Graph g, Record r) {
    return Expressions.<Node>evaluateTo(e, g, r, Kind.NODEID).second;
  }

  /**
   * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#RELID RELID}; raises a {@link DynamicCastException} otherwise.
   * @param e {@link Expression} to evaluate
   * @param g {@link Graph} used for evaluation
   * @param r {@link Record} used for evaluation
   * @return The {@link Relation} <tt>e</tt> evaluates to
   * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#RELID RELID}
   * @throws UndefinedNameException
   * @throws UnspecifiedBehaviourException
   * @see Expression#evaluate
   */
  public static Relation evaluateToRelation(Expression e, Graph g, Record r) {
    return Expressions.<Relation>evaluateTo(e, g, r, Kind.RELID).second;
  }

  /**
   * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#MAP MAP}; raises a {@link DynamicCastException} otherwise.
   * @param e {@link Expression} to evaluate
   * @param g {@link Graph} used for evaluation
   * @param r {@link Record} used for evaluation
   * @return The map <tt>e</tt> evaluates to
   * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
   * @throws UndefinedNameException
   * @throws UnspecifiedBehaviourException
   * @see Expression#evaluate
   */
  public static Map<PropertyKey, Value> evaluateToMap(Expression e, Graph g, Record r) {
    return Expressions.<Map<PropertyKey, Value>>evaluateTo(e, g, r, Kind.MAP).second;
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#MAP MAP} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate
    * @param g {@link Graph} used for evaluation
    * @param r {@link Record} used for evaluation
    * @return The {@link Map} <tt>e</tt> evaluates to, or Optional.empty() if it is e evaluates to a {@link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<Map<PropertyKey,Value>> evaluateToMapOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<Map<PropertyKey,Value>>evaluateTo(
              e, g, r, Kind.MAP, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#NODEID NODEID} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate
    * @param g {@link Graph} used for evaluation
    * @param r {@link Record} used for evaluation
    * @return The {@link Node} <tt>e</tt> evaluates to, or Optional.empty() if it is e evaluates to a @link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<Node> evaluateToNodeOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<Node>evaluateTo(
              e, g, r, Kind.NODEID, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#RELID RELID} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate
    * @param g {@link Graph} used for evaluation
    * @param r {@link Record} used for evaluation
    * @return The {@link Node} <tt>e</tt> evaluates to, or Optional.empty() if it is e evaluates to a @link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<Relation> evaluateToRelationOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<Relation>evaluateTo(
              e, g, r, Kind.RELID, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#LIST LIST} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate.
    * @param g {@link Graph} used for evaluation.
    * @param r {@link Record} used for evaluation.
    * @return The {@link List<Value>} <tt>e</tt> evaluates to, or Optional.empty() if e evaluates to a {@link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<List<Value>> evaluateToListOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<List<Value>>evaluateTo(
              e, g, r, Kind.LIST, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#PATH PATH} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate.
    * @param g {@link Graph} used for evaluation.
    * @param r {@link Record} used for evaluation.
    * @return The {@link Path} <tt>e</tt> evaluates to, or Optional.empty() if e evaluates to a {@link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<Path> evaluateToPathOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<Path>evaluateTo(
              e, g, r, Kind.PATH, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#STRING STRING} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate.
    * @param g {@link Graph} used for evaluation.
    * @param r {@link Record} used for evaluation.
    * @return The {@link String} <tt>e</tt> evaluates to, or Optional.empty() if e evaluates to a {@link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<String> evaluateToStringOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<String>evaluateTo(
              e, g, r, Kind.STRING, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#STRING STRING} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate.
    * @param g {@link Graph} used for evaluation.
    * @param r {@link Record} used for evaluation.
    * @return The {@link Boolean} <tt>e</tt> evaluates to, or Optional.empty() if e evaluates to a {@link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<Boolean> evaluateToBooleanOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<Boolean>evaluateTo(
              e, g, r, Kind.BOOL, Kind.NULL).second);
  }

  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#INT INT} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate
    * @param g {@link Graph} used for evaluation
    * @param r {@link Record} used for evaluation
    * @return The {@link BigInteger} <tt>e</tt> evaluates to, or Optional.empty() if it is e evaluates to a @link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<BigInteger> evaluateToIntegerOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<BigInteger>evaluateTo(
              e, g, r, Kind.INT, Kind.NULL).second);
  }



  /**
    * {@link Expression#evaluate Evaluates} an {@link Expression}, expecting the computed {@link Value} to be of {@link Kind Kind} {@link Kind#NODEID NODEID} or {@link Kind#RELID RELID} or {@link Kind#NULL NULL}; raises a {@link DynamicCastException} otherwise.
    * @param e {@link Expression} to evaluate
    * @param g {@link Graph} used for evaluation
    * @param r {@link Record} used for evaluation
    * @return The {@link Id} <tt>e</tt> evaluates to, or Optional.empty() if it is e evaluates to a @link Kind#NULL NULL} {@link Value}.
    * @throws DynamicCastException if the evaluated value is not of {@link Kind Kind} {@link Kind#MAP MAP}
    * @throws UndefinedNameException
    * @throws UnspecifiedBehaviourException
    * @see Expression#evaluate
  ***/
  public static Optional<Id> evaluateToIdOrNull(Expression e, Graph g, Record r) {
    return Optional.ofNullable(
            Expressions.<Id>evaluateTo(
              e, g, r, Kind.NODEID, Kind.RELID, Kind.NULL).second);
  }
}
