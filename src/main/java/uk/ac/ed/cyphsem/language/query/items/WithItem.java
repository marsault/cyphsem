/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query.items;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.AggregatingFunction;

import uk.ac.ed.cyphsem.table.Name;

import java.util.Objects;
import java.util.Optional;

/** Implements item present in a <tt>WITH</tt> and <tt>RETURN</tt> clauses.
  * A {@link WithItem} holds an {@link Expression}, optionally a {@link Name}, optionally an {@link AggregatingFunction}, and a boolean that indicates if the keyword <tt>DISTINCT</tt> is present.
  * <br>
  * For instance:
  * <ul><li>
  * if item is <tt>a[2..3]</tt>, field {@link #exp} contains <tt>a[2..3]</tt>; {@link Name} and{@link #name} are {@link Optional#empty empty} ; and
  {@link #distinct} is set to {@code true}
  * </li><li>
  * if item is <tt>count(DISTINCT a+1) AS c</tt>, then field {@link #exp} contains <tt>a+1</tt>, field {@link #distinct} is set to {@code true}, field {@link #name} is present and set to <tt>c</tt>, and {@link #aggfunction} is present and set to {@link AggregatingFunction#COUNT}.
  *</li></ul>
**/
public class WithItem {

  protected Optional<AggregatingFunction> aggfunction = Optional.empty();

  /** Name of this <tt>WITH</tt>/<tt>RETURN</tt> item. */
  protected Optional<Name> name = Optional.empty();

  /** Whether or not this <tt>WITH</tt>/<tt>RETURN</tt> item has the keyword <tt>DISTINCT</tt>.
    * Argument is only meaningul if {@link #aggfunction} is {@link Optional#isPresent} present.
  **/
  public final Boolean distinct;

  /** Expression of this <tt>WITH</tt>/<tt>RETURN</tt> item. **/
  public Expression exp;

  /** Default name, build at construction from {@link #exp}.
    * Current implementation is: {@code defaultName = '`' exp.toString() + '`' }
    * </pre>
  **/
  protected Name defaultName;


  /** Constructs a {@link WithItem} that wraps given arguments.
  **/
  public WithItem
    ( Optional<AggregatingFunction> af, Boolean distinct, Optional<Name> name,
      Expression exp )
  {
    this.name = name;
    this.aggfunction = af;
    this.distinct = distinct;
    this.exp = exp;
    if ((exp instanceof Name) && !isAggExp())
      defaultName = (Name) exp;
    else
      this.defaultName = new Name("`"+expToString()+"`");
  }

  /** Constructs a {@link WithItem} that is not named nor aggregating.
    *<br>
    * Same as  {@link WithItem#WithItem WithItem}{@code (Optional.empty(), false, Optional.empty(), exp) }.
  **/
  public WithItem(Expression exp) {
    this(Optional.empty(), false, Optional.empty(), exp);
  }

  /** Constructs a named {@link WithItem} that is not aggregating.
    *<br>
    * Same as  {@link WithItem#WithItem WithItem}{@code (Optional.empty(), false, Optional.of(name), exp) }.
  **/
  public WithItem(Name name, Expression exp) {
    this(Optional.empty(), false, Optional.of(name), exp);
  }


  /** Constructs an aggregating {@link WithItem} that has no {@link Name}.
    *<br>
    * Same as  {@link WithItem#WithItem WithItem}{@code (Optional.empty(), false, Optional.of(name), exp) }.
  **/
  public WithItem(AggregatingFunction af, Boolean distinct, Expression exp) {
    this(Optional.of(af), distinct, Optional.empty(), exp);
  }

  /** Constructs an aggregating and named {@link WithItem}.
    *<br>
    * Same as  {@link WithItem#WithItem WithItem}{@code (Optional.empty(), false, Optional.of(name), exp) }.
  **/
  public WithItem(AggregatingFunction af, Boolean distinct, Name name, Expression exp) {
    this(Optional.of(af), distinct, Optional.of(name), exp);
  }


  /** Returns {@link #name held Name} if {@link Optional#isPresent present}, or {@link #defaultName default Name} otherwise.
  */
  public Name name() {
    return name.orElse(defaultName);
  }

  /** Indicate whether this <tt>WITH</tt>/<tt>RETURN</tt> item is aggregating. */
  public boolean isAggExp() { return aggfunction.isPresent(); }

  @Override
  public int hashCode() {
    return Objects.hash(aggfunction.orElse(null), name.orElse(null), distinct,
                        exp);
  }

  @Override
  public boolean equals (Object o) {
    if (o == null || !getClass().equals(o.getClass()))
      return false;
    WithItem other = (WithItem) o;
    return ( name().equals(other.name())
             && exp.equals(other.exp)
             && distinct.equals(other.distinct)
             && aggfunction.equals(other.aggfunction) );
  }

  @Override public
  String toString ()
  {
    String res = expToString();
    res += " AS "+name();
    return res;
  }

  private String expToString() {
    if (aggfunction.isPresent()
        && aggfunction.get() == AggregatingFunction.COUNT_STAR)
      return "COUNT(*)";
    String str = "";
    str += aggfunction.map( af -> af.toString()+"(" ).orElse("");
    str += distinct?"DISTINCT ":"";
    str += exp.toString();
    str += aggfunction.isPresent()?")":"";
    return str;
  }

  /** Returns {@link #aggfunction held AggregatingFunction} without checking its presence.
    * @throws NoSuchElementException if {@link #aggfunction} is {@link Optional#empty empty}.
  */
  public AggregatingFunction aggregatingFunction() {
    return aggfunction.get();
  }

  /** Returns {@link #exp held Expression}. **/
  public Expression exp() {
    return exp;
  }
}
