/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.Utils.Policy;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.language.pattern.PathPattern;
import uk.ac.ed.cyphsem.language.pattern.AssignmentResult;

import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Optional;

/** Implements the <tt>OPTIONAL MATCH</tt> clause, possibly holding a <tt>WHERE</tt> subclause.
*/
public class OptionalMatchClause extends MatchClause {

  /** Optional {@link WhereClause} held by this optional match clause. */
  Optional<WhereClause> where =  Optional.empty();

  /** Constructs an {@link OptionalMatchClause}
    * Parameter {@code where} may be {@code null}, in which case field {@link #where} is set to empty.
  **/
  public OptionalMatchClause(WhereClause where, List<PathPattern> args) {
    super(args);
    this.where = Optional.ofNullable(where);
  }

  /** Constructs an <tt>WHERE</tt>-less {@link OptionalMatchClause}.
    * Same as {@link #OptionalMatchClause OptionalMatchClause}{@code (null, Arrays.asList(args))}.
  */
  public OptionalMatchClause(PathPattern... args) {
    this(null, Arrays.asList(args));
  }

  /** Constructs an <tt>WHERE</tt>-less {@link OptionalMatchClause}. */
  public OptionalMatchClause(List<PathPattern> args) {
    this(null, args);
  }



  @Override
  public String toString() {
    String res = "";
    String sep = "";
    res += "OPTIONAL MATCH ";
    for (PathPattern p : patterns) {
      res += sep + p;
      sep = ",";
    }
    res += where.map(x -> " "+where.toString()).orElse("");
    return res;
  }

  @Override
  public Table apply (Graph graph, Record record, Interruptor interruptor)
  throws InterruptedException
  {
    interruptor.check();
    Set<Name> top_level_variables = new HashSet<Name>();
    for (PathPattern pattern : patterns)
      pattern.addTopLevelVariables(top_level_variables);

    Table match_table = super.apply(graph, record, interruptor);
    if (where.isPresent())
      match_table = where.get().apply(graph, match_table, interruptor);

    if (match_table.isEmpty()) {
      Record result = new Record (record);
      result.bindAllUnboundToNull(top_level_variables);
      return new Table(result);
    }
      return match_table;
  }

}
