/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;


import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.language.query.items.*;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/** Implements Cypher clauses (and subclauses) of the form <tt>WITH <var>i1</var>,<var>i2</var>,...,<var>in</var></tt> or <tt>WITH *,<var>i1</var>,<var>i2</var>,...,<var>in</var></tt>, where <var>i1</var>,<var>i2</var>,...,<var>in</var> are {@link WithItem}s.
  * <br>
  * Note that {@link WithClause} is not declared to implement {@link StandardReadingClause} although
it actually implements the required method ({@link #apply(uk.ac.ed.cyphsem.datamodel.graph.Graph, uk.ac.ed.cyphsem.table.Record, uk.ac.ed.cyphsem.Interruptor)}).
  * Indeed a {@link WithClause} does not satisfies the implicit contract of a {@link StandardReadingClause} when it is {@link #aggregating aggregating}.
***/
public class WithClause implements ReadingClause {

  /** Indicates whether the token <tt>*</tt> is present.*/
  protected boolean hasStar;

  /** List of explicit items that are aggregating. */
  protected List<WithItem> aggregatingItems = new ArrayList<WithItem>();

  /** List of explicit items that are not aggregating. */
  protected List<WithItem> simpleItems = new ArrayList<WithItem>();

  /** Indicates whether this {@link WithClause} is aggregating. */
  protected boolean aggregating;


  /** Constructs a {@link WithClause} from given parameters.
    * Elements of {@code extras} are held, in field {@link #aggregatingItems} if aggregating and {@link #simpleItems} otherwise.
    * The parameter {@code extras} itself is not held.
    * @param hasStar indicates whether the token <tt>*</tt> is present
    * @param extras Lists of items
  ***/
  public WithClause(boolean hasStar, List<WithItem> extras) {
    this.hasStar = hasStar;
    for (WithItem ne : extras) //{
      (ne.isAggExp()?aggregatingItems:simpleItems).add(ne);
    aggregating = !aggregatingItems.isEmpty();
  }


  /** Constructs a <tt>WITH *</tt> clause.
    * Shorthand for {@link #WithClause WithClause}{@code (true, Collections.emptyList())}.
  ***/
  public WithClause() { this(true,Collections.emptyList()); }




  @Override
  public String toString() {
    String ret = "WITH ";
    String sep = "";
    if (hasStar) {
      ret += "*";
      sep = ", ";
    }
    for (List<WithItem> extras : Arrays.asList(simpleItems,aggregatingItems))
      for (WithItem pair : extras) {
        ret += sep;
        ret += pair.toString();
        sep =", ";
      }
    return  ret;
  }


  /** Applies the semantics function associated with this
    * {@link StandardReadingClause} to a single {@link Record},
    * or throws a {@link InterruptedException} if computation is interrupted
    * through given {@link Interruptor}.
    * <br>
    * The general contract of this interface is that its implementation should
    * not modify input {@code Graph} or {@link Record}.
    * @param graph Input {@link Graph}
    * @param record Input {@link Record}
    * @param interruptor Interruptor provided by main thread to signal this thread
    * to interrupt the computation.
    * @return Output records.
    * @throws InterruptedException If computation was interrupted, after a call
    * to {@link Interruptor#check}.
  **/
  public
  Table apply (Graph graph, Record record, Interruptor interruptor)
  throws InterruptedException
  {
    interruptor.check();
    Record result = hasStar ? new Record(record) : new Record() ;
    for (WithItem pair : simpleItems) {
      Name name = pair.name();
      if (result.isBound(name))
        throw new UnspecifiedBehaviourException(
            "All names in a WithClause must be distinct."
          );
      try {
        result.bind(name, pair.exp.evaluate(graph,record));
      }
      catch (RecordInconsistencyException e) {
        throw new UnreachableStatementError();
      }
    }
    return new Table(result);
  }

  @Override
  public
  Table apply (Graph graph, Table table, Interruptor interruptor)
  throws InterruptedException
  {
    Table result = new Table();
    if (!(aggregating)) {
      for (Record record : table.recordSet())
        result.addAll( apply(graph, record, interruptor),
                       table.multiplicityOf(record)       );
      return result;
    }
    interruptor.check();

    HashMap<Record, Table> map= new HashMap<Record, Table>();

    for (Pair<Record,Integer> entry : table.entries()) {
      Record in = entry.first;
      Integer multiplicity = entry.second;
      Record out = apply(graph, in, interruptor).anyOne();
      map.putIfAbsent(out, new Table());
      map.get(out).add(in, multiplicity);
    }


    for (Map.Entry<Record,Table> entry : map.entrySet()) {
      Record r = new Record(entry.getKey());
      for (WithItem agg_exp: aggregatingItems) {
        AggregatingFunction agg_fun  = agg_exp.aggregatingFunction(); // unsafe but ok
        List<Record> input = new ArrayList<>(entry.getValue());
        List<Value> operands = Utils.map(input, x -> agg_exp.exp().evaluate(graph, x));
        if (agg_exp.distinct)
          operands = Utils.distinct(operands);
        Name agg_exp_name = agg_exp.name();
        if (r.isBound(agg_exp_name))
          throw new UnspecifiedBehaviourException(
              "All names in a WITH clause must be distinct and "
              + agg_exp_name +"is used multiple times"
            );
        try { r.bind(agg_exp_name, agg_fun.call(operands)); }
        catch (RecordInconsistencyException e)  {
          throw new UnreachableStatementError();
        }
      }
      result.add(r);
    }

    return result;
  }
}
