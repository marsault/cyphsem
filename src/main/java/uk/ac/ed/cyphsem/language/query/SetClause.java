/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.Parameters.*;
import uk.ac.ed.cyphsem.Parameters;
import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;
import uk.ac.ed.cyphsem.exception.NonDeterminismException;


import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.value.Values;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Label;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Id;
import uk.ac.ed.cyphsem.datamodel.graph.RelationType;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;

import uk.ac.ed.cyphsem.language.pattern.PathPattern;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern.Direction;
import uk.ac.ed.cyphsem.language.pattern.NodePattern;
import uk.ac.ed.cyphsem.language.pattern.NamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.UnnamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.SimpleRelationPattern;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.Name;

import uk.ac.ed.cyphsem.utils.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.language.query.items.SetItem.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.function.Function;

/** Represents a <tt>SET</tt> clause.
***/
public class SetClause implements WritingClause {

  /** {@link SetItem}s held by this {@link SetClause}. */
  List<SetItem> items;

  /** Constructs a {@link SetClause} wrapping given item list. ***/
  public SetClause(List<SetItem> items)
  { this. items = items; }

  /** Constructs a {@link SetClause} wrapping given items.
    * <br>
    * Shorthand for {@link #SetClause SetClause}{@code (Arrays.asList(args))}.
  ***/
  public SetClause(SetItem... args)
  { this(Arrays.asList(args)); }


  /** {@inheritDoc}
    * <br><br>
    * Implementation in {@link SetClause} calls {@link #apply(uk.ac.ed.cyphsem.language.query.QueryData, uk.ac.ed.cyphsem.Interruptor, uk.ac.ed.cyphsem.Parameters.SetVersion) apply}{@code (qd, interruptor,Parameters.global.set())}.
  **/
  @Override
  public QueryData apply(QueryData qd, Interruptor interruptor)
  throws InterruptedException
  {
    return apply(qd, interruptor, Parameters.global.set());
  }

  /** Applies semantics function associated to this {@link MergeClause}, interpreted with the given {@link SetVersion version}.
  **/
  public QueryData apply (QueryData qd, Interruptor interruptor,Parameters.SetVersion version)
  throws InterruptedException
  {
    //System.out.println(toString());
    interruptor.check();
    Graph graph = qd.graph();
    Table input_table = qd.table();
    SetChanges c = null;
    switch (version) {
      case LEGACY:
        for (Record record : input_table)
          for (SetItem i : items) {
            interruptor.check();
            c = new SetChanges(true);
            i.addChanges(c, graph, record);
            c.execute(x->x, x->x);
          }
        return qd;
      case PERMISSIVE: c = new SetChanges(true, items, graph, input_table); break;
      // Here we could detect non-determinism and push a warning to interruptor
      case STRICT: c = new SetChanges(false, items, graph, input_table); break;
    }
    interruptor.check();
    c.execute(x->x, x->x);
    return qd;
  }

  @Override
  public String toString()
  {
    return "SET "+StringOf.iterable(items,"","",",");
  }
}
