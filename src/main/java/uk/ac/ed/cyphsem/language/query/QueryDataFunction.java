/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;

// import java.util.function.Function;
// import java.util.function.BiFunction;

/**
  * Base interface for all semantics functions that takes as arguments a {@link Table}
  * and a {@link Graph}, and returns a {@link Table} and a {@link Graph}.
  * This includes {@code Query Queries}, {@code QueryBis}'s and {@code Clause}s.
  * <br>
  * The pair ({@link Table},{@link Graph}) is represented by the
  * class {@code QueryData}.
*/
public interface QueryDataFunction {
// extends  Function<QueryData,QueryData> {

  /** Applying the semantics function associated with this clause/query,
    * or throws a {@link InterruptedException} if computation is interrupted
    * through given {@link Interruptor}.
    * <br>
    * Current implementation always copies input {@link Table} before
    * modification (and returns the copy), while input {@link Graph} may be
    * modified (and will be returned).
    * @param qd Input pair ({@link Table},{@link Graph})
    * @param interruptor Interruptor provided by main thread to signal this thread
    * to interrupt the computation.
    * @return The computed {@link QueryData}
    * @throws InterruptedException If computation was interrupted due to a call
    * to {@link Interruptor#check}.
    * @fordev Given {@link Interruptor} should be {@link Interruptor#check checked} reasonably
    * often during the computation of output {@link QueryData}.
    */
  public QueryData apply(QueryData qd, Interruptor interruptor) throws InterruptedException;


  /** Applying the semantics function associated with this clause/query.
    * Calls {@link #apply} with a new {@link Interruptor}, hence no {@link InterruptedException}
    * may ever be thrown.
    */
  default public QueryData apply(QueryData qd) {
    try {
      return apply(qd, new Interruptor());
    } catch (InterruptedException ie) {
      throw new UnreachableStatementError();
    }
  }

//   /** Applying the semantics function associated with this clause/query.
//     * Calls {@link #apply} with a new {@link Interruptor}, hence no {@link InterruptedException}
//     * may ever be thrown.
//     */
//   default public QueryData apply(Graph g, Table t) {
//     return apply(new QueryData(g,t));
//   }

}
