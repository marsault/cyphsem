/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.language.expression.Expression;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;

/** Implements Cypher clauses of the form <tt>UNWIND <var>e1</var> AS <var>a</var></tt>,
  * where <var>a</var> is a {@link Name} and <var>e1</var> is an {@link Expression} that usually evaluates to a {@link Value} of {@link Kind Kind} {@link Kind#LIST}.
  * <br>
  * Field {@link #expression} stores <var>e1</var>.
  * Field {@link #name} stoes <var>n</var>.
***/
public class UnwindClause implements StandardReadingClause {

  /** {@link Expression} of this {@link  UnwindClause}. */
  protected Expression expression;

  /** {@link Name} of the new column to be added to the input {@link Table}. */
  protected Name name;

  /** Construct an {@link UnwindClause} that wraps given parameters */
  public UnwindClause(Expression expression, Name name)
  { this.expression = expression;  this.name = name;  }

  @Override
  public String toString() { return ("UNWIND " + expression + " AS " + name); }


  /** {@inheritDoc}
    * @throws UnspecifiedBehaviourException if parameter {@code record} has {@link #name} in its domain.
  ***/
  @Override
  public Table apply (Graph graph, Record record, Interruptor interruptor)
  throws InterruptedException
  {
    interruptor.check();
    if (record.domain().contains(name))
      throw new UnspecifiedBehaviourException(
          "Input record (" + record + ") "
          + "already has a column called " + name + "."
        );
    Table result = new Table();
    Value value = expression.evaluate(graph, record);
    List<Value> list;
    if ( value.isOfKind(Value.Kind.LIST) )
      list = value.unsafeCastAsList();
    else
      list = Arrays.asList(value);
    for(Value v : list) {
      Record new_record = new Record(record);
      new_record.bind(name, v);
      result.add(new_record);
    }
    return result;
  }


}
