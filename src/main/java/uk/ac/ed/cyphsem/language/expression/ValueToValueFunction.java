/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;

import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;

import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;

import uk.ac.ed.cyphsem.utils.StringOf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is a simple interface for functions from values to {@link Value}.
 * Even though this interface suggests that the functions take any number of
 * of arguments, it is not the case.
 * This interface is generally for functions of a given arity (usually one or
 * two).
 */
@FunctionalInterface
public interface ValueToValueFunction {

  /**
   * Calls the function on given operands.
   */
  public Value call(List<Value> operands);

  /**
   * Calls the function on given operands.
   */
  default public Value call(Value... vals) {
      return call (Arrays.asList(vals));
  }


  public default String toString(List<Expression> exps) {
    return (toString()+StringOf.iterable(exps, "(",")",","));
  }
//   public static abstract class Helper implements ValueToValueFunction {
//     public abstract Value call(List<Value> operands);

//     public Value call(Value... vals) {
//       return call (Arrays.asList(vals));
//     }
//   }

//   public static class OnlyNullDefinedFunction implements ValueToValueFunction {
//
//     OnlyNullDefinedFunction() { }
//     public Value call(List<Value> operands) {
//       for(Value v : operands)
//         if (v.isOfKind(Value.Kind.NULL))
//           return null;
//       throw new UnspecifiedBehaviourException(toString()+": defined only if one operand is null.");
//     }
//   }

//   public static enum FunctionName {
//     SPECIAL,
//     STARTS_WITH, ENDS_WITH, CONTAINS,
//     OR, AND, XOR, NOT,
//     IS_NULL, IS_NOT_NULL,
//     LE, LEQ, GE, GEQ,
//     EQ, NEQ,
//     IN,
//   }

//   public static final Map<FunctionName,ValueToValueFunction> functions
//     = initialize_map();

//   public static Map<FunctionName,ValueToValueFunction> initialize_map() {

//   Map<FunctionName,ValueToValueFunction> functions = new EnumMap<FunctionName, ValueToValueFunction>(FunctionName.class);

// /* Binary string functions  ==================================================*/
//       List<Kind> string = new ArrayList<Kind>();
//       string.add(Kind.STRING);
//       string.add(Kind.NULL);
//       List<List<Kind>> bin_string = new ArrayList<List<Kind>>();
//       bin_string.add(string);
//       bin_string.add(string);
//
//       //starts with
//       functions.put(FunctionName.STARTS_WITH,
//         new AbstractTypedVTVFunction (FunctionName.STARTS_WITH) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_string,operands);
//             String x = (String) operands.get(0).content;
//             String y = (String) operands.get(1).content;
//             if ((x==null) || (y==null))
//               return ( new Value() );
//             else
//               return ( new Value (x.startsWith(y)) );
//           }
//         });
//       //ends with
//       functions.put(FunctionName.ENDS_WITH,
//         new AbstractTypedVTVFunction (FunctionName.ENDS_WITH) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_string,operands);
//             String x = (String) operands.get(0).content;
//             String y = (String) operands.get(1).content;
//             if ((x==null) || (y==null))
//               return ( new Value() );
//             else
//               return ( new Value (x.endsWith(y)) );
//           }
//         });
//       //contains
//       functions.put(FunctionName.CONTAINS,
//         new AbstractTypedVTVFunction (FunctionName.CONTAINS) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_string,operands);
//             String x = (String) operands.get(0).content;
//             String y = (String) operands.get(1).content;
//             if ((x==null) || (y==null))
//               return ( new Value() );
//             else
//               return ( new Value (x.contains(y)) );
//           }
//         });
//
// /* Boolean functions  ========================================================*/
//       List<Kind> trilean = new ArrayList<Kind>();
//       trilean.add(Kind.BOOL);
//       trilean.add(Kind.NULL);
//       List<List<Kind>> uni_trileans = new ArrayList<List<Kind>>();
//       uni_trileans.add(trilean);
//       List<List<Kind>> bin_trileans = new ArrayList<List<Kind>>(uni_trileans);
//       bin_trileans.add(trilean);
//
//       //or
//       functions.put(FunctionName.OR,
//         new AbstractTypedVTVFunction (FunctionName.OR) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_trileans,operands);
//             Boolean x = (Boolean) operands.get(0).content;
//             Boolean y = (Boolean) operands.get(1).content;
//             if (((x != null) && x.booleanValue()) || ((y != null) && y.booleanValue()) )
//               return (new Value(Boolean.valueOf(true)));
//             if ((x != null) && (y != null) && !(x||y) )
//               return (new Value(Boolean.valueOf(false)));
//             return (new Value());
//           }
//         });
//       //and
//       functions.put(FunctionName.AND,
//         new AbstractTypedVTVFunction (FunctionName.AND) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_trileans,operands);
//             Boolean x = (Boolean) operands.get(0).content;
//             Boolean y = (Boolean) operands.get(1).content;
//             if (((x != null) && !x.booleanValue()) || ((y != null) && !y.booleanValue()) )
//               return (new Value(Boolean.valueOf(false)));
//             if ((x != null) && (y != null) && x && y )
//               return (new Value(Boolean.valueOf(true)));
//             return (new Value());
//           }
//         });
//       //xor
//       functions.put(FunctionName.XOR,
//         new AbstractTypedVTVFunction (FunctionName.XOR) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_trileans,operands);
//             Boolean x = (Boolean) operands.get(0).content;
//             Boolean y = (Boolean) operands.get(1).content;
//             if ((x == null) || (y == null))
//               return (new Value());
//             boolean l = x.booleanValue();
//             boolean r = y.booleanValue();
//             return (new Value(Boolean.valueOf( (l || r) && !(l && r) )));
//           }
//         });
//       //not
//       functions.put(FunctionName.NOT,
//         new AbstractTypedVTVFunction (FunctionName.NOT) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(uni_trileans,operands);
//             Boolean x = (Boolean) operands.get(0).content;
//             if (x == null)
//               return (new Value());
//             boolean l = x.booleanValue();
//             return (new Value(Boolean.valueOf( !l)));
//           }
//         });
//
// /* Comparison to NULL  =======================================================*/
//       //is null
//       functions.put(FunctionName.IS_NULL,
//         new AbstractTypedVTVFunction (FunctionName.IS_NULL) {
//           public Value call(List<Value> operands) {
//             if (operands.size() != 1)
//               throw new UnspecifiedBehaviourException("Function got too many operands.");
//             return (new Value(operands.get(0).isOfKind(Kind.NULL)));
//           }
//         });
//       //is not null
//       functions.put(FunctionName.IS_NOT_NULL,
//         new AbstractTypedVTVFunction (FunctionName.IS_NOT_NULL) {
//           public Value call(List<Value> operands) {
//             if (operands.size() != 1)
//               throw new UnspecifiedBehaviourException("Function got too many operands.");
//             return (new Value(operands.get(0).isOfKind(Kind.NULL)));
//           }
//         });
//
// /* Integer comparison  =======================================================*/
//       List<Kind> int_t = new ArrayList<Kind>();
//       int_t.add(Kind.INT);
//       int_t.add(Kind.NULL);
//       List<List<Kind>> bin_int = new ArrayList<List<Kind>>();
//       bin_int.add(int_t);
//       bin_int.add(int_t);
//
//       //less than
//       functions.put(FunctionName.LE,
//         new AbstractTypedVTVFunction (FunctionName.LE) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_int,operands);
//             Integer x = (Integer) operands.get(0).content;
//             Integer y = (Integer) operands.get(1).content;
//             if ((x == null) || (y == null))
//               return (new Value());
//             return (new Value(x < y));
//           }
//         });
//       //less than or equal to
//       functions.put(FunctionName.LEQ,
//         new AbstractTypedVTVFunction (FunctionName.LEQ) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_int,operands);
//             Integer x = (Integer) operands.get(0).content;
//             Integer y = (Integer) operands.get(1).content;
//             if ((x == null) || (y == null))
//               return (new Value());
//             return (new Value(x <= y));
//           }
//         });
//       //greater than
//       functions.put(FunctionName.GE,
//         new AbstractTypedVTVFunction (FunctionName.GE) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_int,operands);
//             Integer x = (Integer) operands.get(0).content;
//             Integer y = (Integer) operands.get(1).content;
//             if ((x == null) || (y == null))
//               return (new Value());
//             return (new Value(x > y));
//           }
//         });
//       //greater than or equal to
//       functions.put(FunctionName.GEQ,
//         new AbstractTypedVTVFunction (FunctionName.GEQ) {
//           public Value call(List<Value> operands) {
//             assertOperandsKind(bin_int,operands);
//             Integer x = (Integer) operands.get(0).content;
//             Integer y = (Integer) operands.get(1).content;
//             if ((x == null) || (y == null))
//               return (new Value());
//             return (new Value(x >= y));
//           }
//         });
//
// /* Equality/Inequality  ======================================================*/
//       //equal
//       functions.put(FunctionName.EQ,
//         new AbstractTypedVTVFunction (FunctionName.EQ) {
//           public Value call(List<Value> operands) {
//             if (operands.size() != 2)
//               throw new UnspecifiedBehaviourException(toString()+":must have two arguments.");
//
//             Kind left_kind = operands.get(0).kind;
//             Kind right_kind = operands.get(1).kind;
//             Object left = operands.get(0).content;
//             Object right = operands.get(1).content;
//
//             if ((left_kind == Kind.NULL) || (right_kind == Kind.NULL))
//               return new Value();
//
//             if (left_kind != right_kind)
//               return new Value(false);
//
//             switch (left_kind) {
//               case LIST: {
//                 @SuppressWarnings("unchecked")
//                 List<Value> left_as_list = (List<Value>) left;
//                 @SuppressWarnings("unchecked")
//                 List<Value> right_as_list = (List<Value>) right;
//                 int n = left_as_list.size();
//                 if (right_as_list.size() != n)
//                   return (new Value());
//                 Value return_value = new Value(true);
//                 for (int i=0; i<n; i++) {
//                   List<Value> new_ops = new ArrayList<Value>();
//                   new_ops.add(left_as_list.get(i));
//                   new_ops.add(right_as_list.get(i));
//                   Value ret = functions.get(FunctionName.EQ).call(new_ops);
//                   if (ret.kind == Kind.NULL) {
//                     return_value = new Value();
//                   } else if (ret.kind == Kind.BOOL) {
//                     if (! Boolean.valueOf((Boolean) ret.content))
//                       return new Value (false);
//                   } else {
//                     throw new Error ("VTV::EQ::LIST impossible case");
//                   }
//                 }
//                 return (return_value);
//               }
//               case MAP:
//                 @SuppressWarnings("unchecked")
//                 Map<PropertyKey,Value> left_as_map = (Map<PropertyKey,Value>) left;
//                 @SuppressWarnings("unchecked")
//                 Map<PropertyKey,Value> right_as_map = (Map<PropertyKey,Value>) right;
//                 Set<PropertyKey> left_keys = left_as_map.keySet();
//                 Set<PropertyKey> right_keys = right_as_map.keySet();
//                 if (!left_keys.containsAll(right_keys))
//                   return new Value(false);
//                 if (!right_keys.containsAll(left_keys))
//                   return new Value(false);
//                 //Keysets are equal, verifying value
//                 Value return_value = new Value(true);
//                 for(PropertyKey el : right_as_map.keySet()) {
//                   List<Value> new_ops = new ArrayList<Value>();
//                   new_ops.add(left_as_map.get(el));
//                   new_ops.add(right_as_map.get(el));
//                   Value ret = functions.get(FunctionName.EQ).call(new_ops);
//                   if (ret.kind == Kind.NULL) {
//                     return_value = new Value();
//                   } else if (ret.kind == Kind.BOOL) {
//                     if (! Boolean.valueOf((Boolean) ret.content))
//                       return new Value (false);
//                   } else {
//                     throw new Error ("VTV::EQ::MAP impossible case");
//                   }
//                 }
//                 return (return_value);
//               case PATH:
//                 Path left_as_path = (Path) left;
//                 Path right_as_path = (Path) right;
//                 return new Value(left_as_path.semEquals(right_as_path));
//               default:
//                 return new Value(left.equals(right));
//             }
//           }
//         });
//       //not equal
//       functions.put(FunctionName.NEQ,
//         new AbstractTypedVTVFunction (FunctionName.NEQ) {
//           public Value call(List<Value> operands) {
//             Value eq_res = functions.get(FunctionName.EQ).call(operands);
//             if (eq_res.kind == Kind.BOOL)
//               return  new Value(! (Boolean) eq_res.content);
//             else
//               return new Value();
//           }
//         });
// /* IN   ====================================================== */
//       functions.put(FunctionName.IN,
//         new AbstractTypedVTVFunction (FunctionName.IN) {
//           public Value call(List<Value> operands) {
//             if (operands.size() != 2)
//               throw new UnspecifiedBehaviourException(toString()+":must have two arguments.");
//             List<Kind> l = new ArrayList<Kind>(1);
//             l.add(Kind.LIST);
//             assertOperandKind(l,operands.get(1),1);
//
//             @SuppressWarnings("unchecked")
//             List<Value> vals = (List<Value>) operands.get(1).content;
//             Value ret = new Value(false);
//             for (Value val : vals) {
//               List<Value> new_ops = new ArrayList<Value>();
//               new_ops.add(val);
//               new_ops.add(operands.get(0));
//               try {
//                 Value subret = functions.get(FunctionName.EQ).call(new_ops);
//                 if ( subret.isOfKind(Kind.BOOL) && ((Boolean) subret.content) )
//                   return (new Value(true));
//                 if (subret.isOfKind(Kind.NULL))
//                   ret = new Value();
//               } catch (UnspecifiedBehaviourException e) { }
//             }
//             return ret;
//           }
//
//
//         });


//     return functions;
//     }
}
