/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;

/** {@link ReadingClause} is a type for {@link Clause}s that do not change the
  * input graph.
  * In particular, the {@link QueryData}-to-{@link QueryData} method {@link #apply apply}
  * is implemented here using a new abstract method {@link #apply(uk.ac.ed.cyphsem.datamodel.graph.Graph,uk.ac.ed.cyphsem.table.Table,uk.ac.ed.cyphsem.Interruptor) apply} returning a {@link Table}.
*/
public interface ReadingClause extends Clause {

  /** {@inheritDoc}
    * <br><br>
    * Implemented in {@link ReadingClause} calling new abstract
    * method {@link ReadingClause#apply(uk.ac.ed.cyphsem.datamodel.graph.Graph,uk.ac.ed.cyphsem.table.Table,uk.ac.ed.cyphsem.Interruptor) apply}
    * and copying graph from parameter {@code qd}.
    * @param qd {@inheritDoc}
    * @param interruptor {@inheritDoc}
    * @return A {@link QueryData}, the graph of which is the same as the one in parameter{@code qd}.
  */
  default public
  QueryData apply (QueryData qd, Interruptor interruptor) throws InterruptedException {
    interruptor.check();
    return new QueryData (qd.graph(), apply(qd.graph(), qd.table(), interruptor));
  }

  /** Applies the semantics function associated with this {@link ReadingClause},
    * or throws a {@link InterruptedException} if computation is interrupted
    * through given {@link Interruptor}.
    * <br>
    * The general contract of this interface is that its implementation should
    * not modify input {@code Graph} or {@link Table}.
    * @param graph Input {@link Graph}
    * @param table Input {@link Table}
    * @param interruptor Interruptor provided by main thread to signal this thread
    * to interrupt the computation.
    * @throws InterruptedException If computation was interrupted, after a call
    * to {@link Interruptor#check}.
    * @fordev Given {@link Interruptor} should be {@link Interruptor#check checked} reasonably
    * often during the computation of output {@link QueryData}
  **/
  public
  Table apply (Graph graph, Table table, Interruptor interruptor) throws InterruptedException;

}
