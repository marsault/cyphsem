/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.exception.UndefinedNameException;

import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;

import uk.ac.ed.cyphsem.Utils.Policy;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
This class contains the current informations regarding assignment of
variables during the evaluation of a <tt>MATCH</tt> clause (or other clauses
where pattern matching is, such as <tt>MERGE</tt>).
**/
public class AssignmentResult {

  /** Path matched up until now by the current {@link PathPattern}. */
  public Path path;

  /** Record considered, where some of the newly bound variables are set. */
  public Record record;

  /** The set of by the current {@link PathPattern}, as well as the previous
  ones.
  ***/
  public Set<Relation> relations;

  /** Conditions for the current path to match the current path pattern and that
  may not be tested yet.
  An {@link Expression} is added in the set whenever its evaluation raises
  a {@link UndefinedNameException}, that is a variable occurring in the Expression
  is not set yet.
  Typically, this occurs with patterns such as <tt>({name:b.name})--&gt;(b)</tt>.
  */
  public Set<Expression> postConditions;

  /** Construcs an AssignmentResult wrapping the given parametters. */
  public AssignmentResult(Path path,
                          Record record,
                          Set<Relation> relations,
                          Set<Expression> postConditions
                          ) {
    this.path = path;
    this.record = record;
    this.relations= relations;
    this.postConditions = postConditions;
  }

  /** Verify that wrapped record assign name {@code n} to value {@code v}, or add
  this assignment to the record if the record has not assignment for the name.
    * @return this assignment result if the wrapped record already assigns {@code n} to {@code v}.
    Otherwise, {@link #record} is copied and the extra assignment added to the copy;
    and this function returns a new assignment result that wraps {@link #path}, {@link #relations}
      {@link #postConditions} and the new record.
    * @throws RecordInconsistencyException if wrapped record set name {@code n}
  to a {@link Value} that is not {@code v}.
  ***/
  public AssignmentResult verifyOrAdd(Name n, Value v)
    throws RecordInconsistencyException
  {
    Record newRecord = Record.ensureBinding(record,n,v);
    if (record == newRecord)
      return this;
    else
      return new AssignmentResult(path, newRecord, relations, postConditions);
  }

  /** Adds an expression to the set of post-conditions.
    * @return this if the postcondition was already in the set, or a new assignment result otherwise.
  */
  public AssignmentResult addPostCondition(Expression e) {
    if (postConditions.contains(e))
      return this;
    Set<Expression> newPostConditions = new HashSet<Expression>(postConditions);
    newPostConditions.add(e);
    return new AssignmentResult(path, record, relations, newPostConditions);
  }

  /** Returns a copy of this {@link AssignmentResult} corresponding to the
    * match of a new relation.
    * Field {@link #path} is copied and the given relation is appended to the copy.
    * Field {@link #relations} is copied and the given relation is added to the copy.
  */
  public AssignmentResult addRelation(Relation r) {
    Set<Relation> newRelations = new HashSet<Relation>(relations);
    newRelations.add(r);
    Path newPath = new Path(path);
    newPath.add(r);
    return new AssignmentResult(newPath, record, newRelations, postConditions);
  }


  @Override
  public int hashCode()
  {
    return Objects.hash(path, record, relations, postConditions);
  }

  @Override
  public boolean equals (Object o) {
    if (!(o instanceof AssignmentResult))
      return false;
    AssignmentResult other = (AssignmentResult) o;
    return ( path.equals(other.path)
             && record.equals(other.record)
             && relations.equals(other.relations)
             && postConditions.equals(other.postConditions) );
  }

  @Override
  public String toString()
  {
    return "<"+path+","+record+","+relations+","+postConditions+">";
  }

  /** Indicate whether this assignment result satisfies all postConditions in
  the context of the given graph.
  **/
  public boolean satisfiesPostConditions(Graph g)
  {
    for (Expression e : postConditions) {
      if (! Expressions.semEquals(e, new Value(true), g, record, Policy.PROPAGATE) )
        return false;
    }
    return true;
  }

  /** Filter out the post-conditions that are already true in the context of the
  given graph.
    * In fact, it is more likley that the wrapped record has changed that the
  given graph.
    * @return {@code false} if any post-condition was evaluated to {@code false},
      in which case the filtering was not completely run and this assignment result
      is left in an inconsistent state.
  **/
  public boolean filterPostConditions(Graph g)
  {
    Set<Expression> toRemove = new HashSet<Expression>();
    for(Expression e : postConditions) {
      try {
        if (Expressions.semEquals(e, new Value(true), g, record, Policy.PROPAGATE) )
          toRemove.add(e);
        else
          return false;
      } catch (UndefinedNameException une) { }
    }
    if (!toRemove.isEmpty()) {
      postConditions = new HashSet<Expression>(postConditions);
      postConditions.removeAll(toRemove);
    }

    return true;
  }

}
