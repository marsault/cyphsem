/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.language.expression.Expression;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.RelationType;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


//This class corresponds to a relation pattern with NO iteration
public class SimpleRelationPattern extends RelationPattern {

  public
  SimpleRelationPattern
      ( NodePattern previous, UnnamedPathPattern next, Object... others )
  {
    super(previous, next, others);
  }


  @Override
  public List<AssignmentResult>
  computeAssignment
    ( Graph graph, AssignmentResult partial_ar, Interruptor i )
    throws InterruptedException
  {
    i.check();
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();

    for (AssignmentResult assignment : before(graph, partial_ar, i))
      for ( AssignmentResult assignment2 : matchingRelations (graph, assignment)) {
        try {
          AssignmentResult newAR =
              (name == null)  ? assignment2
                              : assignment2.verifyOrAdd(
                                  name,
                                  new Value(assignment2.path.lastRelation()) );
          result.addAll(after(graph, newAR,i));
        }
        catch (RecordInconsistencyException e) {
          // A RecordInconsistency here means that this RelationPattern has
          // a name that is already bound to a Relation different from the
          // one dangling from `assignment2.path`.
          // As a result, this assignment is invalid and is not added to
          // the returned AssignementResult (called `result`).
        }
      }

    return result;
  }

  @Override
  public String iterationString() { return ""; }

  @Override
  public Map<Name,VarStatus>
  assertSuitable(
      boolean forUpdate,
      Map<Name,VarStatus> varStatus
    )
  throws AssertionFailedException
  {
    previous.assertSuitable(forUpdate,varStatus);
    assertSuitable(forUpdate,varStatus,VarStatus.REL);
    return next.assertSuitable(forUpdate, varStatus);
  }

}
