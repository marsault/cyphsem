/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.*;
import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of expressions of the form {@code [}<var>e{1}</var>,&hellip;,<var>e{n}</var><tt class="inline">]</tt>, where e{1}</var>,&hellip;,<var>e{n}</var> may be any kind of {@link Expression}s.
 */
public class ExplicitListExpression implements Expression {

  /**
   * Contains the sub-expressions <var>e{1}</var>,..,<var>e{n}</var>
   */
  protected List<Expression> subExpressions;


  public ExplicitListExpression (Expression... exps) {
    this.subExpressions = new ArrayList<Expression>(exps.length);
    for (Expression e:exps)
      subExpressions.add(e);
  }
  public ExplicitListExpression (List<Expression> subExpressions) {
    this.subExpressions = subExpressions;
  }

  /**
   * Constructs the expression corresponding to the empty list : <tt>[]</tt>.
   */
  public ExplicitListExpression () {
    this.subExpressions = new ArrayList<Expression>();
  }

  /** {@inheritDoc}
    * @return {@inheritDoc} Returned {@link Value} is of {@link Value.Kind Kind}
    * {@link Value.Kind#LIST LIST}.
  ***/
  @Override
  public Value evaluate(Graph graph, Record record) {
    List<Value> l = new ArrayList<Value>(subExpressions.size());
    for (Expression e : subExpressions)
      l.add(e.evaluate(graph,record));
    return (new Value(l));
  }

  @Override
  public String toString() {
    return StringOf.iterable(subExpressions,"[","]",",");
  }


}
