/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.*;
import uk.ac.ed.cyphsem.Parameters.*;
import uk.ac.ed.cyphsem.Parameters.MergeVersion.*;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

import java.util.function.Function;

/** Implements Cypher clauses of the form <tt>MERGE p1,..,pn</tt>,
  * where p1, ..., pn are path patterns.
***/
public class MergeClause implements WritingClause, PatternHolder {


  /** Path patterns held by this {@link  MergeClause}. */
  protected List<PathPattern> patterns;

  /** Compiled version of field {@link #patterns}. (Unused in current implementation.) */
  CompiledPattern compiledPattern = new CompiledPattern(true);

  /** An instance of {@link MatchClause} created with the same pattern as this {@link MergeClause}. */
  protected MatchClause subMatch;

  /** An instance of {@link CreateClause} created with the same pattern as this {@link MergeClause}. */
  protected CreateClause subCreate;

  /** Implements a directive for the creation of a Node.
    * <br>
    * This class is useful for {@link MergeVersion#COLLAPSE COLLAPSE}
    * and {@link MergeVersion#AGGRESSIVE AGGRESSIVE} versions of
    * <tt>MERGE</tt>.
    * It is implemented in such a way that two {@link NodeDirective}s are equal
    * if they should be collapsed together by <tt>MERGE</tt>.
    * In order to differenciate the two versions of <tt>MERGE</tt>, one uses
    * field {@link NodeDirective#ignoreName ignoreName}.
    * <br><br>
    * This object usage relies heavily on its hash and the hash is quite long
    * to compute. The hash is then computed only once at construction.
    * As a result, a {@link NodeDirective} (or its content) should not be
    * modified after creation.
    *
  **/
  protected static class NodeDirective {

    /** Whether this {@link NodeDirective} should ignore field {@link #name}
      * for methods {@link #equals} and {@link #hashCode}.
    **/
    public final boolean ignoreName;

    /** Name of the pattern creating the node. */
    public final Name name;

    /** Labels of the node to be created. */
    public final Set<Label> labels;

    /** Properties of the node to be created. */
    public final Map<PropertyKey,Value> propertyMap;

    /** The hash code of this object, computed once at construction. **/
    public final int hash;

    /** Constructs a {@link NodeDirective} wrapping the given parameters. */
    public NodeDirective
      ( boolean ignoreName, Name name, Set<Label> labels,
        Map<PropertyKey,Value> propertyMap )
    {
      this.ignoreName = ignoreName;
      this.name = name; this.labels = labels; this.propertyMap = propertyMap;
      hash = Utils.hashTuple(
          ignoreName?1:name,
          Utils.hashColl(labels),
          propertyMap);
    }

    /** Returns field {@link #hash} and <b>does</b> not actually compute the hash of
      * this object. */
    @Override
    public int hashCode() {
      return hash;
    }

    /** Indicates whether some other object is equal to this one.
      * Returns {@code true} if parameter {@code o} is an instance of {@link NodeDirective}
        and
        <ul><li> all fields except {@link  #ignoreName} of {@code o} are equal to the one of this {@link NodeDirective};
        </li><li> or all fields except {#name} of {@code o} are equal to the one of this {@link NodeDirective}
        and {@link #ignoreName} is {@code true}</li></ul>
      */
    @Override
    public boolean equals (Object o) {
      if (! (o instanceof NodeDirective))
        return false;
      NodeDirective other = (NodeDirective) o;
      return ( ((ignoreName && other.ignoreName) || name.equals(other.name))
               && propertyMap.equals(other.propertyMap)
               && labels.equals(other.labels)          );
    }

    @Override
    public String toString() {
      List<String> items = new ArrayList<>(4);
      items.add(name.toString());
      if (!labels.isEmpty())
        items.add(StringOf.iterable(labels,":","",":"));
      if (! propertyMap.isEmpty())
        items.add(StringOf.iterable(propertyMap.entrySet(),"{","}",","));
      items.add("#"+String.format("%08X", hash));
      return StringOf.iterable(items,"(",")"," ");
    }
  }

    /** Implements a directive for the creation of a {@link Relation}.
    * <br>
    * This class is useful for {Parameters.MergeVersion#COLLAPSE COLLAPSE}
    * and {Parameters.MergeVersion#AGGRESSIVE AGGRESSIVE} versions of
    * <tt>MERGE</tt>.
    * It is implemented in such a way that two {@link RelDirective}s are equal
    * if they should be collapsed together by <tt>MERGE</tt>.
    * In order to differenciate the two versions of <tt>MERGE</tt>, one uses
    * field {@link RelDirective#ignoreName ignoreName}.
    * <br><br>
    * This object usage relies heavily on its hash and the hash is quite long
    * to compute. The hash is then computed only once at construction.
    * As a result, a {@link NodeDirective} (or its content) should not be
    * modified after creation.
    *
  **/
  protected static class RelDirective {

    /** Whether this {@link RelDirective} should ignore field {@link #name}
      * for methods {@link #equals} and {@link #hashCode}.
    **/
    public final boolean ignoreName;

    /** Name of the pattern creating the Relation **/
    public final Name name;

    /** Type of the Relation to be created */
    public final RelationType type;

    /** Properties of the Relation to be created */
    public final Map<PropertyKey,Value> propertyMap;

    /** Source node of the {@code Relation} to be created. */
    public final Node source;

    /** Target node of the {@code Relation} to be created. */
    public final Node target;

    /** The hash code of this object, computed once at construction. **/
    public final int hash;

    /** Constructs a {@link NodeDirective} wrapping the given parameters. */
    public RelDirective
      ( boolean ignoreName, Name name, RelationType type,
        Map<PropertyKey,Value> propertyMap,
        Node source, Node target )
    {
      this.ignoreName = ignoreName;
      this.name = name; this.propertyMap = propertyMap;
      this.source = source; this.target = target;
      this.type = type;
      hash = Utils.hashTuple(
          (ignoreName)?1:name,
          type,
          propertyMap,
          source,
          target
        );
    }

    /** Returns field {@link #hash} and <b>does</b> not actually compute the hash of
      * this object. */
    @Override
    public int hashCode() {
      return hash;
    }

    /** Indicates whether some other object is equal to this one.
      * Returns {@code true} if parameter {@code o} is an instance of {@link NodeDirective}
        and
        <ul><li> all fields except {@link  #ignoreName} of {@code o} are equal to the one of this {@link NodeDirective};
        </li><li> or all fields except {#name} of {@code o} are equal to the one of this {@link NodeDirective}
        and {@link #ignoreName} is {@code true}</li></ul>
      */
    @Override
    public boolean equals (Object o) {
      if (! (o instanceof RelDirective))
        return false;
      RelDirective other = (RelDirective) o;
      return ( ((ignoreName && other.ignoreName) || name.equals(other.name))
               && propertyMap.equals(other.propertyMap)
               && type.equals(other.type)
               && source.equals(other.source)
               && target.equals(other.target) );
    }

    @Override
    public String toString() {
      List<String> items = new ArrayList<>(4);
      items.add(name.toString());
      items.add(type.toString());
      if (! propertyMap.isEmpty())
        items.add(StringOf.iterable(propertyMap.entrySet(),"{","}",","));
      items.add("#"+String.format("%08X", hash));
      return source
             + StringOf.iterable(items,"-[","]->"," ")
             + target;
    }
  }


  /** Computes the grouping expression contained in held patterns.
    * Used only if applying the {@link MergeVersion#COLLAPSE COLLAPSE}
    * version of <tt>MERGE</tt>.
  **/
  public Set<Expression> groupingExpressions(Set<Name> boundVariables) {
    Set<Expression> result = new HashSet<>();
    for (Name name: compiledPattern.idVariables())
      result.add(compiledPattern.propertyOf(name));
    for (Name name : compiledPattern.genuineVariables())
      if (boundVariables.contains(name))
        result.add(name);
    if (result.contains(null))
      throw new Error("PROBLEM::!!");
    return result;
  }

  @Override //from PatternHolder
  public List<PathPattern> patterns() { return patterns; }

  /** Constructs a {@link MergeClause} wrapping given patterns.
    * Also constructs fields {@link #subMatch} and {@link #subCreate} from
    * the same patterns.
  **/
  public MergeClause(List<PathPattern> patterns) {
    this.patterns = patterns;
    compiledPattern.addPatterns(patterns);
//     System.out.println(compiledPattern.toCypherPatternTuple());
//     PatternHolder.super.assertPatternTupleIsSuitable();
    subMatch = new MatchClause(patterns);
    subCreate = new CreateClause(patterns);
//
//     for (PathPattern p : patterns) {
//       boolean b = true;
//       for (UnnamedPathPattern up : p.serialize()) {
//         Name name = b ? ((NodePattern) up).name()
//                       : ((SimpleRelationPattern) up).name();
//         if (name == null) {
//           name = Names.fresh();
//           boggusNames.add(name);
//         }
//           nameOf.put(up,name);
//         b = !b;
//       }
//     }
//     for (PathPattern p : patterns) {
//       boolean b = false;
//       for (UnnamedPathPattern up : p.serialize()) {
//         if (b) {
//           SimpleRelationPattern rp = (SimpleRelationPattern) up;
//           relsExtremities.put(
//             nameOf.get(up),
//             new Pair<>(
//               nameOf.get(rp.getPrevious()),
//               nameOf.get( (rp.getNext() instanceof NodePattern)
//                           ? rp.getNext()
//                           : ((RelationPattern) rp.getNext()).getPrevious() ))
//           );
//         }
//         b = !b;
//       }
//     }
//     for (PathPattern p : patterns) {
//       boolean b = false;
//       for (UnnamedPathPattern up : p.serialize()) {
//         if (b) {
//           SimpleRelationPattern rp = (SimpleRelationPattern) up;
//           relContent.put(
//             nameOf.get(rp),
//             new Pair<>(rp.types(),rp.getMap())
//           );
//         }
//         else {
//           NodePattern rp = (NodePattern) up;
//           Name name = nameOf.get(rp);
//           if (!nodeContent.containsKey(name) || !rp.isEmpty() )
//             nodeContent.put(
//                 name,
//                 new Pair<>(rp.getLabels(),rp.getMap())
//             );
//         }
//         b = !b;
//       }
//     }
  }

  /** {@inheritDoc}
    * <br><br>
    * Implementation in {@link MergeClause} calls {@link #apply(uk.ac.ed.cyphsem.language.query.QueryData, uk.ac.ed.cyphsem.Interruptor, uk.ac.ed.cyphsem.Parameters.MergeVersion) apply}{@code (qd, interruptor,Parameters.global.merge())}.
  **/
  @Override
  public QueryData apply(QueryData qd, Interruptor interruptor)
  throws InterruptedException
  {  return apply(qd, interruptor, Parameters.global.merge());  }


  /** Applies semantics function associated to this {@link MergeClause}, interpreted with the given {@link SetVersion version}.
  **/
  public QueryData apply
    ( QueryData qd, Interruptor interruptor, MergeVersion version)
  throws InterruptedException
  {
    interruptor.check();
    final Graph graph_in = qd.graph();
    Graph graph_out = graph_in;
    final Table table_in = qd.table();
    compiledPattern.assertValidInContextOf(table_in.domain());
//     System.out.println(table_in);
//     System.out.println(table_in.domain());
    Table table_out = new Table();
    switch (version) {
      case ATOMIC: {
        Table toCreate = new Table();
        for (Record record : table_in) {
          Table match_result = subMatch.apply(graph_in, record, interruptor);
          if (!match_result.isEmpty())
            table_out.addAll(match_result);
          else
            toCreate.add(record);
        }
        QueryData qd2 = subCreate.apply(new QueryData(graph_in, toCreate));
        graph_out = qd2.graph();
        table_out.addAll(qd2.table());
        break;
      }
      case GROUPING: {
        List<Record> toCreate = new ArrayList<Record>();
        for (Record record : table_in.recordSet()) {
          Table match_result = subMatch.apply(graph_in, record, interruptor);
          if (!match_result.isEmpty())
            table_out.addAll(match_result,table_in.multiplicityOf(record));
          else
            toCreate.add(record);
        }
        final List<Expression> group_exps
          = new ArrayList<>(groupingExpressions(table_in.domain()));
        Function<Record,List<Value>> splitting_function
          = (r -> new ArrayList<>(Utils.map(group_exps, y -> y.evaluate(graph_in,r))));
        Collection<List<Record>> splitted_table = Utils.split(toCreate, splitting_function);
        for (List<Record> agg_list : splitted_table) {
          Record one_in = agg_list.get(0);
          QueryData qd2 = subCreate.apply(
              new QueryData(graph_in, new Table(one_in)), interruptor);
          Record one_out = qd2.table().anyOne();
          table_out.add(one_out);
          Set<Name> newlyBoundVariables = new HashSet<Name> (one_out.domain());
          newlyBoundVariables.removeAll(one_out.domain());
          for (int i = 1; i<agg_list.size(); i++) {
            Record in_i = agg_list.get(i);
            Record out_i = new Record(in_i);
            for (Name name : newlyBoundVariables)
              out_i.bind(name, one_out.get(name));
            table_out.add(out_i,table_in.multiplicityOf(in_i));
          }
        }
        break;
      }
      case LEGACY: {
        for (Record record : table_in) {
          Table match_result = subMatch.apply(graph_in, record, interruptor);
          if (!match_result.isEmpty())
            table_out.addAll(match_result);
          else {
            QueryData qd2 = subCreate.apply(new QueryData(graph_in,record));
            graph_out = qd2.graph();
            table_out.addAll(match_result);
          }
        }
        break;
      }
      case COLLAPSE:
      case AGGRESSIVE: {
        Table toMerge = new Table();
        for (Record record : table_in) {
          Table match_result = subMatch.apply(graph_in, record, interruptor);
          if (!match_result.isEmpty())
            table_out.addAll(match_result);
          else
            toMerge.add(record);
        }
        boolean is_aggressive = (version == MergeVersion.AGGRESSIVE);
        Function<Name,Boolean> f = x -> toMerge.hasName(x) ? false : is_aggressive;
//         long nodeMajId = graph_out.nodeMajId;
//         long relMajId = graph_out.relMajId;

        List<Node> created_nodes = new ArrayList<>();
        List<Relation> created_rels = new ArrayList<>();
        Map<NodeDirective, Node> nodeDir_to_node = new HashMap<>();
        Map<RelDirective, Integer> relDir_mult = new HashMap<>();
        Map<Pair<RelDirective,Integer>,Relation> relDir_to_rel = new HashMap<>();

        for (Record record_in : toMerge) {
//           System.out.println(record_in);
          Map<Name, Node> node_by_name = new HashMap<>();
          Map<Name, Relation> rel_by_name = new HashMap<>();
          for (Name name : compiledPattern.nodeVariables() )
//           Map.Entry<Name,Pair<Set<Label>,ExplicitMapExpression>> entry : nodeContent.entrySet()
          {
// //             Name name = entry.getKey();
//             ExplicitMapExpression exp_map = entry.getValue().second;
            Map<PropertyKey, Value> map
              = compiledPattern.propertyOf(name)
                               .evaluate(graph_in, record_in).unsafeCastAsMap();
            Set<Label> labels = compiledPattern.labelsOf(name);
            NodeDirective node_dir = new NodeDirective(f.apply(name), name, labels, map);
//             System.out.println(node_dir);
            Node new_node = null;

            if (toMerge.hasName(name)) {
              new_node = Expressions.evaluateToNode(name, graph_in, record_in);
//               System.out.println("Bound: "+new_node);
            } else if (nodeDir_to_node.containsKey(node_dir)) {
              new_node = nodeDir_to_node.get(node_dir);
//               System.out.println("Previously created: "+new_node);
            }
            else {
              new_node = graph_in.makeNode(labels, map);
              created_nodes.add(new_node);
              nodeDir_to_node.put(node_dir,new_node);
//               System.out.println("Fresh node: "+new_node);
            }
            node_by_name.put(name,new_node);
          }
          Map<RelDirective,Integer> same_relDir = new HashMap<>();
          for (  Name name : compiledPattern.relationVariables() )
//           Map.Entry<Name,Pair<Optional<RelationType>,ExplicitMapExpression>>
//                 entry : relContent.entrySet() )
          {
//             Name name = entry.getKey();
//             ExplicitMapExpression exp_map
//               = compiledPattern.propertyOf(name)
//                                .evaluate(graph_in, record_in).unsafeCastAsMap();
            Map<PropertyKey, Value> map
              = compiledPattern.propertyOf(name)
                               .evaluate(graph_in, record_in).unsafeCastAsMap();
            RelationType type = compiledPattern.anyTypeOf(name);
            Pair<Name,Name> pair = new Pair<>(compiledPattern.sourceOf(name),
                                              compiledPattern.targetOf(name));
            RelDirective new_relDir = new RelDirective( f.apply(name), name, type, map,
                                    node_by_name.get(pair.first),
                                    node_by_name.get(pair.second) );
//             System.out.println(new_relDir);
            Integer i = 0;
            Relation new_rel = null;
            if (same_relDir.containsKey(new_relDir))
              i = same_relDir.get(new_relDir);
            else if (relDir_mult.containsKey(new_relDir))
              i = relDir_mult.get(new_relDir);
            if (i > 0) {
              same_relDir.put(new_relDir,i-1);
              new_rel = relDir_to_rel.get (new Pair<> (new_relDir, i));
            }
            else {
              int j = relDir_mult.getOrDefault(new_relDir,0)+1;
              relDir_mult.put(new_relDir, j);
              new_rel = graph_in.makeRelation (
                  new_relDir.type, new_relDir.source, new_relDir.target,
                  new_relDir.propertyMap );
              created_rels.add(new_rel);
              relDir_to_rel.put(new Pair<> (new_relDir, j), new_rel);
            }
            rel_by_name.put(name,new_rel);
          }
          Record record_out = new Record(record_in);
          for (Name name : compiledPattern.genuineNodeVariables())
              record_out.bind(name, new Value(node_by_name.get(name)));
          for (Name name : compiledPattern.genuineRelationVariables())
              record_out.bind(name, new Value(rel_by_name.get(name)));
          for (Name name : compiledPattern.genuinePathVariables()) {
            List<Node> nodes = new ArrayList<>();
            List<Relation> relations = new ArrayList<>();
            for (Name el_name : compiledPattern.pathOf(name)) {
              if (compiledPattern.isNodeVariable(el_name))
                nodes.add(node_by_name.get(el_name));
              else if (compiledPattern.isRelationVariable(el_name))
                relations.add(rel_by_name.get(el_name));
              else
                throw new UnreachableStatementError();
            }
            record_out.bind(name,
                            new Value(new Path(nodes,relations)));
          }
          table_out.add(record_out);
        }
        for (Node node : created_nodes)
          graph_out.addNode(node);
        for (Relation relation : created_rels)
          graph_out.addRelation(relation);




//         System.out.println(created_nodes);
//         System.out.println(created_rels);
//         System.out.println(nodeDir_to_node);
//         System.out.println(relDir_to_rel);
      }
    }
    return new QueryData(graph_out, table_out);
  }

  @Override
  public String toString()
  { return PatternHolder.super.formatToString("MERGE"); }

}
