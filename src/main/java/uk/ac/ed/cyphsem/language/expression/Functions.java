/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.datamodel.value.*;

import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;

import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Optional;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.lang.Error;


/**
 * This enum lists the different functions supported by CyphSem.
 * Each instance of Functions is a {@link ValueToValueFunction}, hence may be
 called on {@link Value}s.
 * For instance:
 <tt><pre>
  Value v1,v2 = ...
  boolrean b = Functions.XOR(v1,v2);
</tt></pre>
 */
public enum Functions implements ValueToValueFunction {
/* Binary string functions  ==================================================*/
  STARTS_WITH(new BinaryFunction<String,Boolean>(
    Kind.STRING,
    (x,y) -> x.startsWith(y)
  ))
  ,
  ENDS_WITH (new BinaryFunction<String,Boolean>(
    Kind.STRING,
    (x,y) -> x.endsWith(y)
  ))
  ,
  CONTAINS (new BinaryFunction<String,Boolean>(
    Kind.STRING,
    (x,y) -> x.contains(y)
  )),
/* Boolean functions  ========================================================*/
  OR (new BinaryFunction<Boolean,Boolean>(
    Kind.BOOL,
    (x,y) -> x.booleanValue() || y.booleanValue(),
    x -> x.booleanValue() ? Optional.of(true) : Optional.empty()
  ))
  ,
  AND (new BinaryFunction<Boolean,Boolean>(
    Kind.BOOL,
    (x,y) -> x.booleanValue() && y.booleanValue(),
    x -> x.booleanValue() ? Optional.empty() : Optional.of(false)
  ))
  ,
  XOR (new BinaryFunction<Boolean,Boolean>(
    Kind.BOOL,
    (x,y) -> x.booleanValue() ^ y.booleanValue()
  ))
  ,
  NOT (new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        assertOperandsKind(NOT,uni_bool,operands);
        Boolean x = operands.get(0).asNullableBoolean();
        if (x == null)
          return (new Value());
        boolean l = x.booleanValue();
        return (new Value(Boolean.valueOf( !l)));
      }
  })
  ,
/* Comparison to NULL  =======================================================*/
  IS_NULL (new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        if (operands.size() != 1)
          throw new UnspecifiedBehaviourException("Function got too many operands.");
        return (new Value(operands.get(0).isOfKind(Kind.NULL)));
      }
  })
  ,
  IS_NOT_NULL (new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        if (operands.size() != 1)
          throw new UnspecifiedBehaviourException("Function got too many operands.");
        return (new Value(!operands.get(0).isOfKind(Kind.NULL)));
      }
  })
  ,
/* Integer comparison  =======================================================*/
  LE ("<", new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        BigInteger x = operands.get(0).asNullableInteger();
        BigInteger y = operands.get(1).asNullableInteger();
        if ( (x == null) || (y == null) )
          return (new Value());
        return (new Value(x.compareTo(y) < 0));
      }
  })
  ,
  LEQ ("<=", new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        assertOperandsKind(LEQ,bin_int,operands);
        BigInteger x = operands.get(0).asNullableInteger();
        BigInteger y = operands.get(1).asNullableInteger();
        if ((x == null) || (y == null))
          return (new Value());
        return (new Value(x.compareTo(y) <= 0));
      }
  })
  ,
  GE (">", new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        assertOperandsKind(GE,bin_int,operands);
        BigInteger x = operands.get(0).asNullableInteger();
        BigInteger y = operands.get(1).asNullableInteger();
        if ((x == null) || (y == null))
          return (new Value());
        return (new Value(x.compareTo(y) > 0));
      }
  })
  ,
  GEQ (">=", new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        assertOperandsKind(GEQ,bin_int,operands);
        BigInteger x = operands.get(0).asNullableInteger();
        BigInteger y = operands.get(1).asNullableInteger();
        if ((x == null) || (y == null))
          return (new Value());
        return (new Value(x.compareTo(y) >= 0));
      }
  })
  ,
/* Equality/Inequality  ======================================================*/
  EQ  ("=",new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        if (operands.size() != 2)
          throw new UnspecifiedBehaviourException(EQ+": must have two arguments.");

        Kind left_kind = operands.get(0).getKind();
        Kind right_kind = operands.get(1).getKind();
        Value left = operands.get(0);
        Value right = operands.get(1);

        if ((left_kind == Kind.NULL) || (right_kind == Kind.NULL))
          return new Value();

        if (left_kind != right_kind) {
          return new Value(false);
//           if ( left_kind.isComposite() || right_kind.isComposite() )
//             return new Value(false);
//           else
//             throw new UnspecifiedBehaviourException(EQ+": two values of different base type cannot be tested for equality.");
        }
        switch (left_kind) {
          case LIST: {
            List<Value> left_as_list = left.unsafeCastAsList();
            List<Value> right_as_list = right.unsafeCastAsList();
            int n = left_as_list.size();
            if (right_as_list.size() != n)
              return (new Value(false));
            Value return_value = new Value(true);
            for (int i=0; i<n; i++) {
              List<Value> new_ops = new ArrayList<Value>();
              new_ops.add(left_as_list.get(i));
              new_ops.add(right_as_list.get(i));
              Value ret = Functions.EQ.call(new_ops);
              if (ret.getKind() == Kind.NULL) {
                return_value = new Value();
              } else if (ret.getKind() == Kind.BOOL) {
                if (! Boolean.valueOf(ret.asBoolean()))
                  return new Value (false);
              } else {
                throw new Error ("VTV::EQ::LIST impossible case");
              }
            }
            return (return_value);
          }
          case MAP:
            Map<PropertyKey,Value> left_as_map = left.unsafeCastAsMap();
            Map<PropertyKey,Value> right_as_map = right.unsafeCastAsMap();
            Set<PropertyKey> left_keys = left_as_map.keySet();
            Set<PropertyKey> right_keys = right_as_map.keySet();
            if (!left_keys.containsAll(right_keys))
              return new Value(false);
            if (!right_keys.containsAll(left_keys))
              return new Value(false);
            //Keysets are equal, verifying value
            Value return_value = new Value(true);
            for(PropertyKey el : right_as_map.keySet()) {
              Value ret = Functions.EQ.call(
                  left_as_map.get(el),
                  right_as_map.get(el)
                );
              if (ret.isOfKind(Kind.NULL)) {
                return_value = new Value();
              } else if (ret.isOfKind(Kind.BOOL)) {
                if (! Boolean.valueOf(ret.asBoolean()))
                  return new Value (false);
              } else {
                throw new Error ("VTV.EQ.MAP unreachable statement");
              }
            }
            return (return_value);
//           case PATH:
//             Path left_as_path = left.unsafeCastAsPath();
//             Path right_as_path = left.unsafeCastAsPath();
//             return new Value(left_as_path.equals(right_as_path));
          default:
            return new Value(left.equals(right));
        }
      }
  })
  ,
  NEQ ("<>",new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        Value eq_res = Functions.EQ.call(operands);
        if (eq_res.getKind() == Kind.BOOL)
          return  new Value(!eq_res.asBoolean());
        else
          return new Value();
      }
  })
  ,
/* IN   ====================================================== */
   IN (new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        if (operands.size() != 2)
          throw new UnspecifiedBehaviourException(toString()+":must have two arguments.");
        Value el = operands.get(0);
        List<Value> vals =  operands.get(1).asNullableList();
        if (vals == null)
          return new Value();
        Value ret = new Value(false);
        for (Value val : vals) {
          try {
            Boolean b = Functions.EQ.call(val, el).asNullableBoolean();
            if (b == null)
              ret = new Value();
            else if (b)
              return (new Value(true));
          } catch (UnspecifiedBehaviourException e) { }
        }
        return ret;
      }
  })
  ,
/* Arihtmetic : TODO (proof of concept)  ==================================== */
  PLUS ("+", new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        assertOperandsKind(PLUS, bin_int, operands);
        BigInteger left = operands.get(0).asNullableInteger();
        BigInteger right = operands.get(1).asNullableInteger();
        if ( (left==null) || (right==null) )
          return ( new Value() );
        else
          return ( new Value(left.add(right)) );
      }
  })
  ,
  MINUS ("-", new ValueToValueFunction () {
      public Value call(List<Value> operands) {
        assertOperandsKind(MINUS, bin_int, operands);
        BigInteger left = operands.get(0).asNullableInteger();
        BigInteger right = operands.get(1).asNullableInteger();
        if ( (left==null) || (right==null) )
          return ( new Value() );
        else
          return ( new Value(left.subtract(right)) );
      }
  })
  ;

  /**
   * The wrapped ValueToValueFunction to be called.
   */
  private ValueToValueFunction function;

  /**
   * The function as it is printed.
   */
  private String name;


  private Functions(ValueToValueFunction function) {
    this(null, function);
  }

  private Functions (String name, ValueToValueFunction function) {
    this.function = function;
    if (name != null)
      this.name = name;
    else
      this.name = super.toString().replaceAll("_"," ");
  }

  /**
   *{@inheritDoc}
   * Simply calls boxed {@link ValueToValueFunction} on arguments.
   */
  @Override
  public Value call(List<Value> operands) { return (function.call(operands)); }

  /**
   *{@inheritDoc}
   * Simply calls boxed {@link ValueToValueFunction} on arguments.
   */
  @Override
  public Value call(Value... operands) { return (function.call(operands)); }

  @Override public String toString() { return name; }

  /**
   * Assert that an operand is of acceptable kinds, possibly
   * throwing an {@link UnspecifiedBehaviourException}.
   * @param f Function (used for error-message formatting)
   * @param acceptableKinds Acceptable {@link Kind}s for the operand.
   * @param operand {@link Value} the {@link Kind} of which is being tested
   * @param i  (used for error-message formatting)
   * @throws UnspecifiedBehaviourException Is raised if operand is not of any acceptable {@link Kind}
   */
  private static
  void
  assertOperandKind
    ( ValueToValueFunction f, List<Value.Kind> acceptableKinds, Value operand,
      int i )
  {
    for (Value.Kind kind : acceptableKinds)
      if (operand.isOfKind(kind))
        return ;

    throw new DynamicCastException(f.toString()+": operand n°"+i+" if of kind "+operand.getKind()+"; acceptable kinds are"+acceptableKinds+".");
  }


  /**
   * Assert each operand are of acceptable kinds, possibly
   * throwing an {@link UnspecifiedBehaviourException}.
   * @param f Function (used for error-message formatting)
   * @param acceptableKinds Acceptable {@link Kind}s for each operand.
   * @param operands {@link Value}s the {@link Kind} of which is being tested
   * @throws UnspecifiedBehaviourException Is raised if an operand is not of any acceptable {@link Kind} or if
   * the respective sizes of <tt>acceptableKinds</tt> and <tt>operands</tt> differ.
   */
  private static
  void
  assertOperandsKind
    ( ValueToValueFunction f, List<List<Value.Kind>> acceptableKinds,
      List<Value> operands )
  {
    int max = operands.size();
    if (acceptableKinds.size() != max)
      throw new UnspecifiedBehaviourException(f.toString()+": too many operands.");

    for (int i = 0; i<max ; i++)
      assertOperandKind(f, acceptableKinds.get(i), operands.get(i), i);
  }

  // These fields contains common acceptableKinds directives.
  // Binary String
  static private List<List<Kind>> bin_string = init_bin (Kind.STRING);
  // Binary Boolean
  static private List<List<Kind>> bin_bool = init_bin (Kind.BOOL);
  // Unary Boolean
  static private List<List<Kind>> uni_bool = init_uni (Kind.BOOL);
  // Binary Integer
  static private List<List<Kind>> bin_int = init_bin (Kind.INT);

  static private List<List<Kind>> init_bin(Kind k) {
    List<List<Kind>> l = new ArrayList<List<Kind>>();
    List<Kind> t = new ArrayList<Kind>();
    t.add(k); t.add(Kind.NULL);
    l.add(t); l.add(t);
    return l;
  }

  static private List<List<Kind>> init_uni(Kind k) {
    List<List<Kind>> l = new ArrayList<List<Kind>>();
    List<Kind> t = new ArrayList<Kind>();
    t.add(k); t.add(Kind.NULL);
    l.add(t);
    return l;
  }

  /**
   * Helper class for binary functions, that is, functions taking two arguments
   * of the same type (<tt>E</tt>) and returning a value of possible
   * another type (<tt>F</tt>).
   * Since input values must be cast,
   */
  public static class BinaryFunction<E,G> implements ValueToValueFunction {
    /** The {@link Kind} expected of operands.
      * It is used for dynamic casting prior to calling {@code internalFunction}
      * or {@code unaryFunction}.
      */
    private Kind kind;

    /** Function called if neither operands are nulls. */
    private BiFunction<E,E,G> internalFunction;

    /** Function called if one operand is a {@link Kind#NULL NULL} {@link Value}.
      * If it returns {@code Optional.empty()}, it means that the function
      * returns a {@link Kind#NULL NULL} {@link Value}.
      */
    private Function<E,Optional<G>> unaryFunction;

    /** Constructs a BinaryFunction that always returns null if at least one
      * operand is a {@link Kind#NULL NULL} {@link Value}.
      */
    BinaryFunction(Kind k, BiFunction<E,E,G> function)  { this(k,function, x -> Optional.empty()); }

    /** Constructs a BinaryFunction wrapping arguments. */
    BinaryFunction(Kind kind, BiFunction<E,E,G> internalFunction, Function<E,Optional<G>> unaryFunction)
    { this.kind = kind;
      this.internalFunction = internalFunction;
      this.unaryFunction = unaryFunction; }

    /** {@inheritDoc} */
    @Override
    public Value call(List<Value> operands) {
      if (operands.size() != 2)
        throw new UnspecifiedBehaviourException("This function is binary");
      E left = operands.get(0).<E>castAs(kind, Kind.NULL).second;
      E right = operands.get(1).<E>castAs(kind, Kind.NULL).second;
      Optional<G> result = null;
      if (left == null && right == null)
        result = Optional.empty();
      else if (left == null || right == null) {
        E middle = (right == null) ? left : right;
        result = unaryFunction.apply(middle);
      }
      else
        result = Optional.of(internalFunction.apply(left,right));
      return result.map(x -> Values.of(x)).orElse(new Value());
    }

  }


}
