/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.language.expression.*;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.RecordComparator;

import uk.ac.ed.cyphsem.utils.*;

import java.lang.RuntimeException;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;


/** Implements <tt>ORDER BY</tt> clauses.
  * It holds a list of {@link SubClause}s that will be applied position-wise.
*/
public class OrderBy implements ReadingClause {

  /** Represents items of a <tt>ORDER</tt> BY clause*/
  public static class OrderedExpression extends Pair<Boolean,Expression> {
    /** Indicate whether this OrderedExpression is ascending or descending */
    public boolean ascending() { return first; }
    /** Expression used for ordering. */
    public Expression exp() { return second; }
    /** Constructs an ascending {@link OrderedExpression} holding given {@link Expression}. */
    public OrderedExpression(Expression exp) { super(true,exp); }
    /** Construcs an ascending or descending {@link OrderedExpression}. */
    public OrderedExpression(boolean ascending, Expression exp)
    { super(ascending,exp); }
    @Override public String toString() {
      return exp().toString()+(ascending()?"":" DESC");
    }
  }

  /** Interface for the subclauses of OrderBy. */
  public static interface SubClause {

    /** Filters some record from given lists.
      * @param record_list List of record to filter; may be modified during computation.
      * @param comparator
      * @param interruptor Interruptor provided by main thread to signal this thread to interrupt the computation.
      * @return The remaining {@link Record}s.
      * @throws InterruptedException If computation was interrupted due to a call
      * to {@link Interruptor#check}.
    ***/
    public List<Record> filter
      ( List<Record> record_list,
        RecordComparator comparator,
        Interruptor interruptor)
    throws InterruptedException;

    /** Checks whether keeping/filtering-out the first {@code i} records of
    {@code record_list} causes a non-deterministic behaviour, in which case
    a warning is pushed to {@code interruptor}.
    */
    public static void checkDeterminism
      ( RecordComparator comparator, Interruptor interruptor,
        List<Record> record_list, int i )
    throws InterruptedException
    {
      if (0< i && record_list.size() > 2 && i < record_list.size()) {
        int comparison
            = comparator.compare(record_list.get(i),record_list.get(i-1));
        if (comparison == 0)
          interruptor.pushWarning("", Interruptor.Issue.NON_DETERMINISM);
      }
    }
  }

  /** Represents a <tt>SKIP</tt> subclause of an <tt>ORDER BY</tt> clause.
  */
  public static class Skip implements SubClause {

    /** Expression used to constructs this {@link Skip} */
    protected Expression exp;

    /** Held integer. */
    protected int i;

    /** Constructs a OrderBy.SubClause from given expression.
      * Statically evaluate {@code exp} and cast it has an BigInteger and converts it to an int.
      * @throws UndefinedNameExpression if given expression has any variable.
      * @throws DynamicCastException if given expression does not evaluate to a {@link Value} of {@link Kind Kind} {@link Kind#INT INT}.
      * @see Expressions#staticEvaluation
      * @see Value#asInteger
      * @see BigInteger#intValue
      */
    public Skip(Expression exp) {
      this.exp = exp;
      this.i = Expressions.staticEvaluation(exp).asInteger().intValue();
    }

    /** {@inheritDoc}
      * <br><br>
      * Implementation of {@link Skip} removes the first {@link #i} elements of given {@link Record} list.
    */
    @Override public List<Record> filter
      ( List<Record> record_list,  RecordComparator comparator,
        Interruptor interruptor)
    throws InterruptedException
    {
      interruptor.check();
      int start = i;
      if (start < 0)
        start = 0;
      if (start > record_list.size())
        start = record_list.size();
      SubClause.checkDeterminism(comparator, interruptor, record_list, start);
      return record_list.subList(start,record_list.size());
    }

    @Override public
    String toString()
    { return "SKIP "+exp.toString(); }
  }

  /** Represents a <tt>LIMIT</tt> subclause of an <tt>ORDER BY</tt> clause. **/
  public static class Limit implements SubClause {
    Expression exp;
    public Limit(Expression exp) { this.exp = exp; }

    @Override public
    List<Record> filter
      ( List<Record> record_list,  RecordComparator comparator,
        Interruptor interruptor)
    throws InterruptedException
    {
      interruptor.check();
      int end = Expressions.staticEvaluation(exp).unsafeCastAsInteger()
                                                 .intValue();
      int size = record_list.size();
      if (end > size)
        end = record_list.size();
      SubClause.checkDeterminism(comparator, interruptor, record_list, end);
      return record_list.subList(0,end);
    }
    @Override public
    String toString()
    { return "LIMIT "+exp.toString(); }
  }

  /** Ordered expressions defining how to order input Table. */
  List<OrderedExpression> orderDescription;

  /** Subclauses of this {@link OrderBy}. */
  List<SubClause> subclauses;

  /** Constructs an {@link OrderBy} wrapping given parameter. */
  public OrderBy (List<OrderedExpression> od,List<SubClause> sc)
  { orderDescription=od; subclauses=sc; }

  @Override
  public String toString() {
    String res = "";
    String sep = "";
    res += "ORDER BY ";
    List<Object> items = new ArrayList<>(subclauses.size()+1);
    items.add(StringOf.iterable(orderDescription,"ORDER BY","",", "));
    items.addAll(subclauses);
      return StringOf.iterable(items,"\n  ");
  }

  @Override
  public
  Table apply (Graph graph, Table table, Interruptor interruptor)
  throws InterruptedException
  {
    return (new Table(applyKeepingOrder(graph, table, interruptor)));
  }

  public
  List<Record>
  applyKeepingOrder (Graph graph, Table table, Interruptor interruptor)
  throws InterruptedException
  {
    interruptor.check();
    List<Record> record_list = new ArrayList<>(table);
    Collections.shuffle(record_list);
    RecordComparator comparator = new RecordComparator(graph,orderDescription);
    Collections.sort(record_list, comparator);
    for (SubClause subclause : subclauses)
      record_list = subclause.filter(record_list,comparator, interruptor);
    return record_list;
  }

}
