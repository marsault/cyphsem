/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.Parameters;
import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;
import uk.ac.ed.cyphsem.exception.NonDeterminismException;


import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.value.Values;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Label;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Id;
import uk.ac.ed.cyphsem.datamodel.graph.RelationType;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;

import uk.ac.ed.cyphsem.language.pattern.PathPattern;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern;
import uk.ac.ed.cyphsem.language.pattern.RelationPattern.Direction;
import uk.ac.ed.cyphsem.language.pattern.NodePattern;
import uk.ac.ed.cyphsem.language.pattern.NamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.UnnamedPathPattern;
import uk.ac.ed.cyphsem.language.pattern.SimpleRelationPattern;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.Name;

import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.function.Function;

/** Represents a <tt>REMOVE</tt> clause.
* <br>
* <tt>REMOVE</tt> items are of two kinds, stored respectively in fields {@link #properties}
and {@link #labels}.
*/


public class RemoveClause implements WritingClause {

  /** Properties to be removed.  Each key of the map should evaluate at runtime to a {@link Node} or a {@link Relation}. */
  protected Map<Expression,Set<PropertyKey>> properties = new HashMap<>();

  /** Labels to be removed.  Each key of the map should evaluate at runtime to a {@link Node}.*/
  protected Map<Expression,Set<Label>> labels = new HashMap<>();

//   public Map<Expression,Set<PropertyKey>> properties() { return properties; }
//   public Map<Expression,Set<Label>> labels() { return labels; }

  /** Constructs a {@link RemoveClause} from lists of pairs.
    * Among parameters, {@link Expression}s, {@link PropertyKey}s and {@link Label}s are held without
    * copy while {@link List}s and {@link Pair}s are not kept at all.*/
  public RemoveClause
    ( List<Pair<Expression,PropertyKey>> x, List<Pair<Expression,Label>> y )
  {
    for (Pair<Expression,PropertyKey> pair : x)
      addProperty(pair.first,pair.second);
    for (Pair<Expression,Label> pair : y)
      addLabel(pair.first,pair.second);
  }

  /** Constructs a {@link RemoveClause} from a {@link Collection} of pairs. */
  public RemoveClause
    ( Collection<Pair<Expression,Either<List<Label>,List<PropertyKey>>>> coll)
  {
    for (Pair<Expression,Either<List<Label>,List<PropertyKey>>> pair : coll)
      pair.second.consumeBy
        ( x -> addLabels(pair.first, x) ,
          x -> addProperties(pair.first, x) );
  }

  /** Adds a (Expression,PropertyKey) to the items to delete. */
  public void addProperty(Expression name,PropertyKey key) {
    if (!properties.containsKey(name))
      properties.put(name, new HashSet<>());
    properties.get(name).add(key);
  }

  /** Adds multiple properties with the same expression to the items to delete. */
  public void addProperties(Expression name, Collection<? extends PropertyKey> keys) {
    if (!properties.containsKey(name))
      properties.put(name, new HashSet<>());
    properties.get(name).addAll(keys);
  }

  /** Adds a (Expression,PropertyKey) to the items to delete. */
  public void addLabel(Expression name, Label label) {
    if (!labels.containsKey(name))
      labels.put(name, new HashSet<>());
    labels.get(name).add(label);
  }

  /** Adds multiple labels with the same expression to the items to delete. */
  public void addLabels(Expression name, Collection<? extends Label> labels) {
    if (!this.labels.containsKey(name))
      this.labels.put(name, new HashSet<>());
    this.labels.get(name).addAll(labels);
  }

  @Override
  public QueryData apply (QueryData qd, Interruptor interruptor)
  throws InterruptedException
  {
    Map<Id,Set<PropertyKey>> property_changes = new HashMap<>();
    Map<Node,Set<Label>> label_changes = new HashMap<>();
    for (Record record : qd.table()) {
      for (Map.Entry<Expression,Set<PropertyKey>> entry : properties.entrySet()) {
        Id id = Expressions.evaluateToId(entry.getKey(), qd.graph(), record);
        if (! property_changes.containsKey(id))
          property_changes.put(id, new HashSet<>());
        property_changes.get(id).addAll(entry.getValue());
      }
      for (Map.Entry<Expression,Set<Label>> entry : labels.entrySet()) {
        Node node = Expressions.evaluateToNode(entry.getKey(), qd.graph(), record);
        if (! label_changes.containsKey(node))
          label_changes.put(node, new HashSet<>());
        label_changes.get(node).addAll(entry.getValue());
      }
    }
    for (Map.Entry<Id,Set<PropertyKey>> entry : property_changes.entrySet())
      for(PropertyKey key : entry.getValue())
        entry.getKey().removeProperty(key);
    for (Map.Entry<Node,Set<Label>> entry : label_changes.entrySet())
      for(Label label : entry.getValue())
        entry.getKey().removeLabel(label);
    return qd;
  }

  @Override
  public String toString()
  {
    List<String> items = new ArrayList<>();
    for (Map.Entry<Expression,Set<PropertyKey>> entry : properties.entrySet())
      items.add(entry.getKey()+StringOf.iterable(entry.getValue(),".","","."));
    for (Map.Entry<Expression,Set<Label>> entry : labels.entrySet())
      items.add(entry.getKey()+StringOf.iterable(entry.getValue(),":","",":"));
    return
      "REMOVE"+StringOf.iterable(items," ","",", ");
  }
}
