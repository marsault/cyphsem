/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.language.query.Query;
import uk.ac.ed.cyphsem.language.query.QueryData;

import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.table.Table;


/**
 * Implements expressions of the form  <tt>EXISTS &lt;subquery&gt;<tt><br>
 * This class is still under developpement since it is not supported by Cypher 9.
 */
public class ExistentialSubqueryExpression implements Expression {

  protected Query query;

  public ExistentialSubqueryExpression (Query query) {
    this.query = query;
  }

  @Override
  public Value evaluate(Graph graph, Record record) {
    return new Value(!query.apply(new QueryData(graph, new Table(record))).table().isEmpty());
  }

}
