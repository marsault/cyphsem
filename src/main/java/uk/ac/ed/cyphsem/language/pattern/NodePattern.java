/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.Utils.Policy;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;
import uk.ac.ed.cyphsem.exception.UndefinedNameException;

import uk.ac.ed.cyphsem.language.expression.ExplicitMapExpression;
import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;
import uk.ac.ed.cyphsem.language.expression.Functions;
import uk.ac.ed.cyphsem.language.expression.FunctionalExpression;
import uk.ac.ed.cyphsem.language.expression.KeyExpPair;
import uk.ac.ed.cyphsem.language.expression.UnaryMapExpression;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Label;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;
import uk.ac.ed.cyphsem.datamodel.graph.Node;

import uk.ac.ed.cyphsem.language.pattern.PathPattern.VarStatus;

import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.*;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;



public class NodePattern extends UnnamedPathPattern {

//   Name name;
  List<Label> labels;
  ExplicitMapExpression map;

  public NodePattern() { this(null, null, null); }

  public NodePattern(Name name) { this(name, null, null); }
  public NodePattern(List<Label> labels) { this(null, labels, null); }
  public NodePattern(ExplicitMapExpression e) { this(null, null, e); }

  public NodePattern(Name name, List<Label> labels)
    { this(name, labels, null); }
  public NodePattern(Name name, ExplicitMapExpression e)
    { this(name, null, e); }
  public NodePattern(List<Label> labels, ExplicitMapExpression e)
    { this(null, labels, e); }



  public
  NodePattern
      (Name name, List<Label> labels, ExplicitMapExpression map)
  {
    this.name = name;
    if (labels != null)
      this.labels = labels;
    else
      this.labels = new ArrayList<Label>();
    this.map = map;
  }

//   () {};
  @Override
  public Map<Name,VarStatus>
  assertSuitable(
      boolean forUpdate,
      Map<Name,VarStatus> varStatus
    )
  throws AssertionFailedException
  {
    return assertSuitable(forUpdate,varStatus,VarStatus.NODE);
  }

  public boolean isEmpty() {
    return ( (labels == null || labels.isEmpty())
             && (map == null || map.isEmpty() ) );
  }

  @Override
  public
  List<AssignmentResult>
  computeAssignment
    (Graph graph, AssignmentResult partial_ar, Interruptor i )
  throws InterruptedException
  {
    i.check();
    AssignmentResult newAR = partial_ar;
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();

    Node x = partial_ar.path.dst();
    for (Label l: labels)
      if (!x.hasLabel(l))
        return result;

    if ( name != null ) {
      try {  newAR = newAR.verifyOrAdd(name, new Value(x));  }
      catch (RecordInconsistencyException e) { return result; }
    }

    if (map != null)
      for (KeyExpPair pair : map) {
        PropertyKey key = pair.key;
        Expression left = pair.exp;
        Expression right = new UnaryMapExpression(new Value(x), key);
        boolean b = true;
        try {
          if (! Expressions.semEquals(left, right, graph, newAR.record,
                                      Policy.PROPAGATE) )
            return result;
        } catch (UndefinedNameException e) {
          newAR = newAR.addPostCondition(new FunctionalExpression(Functions.EQ,left,right));
        }
      }

    result.add(newAR);
    return result;
  }

  @Override
  public int length() { return 0; }

  @Override
  public void addTopLevelVariables(Set<Name> temp_set) {
    if (name != null)
      temp_set.add(name);
  }

  @Override
  public String toString() {
    List<Object> parts = new ArrayList<>(3);
    //Name
    if (name != null)
      parts.add(name);
    //Labels
    if (labels != null && labels.size() !=0)
      parts.add(StringOf.iterable(labels,":","",":"));
    //Property map
    if (map != null && !map.isEmpty())
      parts.add(map);
    //Concatenate
    return StringOf.iterable(parts,"(",")"," ");
  }

  public Name getName() {  return name;  }
  public Name name() {  return name;  }

  public List<Label> getLabels() {  return labels;  }

  public ExplicitMapExpression getMap() {  return map;  }

  public List<UnnamedPathPattern> serialize() {
    List<UnnamedPathPattern> res = new LinkedList<>();
    res.add(this);
    return res;
  }
}
