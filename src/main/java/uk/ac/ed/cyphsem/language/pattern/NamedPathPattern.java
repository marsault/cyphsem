/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.exception.AssertionFailedException;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;


import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;
import uk.ac.ed.cyphsem.exception.RecordInconsistencyException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;


public class NamedPathPattern extends PathPattern {
//   Name name;
  UnnamedPathPattern pattern;

  public NamedPathPattern(Name name, UnnamedPathPattern pattern) {
                                    this.name = name;  this.pattern = pattern; }

  @Override
  public List<AssignmentResult> computeAssignment
    (Graph graph, AssignmentResult partialAR, Interruptor i)
    throws InterruptedException
  {
  List<AssignmentResult> l
      = pattern.computeAssignment(graph, partialAR,i);
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();


    for (AssignmentResult ar : l) {
      try {  result.add(ar.verifyOrAdd(name, new Value(ar.path)));  }
      catch (RecordInconsistencyException e) { }
    }

    return result;
  }

  public Name getName() { return name; }

  @Override
  public int length() { return pattern.length(); }

  @Override
  public String toString() {
    return name+"="+pattern;
  }
  @Override
  public String toThisString() {
    return name+"=...";
  }

  @Override
  public void addTopLevelVariables(Set<Name> set) {
    if (name != null)
      set.add(name);
    pattern.addTopLevelVariables(set);
  }

  @Override
  public List<UnnamedPathPattern> serialize() { return pattern.serialize(); }

  @Override
  public Map<Name,VarStatus> assertSuitable
    (
      boolean forUpdate,
      Map<Name,VarStatus> varStatus
    ) throws AssertionFailedException
  {
    assertSuitable(forUpdate, varStatus, VarStatus.PATH);
    pattern.assertSuitable(forUpdate, varStatus);
    return varStatus;
  }

  @Override
  public boolean isEmpty() {
    return ((pattern == null) || pattern.isEmpty());
  }
}
