/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.table.Record;

import java.util.Map;

/** Implements expressions of the form <tt><var>e</var>.<var>k</var></tt>,
  * where <var>e</var> is an {@link Expression} and <var>k</var> is a
  * {@link PropertyKey}.  Expression <var>e</var> is expected to evaluate to a
  * {@link Kind#NODEID}, a {@link Kind#RELID} or a {@link Kind#MAP}.
***/

public class UnaryMapExpression implements Expression {

  private final Expression exp;
  private final PropertyKey key;

  /** Constructs a UnaryMapExpression, wrapping the parameters. */
  public UnaryMapExpression(Expression exp, PropertyKey key) {
    this.exp=exp;
    this.key=key;
  }

  @Override
  public Value evaluate(Graph graph, Record record) {
    Pair<Kind,Object> pair = Expressions.<Object>evaluateTo(
        exp, graph,record, Kind.NODEID, Kind.RELID, Kind.MAP, Kind.NULL);
    switch (pair.first) {
      case NODEID:
      case RELID:
        return ((Id) pair.second).getPropertyValue(key);
      case MAP:
        @SuppressWarnings("unchecked")
        Map<PropertyKey,Value> m = (Map<PropertyKey,Value>) pair.second;
        if(m.containsKey(key))
          return m.get(key);
        else
          return (new Value());
      case NULL:
        return (new Value());
      default:
        throw new UnreachableStatementError();
    }
  }

  @Override
  public String toString() {
    return exp.toString()+"."+key.toString();
  }
}
