/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query.items;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

/** Represents an item for a {@link SetClause}.
  * Different form of such items are the different static internal class, separated in the two following kinds.
  * <ul><li>
  * {@link PropertyItem} represents items modifying properties of a {@link Node} or {@link Relation}; it contains:
  * <ul><li>
  * {@link PropertyCopy}, representing items such as <tt>e1.k=e2</tt>;
  * </li><li>
  * {@link PropertyOverwrite}, representing items such as <tt>e1=e2</tt> or <tt>e1+=e2</tt>;
  * </li></ul>
  where e1,e2 are {@link Expression} and k is a {@link PropertyKey}.
  * </li><li>
  * {@link LabelItem} represents items adding {@link Label}s to a {@link Node};
  it contains only {@link LabelAdd} representing items such as <tt>e1:l1:l2=e2</tt>,
  where e1,e2 are {@link Expression} and l1,l2 is a {@link PropertyKey}.
  * </li></ul>
***/
public interface SetItem {

  /** Adds to parameter {@code c} the changes induced by this {@link SetItem} in the context of given {@link Graph} and {@link Record}.
    * @param c
    * @param g
    * @param r
    * @throws NonDeterminismException if field {@link SetChanges#permissive} of {@code c} is {@code false} and computed changes are in contradiction.
  ***/
  public void addChanges(SetChanges c, Graph g, Record r);


  /** Adds to parameter {@code c} the changes induced by this {@link SetItem} in the context of given {@link Graph} and {@link Table}.
    * <br>
    * Implementation in {@link SetItem} simply iterate over {@code t} and calls abstract method {@link #addChanges}.
    * @param c
    * @param g
    * @param t
    * @throws NonDeterminismException if field {@link SetChanges#permissive} of {@code c} is {@code false} and computed changes are in contradiction.
  ***/
  default public void addChanges(SetChanges c, Graph g, Table t) {
    for (Record r : t)
      addChanges(c,g,r);
  }


  /** Represents {@link SetClause} items modifying properties of a {@link Node} or {@link Relation}.*/
  public static interface PropertyItem extends SetItem {
    /** {@inheritDoc}
      * <br><br>
      * Implementation in {@link PropertyItem} calls {@code c.}{@link SetChanges#addProp addProp}{@code (t.first, t.second, t.third)} for each triplet {@code t} returned by {@code evaluate(g,r)}.
    */
    default public void addChanges(SetChanges c, Graph g, Record r) {
      for (Triplet<Id,PropertyKey,Value> t : evaluate(g,r))
        c.addProp(t.first, t.second, t.third);
    }
    /** Computes the property changes induced by this item in the context of given {@link Graph} and {@link Record}.
      * @param g
      * @param r
    **/
    public Collection< Triplet<Id,PropertyKey,Value> > evaluate (Graph g, Record r);
  }


  /** {@link LabelItem} represents items adding {@link Label}s to a {@link Node}. **/
  public static interface LabelItem extends SetItem {
    /** {@inheritDoc}
      * <br><br>
      * Implementation in {@link PropertyItem} calls {@code c.}{@link SetChanges#addProp addProp}{@code (p.first, p.second)} for each pair {@code p} returned by {@code evaluate(g,r)}.
    ***/
    default public void addChanges(SetChanges c, Graph g, Record r) {
      for (Pair<Node,Label> p: evaluate(g,r))
        c.addLabel(p.first, p.second);
    }

    /** Computes the label changes induced by this item in the context of given {@link Graph} and {@link Record}.
      * @param g
      * @param r
    ***/
    public Collection<Pair<Node,Label>> evaluate (Graph g, Record r);
  }

  /** Represents items copying the properties from a given {@link Node} or {@link Relation} to another {@link Node} or {@link Relation}.
    * <br>
    * Field {@link #first} (inherited from {@link Triplet}) contains an {@link Expression} that should evaluate to an {@link Id} and will be the target {@link Node} or {@link Relation}.
    * <br>
    * Field {@link #second} contains a {@link Boolean} that indicate if a property existing in target should be overwritten by the property with the same key of source.
    * <br>
    * Field {@link #third} contains an {@link Expression} that should evaluate to an {@link Id} and will be the source {@link Node} or {@link Relation}.
  **/
  public static class PropertyCopy extends Triplet<Expression,Boolean,Expression>
  implements PropertyItem {
    /** Returns the {@link Expression} corresponding to the target {@link Node}/{@link Relation}. */
    public Expression target() {return first; }
    /** Returns the {@link Expression} corresponding to the source {@link Node}/{@link Relation}. */
    public Expression source() { return third; }
    /** Indicate whether a property existing in target should be overwritten by the the property with the same key of source. */
    public boolean isPlusEqual() { return second; }

    public PropertyCopy(Expression l, Expression r, boolean b)    {  super(l,b,r);}

    /** {@inheritDoc}
      * * @throws DynamicCastException if fields {@link #first} or {@link #third} do not evaluate to an {@link Id}.
    ***/
    public Collection< Triplet<Id,PropertyKey,Value> > evaluate (Graph g, Record rec)
    {
      final Id l = Expressions.evaluateToId(target(), g, rec);
      final Id r = Expressions.evaluateToId(source(), g, rec);
      return
        Utils.stream(r.propertyKeySet(),
          x -> x.filter( e -> (!isPlusEqual() || !l.hasPropertyKey(e)))
                .map( e -> new Triplet<Id,PropertyKey,Value>(l, e, r.getPropertyValue(e))));
    }
    @Override
    public String toString() {
      return target()+(isPlusEqual()?"+=":"=")+source();
    }
  }

  /** Represents items overwriting one or several property of a {@link Node} or {@link Relation}, such as <tt>e1.k=e2</tt> (e1,e2 are{@link Expression}s, k is a {@link PropertyKey}).
    * <br>
    * Field {@link #first} (inherited from {@link Triplet}) contains an {@link Expression} that should evaluate to an {@link Id} and will be the target {@link Node} or {@link Relation}.
    * <br>
    * Field {@link #second} contains a {@link List}&lt;{@link PropertyKey}&gt; that are the keys of the property that will be modified in the source.
    * <br>
    * Field {@link #third} contains an {@link Expression} that should evaluate to a {@link Value#isStorable storable} {@link Value} and that will be the new property value for all the keys in {@link #second}.
  **/
  public static class PropertyOverwrite extends Triplet<Expression,List<PropertyKey>,Expression>
  implements PropertyItem {


    /** Constructs a {@link PropertyOverwrite} wrapping its arguments. */
    public PropertyOverwrite(Expression l, List<PropertyKey> r, Expression x)
    { super(l,r,x); }

    /** {@inheritDoc}
      * @throws DynamicCastException if field {@link #first} does not evaluate to an {@link Id}.
      * @throws AssertionFailedException if field {@link #third} doest not evaluate to a {@link Value#isStorable storable} {@link Value}.
    ***/
    public List< Triplet<Id,PropertyKey,Value> >  evaluate (Graph g, Record rec) {
      Id i = Expressions.evaluateToId(first, g, rec);
      Value v = third.evaluate(g,rec);
      Values.assertStorable(v);
      return Utils.map( second, x -> new Triplet<>(i,x,v));
    }

    @Override public String toString()
    { return (first + StringOf.iterable(second, ".","",".") + "=" + third); }
  }

  /** Represents items adding several labels to a {@link Node}, such as <tt>e1:l1:l2=e2</tt> (e1,e2 are{@link Expression}s and l1,l2 are {@link Label}s).
    * <br>
    * Field {@link #first} (inherited from {@link Pair}) contains an {@link Expression} that should evaluate to a {@link Node}, to which extra labels will be added.
    * <br>
    * Field {@link #second} contains the {@link List} of {@link Label} to add.
  **/
  public static class LabelAdd extends Pair<Expression,List<Label>>
  implements LabelItem {
    /** Constructs a {@link LabelAdd} that wraps the given parameters. */
    public LabelAdd(Expression l, List<Label> r)    {  super(l,r);  }

    /** {@inheritDoc}
      * @throws DynamicCastException if field {@link #first} does not evaluate to a {@link Node}.
    ***/
    public Collection< Pair<Node,Label> > evaluate (Graph g, Record r) {
      Node n = Expressions.evaluateToNode(first, g, r);
      return Utils.map( second, x -> new Pair<>(n,x));
    }
    @Override
    public String toString() {
      return first.toString() +
             StringOf.iterable(second, ":","",":");
    }
  }

}
