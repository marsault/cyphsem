/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.pattern;



/** This interface implements iteration ranges.
  * It is basically a counter that may be incremented (with {@link #next}),
  * decremented (with {@link #previous}), reset to 0 (with {@link #reset}).
  * Methods {@link #mayStop} indicate whether the current state of the counter
  * is within range.
  * Method {@link #mayContinue} indicate whether sufficiently many calls to
  * {@link #next}, will eventually make {@link #mayStop} return {@code true}.
  */
public interface Iteration {

  /** Resets this object. */
  void reset();

  /** Is current state of this objects */
  boolean mayStop();

  /** Indicates whether sufficiently many calls to
  * {@link #next}, will eventually make {@link #mayStop} return {@code true}.
  **/
  boolean mayContinue();

  /** Increases iteration by one step. */
  void next();

  /** Decreases iteration by one step. */
  void previous();

  /** Get the current iteration step. */
  int getCurrent();

  /** Iteration range of the form <tt>*..</tt><var>d<var>, for some integer <var>d</var>. */
  public static class Fixed implements Iteration {
    private int total;
    private int current = 0;

    public Fixed (int i) { total = i; }
    @Override public void next() {  current += 1; }
    @Override public void previous() {  current -= 1; }
    @Override public void reset() { current = 0; }
    @Override public boolean mayStop() { return (current == total); }
    @Override public boolean mayContinue() { return (current < total); }
    @Override public String toString() { return ("*"+total); }
    public int getCurrent() {return (current); }
  }

  /** Iteration range of the form <tt>*</tt><var>d<var>, for some integer <var>d</var>. */
  public static class Until implements Iteration {
    private int total;
    private int current = 0;
    public Until (int i) { total = i; };
    @Override public void next() {  current += 1; }
    @Override public void previous() {  current -= 1; }
    @Override public void reset() { current = 0; }
    @Override public boolean mayStop() { return (current >= 1); }
    @Override public boolean mayContinue() { return (current < total); }
    @Override public String toString() { return ("*.."+total); }
    public int getCurrent() {return (current); }
  }

  /** Iteration range of the form <tt>*<var>d<var>..</tt>, for some integer <var>d</var>. */
  public static class From implements Iteration {
    private int total;
    private int current = 0;
    public From (int i) { total = i; };
    @Override public void next() {  current += 1; }
    @Override public void previous() {  current -= 1; }
    @Override public void reset() { current = 0; }
    @Override public boolean mayStop() { return (current >= total); }
    @Override public boolean mayContinue() { return true; }
    @Override public String toString() { return ("*"+total+".."); }
    public int getCurrent() {return (current); }
  }

  /** Universal iteration range, that is, of the form: <tt>*</tt> */
  public static class Star extends From {
    public Star() { super(1); }
    @Override public String toString() { return ("*"); }
  }

  /** Iteration range of the form <tt>*<var>d1<var>..<var>d2<var></tt>, for
    * some integer <var>d1</var>,<var>d2</var>.
  **/
  public static class Between implements Iteration {
    private int start;
    private int end;
    private int current = 0;
    public Between (int start, int end) { this.start=start; this.end=end; }
    @Override public void next() {  current += 1; }
    @Override public void previous() {  current -= 1; }
    @Override public void reset() { current = 0; }
    @Override public boolean mayStop() { return (current >= start); }
    @Override public boolean mayContinue() { return (current < end); }
    @Override public String toString() { return ("*"+start+".."+end); }
    public int getCurrent() {return (current); }
  }
}
