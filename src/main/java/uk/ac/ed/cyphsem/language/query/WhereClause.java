/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;

import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.language.expression.Expressions;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Name;
import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.Utils.Policy;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/** Implements Cypher clauses (and subclauses) of the form <tt>WHERE <var>e</var></tt>,
  * where <var>e</var> is an {@link Expression} expected to evaluate to a {@link Value} of {@link Kind Kind} {@link Kind#BOOL BOOL} or {@link Kind#NULL NULL}.
***/
public class WhereClause implements StandardReadingClause {

  /** Expression used to filter lines of the input table.
    * Expected to evaluate to a {@link Value} of {@link Kind Kind} {@link Kind#BOOL BOOL} or {@link Kind#NULL NULL}.
  ***/
  public Expression expression;

  /** Constructs a {@link WhereClause} that wraps given parameter. */
  public WhereClause(Expression expression)
  { this.expression = expression; }

  @Override
  public String toString() {  return  "WHERE "+expression.toString();  }

  public Table apply (Graph graph, Record record, Interruptor interruptor)
  throws InterruptedException
  {
    interruptor.check();
    Value value = expression.evaluate(graph, record);
    Optional<Boolean> b = Optional.empty();
    if (value.isOfKind(Kind.NULL))
      b = Optional.of(false);
    if (value.isOfKind(Kind.BOOL))
      b = Optional.of(value.unsafeCastAsBoolean());
    //if (value.isOfKind(Value.Kind.LIST)) // FIXME: This is to emulate Neo4j
      //b = Optional.of(!value.unsafeCastAsList().isEmpty());

    Optional<Table> result =    b.map(x -> x ? new Table(record) : new Table());
    return result.orElseThrow( () -> new DynamicCastException (
                "WHERE: expression (" + expression +") "
                + "evaluates to (" + value + ") "
                + "of kind (" + value.getKind()+") "
                + "in the context of record "+record+". Expected bool (or list).") );
  }

}
