/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.Interruptor;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.table.Table;
import uk.ac.ed.cyphsem.table.Record;

import java.util.Collection;

import java.util.Map;

/**
This is an helper interface for reading clauses that are
{@link Record}-to-{@link Table}.
For computing the output {@link Table}, we consider all records
independently and collect their respective outputs in a new {@link Table}.
*/
public interface StandardReadingClause extends ReadingClause {

//   public ReadingClause.Result evaluate(Graph graph, Interruptor i)
//   throws InterruptedException;


  /** {@inheritDoc}
    * <br><br>
    * Implemented in {@link StandardReadingClause} calling new abstract
    * method {@link StandardReadingClause#apply(uk.ac.ed.cyphsem.datamodel.graph.Graph,uk.ac.ed.cyphsem.table.Record,uk.ac.ed.cyphsem.Interruptor) apply}
    * and collecting their returned values in a new {@link Table}.
    * @param graph {@inheritDoc}
    * @param table {@inheritDoc}
    * @param interruptor {@inheritDoc}
    * @return A {@link QueryData}, the graph of which is the same as the one in parameter{@code qd}.
  */
  default public Table apply (Graph graph, Table table, Interruptor interruptor)
  throws InterruptedException {
    interruptor.check();
    Table result = new Table();
    for (Record record : table.recordSet())
      result.addAll(apply(graph, record, interruptor), table.multiplicityOf(record));
    return result;
  }

  /** Applies the semantics function associated with this
    * {@link StandardReadingClause} to a single {@link Record},
    * or throws a {@link InterruptedException} if computation is interrupted
    * through given {@link Interruptor}.
    * <br>
    * The general contract of this interface is that its implementation should
    * not modify input {@code Graph} or {@link Record}.
    * @param graph Input {@link Graph}
    * @param record Input {@link Record}
    * @param interruptor Interruptor provided by main thread to signal this thread
    * to interrupt the computation.
    * @return Output records.
    * @throws InterruptedException If computation was interrupted, after a call
    * to {@link Interruptor#check}.
    * @fordev Given {@link Interruptor} should be {@link Interruptor#check checked} reasonably
    * often during the computation of output {@link QueryData}
  **/
  public Collection<? extends Record> apply (Graph graph, Record record, Interruptor interruptor) throws InterruptedException;

//   public static abstract class AbstractHelper implements Clause {
//
  //   @Override
  //   public abstract ReadingClause.Result evaluate(Graph graph, Interruptor i)
  //   throws InterruptedException;
  //
  //   public ReadingClause.Result evaluate(Graph graph) {
  //       try {  return evaluate(graph, new Interruptor());  }
  //       catch (InterruptedException e) {  throw new UnreachableStatementError();  }
  //     }
  //   }

}
