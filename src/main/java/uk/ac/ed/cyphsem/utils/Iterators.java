/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import uk.ac.ed.cyphsem.utils.ConcurrentIterable;

import java.lang.Iterable;

/** Interface collecting functions regarding iterators.
  *
**/
public interface Iterators {


  /** Combines two {@link Iterable} into one {@link Iterable} of pairs. */
  public static <L,R>
  ConcurrentIterable<L,R> combine(Iterable<L> i1, Iterable<R> i2) {
    return new ConcurrentIterable<L,R>(i1,i2);
  }

  /** Combines two {@link Iterable} into one {@link Iterable} of pairs,
    * with default values if they had different sizes. */
  public static <L,R>
  ConcurrentIterable<L,R> combine(Iterable<L> i1, Iterable<R> i2, L left, R right) {
    return new ConcurrentIterable<L,R>(left, i1, i2, right);
  }

    /** Combines two {@link Iterable} that contain elements of the same type
      * into one {@link Iterable}&lt;{@link OptPair}&gt;, the same default values to pad the smaller
      * {@link Iterable}. */
  public static <B>
  ConcurrentIterable<B,B> combine(Iterable<B> i1, Iterable<B> i2, B default_value) {
    return new ConcurrentIterable<B,B>(default_value, i1, i2, default_value);
  }


}
