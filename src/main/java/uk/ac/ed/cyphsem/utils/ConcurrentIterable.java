/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import uk.ac.ed.cyphsem.utils.OptPair;

import java.lang.Iterable;
import java.lang.RuntimeException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import java.util.Optional;

/**
  * Utility for concurrently iterating over two {@link Iterable}s.
  * The two given {@link Iterable}s need not be of the same size, hence the use
  * of {@link OptPair} where either element may be absent.
  * <br> Default elements may be provided to fill either {@link Iterable}.
**/
public class ConcurrentIterable<L,R> implements Iterable<OptPair<L,R>> {

  /** Left iterable */
  protected Iterable<? extends L> _iterableLeft;

  /** Right iterable */
  protected Iterable<? extends R> _iterableRight;

  /** Default element used to fill the pair if left iterable runs out first. */
  protected Optional<L> _defaultLeft = Optional.empty();

  /** Default element used to fill the pair if right iterable runs out first. */
  protected Optional<R> _defaultRight = Optional.empty();

  /** Constructs a ConcurrentIterable from given iterables. */
  public ConcurrentIterable (Iterable<? extends L> i1, Iterable<? extends R> i2) {
    _iterableLeft = i1;
    _iterableRight = i2;
  }

  /** Constructs a ConcurrentIterable from given iterables and a default element for left iterable. */
  public ConcurrentIterable (L defaultLeft, Iterable<? extends L> i1, Iterable<? extends R> i2) {
    this(i1,i2);
    _defaultLeft = Optional.of(defaultLeft);
  }

  /** Constructs a ConcurrentIterable from given iterables and a default element for both iterables. */
  public ConcurrentIterable (L defaultLeft, Iterable<L> i1, Iterable<R> i2, R defaultRight) {
    this(i1,i2);
    _defaultLeft = Optional.of(defaultLeft);
    _defaultRight = Optional.of(defaultRight);
  }

  /** Constructs a ConcurrentIterable from given iterables and a default element for right iterable. */
  public ConcurrentIterable (Iterable<L> i1, Iterable<R> i2, R defaultRight) {
    this(i1,i2);
    _defaultRight = Optional.of(defaultRight);
  }

  /**
    * Returns an anonymous {@link Iterator} wrapping the iterators of
    * wrapped {@link Iterable}.
    * Constructs an {@link OptPair} on the fly from return values from the call to {@code next()}
    * on both wrapper {@link Iterator}.
    * If one {@link Iterator} is out before the other, the {@link OptPair} are
    * filled with the default value if provided.
  */
  @Override public
  Iterator<OptPair<L,R>> iterator()
  {
    return new Iterator<OptPair<L,R>>() {
      Iterator<? extends L> _leftIterator = _iterableLeft.iterator();
      Iterator<? extends R> _rightIterator = _iterableRight.iterator();
      public boolean hasNext() { return (_leftIterator.hasNext() || _rightIterator.hasNext()); }
      public OptPair<L,R> next() {
        if (!hasNext())
          throw new NoSuchElementException("Both Iterable are out of elements.");

        Optional<L> next_left
          = (_leftIterator.hasNext()) ? Optional.of(_leftIterator.next())
                                      : _defaultLeft;
        Optional<R> next_right
          = (_rightIterator.hasNext()) ? Optional.of(_rightIterator.next())
                                       : _defaultRight;
        return new OptPair<L,R>(next_left, next_right);
      }
      public void remove() { _leftIterator.remove(); _rightIterator.remove(); }
    };
  }




}
