/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** This class collects different function to build immutable objects.
***/
public class Immutable {

  /** Builds an immutable set from given elements.
    * Shorthand for: {@code Immutable.of(new HashSet<E>(Arrays.asList(objs)))}.
  ***/
  public static <E> Set<E> setOf(E... elements) {
    return Immutable.of( new HashSet<E>(Arrays.asList(elements)) );
  }

  /** Builds an immutable empty set.
    * Shorthand for: {@link Collections#emptySet Collection.emptySet();} .
  ***/
  public static <E> Set<E> emptySet() {
    return Collections.emptySet();
  }

  /** Builds an immutable list from given elements.
    * Shorthand for: {@code Immutable.of( Arrays.asList(elements) ) }.
  ***/
  public static <E> List<E> listOf(E... elements) {
    return Immutable.of( Arrays.asList(elements) );
  }

  /** Builds an immutable empty list.
    * Shorthand for: {@link Collections#emptyList Collection.emptyList();} .
  ***/
  public static <E> List<E> emptyList() {
    return Collections.emptyList();
  }

  /** Builds an immutable map containing the single mapping given. */
  public static <K,V> Map<K,V> mapOf(K key1, V value1) {
    return _mapOf(key1, value1);
  }

  /** Builds an immutable empty map.
    * Shorthand for: {@link Collections#emptyMap Collection.emptyMap();} .
  ***/
  public static <K,V> Map<K,V> emptyMap() {
    return Collections.emptyMap();
  }

  /** Builds an immutable map containing the two given mappings.
    * Later mappings overwrite earlier ones.
  ***/
  public static <K,V> Map<K,V> mapOf(K key1, V value1, K key2, V value2) {
    return _mapOf(key1, value1, key2, value2);
  }

  /** Builds an immutable map containing the three given mappings.
    * Later mappings overwrite earlier ones.
  ***/
  public static <K,V> Map<K,V> mapOf
    ( K key1, V value1, K key2, V value2, K key3, V value3 )
  {
    return _mapOf(key1, value1, key2, value2, key3, value3);
  }

  /** Builds an immutable map containing the four given mappings.
    * Later mappings overwrite earlier ones.
  ***/
  public static <K,V> Map<K,V> mapOf
    ( K key1, V value1, K key2, V value2, K key3, V value3, K key4, V value4)
  {
    return _mapOf(key1, value1, key2, value2, key3, value3, key4, value4);
  }

  /** Builds an immutable map containing the five given mappings.
    * Later mappings overwrite earlier ones.
  ***/
    public static <K,V> Map<K,V> mapOf
    ( K key1, V value1, K key2, V value2, K key3, V value3, K key4, V value4,
      K key5, V value5)
  {
    return _mapOf(key1, value1, key2, value2, key3, value3, key4, value4,
                  key5, value5 );
  }



  /** Builds an immutable set wrapping given {@link Set}.
    * Shorthand for: {@code Collections.unmodifiableSet(e)}.
  ***/
  public static <E> Set<E> of(Set<? extends E> e) {
    return Collections.unmodifiableSet(e);
  }

  /** Builds an immutable list wrapping given {@link List}.
    * Shorthand for: {@code Collections.unmodifiableSet(e)}.
  ***/
  public static <E> List<E> of(List<? extends E> e) {
    return Collections.unmodifiableList(e);
  }

  /** Builds an immutable set wrapping given {@link Map}.
    * Shorthand for: {@code Collections.unmodifiableMap(map)}.
  ***/
  public static <K,V> Map<K,V> of(Map<? extends K,? extends V> map) {
    return Collections.unmodifiableMap(map);
  }


  /** Internal function building a map of given {@link Object}s.
    * @beware Every parameter at an odd position should be of type K and every parameter at an even position should be of type V.
  ***/
  private static <K,V> Map<K,V> _mapOf(Object... objs) {
    boolean b = true;
    K key = null ;
    Map<K,V> map = new HashMap<>(objs.length/2);
    for (Object o : objs) {
      if (b)
        key = (K) o;
      else
        map.put(key, (V) o);
      b = !b;
    }
    return Immutable.of(map);
  }

}
