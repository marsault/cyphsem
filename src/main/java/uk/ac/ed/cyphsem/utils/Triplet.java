/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import java.util.Objects;

/** Generic class representing a triplet.
  * Wrapped elements are stored in fields {@link #first}, {@link #second}, and {@link #third}
  * Elements are final and public because this class is usually used as easy
  * container, typically for returning three values or as keys for temporary maps.
  * Methods {@code #hashCode} and {@code #equals} are written in such a way
  * that subclasses should not have to rewrite them.
  * @param <F> type of first element
  * @param <S> type of second element
  * @param <T> type of third element
  * @see Pair
*/
public class Triplet<F,S,T> {

  /** First contained element */
  public final F first;

  /** Second contained element */
  public final S second;

  /** Third contained element */
  public final T third;

  /** Constructs a {@link Triplet} from given parameters. */
  public Triplet(F first, S second, T third)
    {  this.first = first; this.second = second; this.third = third;  }

  /** Static function for constructing a {@code Triplet}.
    * Simply calls the constructor.
  **/
  public static <F,S,T> Triplet<F,S,T> of (F first, S second, T third) {
    return new Triplet<F,S,T>(first, second, third);
  }

  @Override @SuppressWarnings("unchecked")
  public boolean equals(Object o) {
    if ( (o == null) || (! getClass().equals(o.getClass())) )
      return false;
    Triplet<?,?,?> other = (Triplet<?,?,?>) o;
    boolean b = (first == null) ? (other.first == null) : first.equals(other.first);
    boolean c = second == null ? other.second == null : second.equals(other.second);
    boolean d = third == null ? other.third == null : third.equals(other.third);
    return (b && c && d);
  }

  /** This function returns an {@code int} used in the computation of the hash
    * code for this function.
    * This allows to differentiate the hash code of an instance of {@code Triplet}
    * containing two elements from and instance of a subclass of {@code Triplet}
    * containing the same two elements.
    * Default implementation returns {@code this.getClass().hashCode()}.
  **/
  protected int seed()   {  return this.getClass().hashCode();  }

  @Override
  public int hashCode()
    {  return Objects.hash(seed(),first,second,third);  }

  @Override
  public String toString() {
    return "("+first+","+second+","+third+")";
  }
}
