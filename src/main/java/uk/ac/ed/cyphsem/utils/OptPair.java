/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import java.util.NoSuchElementException;

import java.util.Optional;

public class OptPair<L,R> {

  /** Enum listing the possible content of an {@link OptPair}. */
  public static enum Presence {
    /** Only left is non-null */
    LEFT,
    /** Only right element is non-null */
    RIGHT,
    /** Both elements are non-null */
    BOTH;
  }

  /** Left element of this pair. */
  protected final L _left;

  /** Right element of this pair. */
  protected final R _right;

  /** Which element of this pair is present. */
  protected final Presence _presence;

/*
    OptPair(L left, R right, Presence option)
    { _left=left; _right=right; _presence=option; }*/

    /** Constructs an {@link OptPair} from two {@link Optional} content.
      * @throws IllegalArgumentException if neither parameter are present.
    **/
    OptPair(Optional<L> left, Optional<R> right)
    {
      if (left.isPresent())
        _left = left.get();
      else
        _left = null;

      if (right.isPresent())
        _right = right.get();
      else
        _right = null;

      if ( (! right.isPresent()) && (! left.isPresent()) )
        throw new IllegalArgumentException("OptPair must have at least one non-empty element.");

      if (right.isPresent()) {
        if (left.isPresent())
          _presence = Presence.BOTH;
        else
          _presence = Presence.RIGHT;
      }
      else
        _presence = Presence.LEFT;

    }

    /** Indicate whether this pair has a left element */
    public boolean hasLeft() { return (_presence != Presence.RIGHT); }

    /** Indicate whether this pair has a right element */
    public boolean hasRight() { return (_presence != Presence.LEFT); }

    /** Indicate whether this pair has both a right and a left element */
    public boolean hasBoth() { return (_presence == Presence.BOTH);  }

    /** Returns the left element of this pair.
      * @throws NoSuchElementException if this pair has no left element.
    **/
    public L left() {
      if (hasLeft())
        return _left;
      throw new NoSuchElementException("Pair has no left element.");
    }

    /** Returns the right element of this pair.
      * @throws NoSuchElementException if this pair has no right element.
    **/
    public R right() {
      if (hasRight())
        return _right;
      throw new NoSuchElementException("Pair has no right element.");
    }

    /** Returns the unique element contained in this pair, cast as given type
      * parameter.
      * @param <U> Class to which contained element will be cast to.
      * @return Unique element contained in this pair, casted to class {@code U}.
      * @throws ClassCastException if contained element cannot be casted to {@code U}.
      * @throws NoSuchElementException if this pair has no right element.
      * @beware This method only works if param {@code <U>} is choosen correctly. Use with caution.
    **/
    @SuppressWarnings("unchecked")
    public <U> U either() {
      if (hasBoth())
        throw new NoSuchElementException("Both elements of pairs are bound.");
      if (hasLeft())
        return ((U) left());
      else
        return ((U) right());
    }


  @Override
  public String toString() {
    String l = hasLeft() ? ""+left() : "" ;
    String r = hasRight() ? ""+right() : "" ;
    return "("+l+","+r+")";
  }

}
