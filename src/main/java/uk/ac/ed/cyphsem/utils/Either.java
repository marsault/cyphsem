/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import java.util.Optional;
import java.util.NoSuchElementException;

import java.util.function.Consumer;
import java.util.function.Function;


/** Class representing union of types, that is containing a first element of a
  * type, or a second element of another type.
  * It is impossible to create an {@link Either} containing by using public/package function.
**/
public class Either<L,R> extends Pair<Optional<L>,Optional<R>> {

  /** Internal constructor for Either from two parameters, exactly one of which
    * should be {@link Optional#empty() empty}.
    * @throws IllegalArgumentException if neither parameter is {@link Optional#empty() empty}.
    * @throws IllegalArgumentException if both parameters are {@link Optional#empty() empty}.
  **/
  protected Either(Optional<L> left, Optional<R> right) {
    super(left,right);
    if ( left.isPresent() == right.isPresent() )
      throw new IllegalArgumentException("Exactly one of the two given elements should be empty.");
  }

  /** Returns an Either where first element is the given parameter, and second
    * element is absent.
    * @throws NullPointerException - if {@code value} is null
  **/
  public static <L,R> Either<L,R> first(L value) {
    return new Either<L,R>(Optional.of(value), Optional.empty());
  }

  /** Returns an Either where second element is the given parameter, and first
    * element is absent.
    * @throws NullPointerException - if {@code value} is null
  **/
  public static <L,R> Either<L,R> second(R value) {
    return new Either<L,R>(Optional.empty(), Optional.of(value));
  }


  /** If first value is present, returns it, otherwise throws {@link NoSuchElementException}.
    * @return the non-null first value wrapped by this {@link Either}.
    * @throws NoSuchElementException if the first value is not present.
  **/
  public L getFirst() {
    return first.get();
  }

  /** If second value is present, returns it, otherwise throws {@link NoSuchElementException}.
    * @return the non-null second value wrapped by this {@link Either}.
    * @throws NoSuchElementException if the first value is not present.
  **/
  public R getSecond() {
    return second.get();
  }

  /** Indicate if the first value is present. **/
  public boolean isFirstPresent() {
    return first.isPresent();
  }

  /** Indicate if the second value is present. **/
  public boolean isSecondPresent() {
    return second.isPresent();
  }

  /** Consume either first or second value, respectively by first or
  second {@link Consumer}
  **/
  public void consumeBy(Consumer<? super L> firstConsumer, Consumer<? super R> secondConsumer) {
    first.ifPresent(firstConsumer);
    second.ifPresent(secondConsumer);
  }

  /** Apply either first or second given function to first or second value, respectively. */
  public <T> T appliedWith(Function<? super L,T> firstFunction, Function<? super R,T> secondFunction) {
    return first.isPresent()
            ? firstFunction.apply(first.get())
            : secondFunction.apply(second.get());
  }

  /** Apply given functions to wrapped values. */
  public <L2,R2> Either<L2,R2> map
    ( Function<? super L,L2> firstFunction, Function<? super R,R2> secondFunction)
  { return new Either<> (first.map(firstFunction),second.map(secondFunction)); }
}
