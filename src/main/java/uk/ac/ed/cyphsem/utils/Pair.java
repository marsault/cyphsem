/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import uk.ac.ed.cyphsem.Utils;

import java.util.function.Function;
import java.util.function.BiFunction;


/** Generic class representing a pair.
  * Wrapped elements are called {@link #first} and {@link #second}.
  * Elements are final and public because this class is usually used as easy
  * container, typically for returning two values or as keys for temporary maps.
  * Methods {@code #hashCode} and {@code #equals} are written in such a way
  * that subclasses should not have to rewrite them.
  * @param <F> type of first element
  * @param <S> type of second element
*/
public class Pair<F,S> {
  /** First contained element */
  public final F first;

  /** Second contained element */
  public final S second;

  /** Constructs a {@link Pair} from given parameters. */
  public Pair(F first, S second) { this.first = first; this.second = second; }

  /** Static function for constructing a {@code Pair}.
    * Simply calls the constructor.
  **/
  public static <F,S> Pair<F,S> of(F first, S second) {
    return new Pair<F,S>(first, second);
  }


  public String toString() {
    return ("("+first+","+second+")");
  }

  @Override
//   @SuppressWarnings("unchecked")
  public boolean equals(Object o) {
    if ( (o == null) || (!getClass().equals(o.getClass())) )
      return false;
    Pair<?,?> other = (Pair<?,?>) o;
    boolean b = first == null ? other.first == null : first.equals(other.first);
    boolean c = second == null ? other.second == null : second.equals(other.second);
    return (b && c);
  }

  /** This function returns an {@code int} used in the computation of the hash
    * code for this function.
    * This allows to differentiate the hash code of an instance of {@code Pair}
    * containing two elements from and instance of a subclass of {@code Pair}
    * containing the same two elements.
    * Default implementation returns {@code this.getClass().hashCode()}.
  **/
  protected int seed()   {  return this.getClass().hashCode();  }

  @Override
  public int hashCode()
  {
    return Utils.hashTuple(seed(),first,second);
  }

  /** Apply a {@link Function} to the {@link #first} element of this pair.
    * @param function the {@link Function} to apply
    * @return A new {@link Pair} that contains 1) the image of the first element
    * of this {@code Pair} by {@code function} and 2) the second element of
    * this {@link Pair}.
  **/
  public <N> Pair<F,N> mapSecond(Function<S,N> function) {
    return new Pair<>(first,function.apply(second));
  }

  /** Apply a {@link BiFunction} to both elements of this pair, and store it as the
    * first element of a new {@link Pair}; second element is the unchanged.
    * @param bifunction the {@link BiFunction} to apply.
    * @return A new {@link Pair} that contains 1) the image of both element
    * of this {@code Pair} by {@code bifunction} and 2) the second element of
    * this {@link Pair}.
  **/
  public <N> Pair<F,N> mapSecond(BiFunction<F,S,N> bifunction) {
    return new Pair<>(first,bifunction.apply(first,second));
  }

  /** Apply a function to the {@link #first} element of this pair.
    * @param function the {@link Function} to apply
    * @return A new {@link Pair} that contains 1) the first element of this {@link Pair}
    * and 2) the image of the second element of this {@code Pair} by {@code function}.
  **/
  public <N> Pair<N,S> mapFirst(Function<F,N> function) {
    return new Pair<>(function.apply(first),second);
  }

  /** Apply a {@link BiFunction} to both elements of this pair, and store it as the
    * second element of a new {@link Pair}; first element is the unchanged.
    * @param bifunction the {@link BiFunction} to apply.
    * @return A new {@link Pair} that contains 1) the first element of
    * this {@link Pair} and 2) the image of both element
    * of this {@code Pair} by {@code bifunction}.
  **/
  public <N> Pair<N,S> mapFirst(BiFunction<F,S,N> bifunction) {
    return new Pair<>(bifunction.apply(first,second),second);
  }
}

