/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import java.util.Objects;

/** Helper class for simple wrapper.
***/

public class Wrapper<E extends Comparable<E>> {

  /** The content of the wrapper. */
  protected E content;


  /** Returns the content of the wrapper (without copy). */
  public E content() { return content; }

  /** Constructs a Wrapper around the parameter. */
  public Wrapper (E content) {
    this.content = content;
  }

  /** Hash is computed by combining (with {@link Objects#hash}) the final
    * class of {@code this} and the hash of the content
  ***/
  @Override
  public int hashCode() {
    return Objects.hash(getClass(),content.hashCode());
  }

  /** Indicate whether {@code this} and {@code that} should be considered equals.
    * @param that
    * @return {@code true } if {@code this} and {@code that} have the same class at runtime,
    * if their respective contents have the same class at runtime and
    * if their contents are equals.
  ***/
  @Override
  public boolean equals(Object that) {
    if (that == null || (! getClass().equals(that.getClass())))
      return false;
    Wrapper <?> other = (Wrapper) that;
    return (content == null) ? other.content == null
                             : content.equals(other.content);
  }

  @Override
  public String toString() {  return content.toString();  }

  /** Compares this object with the specified object for order.
    * Returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
    * <br><br>
    * This method allows any class C extending Wrapper&lt;E&gt; to implements interface {@link Comparable}&lt;E&gt;.
    * @see Comparable#compareTo */
  public int compareTo(Wrapper<? extends E> that) {
    return content().compareTo(that.content());
  }

}
