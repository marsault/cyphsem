/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;

/** This is collection of static function that helps format to string.
  *
**/
public interface StringOf {

  /** Format an {@link Iterable} as a {@link String}, using given start, separation and end markers,
    * and printing each elements by given function.
    * @param <E> Type of the elements in the {@link Iterable}
    * @param it {@link Iterable} to print
    * @param el_printer Function used to print each element.
    * @param start Marker to be printed before any elements.
    * @param end Marker to be printed after all elements.
    * @param sep Marker to be printed between elements.
    */
  public static <E> String iterable(
    Iterable<E> it,
    Function<? super E,String> el_printer,
    String start, String end, String sep
  )
  {
    StringBuilder sb = new StringBuilder();
    sb.append(start);
    String mid = "";
    for (E o : it) {
      sb.append(mid);
      sb.append(el_printer.apply(o));
      mid = sep;
    }
    sb.append(end);
    return sb.toString();
  }

  /** Format an {@link Iterable} as a {@link String}, using given start,
    * separation and end markers.
    * Calls {@link #iterable} with {@code el_printer} set to x->x.toString().
  */
  public static String iterable(
  Iterable<?> l,
  String start, String end, String sep)
  {
    return iterable (l, x -> x.toString(), start, end, sep);
  }

  /** Format an {@link Iterable} as a {@link String}, using given start/end markers
    * and @code{","} as seperation marker.
  */
  public static String iterable(Iterable<?> l, String start, String end) {
    return iterable(l,start,end,",");
  }

  /** Format an {@link Iterable} as a {@link String}, using given separation marker.
  */
  public static String iterable(Iterable<?> l, String sep) {
    return iterable(l,"","",sep);
  }

  /** Format Objects as a {@link String}, using given start, separation and
    * end markers.
    * Instances of {@link Optional} are treated in a specific way : empty instances
    * are not printed at all, and non-empty instances are unboxed.
  */
  public static String objects(String start, String end, String sep, Object... objs)
  {
    return
      iterable(
        Stream.of(objs)
          .filter(x -> (!(x instanceof Optional))
                       || ((x instanceof Optional) && ((Optional) x).isPresent()))
          .map(x -> (x instanceof Optional)? ((Optional) x).get() : x)
          .collect(Collectors.toList()),
        start, end, sep);
  }


  /** Format Map as a {@link String} using multiple markers.
    * @param globalStart Marker to be printed before any elements.
    * @param globalEnd Marker to be printed after all elements.
    * @param globalSep Marker to be printed between elements.
    * @param eachStart Marker to be printed before each element.
    * @param eachEnd Marker to be printed after each element.
    * @param eachSep Marker to be printed between key and value.
  */
  public static <E,F> String map(Map<E,F> map,
    String globalStart, String globalEnd, String globalSep,
    String eachStart, String eachEnd, String eachSep)
  {
    return iterable(map.entrySet(),
                    x -> eachStart+x.getKey().toString()+eachSep+x.getValue().toString()+eachEnd,
                    globalStart, globalEnd, globalSep);
  }

  /** Format map as a property map, that is with
    * <code>"{"</code>, {@code ","} and  <code>"}"</code> as global markers
    * and {@code ""} {@code ":"} and {@code ""} as element markers.
  */
  public static <E,F> String propertyMap(Map<E,F> map) {
    return map(map, "{", "}", ",", "","",":");
  }

  /** Functions reading full content of a given file.
    * @param file File to read.
    * @return A string with the whole content of given file.
    * @throws IOException
  ***/
  public static String file(Path file) throws IOException {
    List<String>  lines = Files.readAllLines(file);
    return iterable(lines,"\n");
  }


}
