/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.function.Supplier;

/** This class implements the nodes in a graph
  * A node bears a set of {@link Label}s and posseses a set of properties, modelised as a Map&lt;{@link PropertyKey},{@link Value}&gt; (see {@link Id#properties}).
  * A node also has access to the {@link Relation}s of which it is an endpoint, hence a node gives access to the whole connected subgraph.
  * Note that methods {@link #equals} and {@link #hashCode} are implemented by superclass {@link Wrapper} (that wraps the identifier of the node). As a result, two nodes are equals if they have the same id (labels, properties, adjacent relation, etc. are ignored).
***/
public class Node extends Id implements Comparable<Node> {

  /** A supplier for Node ids. */
  public static class IdSupplier implements Supplier<Integer> {
    protected int next = 0;
    @Override
    public Integer get() { return next++; }
    /** Method to notify the supplier that given Node was created
      * without using this supplier.
    ***/
    public void update(Node n) { update(n.content()); }
    
    /** Notifies this supplier that a Node with given id was created without using
      * this supplier 
    ***/
    public void update(int i)  { next = (i >= next) ? i+1 : next; }
  }


  /** The labels of this node */
  protected Set<Label> labels = new HashSet<Label>();

  /** Returns the {@link Label}s of this {@link Node}, as an {@link Collections#unmodifiableSet unmodifiable Set}. */
  public Set<Label> labels() { return Collections.unmodifiableSet(labels); }

  /** The {@link Relation}s that loops on this node. */
  public Set<Relation> loop = new HashSet<Relation>();

  /** The {@link Relation}s going out of this node (except loops). */
  public Set<Relation> out = new HashSet<Relation>();
  /** The {@link Relation}s coming in this node (except loops). */
  public Set<Relation> in = new HashSet<Relation>();

  /** Constructs a {@link Node} with the given id, no labels and no properties. */
  public Node(int content) { super(content); }

  /** Construcs a {@link Node} by copying the id, labels and properties of the parameter.
    * @param other node to Copy
  ***/
  public Node(Node other) {
    super(other.content());
    for (Label l : other.labels)
      labels.add(l);
    for (PropertyKey key : other.propertyKeySet())
      overwriteProperty(key, other.getPropertyValue(key));
  }

  /** Constructs a Node with the given id, labels and properties.
    * @param id Identifier of the new node
    * @param labels labels of the new node (copied, elements are held, duplicates are removed from copy)
    * @param properties property map of the new node (copied, elements are held, {@code null} are removed)
  ***/
  public Node
    ( int id,
      Collection<? extends Label> labels,
      Map<PropertyKey,Value> properties )
  {
    super(id);
    addLabels(labels);
    for (PropertyKey key : properties.keySet())
      this.overwriteProperty(key, properties.get(key));
  }

 /** Add the given label to the node. */
  public void addLabel(Label l) {
    labels.add(l);
  }

  /** Add given labels to the node. */
  public void addLabels(Collection<? extends Label> extraLabels) {
    this.labels.addAll(extraLabels);
  }


  /** Remove given label to the node.
    * @return {@code true} if the node was bearing the label.
    */
  public boolean removeLabel(Label l) {
    return labels.remove(l);
  }

  /** Indicate if the node bears a given label
    * @return {@code true} if the node bears the label.
  ***/
  public boolean hasLabel(Label label) {
    return labels.contains(label);
  }


  /** String representing this node.  Currently "n"+{@link #content}.
  ***/
  @Override
  public String toString() {
    return "n"+content;
  }

  /** Name of this, as it should be written in a Cypher query.
    * Currently identical to {@link #toString}.
  ***/
  public String toCypherName() {
    return toString();
  }


  /** String representing this node, as it should be written in a Cypher query.
    * The query  {@code "CREATE "+toCypher()} should create a node identical
    * to this one (except for the id which would be fresh.)
  ***/
  public String toCypher() {
    List<Object> items = new ArrayList<>(3);
    items.add(toCypherName());
    if (!labels.isEmpty())
      items.add(StringOf.iterable(labels,":","",":"));
    if (!properties.isEmpty())
      items.add(StringOf.iterable(properties.entrySet(),x->(x.getKey()+":"+x.getValue()),"{","}",","));
    return StringOf.iterable(items,"(",")"," ");
  }

  @Override
  public int compareTo(Node n) { return super.compareTo(n); }

}
