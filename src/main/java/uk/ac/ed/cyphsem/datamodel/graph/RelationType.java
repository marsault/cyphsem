/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.utils.*;

/** Class representing types, that is, atomic data attached to a {@link Relation}.
  * A {@link RelationType} is essentially a wrapper around a String, and is distinct from
  * {@link RelationType} for type checking reasons : nodes bear labels, while
  * relations bear types.
***/
public class RelationType extends Wrapper<String> implements Comparable<RelationType> {
  /** Constructs a RelationType by wrapping the parameter. */
  public RelationType (String content) { super(content); }
  @Override
  public int compareTo(RelationType n) { return super.compareTo(n); }
}
