/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;

import uk.ac.ed.cyphsem.utils.*;
import uk.ac.ed.cyphsem.Utils;

import java.util.Collections;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

/** This class represents what is common between the type {@link Relation} and {@link Node},
  * that is class that bear a property map (field {@link #properties}).
  * It is essentially a wrapper for the type {@link Map}&lt;{@link PropertyKey}{@link Value}&gt;.
  * <br>Note that it is used in multiple places as either a Node or a Relationship.
  *
***/
public class Id extends Wrapper<Integer> {

  /** Constructs an Id as a wrapper of the parameters */
  public Id(int id) { super(id); }

  /** Property map */
  protected Map<PropertyKey,Value> properties = new HashMap<>();

  /** Returns the keys of the wrapped property map.
    * @see Map#keySet()
  ***/
  public Set<PropertyKey> propertyKeySet()
    { return properties.keySet(); }

  /** Test if this object posseses the given {@link PropertyKey}.
    * @param key
    * @return {@code true} if {@code key} is contained in {@link #properties}.
    * @see Map#containsKey
  ***/
  public boolean hasPropertyKey(PropertyKey key)
    { return (properties.containsKey(key)); }

  /** Adds a property.
    * As name suggests, it overwrites silently if the {@link PropertyKey} was already mapped to some value.
    * <br>Moreover, if the given {@link Value} parameter is {@code null}, the previous
    * property is removed, if present.
    * @throws NullPointerException if {@code v} or {@code k} is {@code null}.
    * @see #setPropertyIfUnset
  ***/
  public void overwriteProperty(PropertyKey k, Value v) {
    if (k == null || v == null)
      throw new NullPointerException();
    if (v.isOfKind(Value.Kind.NULL))
      properties.remove(k);
    else
      properties.put(k,v);
  }


  /** Removes the property associated with the given {@link PropertyKey}.
    * @return {@code true} if the property was indeed removed, and {@code false} if parameter {@code k} was not associated with any value.
    */
  public boolean removeProperty(PropertyKey k) {
    return (properties.remove(k) != null);
  }

  /** Adds a property only if no property is already associated with the given {@link PropertyKey}.
    * @return {@code true} if the property was added.
    * @see #overwriteProperty
  ***/
  public boolean setPropertyIfUnset(PropertyKey k, Value v) {
    if (properties.containsKey(k))
      return false;
    else {
      overwriteProperty(k,v);
      return true;
    }
  }

  /** Get the value associated with a key, or a {@link Kind#NULL NULL} {@link Value} if no value is associated with the key.
  ***/
  public Value getPropertyValue(PropertyKey key) {
    if (hasPropertyKey(key))
      return (properties.get(key));
    else
      return (new Value());
  }

  /** Returns the wrapped property map, as an unmodifiable map.
  ***/
  public Map<PropertyKey,Value> propertyMap () {
    return Collections.unmodifiableMap(properties);
  }

  /** Returns a Set view of the wrapped property map. */
  public Set<Pair<PropertyKey,Value>> propertySet () {
    return Utils.map(properties.entrySet(), x -> new Pair<>(x.getKey(), x.getValue()));
  }


}
