package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.value.*;

/** Interface compiling misc functions about {@link Graph}'s ***/
public interface Graphs {

  /** {@link PropertyKey} used to annotate
    * @see annonateWithCyphSemId
  ***/
  public static final PropertyKey cyphsemidKey = new PropertyKey("cyphsem_id");

  public static 
  void annotateWithCyphSemId(Graph graph) 
  {
    for (Node node : graph.nodes())
      node.overwriteProperty(cyphsemidKey, new Value(node.content()));
    for (Relation relation : graph.relations())
      relation.overwriteProperty(cyphsemidKey, new Value(relation.content()));
  }

}
