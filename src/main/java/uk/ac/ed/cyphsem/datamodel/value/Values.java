/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.value;

import uk.ac.ed.cyphsem.exception.DynamicCastException;
import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;
import uk.ac.ed.cyphsem.exception.UndefinedNameException;

import uk.ac.ed.cyphsem.Utils.Policy.*;
import uk.ac.ed.cyphsem.Utils.Policy;
import uk.ac.ed.cyphsem.Utils.ExceptionPolicy;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.language.expression.Functions;

import uk.ac.ed.cyphsem.table.Record;

import java.lang.ReflectiveOperationException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Container for utility functions about {@link Value}s.
 */
public interface Values {


  /** Tests whether two {@link Value}s are semantically equivalent.
    * Extra arguments specify how to handle exceptions.
    * If the computation of equality returns a
    * @param l
    * @param r
    * @param policy1 handler in case a {@link UnspecifiedBehaviourException} is thrown.
    * @param policy2 same as previous, but for {@link UndefinedNameException}.
    * @return {@code true} if values {@code l} and {@code r} are semantically equal,
    * and {@code false} if they are inequal or if they are incomparable with
    * respect to equality.
    * then this function returns {@code false}
  ***/
  public static
  boolean
  semEquals
    ( Value l, Value r,
      ExceptionPolicy<UnspecifiedBehaviourException> policy1,
      ExceptionPolicy<UndefinedNameException> policy2 )
  {
    try {
      Value v = Functions.EQ.call(l,r);
      if (v.isOfKind(Value.Kind.NULL))
        return false;
      return (v.unsafeCastAsBoolean());
    } catch (UnspecifiedBehaviourException e) {
      return policy1.handle(e);
    } catch (UndefinedNameException e) {
      return policy2.handle(e);
    }
  }

  /** Same as {@link #semEquals}, except that {@link UndefinedNameException} are
    * propagated).
  ***/
  public static
  boolean semEquals
    ( Value l, Value r, ExceptionPolicy<UnspecifiedBehaviourException> policy1 )
  {
    return semEquals(l,r, policy1, new ExceptionPolicy<UndefinedNameException>());
  }

  /** Same as {@link #semEquals}, where the {@link ExceptionPolicy ExceptionPolicies}
    * are built position-wise
    * from given {@link Policy Policies}.
    * @param l
    * @param r
    * @param p1 policy in case a {@link UnspecifiedBehaviourException} is thrown.
    * @param p2 same as previous, but for {@link UndefinedNameException}.
  ***/
  public static
  boolean
  semEquals(Value l, Value r, Policy p1, Policy p2)
  {
    return semEquals(l,r,
      new ExceptionPolicy<UnspecifiedBehaviourException>(p1),
      new ExceptionPolicy<UndefinedNameException>(p2)
    );
  }

  /** Same as {@link #semEquals}, where the {@link ExceptionPolicy ExceptionPolicies}
    * are built from given {@link Policy}.
    * @param l
    * @param r
    * @param p policy if a {@link UnspecifiedBehaviourException} or a {@link UndefinedNameException} is thrown.
  ***/
  public static boolean semEquals(Value l, Value r, Policy p)
    {  return semEquals(l,r, p, p);  }


  /** Assert that Value is storable.
    * @param v The value to test.
    * @throws DynamicCastException if the value is not storable.
    * @see Value#isStorable
  ***/
  public static void assertStorable(Value v)
  throws DynamicCastException
  {
    if (!v.isStorable())
      throw new DynamicCastException("Value " + v + "is not storable, it refers to the graph.");

  }

  /** Builds a value from any object.  Works only if Object may be cast to one
    * of the appropriate types.
    * @param o the content to wrap in a value
    * @throws Error if the parameter is not of an appropriate type.
  **/
  @SuppressWarnings("unchecked")
  public static Value of(Object o) {
    if (o == null)
      return new Value();
    Throwable f = null;
    try {
      Constructor<Value>[] constructor_array
          = (Constructor<Value>[]) Value.class.getConstructors();
      for (Constructor<Value> constructor : constructor_array)
        if (constructor.getParameterCount() == 1) {
          Class<?> parameter_class = constructor.getParameterTypes()[0];
          if (parameter_class.isInstance(o))
            return constructor.newInstance(o);
        }
    }
    catch(ReflectiveOperationException e) { f = e; }


    String error_msg = "Values#of called with parameter" + o + ", of unacceptable class "
                + o.getClass() +".";
    if (f == null)
      throw new Error(error_msg);
    else
      throw new Error(error_msg,f);
  }
}
