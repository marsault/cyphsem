/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.Utils;

import uk.ac.ed.cyphsem.exception.GraphException;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.utils.*;


import java.util.Collection;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Collections;

import java.util.function.Supplier;

/** Class implementing a property Graph.
***/
public class Graph {

  /** The nodes of the graph */
  protected Set<Node> nodes = new HashSet<Node>();


  /** The nodes of the graph */
  protected Set<Relation> relations = new HashSet<Relation>();


  /** A supplier for node Id. */
  protected Node.IdSupplier nodeIdSupplier = new Node.IdSupplier();


  /** A supplier for relation Id. */
  protected Relation.IdSupplier relationIdSupplier = new Relation.IdSupplier();


  /** Adds a new node to the graph with given labels and properties.
    * @param labels labels of the new node (duplicates will be removed)
    * @param properties property map
    * @return the new node.
    * @see #makeNode
    * @beware the parameter {@code properties} is not copied.
  ***/
  public Node addNode(Collection<? extends Label> labels, Map<PropertyKey,Value> properties ) {
    Node node = makeNode(labels,properties);
    addNode(node);
    return node;
  }


  /** Returns the {@link Node}s in the graph as an {@link Collections#unmodifiableSet unmodifiable Set}. */
  public Set<Node> nodes() { return Collections.unmodifiableSet(nodes); }


  /** Returns the {@link Relation}s in the graph as an {@link Collections#unmodifiableSet unmodifiable Set}. */
  public Set<Relation> relations()
  { return Collections.unmodifiableSet(relations); }


  /** Create a node just as method {@link #addNode} but does <b>not</b> add it to the graph.
    * The returned Node is then suitable as parameter for method
    * {@link #addNode(uk.ac.ed.cyphsem.datamodel.graph.Node)}.
    * <br> This method is used to create a node now without modifying the
    * graph because further reading is required before commiting the changes to the graph.
    * @param labels labels of the new node (duplicates will be removed)
    * @param properties property map of the new node
    * @return the new node.
    * @see #makeNode
    * @beware the parameter {@code properties} is not copied.
  ***/
  public Node makeNode(Collection<? extends Label> labels, Map<PropertyKey,Value> properties ) {
    return new Node (nodeIdSupplier.get(), labels, properties);
  }


  /** Adds a new node to the graph with no labels nor properties.
    * @return the new node.
    * @see #makeNode
    * @see #addNode
  ***/
  public Node addNode() {
       return addNode(Collections.emptyList(), new HashMap<PropertyKey,Value>()); }


  /** Adds the given node to the graph.
    * @see #makeNode
    * @see #addNode
    * @throws GraphException if the given node is already part of the graph.
  ***/
  public void addNode(Node n) {
    if (nodes.contains(n))
      throw new GraphException("Node "+n+" is already part of the graph");
    nodes.add(n);
    nodeIdSupplier.update(n);
  }


  /** Adds the given relation to the graph.
    * @param r Relation to add to the graph.
    * @throws GraphException if the given {@link Relation} is already part of the graph or if the source or target node is not part of the graph.
  ***/
  public void addRelation(Relation r) {
    assertExists(r.source);
    assertExists(r.target);
    relations.add(r);
    if (r.source.equals(r.target))
      r.source.loop.add(r);
    else {
      r.source.out.add(r);
      r.target.in.add(r);
    }
    relationIdSupplier.update(r);
  }


  /** Adds a new relation to the graph with given type and properties.
    * @param type of the new node (duplicates will be removed)
    * @param source Source node of the new relation
    * @param target Target node of the new relation.
    * @param properties property map of the new {@link Relation}
    * @return the new relation.
    * @see #makeRelation
    * @see #addRelation(uk.ac.ed.cyphsem.datamodel.graph.Relation)
    * @beware the parameter {@code properties} is not copied.
  ***/
  public Relation addRelation
      ( RelationType type, Node source, Node target,
        Map<PropertyKey,Value> properties )
  {
    Relation newRelation = makeRelation(type, source, target, properties);
    addRelation (newRelation);
    return newRelation;
  }


  /** Adds a new relation to the graph with given type and no properties.
    * @param type of the new node (duplicates will be removed)
    * @param source Source node of the new relation
    * @param target Target node of the new relation.
    * @return the new relation.
    * @see #makeNode
    * @see #addRelation
    * @see #addRelation(uk.ac.ed.cyphsem.datamodel.graph.Relation)
  ***/
  public Relation addRelation ( RelationType type, Node source, Node target)
  { return addRelation(type, source, target,  new HashMap<>()); }


  /** Create a {@link Relation} just as method {@link #addRelation} but does <b>not</b> add it to the graph.
    * The returned Relation is then suitable as parameter for method
    * {@link #addRelation(uk.ac.ed.cyphsem.datamodel.graph.Relation)}, provided that parameters {@code source} and {@code source}
    * are part of the graph then.
    * <br> This method is used to create a relation now without modifying the
    * graph because further reading is required before commiting the changes to the graph.
    * @param type of the new node (duplicates will be removed)
    * @param source Source node of the new relation
    * @param target Target node of the new relation.
    * @param properties property map of the new {@link Relation}
    * @return the new relation.
    * @see #makeNode
    * @see #addRelation
    * @see #addRelation(uk.ac.ed.cyphsem.datamodel.graph.Relation)
    * @beware the parameter {@code properties} is not copied.
  ***/
  public
  Relation
  makeRelation
      ( RelationType type, Node source, Node target,
        Map<PropertyKey,Value> properties )
  {
    Relation newRelation
      = new Relation( relationIdSupplier.get(),
                      type, source, target, properties);
    return newRelation;
  }


  /** Constructs an empty graph */
  public Graph() {}


  /** Copy the graph.
    * @return A copy of this object and mappings from nodes of this graph (resp. relations)
    * to the nodes of the new graph.
  ***/
  public Triplet<Graph,Map<Node,Node>,Map<Relation,Relation>> copy ()
  {
    Graph copy = new Graph();
    Map<Node,Node> nmap = new HashMap<Node,Node>();
    Map<Relation,Relation> rmap = new HashMap<Relation,Relation>();
    for (Node this_n : nodes) {
      Node copy_n = new Node(this_n);
      copy.addNode(copy_n);
      nmap.put(this_n,copy_n);
    }
    for (Relation this_r : relations) {
      Relation copy_r = new Relation(this_r, nmap);
      copy.addRelation(copy_r);
      rmap.put(this_r,copy_r);
    }
    for (Node this_n : nodes) {
      Node copy_n = nmap.get(this_n);
      for (Relation this_r : this_n.loop)
        copy_n.loop.add(rmap.get(this_r));
      for (Relation this_r : this_n.in)
        copy_n.in.add(rmap.get(this_r));
      for (Relation this_r : this_n.in)
        copy_n.in.add(rmap.get(this_r));
    }
    return new Triplet<>(copy, nmap, rmap);
  }


  /** Tests if a {@link Node} is part of the graph, and throws
    * a {@link GraphException} if it does not.
    * @param id The {@link Node} expected to be part of the graph.
    * @throws GraphException if the given {@link Node} is not part of the graph.
  ***/
  void assertExists(Node id) {
    if (!nodes.contains(id))
      throw new GraphException("Node " + id + " does not exists");
  }


  /** Tests if a {@link Relation} is part of the graph, and throws
    * a {@link GraphException} if it does not.
    * @param id The {@link Relation} expected to be part of the graph.
    * @throws GraphException if the given {@link Relation} is not part of the graph.
  ***/
  void assertExists(Relation id) {
    if (!relations.contains(id))
      throw new GraphException("Relation " + id + " does not exists");
  }


  /** Removes a {@link Relation} or a {@link Node} from the graph.
    * @param id The {@link Relation} or a {@link Node} to remove
    * @return {@code true} if the given {@link Relation} or a {@link Node}
    *         was actually part of the graph.
    * @throws GraphException if the given {@link Id} is a {@link Node} that is still attached to a Relation.
  ***/
  public boolean remove(Id id)
  throws GraphException
  {
    if (id instanceof Node) {
      Node n = (Node) id;
      if ( (n.out.isEmpty()) && (n.in.isEmpty()) && (n.loop.isEmpty()) )
        return nodes.remove((Node)id);
      else
        throw new GraphException("Node "+n+" still has Relations attached, hence cannot be removed.");
    }
    else if (id instanceof Relation) {
      Relation r = (Relation) id;
      if (relations.remove(r)) {       // If r is indeed removed, modify source
                                       // and target.
        if (r.source == r.target)
          r.source.loop.remove(r);
        else {
          r.source.out.remove(r);
          r.target.in.remove(r);
        }
        return true;
      }
      else return false;
    }
    else
      throw new UnreachableStatementError();
  }

//   /** Returns the number of nodes plus the number of relation in the graph */
//   public int size() {
//     return (nodes.size()+relations.size());
//   }

  //@Deprecated
  public Node getNodeById(Object id) {//fixme
    for (Node node : nodes)
      if (id.equals(node.content()))
        return node;
    throw new GraphException("No node with such id: "+id+" does not belong to "+nodes+".");
  }

  //@Deprecated
  public Relation getRelationById(Object id) {//fixme
    for (Relation rel : relations)
      if (id.equals(rel.content()))
        return rel;
    throw new GraphException("No relation with such id");
  }

//   @Deprecated
//   public String toJson() { return toJson("   "); }
//
//   @Deprecated
//   public String toJson(String indent) {
//     String res = "{\n"+indent+"Nodes:{\n";
//     for (Node n : nodes) {
//       res += n.toJson(indent,indent+indent);
//     }
//     res+=indent + "},\n"+indent+"Relations:{\n";
//     for (Relation r : relations) {
//       res += r.toJson(indent,indent+indent);
//     }
//     res+= indent+"}\n}";
//     return res;
//   }

  /** Builds a Cypher query representing this graph. */
  public String toCypher() {
    Optional<String> nodes_str = Optional.empty();
    Optional<String> rels_str = Optional.empty();
    if (!nodes.isEmpty())
      nodes_str = Optional.of(StringOf.iterable(nodes, x->x.toCypher(), "CREATE ", "", ",\n       "));
    if (!relations.isEmpty())
      rels_str = Optional.of(StringOf.iterable(relations, x->x.toCypher(), "CREATE ", "", ",\n       "));
    return StringOf.objects("","","\n",nodes_str,rels_str);
  }
}
