/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.utils.*;
import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Iterator;

/** Class representing a path in a graph, that is an alternating list of {@link Node}s
  * and {@link Relation}s, that starts and ends with a {@link Node}.
  *
  * Implementation is done with two lists, that are ensured to have consistent sizes.
  *
***/
public class Path implements Iterable<OptPair<Node,Relation>>, Comparable<Path> {

  /** The nodes in the path. */
  protected List<Node> nodes;

  /** The relation in the path. */
  protected List<Relation> relations;


  /** Constructs a {@link Path} from given nodes and relations.
    * It is expected that the size of parameter {@code relations} is one more
    * then the size of parameter {@code nodes}.
    * Note however that it is not required that the <var>i</var>-th node and
    * the <var>(i+1)</var>-th node are the endpoints of the <var>i</va> relation.
    * @param nodes the nodes in the path
    * @param relations the relations in the path
    * @throws IllegalArgumentException if the number of nodes is not one more than the number of relations.
  ***/
  public Path (List<Node> nodes, List<Relation> relations) {
    if( nodes.size() != relations.size() +1 )
      throw new IllegalArgumentException("Paths must have (x+1) nodes for x relations, for some x.)");
    this.nodes = nodes;
    this.relations = relations;
  }

  /** Constructs a copy of given {@link Path}. */
  public Path(Path other) {
    nodes = new ArrayList<>(other.nodes);
    relations = new ArrayList<>(other.relations);
  }

  /** Constructs a {@link Path} from given {@link Id}s.
    * It is expected that <ul>
    * <li>each {@link Id} is either a {@link Node} or {@link Relation};</li>
    * <li>the number of nodes is one more than the number of relations.</li>
    * <ul> Note however that it is not required that the <var>i</var>-th node and
    * the <var>(i+1)</var>-th node are the endpoints of the <var>i</va> relation.
    * @throws DynamicCastException if some provided {@link Id} is neither a {@link Node} nor a {@link Relation}
    * @throws IllegalArgumentException if the number of nodes is not one more than the number of relations.
  ***/
  public Path (Id... ids) {
    List<Node> nodes = new ArrayList<Node>(ids.length/2);
    List<Relation> relations = new ArrayList<Relation>(ids.length/2+1);
    if (ids.length != 0) {
      for (Id i : ids) {
        if (i instanceof Node)
          nodes.add((Node) i);
        else if (i instanceof Relation)
          relations.add((Relation) i);
        else
          throw new DynamicCastException("Provided Id("+i+") is not a Node nor a Relation.");
      }
      if( nodes.size() != relations.size() +1 )
        throw new IllegalArgumentException("Paths must have (x+1) nodes for x relations, for some x.)");
    }
    this.nodes = nodes;
    this.relations = relations;
  }

  /** Adds a relation and node to the path */
  public void add(Relation relation, Node node) {
    nodes.add(node);
    relations.add(relation);
  }

  /** Adds a relation and node to the path, the node being computed.
    * Expects one endpoint of the given relation to be the destination of the path in
    * which case the added node is the other endpoint of the relation (which may be the same if the relation is indeed a loop).
    * @param rel
    * @throws IllegalArgumentException if no endpoint of {@code rel} is the destination of the path.
    * @see #dst
    * @see #add(uk.ac.ed.cyphsem.datamodel.graph.Relation,uk.ac.ed.cyphsem.datamodel.graph.Node)
  ***/
  public void add(Relation rel) {
    add(rel,rel.otherEnd(dst()));
  }


  /** Returns the <var>i</var>-th relation of the path.
    * @param i position of the relation to return.
    * @throws IndexOutOfBoundsException if there are &leq;i relations in the path
    * @see #getNode
    * @see #get
  ***/
  public Relation getRelation(int i) {  return relations.get(i); }

  /** Returns the element of the path at specified position.
    * @param i position of the element to return.
    * @throws IndexOutOfBoundsException if there are &leq;i elements in the path.
    * @see #getNode
    * @see #getRelation
  ***/
  public Id get(int i) {
    if (i%2 == 0)
      return getNode(i/2);
    else
      return getRelation(i/2);
  }

  /** Returns the <var>i</var>-th node of the path.
    * @param i position of the node to return.
    * @throws IndexOutOfBoundsException if there are &leq;i nodes in the path.
    * @see #get
    * @see #getRelation
  ***/
  public Node getNode(int i)  { return nodes.get(i); }

  /** Returns the last {@link Node} of the path. */
  public Node dst()  { return nodes.get(nodes.size()-1); }

  /** Returns last {@link Relation} of the path.
    * @throws IndexOutOfBoundsException if there are no relation in the path.
  ***/
  public Relation lastRelation()  { return relations.get(relations.size()-1); }


  @Override
  public Iterator<OptPair<Node,Relation>> iterator()
  { return new ConcurrentIterable<>(nodes, relations).iterator(); }

//   public Collection<Node> nodes() { return nodes; }
//   public Collection<Node> nodes() { return nodes; }

  @Override
  public String toString() {
    return StringOf.iterable(
        this,
        x-> x.left().toString() + (x.hasRight() ? ("-"+x.right()) : ""),
        "-",
        "-",
        "-");
  }

  @Override
  public int hashCode() {
    return Objects.hash(nodes,relations);
  }


  @Override
  public boolean equals (Object o) {
    if (!(o instanceof Path))
      return false;
    Path other = (Path) o;
    return ( nodes.equals(other.nodes)
             && relations.equals(other.relations) );
  }

  @Override
  public int compareTo(Path other)
  {
    for ( OptPair< OptPair<Node,Relation>, OptPair<Node,Relation>> pair :
          new ConcurrentIterable<>(this, other) )
    {
      // It is not possible that either left or right of pair is missing.
      // Indeed, it would be known at previous iteration of the for loop.
      OptPair<Node,Relation> left = pair.left();
      OptPair<Node,Relation> right = pair.right();
      int c = left.left().compareTo(right.left());
      if (c != 0)
        return c;
      if ( (!left.hasRight()) && (!right.hasRight()) )
        return 0;
      if (! left.hasRight() )
        return -1;
      if (! right.hasRight() )
        return 1;
      c = left.right().compareTo(right.right());
      if (c != 0)
        return c;
    }
    throw new UnreachableStatementError();
  }

//   public String indent() {
//     String res = "";
//     for(int i=0; i<size(); i++) {
//       res +=  " ";
//     }
//     return res;
//   }
}
