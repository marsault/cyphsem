/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.utils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Collections;

import java.util.function.Supplier;

/** This class implements the relations in a graph
  * A relation bears a {@link RelationType} and posseses a set of properties,
    modelised as a Map&lt;{@link PropertyKey},{@link Value}&gt; (see {@link Id#properties}).
  * A relation also has access to its endpoints, hence a relation gives access to the whole connected subgraph.
  *<br>
  * Note that methods {@link #equals} and {@link #hashCode} are implemented by superclass {@link Wrapper} (that wraps the integer identifier of the relation). As a result, two relations are equals if they have the same id (type and properties are ignored).
***/
public class Relation extends Id implements Comparable<Relation> {

  /** A supplier for Relation ids. */
  public static class IdSupplier implements Supplier<Integer> {
    protected int next = 0;
    @Override
    public Integer get() { return next++; }
    
    /** Method to notify the supplier that given Relation was created
      * without using this supplier.
    ***/
    public void update(Relation r) { update(r.content()); }

    /** Method to notify the supplier that a Relation with given id was created
      * without using this supplier
    ***/
    public void update(int i) { next = (i >= next) ? i+1 : next; }

  }

  /** The type of the relation */
  protected final RelationType type;

  /** The source node of the relation. */
  protected final Node source;

  /** The target node of the relation. */
  protected final Node target;

  /** Constructs a relation with given type, source, target and no properties. */
  public Relation(int id, RelationType type, Node source, Node target) {
    this(id, type, source, target, new HashMap<>());
  }

  /** Constructs a {@link Relation} that is a copy of another relation.
    * @param other the relation to copy
    * @param map a map from the old nodes to the new nodes; used to have correct source and target nodes.
  ***/
  public Relation (Relation other, Map<Node,Node> map) {
    this( other.content(),
          other.type, map.get(other.source), map.get(other.target), other.propertyMap());
  }

  /** Constructs a {@link Relation} with given source, type and properties
    * @param id Identifier of the new relation
    * @param type Type of the new relation (held)
    * @param source Source node of the new relation (held)
    * @param target Target node of the new relation (held)
    * @param pm Property map of the new relation (copied, elements held, {@code null} removed)
    * @throws NullPointerException If any of the parameters is {@code null}.
  ***/
  public Relation(int id, RelationType type, Node source, Node target, Map<PropertyKey,Value> pm) {
    super(id);
    if (type == null || source == null || target == null || pm == null)
      throw new NullPointerException();
    this.type = type;
    this.source = source;
    this.target = target;
    for (PropertyKey key : pm.keySet())
      this.overwriteProperty(key, pm.get(key));
  }

  /** Returns the type of the relation */
  public RelationType type() { return type; }

  /** Returns the source {@link Node} of the relation */
  public Node source() { return source; }

  /** Returns the target {@link Node} of the relation */
  public Node target() { return target; }

  /** Returns {@code true} if the relation is of the given type */
  public boolean isOfType(RelationType type) {
    return type().equals(this.type);
  }

  /** String representing this node.  Currently "r"+{@link #content}.
  ***/
  @Override
  public String toString() {
    return "r"+content;
  }

  /** Name of this, as it should be written in a Cypher query.
    * Currently identical to {@link #toString}.
  ***/
  public String toCypherName() {
    return toString();
  }

  /** Indicates the other endpoint of the relation, where one endpoint is given as parameter.
    * @param n One endpoint of the relation
    * @return a node m if the relation goes from n to m or from n to m.  (Note that m may very well be equal to n if this relation is a node.)
    * @throws IllegalArgumentException if {@code n} is not an endpoint of the relation.
  ***/
  public Node otherEnd(Node n) {
    if (n.equals(source))
      return target;
    if (n.equals(target))
      return source;
    throw new IllegalArgumentException("Given node ("+n+") is not the endpoint of the relation "+this+".");
  }

  /** Shorthand for {link #toCypher(boolean) toCypher}{@code (true)}. */
  public String toCypher() { return toCypher(true); }

  /** String representing this node, as it should be written in a Cypher query.
    * The query  {@code "CREATE "+toCypher()} should create a node identical
    * to this one (except for the id which would be fresh.)
  ***/
  public String toCypher(boolean printEndPoint) {
    List<Object> items = new ArrayList<>(3);
    items.add(toCypherName());
    items.add(":"+type);
    if (!properties.isEmpty())
      items.add(StringOf.propertyMap(properties));
    String start = (printEndPoint ? "("+source.toCypherName()+")" : "") + "-[";
    String end = "]->" + (printEndPoint ? "("+target.toCypherName()+")" : "");
    return StringOf.iterable(items,start,end," ");
  }

  @Override
  public int compareTo(Relation n) { return super.compareTo(n); }
}
