/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.value;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.utils.*;


import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.graph.Node;
import uk.ac.ed.cyphsem.datamodel.graph.Path;
import uk.ac.ed.cyphsem.datamodel.graph.PropertyKey;
import uk.ac.ed.cyphsem.datamodel.graph.Relation;

import uk.ac.ed.cyphsem.language.expression.Expression;

import uk.ac.ed.cyphsem.table.Record;

import uk.ac.ed.cyphsem.utils.Pair;

import java.lang.Object;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;


public class Value implements Expression, Comparable<Value> {

  public static enum Kind implements Comparable<Kind> {
    MAP(true,1), NODEID(false,2), RELID(false,3),  LIST(true,4), PATH(true,5),
    STRING(false,6), BOOL(false,7), INT(false,8), NULL(false,9);

    /** Whether this object is composite, that is, whether it has sub-terms. */
    public final boolean composite;
    /** Kind order, used to determine {@link Value} order in {@link OrderBy} clauses.  */
    public final int order;

    /** Constructs a Kind that wraps the parameter. */
    Kind(boolean c, int o) { composite = c; order=o; }
  }

  public static enum Comparison {
    /** Indicate that no information is known.*/
    ANY,
    /** Indicate that the two {@link Values}s are equal. */
    EQ,
    /** Indicate that the two {@link Value} are not equal but order is not known. */
    NEQ,
    /** Indicate that First {@link Value} is strictly smaller than second {@link Value}.*/
    LT,
    /** Indicate that First {@link Value} is strictly greater than second {@link Value}.*/
    GT,
    /** Indicate that {@link Value}s are incomparable.*/
    INCOMPARABLE;

//     /** Does this object implies that values are equal.
//       * @return true if Comparison implies that {@link Value}s are equal.
//     ***/
//     public boolean henceEqual() {
//       switch (this) {
//         case ANY: return null;
//         case INCOMPARABLE:
//         case NEQ:
//         case LT:
//         case GT: return false;
//         case EQ: return true;
//         default:
//       }
//     }
  }

  /** The untyped content of the Value.
    * Its type content is determined by the field {@link #kind} (see general description).
    * Should not be {@code null}, unless kind == Kind.NULL.
  ***/
  protected Object content;

  /** Returns the untyped content of the value
    *
  **/
  public Object content() { return content; }


  /** The kind of Value boxed by this object. */
  Kind kind;

  public Kind getKind() {  return kind;  }

  /** Internal function to check that {@link #content} field is not {@code null}.
    * @throws AssertionFailedException if {@link #content} is {@code null}.
  ***/
  protected void assertContentIsNotNull() {
    if (content == null)
      throw new AssertionFailedException("Content of value cannot be null (use new Value() instead).");
  }

  /** Constructs a Value of {@link Kind} {@link Kind#NODEID NODEID}.
    * @param n Node to box into a value.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value(Node n) { //Value containing a Node Identifier
    content = n;
    kind =  Kind.NODEID;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#RELID RELID}.
    * @param r {@link Relation} to be wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value(Relation r) {
    content = r;
    kind = Kind.RELID;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#INT INT}.
    * @param n Int used to build a {@link BigInteger} that is wrapped into the new {@link Value}.
  ***/
  public Value(int n) {  this(BigInteger.valueOf(n));  }

  /** Constructs a Value of {@link Kind} {@link Kind#INT INT}.
    * @param n Long used to build a {@link BigInteger} that is wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value(long n) {  this(BigInteger.valueOf(n));  }

  /** Constructs a Value of {@link Kind} {@link Kind#INT INT}.
    * @param n {@link BigInteger} to be wrapped into the new {@link Value}.
  ***/
  public Value(BigInteger n) {
    content = n;
    kind = Kind.INT;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#INT INT}.
    * @param n {@link Integer} used to construct a {@link BigInteger} that is wrapped into the new {@link Value}.
  ***/
  public Value(Integer n) {
    content = BigInteger.valueOf(n);
    kind = Kind.INT;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#STRING STRING}.
    * @param s {@link String} to be wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value(String s) {
    content = s;
    kind = Kind.STRING;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#BOOL BOOL}.
    * @param b boolean to be wrapped into the new {@link Value}.
  ***/
  public Value(boolean b) {
    content = b;
    kind = Kind.BOOL;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#BOOL BOOL}.
    * @param b {@link Boolean} to be wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value(Boolean b) {
    content = b;
    kind = Kind.BOOL;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#NULL NULL}. */
  public Value() {
    content = null;
    kind = Kind.NULL;
  }

  /** Constructs a Value of {@link Kind} {@link Kind#LIST LIST}.
    * @param l List of values to be wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value(List<Value> l) { //Value containing a list
    content = l;
    kind = Kind.LIST;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#MAP MAP}.
    * @param m Property-to-value map to be wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value (Map<PropertyKey,Value> m) { //Value containing a map
    content = m;
    kind = Kind.MAP;
    assertContentIsNotNull();
  }

  /** Constructs a Value of {@link Kind} {@link Kind#PATH PATH}.
    * @param p {@link Path} to be wrapped into the new {@link Value}.
    * @throws AssertionFailedException if parameter is {@code null}.
  ***/
  public Value (Path p) {
    content = p;
    kind = Kind.PATH;
    assertContentIsNotNull();
  }

  /** Tests whether this object is of given {@link Kind}.
    * @param k expected {@link Kind}
    * @return {@code true} if field {@link #kind} is equal to parameter,
    *         and {@code false} otherwise.
  ***/
  public boolean isOfKind(Kind k) { return (kind == k); }

  /** Tests whether this object is of given {@link Kind} or is a {@link Kind#NULL NULL} {@link Value}.
    * @param k expected {@link Kind}
    * @return {@code true} if field {@link #kind} is equal to parameter or to {@link Kind#NULL},
    *         and {@code false} otherwise.
  ***/
  public boolean couldBeOfKind(Kind k) { return (kind == Kind.NULL) || isOfKind(k); }

  /** Casts {@link #content} to {@link Node} without checking that {@link #kind}
    * is suitable. */
  public Node unsafeCastAsNode() {
    return unsafeCast();
  }



  /** Casts {@link #content} to {@link Relation} without checking that
    * {@link #kind} is suitable. */
  public Relation unsafeCastAsRelation() {  return unsafeCast();  }




  /** Casts {@link #content} to {@link BigInteger} without checking that
    * {@link #kind} is suitable. */
  public BigInteger unsafeCastAsInteger() {  return unsafeCast();  }

  /** Casts {@link #content} to {@link String} without checking that
    * {@link #kind} is suitable. */
  public String unsafeCastAsString() {  return unsafeCast();  }

  /** Casts {@link #content} to {@link Boolean} without checking that
    * {@link #kind} is suitable. */
  public Boolean unsafeCastAsBoolean() {  return unsafeCast();  }

  /** Casts {@link #content} to {@link List} without checking that {@link #kind}
    * is suitable. */
  @SuppressWarnings("unchecked")
  public List<Value> unsafeCastAsList() { return unsafeCast(); }

  /** Casts {@link #content} to {@link Map} without checking that {@link #kind}
    * is suitable. */
  @SuppressWarnings("unchecked")
  public Map<PropertyKey,Value> unsafeCastAsMap() { return unsafeCast(); }

  /** Casts {@link #content} to {@link Path} without checking that {@link #kind}
    * is suitable. */
  public Path unsafeCastAsPath() {  return ((Path) content);  }

  /** Casts {@link #content} to an arbitrary type without checking
    * that the operation is valid.
    * @param <T> Type to cast
    * @return the content of this object, casted to type T.
  ***/
  @SuppressWarnings("unchecked")
  public <T> T unsafeCast() {
    return ((T) content);
  }

  /** Casts {@link #content} to an arbitrary type after checking that this object's kind
    * is one of provided {@code acceptable_kinds}.
    * Calling function should ensure that the provided parameters make the
    * cast safe.
    * @param <T> Type to cast
    * @return the content of this object, casted to type T.
    * @throws DynamicCastException if the kind of this objectdos not belong to {@code acceptable_kinds}
  ***/
  public <T> Pair<Kind,T> castAs(Value.Kind... acceptable_kinds) {
    for (Kind k: acceptable_kinds)
      if (isOfKind(k))
        return new Pair<>(k,unsafeCast());
    throw new DynamicCastException(this,acceptable_kinds);
  }

  /** Casts {@link #content} to {@link Map}&lt;{@link PropertyKey},{@link Value}&gt; or throws a {@link DynamicCastException} if {@link #kind} is not {@link Kind#MAP}.
    * @see #castAs
  **/
  public Map<PropertyKey,Value> asMap() throws DynamicCastException{
    return this.<Map<PropertyKey,Value>>castAs(Kind.MAP).second;
  }

  /** Casts {@link #content} to {@link Node} or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#NODEID}.
    * @see #castAs
  **/
  public Node asNode() throws DynamicCastException {
    return this.<Node>castAs(Kind.NODEID).second;
  }

  /** Casts {@link #content} to {@link Relation} or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#RELID}.
    * @see #castAs
  **/
  public Relation asRelation() throws DynamicCastException {
    return this.<Relation>castAs(Kind.RELID).second;
  }

  /** Casts {@link #content} to {@link List}&lt;{@link Value}&gt; or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#LIST}.
    * @see #castAs
  **/
  public List<Value> asList() throws DynamicCastException {
    return this.<List<Value>>castAs(Kind.LIST).second;
  }

  /** Casts {@link #content} to {@link Path} or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#PATH}.
    * @see #castAs
  **/
  public Path asPath() throws DynamicCastException {
    return this.<Path>castAs(Kind.PATH).second;
  }

  /** Casts {@link #content} to {@link String} or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#STRING}.
    * @see #castAs
  **/
  public String asString() throws DynamicCastException {
    return this.<String>castAs(Kind.STRING).second;
  }

  /** Casts {@link #content} to {@link Boolean} or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#BOOL}.
    * @see #castAs
  **/
  public Boolean asBoolean() throws DynamicCastException {
    return this.<Boolean>castAs(Kind.BOOL).second;
  }

  /** Casts {@link #content} to {@link Integer} or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#INT}.
    * @see #castAs
  **/
  public BigInteger asInteger() throws DynamicCastException {
    return this.<BigInteger>castAs(Kind.INT).second;
  }

  /** Casts {@link #content} to {@link Map}&lt;{@link PropertyKey},{@link Value}&gt; (possibly {@code null}) or throws a {@link DynamicCastException} if {@link #kind} is not {@link Kind#MAP} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public Map<PropertyKey,Value> asNullableMap() throws DynamicCastException{
    return this.<Map<PropertyKey,Value>>castAs(Kind.MAP,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link Node} (possibly {@code null}) or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#NODEID} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public Node asNullableNode() throws DynamicCastException {
    return this.<Node>castAs(Kind.NODEID,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link Relation} (possibly {@code null}) or throws a {@link DynamicCastException} if {@link #kind} is not {@link Kind#RELID} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public Relation asNullableRelation() throws DynamicCastException {
    return this.<Relation>castAs(Kind.RELID,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link List}&lt;{@link Value}&gt; (possibly {@code null}) or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#LIST}.
    * @see #castAs
  **/
  public List<Value> asNullableList() throws DynamicCastException {
    return this.<List<Value>>castAs(Kind.LIST,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link Path} (possibly {@code null}) or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#PATH} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public Path asNullablePath() throws DynamicCastException {
    return this.<Path>castAs(Kind.PATH,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link String} (possibly {@code null}) or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#STRING} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public String asNullableString() throws DynamicCastException {
    return this.<String>castAs(Kind.STRING,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link Boolean} (possibly {@code null}) or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#BOOL} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public Boolean asNullableBoolean() throws DynamicCastException {
    return this.<Boolean>castAs(Kind.BOOL,Kind.NULL).second;
  }

  /** Casts {@link #content} to {@link Integer} (possibly {@code null}) or throws a {@link DynamicCastException}
    * if {@link #kind} is not {@link Kind#INT} or {@link Kind#NULL}.
    * @see #castAs
  **/
  public BigInteger asNullableInteger() throws DynamicCastException {
    return this.<BigInteger>castAs(Kind.INT,Kind.NULL).second;
  }


  public Value evaluate(Graph graph, Record record) { return this; }

  @Override
  public int hashCode() {
    return Utils.hashTuple(Value.class,content==null?0:content.hashCode());
  }

  /** {@inheritDoc}
    * @beware Implementation in {@link Value} tests content equality, and has nothing to do to with semantic equality, which is defined in function {@link Values#semEquals}.
  ***/
  @Override
  public boolean equals(Object o) {
    if (! (o instanceof Value) )
      return false;
    Value other = (Value) o;
    if ( !(other.isOfKind(kind)) )
      return false;
    if (kind == Kind.NULL)
      return true;
    return ( content.equals(other.content) );
  }

  @Override
  public String toString() {
    switch (kind) {
      case LIST:
        return StringOf.iterable(unsafeCastAsList(),"[","]",",");
      case NULL: return "null";
      case STRING: return ("\""+content.toString()+"\"");
      case MAP:
        return (Utils.mapToString(unsafeCastAsMap()));
      default: return content.toString();
    }
  }


  @Override public
  int compareTo(Value v) {
    Kind k1 = getKind(),
         k2 = v.getKind();
    if (k1 != k2)
      return k1.compareTo(k2);
    switch (k1) {
      case MAP: {
        Map<PropertyKey,Value> m1 = this.unsafeCastAsMap();
        Map<PropertyKey,Value> m2 = v.unsafeCastAsMap();
        Set<PropertyKey> keys = new HashSet<>(m1.keySet());
        keys.addAll(m2.keySet());
        List<PropertyKey> key_list= new ArrayList<>(keys);
        Collections.sort(key_list);
        for (PropertyKey key : key_list) {
          if (! m1.containsKey(key) )
            return -1;
          if (! m2.containsKey(key) )
            return 1;
          int o = m1.get(key).compareTo(m2.get(key));
          if (o != 0)
            return o;
        }
        return 0;
      }
      case NODEID:
        return unsafeCastAsNode().compareTo(v.unsafeCastAsNode());
      case RELID:
        return unsafeCastAsRelation().compareTo(v.unsafeCastAsRelation());
      case PATH:
        return unsafeCastAsPath().compareTo(v.unsafeCastAsPath());
      case NULL:
        return 0;
      case LIST: {
        for (OptPair<Value,Value> p : new ConcurrentIterable<Value,Value>
                                  (unsafeCastAsList(), v.unsafeCastAsList())) {
          if (p.hasBoth()) {
            int comp = p.left().compareTo(p.right());
            if (comp != 0)
              return comp;
          } else
            return (p.hasRight()?-1:1);
        }
        return 0;
      }
      case INT:
        return unsafeCastAsInteger().compareTo(v.unsafeCastAsInteger());
      case STRING:
        return unsafeCastAsString().compareTo(v.unsafeCastAsString());
      case BOOL: {
        boolean b = unsafeCastAsBoolean();
        if (b == v.unsafeCastAsBoolean())
          return 0;
        if (b)
          return -1;
        else
          return 1;
      }
    }
    throw new UnreachableStatementError();
  }

  /** Indicate whether this value is storable, that is, if it contains no
    * reference to a graph.
    * @return {@code true} if this value is storable, and {@code false} otherwise.
  ***/
  public boolean isStorable()
  {
    Collection<Value> c = null;
    switch (getKind()) {
      case MAP: c = unsafeCastAsMap().values();  break;
      case LIST: c = unsafeCastAsList(); break;
      case STRING:
      case BOOL:
      case INT:
      case NULL: return true;
      case NODEID:
      case RELID:
      case PATH: return false;
    }
    return (c.stream().allMatch(x->x.isStorable()));
  }


//   public enum Comparability {
//     NONE,
//     EITHER,
//     WEAK,
//     STRONG;
//
//     public Comparability and(Comparability that)
//     { return ( (this.compareTo(that)>=0)?(that):(this) ); }
//   }
//
//   public
//   Comparability comparabilityWith(Value that) {
//     Kind k1 = this.getKind(),
//          k2 = that.getKind();
//     if (k1 == Kind.NULL || k2 == Kind.NULL)
//       return Comparability.WEAK;
//     if (k1 != k2)
//       return Comparability.NONE;
//     switch (k1) {
//       case MAP: {
//         Map<PropertyKey,Value> m1 = this.unsafeCastAsMap(),
//                                m2 = that.unsafeCastAsMap();
//         if ( !m1.keySet().equals(m2.keySet()) )
//           return Comparability.NONE;
//         Comparability status  = Comparability.STRONG;
//         for (PropertyKey key : m1.keySet()) {
//           status = status.and(m1.get(key).comparabilityWith(m2.get(key)));
//         }
//         return status;
//       }
//       case LIST: {
//         Comparability status  = Comparability.STRONG;
//         for (OptPair<Value,Value> p : Iterators.combine(this.unsafeCastAsList(),
//                                                     that.unsafeCastAsList())) {
//           if (p.hasBoth()) {
//             status = status.and(p.left().comparabilityWith(p.right()));
// //              System.out.println("  "+p.left().comparabilityWith(p.right())+"  "+p.left()+"  "+p.right());
//           }
//         }
//         return status;
//       }
//       default: return Comparability.STRONG;
//     }
//   }
//
//   public
//   boolean comparabilityWithIs (Value that, Comparability expected) {
//     Comparability got = this.comparabilityWith(that);
//     if (got == expected)
//       return true;
//     return (expected == Comparability.EITHER && got.compareTo(expected)>=0);
//   }

}


// public static NULL = new Object();
