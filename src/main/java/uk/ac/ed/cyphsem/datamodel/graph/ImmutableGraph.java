/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import java.util.Collection;

public class ImmutableGraph extends Graph {

  public ImmutableGraph(Graph g) {  this(g.nodes(), g.relations());  }
  public ImmutableGraph(Collection<? extends Node> nodes, Collection<? extends Relation>relations) {
    this.nodes.addAll(nodes);
    this.relations.addAll(relations);
  }


  @Override public void addNode(Node n) {
      throw new UnsupportedOperationException("Attempt to add node to an ImmutableGraph.");  }

  @Override public void addRelation (Relation r)
  {
    throw new UnsupportedOperationException("Attempt to add relation to an ImmutableGraph."); }

//   @Override public void addLabel(Node n,Label l) {
//     throw new UnsupportedOperationException("Attempt to add a label to a node in an ImmutableGraph."); }

//   @Override
//   public void addProperty(Node n, PropertyKey k, Value v) {
//     throw new UnsupportedOperationException("Attempt to add a property to a Node in an ImmutableGraph.");  }
//
//   @Override
//   public void addProperty(Relation n, PropertyKey k, Value v) {
//     throw new UnsupportedOperationException("Attempt to add a property to a Relation in an ImmutableGraph.");  }
}
