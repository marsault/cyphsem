/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.utils.*;

/** Class representing a property key, that is, a lookup key for node and
  * relation properties.
  * It is essentially a wrapper for String.
***/
public class PropertyKey extends Wrapper<String>
implements Comparable<PropertyKey> {

  /** Constructs a PropertyKey by wrapping parameter. */
  public PropertyKey (String content) { super(content); }

  @Override
  public int compareTo(PropertyKey that) { return super.compareTo(that); }
}
