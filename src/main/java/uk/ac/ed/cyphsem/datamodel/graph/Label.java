/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.utils.*;

/** Class representing labels, that is, atomic data attached to a {@link Node}.
  * A {@link Label} is essentially a wrapper around a String, and is distinct from
  * {@link RelationType} for type checking reasons : nodes bear labels, while
  * relations bear types.
***/
public class Label extends Wrapper<String> implements Comparable<Label> {

/** Constructs a {@link Label} wrapping the parameter. */
  public Label (String content) { super(content); }

  @Override
  public int compareTo(Label label) { return super.compareTo(label); }
}
