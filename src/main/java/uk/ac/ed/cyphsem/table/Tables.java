/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import uk.ac.ed.cyphsem.utils.*;

import java.lang.IllegalArgumentException;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/** Collects utility function about {@link Table }
***/
public interface Tables {


  /** Computes the difference between two {@link Table}s considered as multisets of {@link Record}.
    * <br>
    * The returned value maps a {@link Record} to its multiplicity in {@code left} {@link Table} minus its multiplicity in {@code right} {@link Table}.
    * The returned map may then associate some {@link Record} with a negative {@link Integer}, which means that the {@link Record} appears more times in {@code right} than in {@code left}.
    * <br>
    * {@link Record}s that have the same multiplicity in both {@link Table} are not part of the keySet of the returned map.
    * Hence, in particular, if the two {@link Table}s are equal, then this method returns an empty map.
    * @param left Table in which multiplicity of records is counted positively
    * @param right Table in which multiplicity of records is counted negatively
    * @return A mapping witnessing the difference between the two given tables.
    * @throws IllegalArgumentException if the two {@link Table}s are not uniform.
    * @beware This does not compute multiset-minus or multiset-symmetric-difference.
  ***/
  public static Map<Record,Integer> diff(Table left, Table right) {
    Map<Record,Integer> result = new HashMap<>();
    if ( (! left.isEmpty()) && (! right.isEmpty()) )
      if (! left.domain.equals(right.domain()))
        throw new IllegalArgumentException("The two tables are not uniform.");
    Set<Record> records = new HashSet<> (left.recordSet());
    records.addAll(right.recordSet());
    for(Record record : records) {
      int mult = left.multiplicityOf(record) - right.multiplicityOf(record);
      if (mult != 0)
        result.put(record, mult);
    }
    return result;
  }
}
