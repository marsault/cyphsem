/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import uk.ac.ed.cyphsem.language.expression.Expression;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.exception.UndefinedNameException;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.utils.*;

/** Simple {@link String} {@link Wrapper} representing names, that is, variables
  * in expression or column names of {@link Table}s/{@link Record}s.
**/
public class Name extends Wrapper<String> implements Expression, Comparable<Name> {

  /** Constructs a {@link Name} that wraps given parameter. */
  public Name (String content)
  { super(content); }

  /** Constructs a {@link Name} from an expression.
    * Current implementation is to set field {@link #content} to {@code "`" + exp.toString() + "`"}.
  ***/
  public Name (Expression exp)  { this( "`" + exp.toString() + "`" ); }

  /** {@inheritDoc}
    * <br><br>
    * A {@link Name} is evaluated by looking up parameter {@code record}.
    * @throws UndefinedNameException if this {@link Name} is not bound in parameter {@code record}.
    * @see Record#get
  ***/
  public Value evaluate(Graph graph, Record record) {
      return (record.get(this));
  }

  public int compareTo(Name name)  { return super.compareTo(name); }
}
