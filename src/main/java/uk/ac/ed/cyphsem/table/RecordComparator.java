/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import java.util.Comparator;
import java.util.List;

import uk.ac.ed.cyphsem.language.expression.*;

import uk.ac.ed.cyphsem.language.query.OrderBy.OrderedExpression;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.datamodel.value.Value;

/** Implements a Comparator for Record based on a list of {@link OrderedExpression}, stored in field {@link #orderDesc}
  *
**/
public class RecordComparator implements Comparator<Record>  {

  /** Ordered expression used to determine order of {@link Record}. */
  protected List<OrderedExpression> orderDesc;

  /** Graph used for {@link Expression} {@link Expression#evaluate evaluation}. */
  protected Graph graph;


  /** Constructs a {@link RecordComparator} that wraps given parameters */
  public RecordComparator(Graph g, List<OrderedExpression> orderDesc) {
    this.graph = graph;
    this.orderDesc = orderDesc;
  }

  /** {@inheritDoc}
    * <br><br>
    * Implementation in {@link RecordComparator} evaluates each expression in field {@link #orderDesc} twice, once with {@link Graph} in field {@link #graph} and parameter {@code left} and once with once with {@link Graph} in field {@link #graph} and parameter {@code right}.
    * The {@link Value}s thus obtained are compared pairwise with {@link Value#compareTo}.
    * First time we obtain a non-zero integer <var>i</var>, the method returns <var>i<var> if the {@link OrderedExpression} is {@link OrderedExpression#ascending}, or <var>-i</var> otherwise.
  **/
  @Override
  public int compare (Record left, Record right) {
    for (OrderedExpression oe : orderDesc) {
      Value left_value = oe.exp().evaluate(graph,left),
            right_value = oe.exp().evaluate(graph,right);
      int comparison = left_value.compareTo(right_value);
      if (comparison != 0)
        return (comparison * (oe.ascending()?1:-1));
    }
    return 0;
  }


}
