/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

/** Collects utility static functions regarding {@link Name}.
***/
public class Names {

  /** Static counter used for providing fresh {@link Name}s. */
  protected static int freshCounter = 0;

  /** Provides a fresh {@link Name}.
    * <br>
    * Returned {@link Name} is ensured to be distinct from any {@link Name} previously obtained by calls to {@code Names.fresh()}.
    * Returned {@link Name} is illegal with respect to Cypher grammar, hence will never clash with {@link Name}s provided by the user.
  ***/
  public static Name fresh() {
    return new Name ("@"+(freshCounter++));
  }
}
