/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.utils.*;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.lang.Iterable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.lang.Throwable;

import java.util.function.Function;
import java.util.function.Supplier;


/** Represents a ``row from the driving table``.
  * <br>
  * Formally, a record is a function from {@link Name}s to {@link Value}s.
  * It is implemented here as a {@link Map}&lt;{@link Name},{@link Value}&gt; stored in field {@link #map}.
  * <br>
  * Note that methods {@link #get} and {@link #bind}, as implemented here, are less permissive than their counterpart in {@link Map} (that is, {@link Map#get get} and {@link Map#put put}).
***/
public class Record {

  /** Wrapped {@link Name}-to-{@link Value} map. **/
  protected Map<Name,Value> map;

  /** Constructs an empty {@link Record}. **/
  public Record()  { map = new HashMap<Name,Value>() ; }

  /** Constructs a {@link Record} containing a single entry. **/
  public Record(Name name, Value value)  {
    this();
    bind(name, value);
  }

  /** Constructs a {@link Record} containing two entries. **/
  public Record(Name name1, Value value1, Name name2, Value value2)  {
    this(name1, value1);
    bind(name2, value2);
  }

  /** Constructs a {@link Record} containing three entries. **/
  public Record
    ( Name name1, Value value1, Name name2, Value value2, Name name3,
      Value value3
    )
  {
    this(name1, value1, name2, value2);
    bind(name3, value3);
  }

  /** Constructs a copy of given {@link Record}.
    * <br>
    * @fordev Wrapped {@link Map} is copied but not contained {@link Name}s and {@link Value}s.
  ***/
  public Record(Record other)  { map = new HashMap<Name,Value>(other.map); }

  /** Indicates whether this {@link Record} has the same domain as another given {@link Record}. */
  boolean isUniformWith(Record other) { return isUniformWith(other.domain()); }

  /** Indicates whether this {@link Record} has the same domain as another given {@link Record}. */
  boolean isUniformWith(Set<Name> name_set)
  { return (name_set.equals(domain())); }

  /** Indicates whether a given{@link Name} is bound in this {@link Record}. */
  public boolean isBound(Name n) { return map.containsKey(n); }


  /** Gets the {@link Value} associated with a given {@link Name}, or throws {@link UndefinedNameException} if given {@link Name} is not associated with anything.
    * @param n {@link Name} to lookup.
    * @return the value associated with {@code n} (cannot be {@code null} but may be of {@link Kind Kind} {@link Kind#NULL NULL})
    * @throws UndefinedNameException if {@code n} is not a key of {@link #map}
  ***/
  public Value get(Name n)
    throws UndefinedNameException
  {
    if (!isBound(n))
      throw new UndefinedNameException(n,this);
    else
      return map.get(n);
  }

  @Override
  public String toString()  { return StringOf.propertyMap(map); }

  /** Binds the specified {@link Value} with the specified {@link Name} in this {@link Record}, or throws a {@link RecordInconsistencyException} if it contradicts a previous bindings.
    * @throws RecordInconsistencyException if this {@link Record} already binds {@link Name} {@code n} to a {@link Value} different then {@code v}.
  ***/
  public void bind(Name n, Value v)
    throws RecordInconsistencyException
  {
    if (n == null)
      throw new NullPointerException("null Names are not allowed in Records.");
    if (v == null)
      throw new NullPointerException("null Values are not allowed in Records.");
    if (isBound(n)) {
      if (! v.equals(get(n)) )
        throw new RecordInconsistencyException(n+":"+v+" is inconsistent with record "+this);
    } else {
      map.put(n,v);
    }
  }

  /** Binds the specified {@link Name} to the specified {@link Value} in this {@link Record}, possibly overwriting a previous bindings.
    * <br>
    * {@code null} parameters are not allowed.
  ***/
  public void overbind(Name n, Value v)
  {
    unbind(n);
    bind(n,v);
  }

  /** Removes the binding associated with given {@link Name}.
    * <br>
    * A {@code null} parameter is not allowed.
    * @return {@code true} if {@code n} was indeed bound.
  ***/
  public boolean unbind(Name n)
  {
    if (n == null)
      throw new NullPointerException("null Names are not allowed in Records.");
    return (map.remove(n) != null);
  }


  /** Binds each unbound {@link Name} in {@code names} to a {@link Kind#NULL} {@link Value}.
  ***/
  public void bindAllUnboundToNull(Collection<? extends Name> names) {
    for (Name name : names)
      if (! isBound(name) )
        bind(name, new Value());
  }


  /** Returns the set of {@link Name}s that are bound in this {@link Record}.
    * @see Map#keySet
  ***/
  public Set<Name> domain() { return map.keySet(); }

  /** Returns the set of {@link Name}-to-{@link Value} bindings in this {@link Record}.
    * @see Map#keySet
  ***/
  public Set< Map.Entry<Name,Value>> bindings()
  { return map.entrySet(); }


  /** Indicates whether some other object is "equal to" this one.
    * <br>
    * Returns {@code true} if all the following hold.
    * <ul>
    * <li> Parameter {@code o} is non-{@code null}. </li>
    * <li> Parameter {@code o} is an instance of {@link Record}. </li>
    * <li> The respective fields {@link #map} of parameter {@code o} and this {@link Record} are {@link Map#equals equal}.</li>
    * </ul>
  ***/
  @Override
  public boolean equals (Object o)
  {
    if (! (o instanceof Record))
      return false;
    return map.equals( ((Record) o).map );
  }

  @Override
  public int hashCode ()  { return Objects.hash(getClass(), map); }

  /** Verifies that {@link Record} {@code r} binds {@link Name} {@code n} to {@link Value} {@code v}, or
  if {@code n} is unbound, add this binding to a copy of {@code r}.
    * @return this {@link Record} if it already binds {@code n} to {@code v}.
    Otherwise, {@code r} is copied and the extra binding is added to the copy.
    * @throws RecordInconsistencyException if @{link Record} {@code r} binds {@link Name} {@code n}
  to a {@link Value} that is not {@code v}.
  ***/
  public static Record ensureBinding(Record r, Name n, Value v)
    throws RecordInconsistencyException
  {
    if (r.isBound(n)) {
      if (v.equals(r.get(n)))
        return r;
      else
        throw new RecordInconsistencyException(n+":"+v+" is inconsistent with record "+r);
    }
    else {
      Record r2 = new Record(r);
      r2.bind(n,v);
      return r2;
    }
  }

  /** Copy this {@link Record} and maps each values thanks to the given mapping.
    * A {@link Value} mapped to {Optional#empty() EMPTY} keeps its old value.
  * @see Table#map
  */
  public Record map(Function<Value,Optional<Value>> mapping) {
    Record result = new Record();
    for (Name key : domain() ) {
      Value old_value = get(key);
      Value new_value = mapping.apply(old_value).orElse(old_value);
      result.bind(key,new_value);
    }
    return result;
  }
}
