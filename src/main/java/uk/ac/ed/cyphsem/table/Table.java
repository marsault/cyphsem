/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import uk.ac.ed.cyphsem.datamodel.value.Value;

import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.utils.*;

import java.lang.IllegalArgumentException;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Optional;

import java.util.function.Function;

/** Implements a multiset of Record.
  *
***/
public class Table extends AbstractCollection<Record> {

  /** Map giving the multiplicity.
    * <br> The contract of this class ensures that
    * <ul><li>
    * no {@link Record} is associated with 0.
    * </li><li>
    * no {@link Record} is assocaited with a negative integer.
    * </li></ul>
  ***/
  protected HashMap<Record,Integer> map = new HashMap<>();

  /** Domain of this {@link Table}, that is the domain of each {@link Record} in this {@link Table}.
    * If not set at object construction, set when the first {@link Record} is added to the {@link Table}.
  ***/
  Set<Name> domain = null;

  /** Size of this {@link Table}, counting multiplicity.
    * Updated by method {@link #put}.
  ***/
  protected int size = 0;

  /** Constructs an empty {@link Table}. */
  public Table() { }

  /** Constructs an empty {@link Table} with given domain. */
  public Table(Set<Name> domain) { this.domain = domain;  }

  /** Constructs a {@link Table} with given number of copies of given {@link Record}. */
  public Table(Record r, Integer i) {  this(); add(r,i); }

  /** Constructs a shallow copy of given {@link Table}. */
  public Table(Table t) {  this();  addAll(t);  }

  /** Constructs a {@link Table} containing all given {@link Record}s. */
  public Table(Iterable<Record> records) { this(); addAll(records); }

  /** Constructs a {@link Table} containing all given {@link Record}s. */
  public Table(Record... records) { this(); addAll(Immutable.listOf(records)); }

  @Override
  public boolean isEmpty() {
    return map.isEmpty();
  }

  @Override
  public int size() {
    return size;
  }

  /** Adds multiple copies of given {@link Record}.
    * @param r Element to remove.
    * @param i Number of copies to add. If negative, copies of {@code o} are effectively remove.
    * @return the number of copies effectively added. (If {@code i} is negative, returned value may also be negative.)
    * @throws IllegalArgumentException if {@code r} is not uniform with this {@link Table}.
    * @see remove(uk.ac.ed.cyphsem.table.Record,int)
 ***/
  public int add(Record r, int i) {
    int j = multiplicityOf(r);
    int new_multiplicity = (j+i)>0 ? j+i : 0;
    put(r,new_multiplicity);
    return (new_multiplicity-j);
  }

  /** Return the multiplicity of given {@link Record} in this {@link Table}. */
  public Integer multiplicityOf(Record r) {
    if (map.containsKey(r))
      return map.get(r);
    else
      return 0;
  }

  /** Returns the sets of {@link Record}s in this {@link Table}. */
  public Set<Record> recordSet() {
    return map.keySet();
  }

  /** Sets the multiplicity of given record.
    * <br>
    * Since no {@link Record} with 0-multiplicity belongs to a {@link Table}, if parameter {@code i} is equals to 0, this effectively removes {@code r} from this {@link Table}.
    * @throws IllegalArgumentException if parameter {@code i} is negative.
    * @throws IllegalArgumentException if parameter {@code r} is not uniform with this {@link Table}.
    * @fordev This method is the only one accessing to field {@link #map} and hence is the only one updating field {@link #size}.
  ***/
  protected void put(Record r, Integer i) {
    if (i<0)
      throw new IllegalArgumentException("Record "+r+" cannot have negative multiplicity.");
    if (domain == null)
      domain = r.domain();
    else if (!r.isUniformWith(domain))
      throw new IllegalArgumentException ("Record "+r+" is not uniform with Table, whose domain is "+domain);
    size -= multiplicityOf(r);
    if (i == 0)
      map.remove(r);
    else
      map.put(r,i);
    size += multiplicityOf(r);
  }

  /** Adds one copy of given {@link Record}.
    * @param r Element to add.
    * @return {@code true}
    * @throws IllegalArgumentException if parameter {@code r} is not uniform with this {@link Table}.
  **/
  @Override
  public boolean add(Record r) {
    add(r,1);
    return true;
  }

  /** Adds one copy of each given {@link Record}.
    * If the given {@link Iterable} has multiple occurrence of the same {@link Record}, it will be added multiple times.
    * @param records Elements to add.
    * @throws NullPOinterException if some element of parameter {@code records} is {@code null}
    * @throws IllegalArgumentException if parameter {@code r} is not uniform with this {@link Table}.
  **/
  public void addAll(Iterable<? extends Record> records) {
    for (Record r: records)
      add(r);
  }

  /** Adds multiple copies of each given {@link Record}.
    * If a {@link Record} occurs <var>j</var> times in the {@link Iterable}, then {@code inputMultiplicity} times <var>j</var> copies of it will be added.
    * <br><br>
    * Effectively removes copies if parameter {@code i} is negative.
    * @param records Elements to add.
    * @throws NullPOinterException if some element of parameter {@code records} is {@code null}
    * @throws IllegalArgumentException if some {@link Record} in {@code records} is not uniform with this {@link Table}.
  **/
  public void addAll(Iterable<? extends Record> records, int inputMultiplicity) {
    for (Record r: records)
      add(r,inputMultiplicity);
  }

  /** Adds all records in given {@link Table} with their multiplicity.
    * @throws IllegalArgumentException if given table is not uniform with {@link Table}.
  ***/
  public void addAll(Table t) {
    for (Record r: t.recordSet())
      add(r,t.multiplicityOf(r));
  }

  /** Adds all records in given {@link Table} with their multiplicity times given {@code multiplicativeFactor}.
    * @throws IllegalArgumentException if given table is not uniform with {@link Table}.
  ***/
  public void addAll(Table t, int multiplicativeFactor) {
    for (Record r: t.recordSet())
      add(r,t.multiplicityOf(r)*multiplicativeFactor);
  }

  /** Only keeps one copy of each {@link Record}. ***/
  public void unique () {
    for (Record r: recordSet())
      map.put(r,1);
  }

  /** Returns the domain of this {@link Table}. ***/
  public Set<Name> domain() { return domain; }

  /** Indicates whether given {@link Name} is part of the domain of this {@link Table} */
  public boolean hasName(Name name) { return domain.contains(name); }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Table))
      return false;

    Table other = (Table) o;
    if (isEmpty() || other.isEmpty())
      return (isEmpty() && other.isEmpty());
    if (!domain.equals(other.domain()))
      return false;

    Set<Record> records = recordSet();
    if (!records.equals(other.recordSet()))
      return false;

    for (Record record : records)
      if ( !multiplicityOf(record).equals(other.multiplicityOf(record)) )
        return false;

    return true;
  }

  @Override
  public int hashCode() {
    return map.hashCode();
  }

  /** Returns any element contained in this {@link Table} or throws an {@link NoSuchElementException} if {@link #isEmpty empty}.
    * @throws NoSuchElementException if Table is empty.
  ***/
  public Record anyOne() {
    if (isEmpty())
      throw new NoSuchElementException("Table.any() : Table is empty.");
    return (iterator().next());
  }


  /** Iterator used to traverse this {@link Table}.
  ***/
  private class TableIterator implements Iterator<Record>{
    /** Iterator of the set of {@link #recordSet Record Set} of the table. */
    Iterator<Record> boxedIterator = recordSet().iterator();

    /** Current Record. */
    Record current;

    /** Remaining copies of {@link #current current Record}. */
    int remainingCopies = 0;


    @Override
    public Record next() {
      if (remainingCopies < 0)
        throw new UnreachableStatementError();
      if (remainingCopies == 0) {
        current = boxedIterator.next();
        remainingCopies = multiplicityOf(current);
      }
      remainingCopies--;
      return current;
    }

    @Override
    public boolean hasNext() {
      return ( remainingCopies > 0 || boxedIterator.hasNext() );
    }

//     /** May be used once per time */
//     @Override
//     public void remove() {
//       if (hasRemoved)
//         throw new IllegalStateException("Attempted to remove twice the same element.");
//       else
//         add(current, -1);
//     }
  }

  @Override
  public Iterator<Record> iterator() {
    return new TableIterator();
  }


  /** Returns on immutable view of the entries in this {@link Table}. */
  public Collection<Pair<Record,Integer>> entries() {
    return new AbstractCollection<Pair<Record,Integer>>() {
      @Override
      public Iterator<Pair<Record,Integer>> iterator() {
        Iterator<Record> boxedIterator = recordSet().iterator();
        return new Iterator<Pair<Record,Integer>>() {
          @Override
          public Pair<Record,Integer> next() {
            Record r = boxedIterator.next();
            return new Pair<>(r, multiplicityOf(r));
          }
          @Override
          public boolean hasNext() {
            return ( boxedIterator.hasNext() );
          }
        };
      }

      @Override
      public int size() { return recordSet().size(); }
    };
  }

  /** Indicates whether given {@link Record} is in this {@link Table}.
    * @return {@code true} if this {@link Table} contains at least one copy of {@code o}.
    * @throws NullPointerException if {@code o} is {@code null}
    * @throws IllegalArgumentException if {@code o} is not an instance of {@link Record}.
  ***/
  @Override
  public boolean contains(Object o) {
    if (o == null)
      throw new NullPointerException("Null elements are not allowed in a Table.");
    if (! (o instanceof Record))
      throw new IllegalArgumentException("Given argument is not a Record.");
    return map.containsKey((Record) o);
  }

  /** Removes one copy of given {@link Record}.
    * @return {@code true} if {@code o} was indeed removed.
    * @throws NullPointerException if {@code o} is {@code null}
    * @throws IllegalArgumentException if {@code o} is not an instance of {@link Record}.
  ***/
  @Override
  public boolean remove(Object o) {
    if (o == null)
      throw new NullPointerException("Null elements are not allowed in a Table.");
    if (! (o instanceof Record))
      throw new IllegalArgumentException("Given argument is not a Record.");
    return (1 == remove((Record) o,1));
  }

  /** Remove multiple copies of given {@link Record}.
    * @param r Element to remove.
    * @param i Number of copies to remove. If negative, copies of {@code o} are effectively added.
    * @return the number of copies effectively removed. (If {@code i} is negative, returned value may also be negative)
    * @throws IllegalArgumentException if {@code r} is not uniform with this {@link Table}.
    * @see add(uk.ac.ed.cyphsem.table.Record,int)
 ***/
  public int remove(Record r, int i) {
    //int j = multiplicityOf(r);
    //int new_multiplicity = (j>i) ? j-i : 0;
    //put(r,new_multiplicity);
    return -add(r,-i);
  }

  /** Copy this {@link Table} and maps each values thanks to the given mapping.
    * A {@link Value} mapped to {Optional#empty() EMPTY} keeps its old value.
    * @see Record#map
    */
  public Table map(Function<Value,Optional<Value>> mapping) {
    Table result = new Table();
    for (Pair<Record,Integer> pair : entries() ) {
      result.add(pair.first.map(mapping), pair.second);
    }
    return result;
  }
}
