/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem;

import java.util.LinkedList;
import java.util.Deque;
import java.util.Optional;
import java.util.EnumSet;

public class Interruptor {
  public static enum Issue {
    NON_DETERMINISM;
  }

  private boolean _interrupted = false ;
  private Deque<String> _warningMessages = new LinkedList<String>();
  private EnumSet<Issue> _issues = EnumSet.noneOf(Issue.class);

  public synchronized void interrupt()
  { _interrupted = true; }

  public synchronized void check() throws InterruptedException {
    if (_interrupted)
      throw (new InterruptedException());
  }

  public void pushWarning(String message)
  { _warningMessages.offer(message);  }

  public void pushWarning(String message, Issue... issues)
  {
    for (Issue issue: issues)
      _issues.add(issue);
    pushWarning(message);
  }

  public String warning() { return _warningMessages.poll(); }

  public boolean hasWarning() { return _warningMessages.isEmpty(); }
}
