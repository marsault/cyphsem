/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.outbridge;

import java.io.File;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import uk.ac.ed.cyphsem.Interruptor;


import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.language.query.QueryData;
import uk.ac.ed.cyphsem.language.parser.Parser;
import uk.ac.ed.cyphsem.exception.AnTLRException;

import uk.ac.ed.cyphsem.language.query.Query;

import uk.ac.ed.cyphsem.table.Table;

/** Class implementing the Cypher Interpreter native to CyphSem.
  * @see Interpreters#CYPHSEM
***/
public class CyphSemInterpreter implements Interpreter {

  /** Stored input graph */
  Graph graph = null;
  /** Stored input query */
  Query query = null;
  /** Interruptor of the query execution currently run (if any). Never {@code null}. */
  Interruptor interruptor = new Interruptor();
  /** The resut of currently run query-execution, when it will be available. */
  Future<QueryData> futureResult = null;

  public CyphSemInterpreter() { }

  public CyphSemInterpreter(Graph graph) { inputGraph(graph); }

  @Override
  public void inputGraph(Graph g)  { graph = g; }

  @Override 
  public void inputQuery(Query q)  { query= q; }

  @Override 
  public Graph outputGraph()
  throws InterruptedException, ExecutionException, ComputationOngoingException
  { 
    if (futureResult.isDone())
      return futureResult.get().graph();
    else
      throw new ComputationOngoingException();
  }
  
  @Override 
  public Table outputTable()  
  throws InterruptedException, ExecutionException, ComputationOngoingException
  { 
    if (futureResult.isDone())
      return futureResult.get().table();
    else
      throw new ComputationOngoingException();
  }

  @Override
  public void run() {
    interruptor = new Interruptor();
    Callable<QueryData> runnable = new Callable<QueryData> () {
        public QueryData call() throws InterruptedException {
          return query.execute(graph, interruptor);
        }
    };
    ExecutorService exe = Executors.newSingleThreadExecutor();
    futureResult = exe.submit(runnable);
  }


  @Override
  public void interrupt()  { interruptor.interrupt(); }

  /**{@inheritDoc}
    * Implemtentation in {@link CyphSemInterpreter} does nothing.
  ***/
  @Override
  public void close() { }


  /**{@inheritDoc} ***/
  @Override public
  String displayName()  { return "CyphSem"; }
}
