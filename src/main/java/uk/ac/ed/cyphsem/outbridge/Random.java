/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.outbridge;

import uk.ac.ed.cyphsem.datamodel.value.Value.*;
import uk.ac.ed.cyphsem.datamodel.value.Value;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Optional;

public class Random {

  Graph graph = null;
  List<Node> nodes = null;
  List<Relation> relations = null;
  java.util.Random seed = new java.util.Random();

  public Random() {
    graph = randomGraph();
    nodes = new ArrayList<Node>(graph.nodes());
    relations = new ArrayList<Relation>(graph.relations());
  }

  public Random(Graph g)
  {
    graph=g;
    nodes = new ArrayList<Node>(graph.nodes());
    relations = new ArrayList<Relation>(graph.relations());
  }

  private int max = 100;
  private int count = max;

  void possiblyReSeed() {
    if (count == 0) {
      seed = new java.util.Random();
      count = seed.nextInt(100)+50;
    }
    count--;
  }

  boolean nextBool() {
    return (seed.nextInt(2) == 0);
  }
  double nextDouble() {
    possiblyReSeed();
    return seed.nextDouble();
  }

  int nextInt() {
    possiblyReSeed();
    return seed.nextInt();
  }

  int nextInt(int a) {
    possiblyReSeed();
    return seed.nextInt(a);
  }

  long nextLong() {
    possiblyReSeed();
    return seed.nextLong();
  }

  public int type_n = 6;
  public static List<RelationType> types= Arrays.asList(
    new RelationType("knows"),
    new RelationType("likes"),
    new RelationType("hates"),
    new RelationType("works_w"),
    new RelationType("lives_w"),
    new RelationType("employs")
  );
  public RelationType randomType() {  return types.get( nextInt(type_n) );  }

  public int key_n = 6;
  public static List<PropertyKey> keys= Arrays.asList(
    new PropertyKey("since"),
    new PropertyKey("until"),
    new PropertyKey("because"),
    new PropertyKey("while"),
    new PropertyKey("at"),
    new PropertyKey("what")
  );
  public PropertyKey randomKey() {  return keys.get( nextInt(key_n) );  }


  public double p_label = 0.6;
  public int label_n = 6;
  public static List<Label> labels = Arrays.asList(
    new Label("human"),
    new Label("smart"),
    new Label("tall"),
    new Label("green"),
    new Label("weird"),
    new Label("blond")
  );
  public Label randomLabel() {  return labels.get( nextInt(label_n) );  };
  public Set<Label> randomLabels() {
    Set<Label> labels = new HashSet<Label>();
    while (nextDouble() < p_label) {
      labels.add(randomLabel());
    }
    return labels;
  }

  public double p_out = 0.7;
  public Graph randomGraph() {
    Graph graph = new Graph();
    int node_n = 12 + nextInt(14);
    List<Node> nodes = new ArrayList<Node>(node_n);
    for (int i = 0; i<node_n; i++) {
      nodes.add(graph.addNode(randomLabels(),randomProperties()));
    }
    for (int i = 0; i<node_n; i++)
      while(nextDouble() < p_out) {
        graph.addRelation( randomType(),
                           nodes.get(i),
                           nodes.get(nextInt(node_n)),
                           randomProperties()
                         );
      }
    return graph;
  }

  double p_map = 0.6 ;
  double p_list = 0.6 ;
  public Map<PropertyKey,Value> randomProperties() {
    Map<PropertyKey,Value> map = new HashMap<PropertyKey,Value>();
    while ( nextDouble() < p_map) {
      Kind kind;
      map.put(randomKey(), randomPropertyValue());
    }
    return map;
  }


  List<Kind> forbidGraphy = Arrays.asList(Kind.NODEID, Kind.RELID, Kind.PATH);
  List<Kind> forbidGraphyOrNull = Arrays.asList(Kind.NODEID, Kind.RELID, Kind.PATH, Kind.NULL);
  List<Kind> underMap = Arrays.asList(Kind.NODEID,Kind.RELID, Kind.PATH, Kind.MAP);
  List<Kind> underMapList = Arrays.asList(Kind.NULL, Kind.NODEID,
                                          Kind.RELID, Kind.PATH, Kind.MAP, Kind.LIST);

  public Kind randomKind(List<Kind> forbiddenKinds) {
    List<Kind> authorized = new ArrayList<Kind>(Arrays.asList(Kind.values()));
    authorized.removeAll(forbiddenKinds);
    return randomFromList(authorized,randomFromList(forbiddenKinds,Kind.NULL));
  }


  public Kind randomKind() {
    return randomKind(Arrays.asList());
  }

//   public Kind randomKind(int i) {
//     return values()[i+nextInt(9-i)];
//   }

//   double p_map = 0.5;
  public Value randomValue() { return randomValue(Arrays.asList()); }
  public Value randomStaticValue() { return randomValue(forbidGraphy); }

  public Value randomPropertyValue() {
    Kind kind = randomKind(underMap);
    if (kind == Kind.LIST) {
      List<Value> list = new ArrayList<Value>(4);
      Kind subkind = randomKind(underMapList);
      while ( nextDouble() < p_list )
//         if (nextDouble() < .90)
          list.add(randomValue(subkind,underMapList));
//         else
//           list.add(new Value());
      return new Value(list);
    } else
      return randomValue(kind,underMap);
  }

  public Value randomValue(List<Kind> forbiddenKinds) {
    return randomValue(randomKind(forbiddenKinds),forbiddenKinds);
  }

  public Value randomValue(Kind kind, List<Kind> forbiddenKinds) {
    switch (kind) {
      case PATH: return new Value(randomPath());
      case NODEID: return new Value(randomNode());
      case RELID: return new Value(randomRelation());
      case NULL: return new Value();
      case INT: return new Value((nextDouble()<0.95)?nextInt(10):nextLong());
      case BOOL: return new Value(nextLong() < 5);
      case LIST:
        List<Value> list = new ArrayList<Value>(5);
        while ( nextDouble() < p_list )
          list.add(randomValue(forbiddenKinds));
        return new Value(list);
      case MAP: return new Value(randomProperties());
      case STRING: return new Value(randomString());
      default: throw new Error("UnreachableStatement");
    }
  }

  public <E> E randomFromList(List<E> l, E default_value) {
    if ((l == null) || l.size() == 0)
      return default_value;
    else
      return (l.get(nextInt(l.size())));
  }

  public Node randomNode() {
    Node default_value = new Node(nextInt(1000000));
    if (nextDouble() <.98)
      return randomFromList(nodes, default_value);
    else
      return default_value;
  }

  public Relation randomRelation() {
    Relation default_value = new Relation(nextInt(1000000), randomType(), randomNode(), randomNode());
    if (nextDouble() <.98)
      return randomFromList(relations, default_value);
    else
      return default_value;
  }

  public Path randomPath() {
    if (nextDouble() > .98)
      return new Path();
    LinkedList<Node> nodes= new LinkedList<Node>();
    LinkedList<Relation> relations= new LinkedList<Relation>();
    nodes.add(randomNode());
    double proba = .95;

    while (nextDouble() < proba) {
      Node end = nodes.peekLast();
      Relation default_value = randomRelation();
      List<Relation> possibilities = new LinkedList<Relation>(end.in);
      possibilities.addAll(end.out);
      possibilities.addAll(end.loop);
      Relation new_relation = randomFromList(possibilities, default_value);
      relations.add(new_relation);
      nodes.add(new_relation.target());
      proba *= 3./5.;
    }

    return new Path(nodes,relations);
  }


  public String randomString() {
    char[] chars = {'a','b','c','d','e' };
    String res = (nextDouble() < 0.95)? ""+chars[nextInt(5)] : "";
    while ( nextDouble() < 0.5 )
      res += chars[nextInt(5)];
    return res;
  }

  public Expression randomExpression(Set<Name> variables) {
    double p = nextDouble();
    if (p < 0.33)
      return randomStaticValue();
    else if (p < 0.66)
      return randomName(variables, true);
    else {
      switch (nextInt(4)) {
        case 0:
          return new UnaryMapExpression(randomName(variables, true), randomKey());
        case 1:
          return new FunctionalExpression(Functions.LEQ, randomExpression(variables), randomExpression(variables));
        default:
          int length = (nextDouble() < 0.95)?1:0;
          while (nextDouble() < proba_qbis) length++;
          List<Expression> exps = new ArrayList<Expression>(length);
          for (int i = 0; i< length; i++)
            exps.add(randomExpression(variables));
          return new ExplicitListExpression(exps);
      }
    }
  }

  double proba_qbis = 0.55;
  public QueryBis randomQueryBis() {
    int length = (nextDouble() < 0.95)?1:0;
    while (nextDouble() < proba_qbis) length++;
    List<Clause> list = new ArrayList<Clause>(length);
    Set<Name> variables= new HashSet<Name>();
    variables.add(randomName());
    list.add(randomMatchClause(variables));
    for (int i = 0; i< length-1; i++)
      list.add(randomClause(variables, !(list.get(i) instanceof UnwindClause) ));
    ReturnQueryBis ret = new ReturnQueryBis();
    if (length == 0)
      return ret;
    else
      return new ClauseQueryBis(list, ret);
  }

  public int name_n = 24;
  public static List<Name> names = Arrays.asList(
    new Name("u1"), new Name("u2"), new Name("u3"), new Name("u4"),
    new Name("v1"), new Name("v2"), new Name("v3"), new Name("v4"),
    new Name("w1"), new Name("w2"), new Name("w3"), new Name("w4"),
    new Name("x1"), new Name("x2"), new Name("x3"), new Name("x4"),
    new Name("y1"), new Name("y2"), new Name("y3"), new Name("y4"),
    new Name("z1"), new Name("z2"), new Name("z3"), new Name("z4")
  );
  public Name randomName() {  return names.get( nextInt(name_n) );  }

  public Name randomName(Set<Name> variables) {
    Name name = randomName();
    variables.add(name);
    return name;
  }

  double p_inset = 0.99;
  public Name randomName(Set<Name> variables, boolean inSet) {
    if ((nextDouble() > p_inset) || (inSet && variables.isEmpty()) )
      return randomName(variables);
    while (true) {
      Name n= randomName();
      if (inSet == variables.contains(n)) {
        variables.add(n);
        return n;
      }
    }
  }

  double proba_tuple = 0.3;
  public Clause randomMatchClause(Set<Name> variables) {
    List<PathPattern> list = new ArrayList<PathPattern>(3);
    list.add( randomPathPattern(variables) );
    while ( nextDouble() < proba_tuple)
      list.add( randomPathPattern(variables) );
    if (nextBool())
      return new MatchClause(list);
    else
      return new OptionalMatchClause(list);
//     return c;
  }

  double p_as = 0.7;
  @SuppressWarnings("fallthrough")
  public Clause randomWithClause(Set<Name> variables) {
    boolean b = true;
    switch ( nextInt(3) ) {
      case 0: return new WithClause();
      case 1: b = false;
      default:
        List<WithItem> list = new ArrayList<WithItem>(5);
        while ( nextDouble()< p_as) {
          Optional<Name> n
            = (nextDouble() < 0.75) ? Optional.of(randomName(variables, false))
                                    : Optional.empty();

          list.add(new WithItem(Optional.empty(), false, n, randomExpression(variables)));
        }
        return new WithClause(b,list);
    }
  }

  public Clause randomWhereClause(Set<Name> variables) {
    return new WhereClause(randomExpression(variables));
  }

  public Clause randomUnwindClause(Set<Name> variables) {
    return new UnwindClause(randomExpression(variables), randomName(variables, false));
  }

  public Clause randomClause(Set<Name> variables, boolean canWhere) {
    switch (nextInt(5)) {
      case 0: return randomWithClause(variables);
      case 1: if (!canWhere && nextDouble()<0.99)
                return randomMatchClause(variables);
              else return randomWhereClause(variables);
      case 2: return randomUnwindClause(variables);
      default: return randomMatchClause(variables);
    }
  }

  public PathPattern randomPathPattern(Set<Name> variables) {
    boolean named = (nextDouble() < p_name);
    Name n = named ? randomName(variables, false) : null;
    UnnamedPathPattern p = randomUnnamedPathPattern(variables);
    if (named)
      return new NamedPathPattern(n, p);
    else
      return p;
  }

  double p_end_path = 0.7;

  public UnnamedPathPattern randomUnnamedPathPattern(Set<Name> variables) {
    if (nextDouble() < 0.7 )
      return randomNodePattern(variables);
    else
      return randomRelationPattern(variables);
  }

  double p_limit = 0.4;
  public RelationPattern randomRelationPattern(Set<Name> variables) {
    NodePattern previous = randomNodePattern(variables);
    UnnamedPathPattern next = randomUnnamedPathPattern(variables);
//     int length = 1;
//     while () {  length++;  }
    List<Object> extras1 = new ArrayList<Object>();
    List<Object> extras2 = new ArrayList<Object>();
    extras1.add(randomDirection());
    while (nextDouble() < p_limit) {
      if (nextDouble() < 0.5)
        extras1.add(randomType());
      else
        extras2.add(randomKeyExpPair(variables));
    }
    if (nextDouble() < 0.4)
      extras1.add(randomName(variables, false));
    if (nextDouble() < 0.33 ) {
      return new MultipleRelationPattern( randomIteration(), previous, next,
                                          extras1.toArray());
    } else {
      extras1.addAll(extras2);
      return new SimpleRelationPattern(previous, next, extras1.toArray());
    }
  }

  double p_iter = 0.75;
  public Iteration randomIteration() {
    int n1 = 0;
    while ( nextDouble() < p_iter)
      n1++;
    int n2 = n1-1;
    while ( nextDouble() < p_iter)
      n2++;
    switch (nextInt(4)) {
      case 0: return new Iteration.Fixed(n1);
      case 1: return new Iteration.Between(n1,n2);
      case 2: return new Iteration.From(n1);
      default: return new Iteration.Until(n1);
    }
  }

  public RelationPattern.Direction randomDirection() {
    return RelationPattern.Direction.values()[nextInt(3)];
  }

  double p_name = 0.4;
  public NodePattern randomNodePattern(Set<Name> variables) {
    int length = 0;
    while (nextDouble() < p_limit) {  length++;  }
    List<KeyExpPair> list = new ArrayList<KeyExpPair>(length);
    List<Label> set = new ArrayList<Label>(length);
    Name name = (nextDouble() < p_name) ? randomName(variables) : null;
    for (int i = 0; i<length; i++) {
      if (nextDouble() < 0.5)
        set.add(randomLabel());
      else
        list.add(randomKeyExpPair(variables));
    }
    return new NodePattern(name, set,new ExplicitMapExpression(list));
  }

  public KeyExpPair randomKeyExpPair(Set<Name> variables) {
    return new KeyExpPair(randomKey(),randomExpression(variables));
  }

}
