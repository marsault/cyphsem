package uk.ac.ed.cyphsem.outbridge;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.utils.StringOf;
import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.query.Query;
import uk.ac.ed.cyphsem.language.query.QueryData;
import uk.ac.ed.cyphsem.language.parser.Parser;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.concurrent.ExecutionException;
import java.util.Optional;

import java.lang.AutoCloseable;
import java.lang.InterruptedException ;

/** This interface is the main interface for connecting impplementation of Cypher to CyphSem.
  * Note that most methods in the interface have default implementation because this interface permits multiples ways to implement all required features. 
  * 
  *  Although the compiler won't complain, note that {@bold at least one variant of each method must still be implemented}.  If a method name is {@code xxxYyy} then the variant are named {@code xxxYyyy1}, {@code xxxYyyy2}.
  */
public interface Interpreter extends AutoCloseable{

  /** Sets the input graph for a later call to {@link #run}. ***/
  void inputGraph(Graph g);

  /** Sets the input query for a later call to {@link #run}. ***/
  void inputQuery(Query q);


  /** Sets the input graph and query for a later call to {@link run}. ***/
  default  void input(Graph g, Query q) {
    inputGraph(g);
    inputQuery(q);
   }

// /** Sets the input graph for a later call to {@link #run}.
//    * @param file File containing the representation of a graph as a Cypher query.
//    * @fordev Note that implementing class needs only to override one of {@link #inputGraph1}, {@link #inputGraph2} and {@link #inputgraph3}.
//  */
//  default void inputGraph3(Path file) 
//  { 
//    throw new Error("No variant of inputGraph is implemented by + "
//                    + this.getClass() + "."); 
//  }
//
//  /** Sets the query to execute in the next call to {@link run}.
//    * @fordev Note that implementing class needs only to override one of {@link #inputQuery1}, {@link #inputQuery2} and {@link #inputQuery3}.
//  ***/
//  default void inputQuery1(Query q) { inputQuery2(q.toString()); }
//  
//  /** Sets the query to execute in the next call to {@link run}.
//    * @fordev Note that implementing class needs only to override one of {@link #inputQuery1}, {@link #inputQuery2} and {@link #inputQuery3}.
//  ***/
//  default void inputQuery2(String str) 
//  { 
//    inputQuery3(Utils.tmpFileOfString(str, ".cypher"));
//  }
//
//  /** Sets the query to execute in the next call to {@link run}.
//    * @param file File containing the query to execute.
//    * @fordev Note that implementing class needs only to override one of {@link #inputQuery1}, {@link #inputQuery2} and {@link #inputQuery3}.
//  ***/
// 
//  default void inputQuery3(Path file) {
//    throw new Error("No variant of inputQuery is implemented by "
//                    + this.getClass() + "."); 
// }

  /** Execute query previously given by one of the {@code inputGraph} on graph previously given by {@code inputQuery} variant.
    * @throws IllegalStateException if no query or no graph was set prior to the call to run().
    * @fordev This function should return immediately and launch computation in its own thread.
  ***/
  void run();


  /** Provides the complete output of previous call to {@link #run} 
    * Default implementation builds a new {@link QueryData} from values returned from {@link outputGraph} and {@link outputTable}.
    * @return The output {@link Graph} and {@link Table}.
    * @throws IllegalStateException If called prior to any call to {@link#run}.
    * @throws ExecutionException If interpretation failed.
    * @throws InterruptedException If computation was interrupted by {@link #interrupt}.
    * @throws ComputationOngoingException If computation is not finished and was not interrupted.
    * @see outputGraph
    * @see outputQuery
  ***/
  default public 
  QueryData output()
  throws InterruptedException, ExecutionException, ComputationOngoingException 
  {
    return new QueryData(outputGraph(),outputTable());
  }

  
  /** Provides the complete output of previous/ongoing interpretation, waiting 
    * as long as necessary.
    * @return The output {@link Graoh} and {@link Table}
    * @throws IllegalStateException If called prior to any call to {@link#run}.
    * @throws ExecutionException If interpretation failed.
    * @throws InterruptedException If computation was interrupted by {@link #interrupt}.
    * 
  ***/
  default public
  QueryData waitForOutput() 
  throws InterruptedException, ExecutionException
  {
    while (true) {
      try {
        return output();
      }
      catch (ComputationOngoingException e){ }
      Thread.sleep(10);
    }
  }


  /** Provides the output graph of previous call to {@link #run}.
    * @return The output {@link Graph} of previous interpretation.
    * @throws IllegalStateException If called prior to any call to {@link#run}.
    * @throws ExecutionException If interpretation failed.
    * @throws InterruptedException If computation was interrupted by {@link #interrupt}.
    * @throws ComputationOngoingException If computation is not finished and was not interrupted.
    * @see output
    * @see outputQuery
  ***/
  Graph outputGraph () 
  throws InterruptedException, ExecutionException, ComputationOngoingException;


//  /** Provides the output graph of previous call to {@link #run}.
//    * @return The string representation of the output graph represented as a Cypher Query.
//    * @fordev Note that implementing class needs only to override one of {@link #outputGraph1}, {@link #outputGraph2} and {@link #outputGraph3}.
//    * @fordev This function should terminate immediately, either by returning the result, or by raising the appropriate exception.
//  ***/
//  default String outputGraph2 () 
//  throws InterruptedException, ExecutionException, ComputationOngoingException    {
//    Path file = outputGraph3 ();
//    try {
//      return StringOf.file(file);
//    } 
//    catch (IOException e) {
//      throw new Error("Problem reading from file (" 
//                      + file + ") given by Interpreter.",e);
//    }
//  }
//
//
//  /** Provides the output graph of previous call to {@link #run}.
//    * @return A file containing the string-representation of the output graph represented as a Cypher Query.
//    * @throws InterpretInterruptedException If interpretation failed.
//    * @throws InterruptedException If computation was interrupted by {@link #interrupt}.
//    * @throws ComputationOngoingException If computation is not finished and was not interrupted.
//    * @fordev Note that implementing class needs only to override one of {@link #outputGraph1}, {@link #outputGraph2} and {@link #outputGraph3}.
//    * @fordev This function should terminate immediately, either by returning the result, or by raising the appropriate exception.
//  */
//  default Path outputGraph3 () 
//  throws InterruptedException, ExecutionException, ComputationOngoingException  {
//    throw new Error("No variant of inputQuery is implemented by "
//                    + this.getClass() + "."); 
//  }

  /** Provides the output table of previous call to {@link #run}.
    * @return The output {@link Table} of previous interpretation.
    * @throws IllegalStateException If called prior to any call to {@link#run}.
    * @throws ExecutionException If interpretation failed.
    * @throws InterruptedException If computation was interrupted by {@link #interrupt}.
    * @throws ComputationOngoingException If computation is not finished and was not interrupted.
    * @see output
    * @see outputGraph
  ***/
  Table outputTable() 
  throws InterruptedException, ExecutionException, ComputationOngoingException;


  /** Interrupts computation of current query.
    * If computation was terminated, this method does nothing.
    * Otherwise, later calls to {@link #outputGraph1} or {@link #outputTable1} (and variants) will result in {@link InterruptedException}.
  ***/
  void interrupt();


  /** {@inheritDoc}
  ***/
  @Override public
  void close();



  /** Interface for {@link Interpreter} factories. 
     * Implementations may simply build one instance (if this is safe) or each call to {@link Factory#make} could build a new instance.

  ***/
  public static interface Factory {
    /*** Builds an {@link Interpreter}. **/
    Interpreter make();
  }


  /*** Provides the name of this Interpreter ***/
  public String displayName();
}


