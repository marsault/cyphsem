/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.outbridge;

import uk.ac.ed.cyphsem.exception.*;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;

import uk.ac.ed.cyphsem.language.query.*;

import uk.ac.ed.cyphsem.table.Table;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;


import java.lang.InterruptedException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import java.io.PrintStream;

public class InterpreterComparator {

  public static class Comparison {
//     public List<Integer> failedWithExceptions;
    public List<ExecutionException> exceptions;
    public List<List<Integer>> succeeded = new LinkedList<List<Integer>>();
    public List<Table> tables = new LinkedList<Table>();

    public List<Integer> behavioralClasses;
    // Map from index of Interpreter to behavior class
    // -3 -> unset
    // -2 -> timeout
    // -1 -> exception
    // >0 -> finished properly with class of Table 

    public Comparison(int n) {
      behavioralClasses = new ArrayList<Integer>(n);
      for (int i = 0; i<n; i++)
        behavioralClasses.add(-3);
      exceptions = new ArrayList<ExecutionException>(n);
      for (int i = 0; i<n; i++)
        exceptions.add(null);
    }

    void addNewBehaviour(Integer index, Table table) {
      behavioralClasses.set(index, succeeded.size());
      List<Integer> l = new LinkedList<Integer>();
      l.add(index);
      succeeded.add(l);
      tables.add(table);
    }

    void addExceptionFails(Integer index, ExecutionException e) {
      exceptions.set(index, e);
      behavioralClasses.set(index, -1);  }
    void addTimeoutFails(Integer index) {  behavioralClasses.add(index, -2);  }
    private void addToClass(Integer index, int classIndex) {
      behavioralClasses.add(index, classIndex);
    }

    void addSuccess(Integer index, Table table) {
      int classIndex = 0;
      for(Table other : tables) {
        if (table.equals(other)) {
          addToClass(index, classIndex);
          return;
        }
        classIndex++;
      }
      addNewBehaviour(index, table);
    }

    public boolean hasTimedOut(Integer i) {
      return (behavioralClasses.get(i).equals(-2));
    }
    public boolean hasFailedWithException(Integer i) {
      return (behavioralClasses.get(i).equals(-1));
    }
    public ExecutionException getException(Integer i) {
      return exceptions.get(i);
    }
    public boolean hasFailed(Integer i) {
      return (hasFailedWithException(i) || hasTimedOut(i));
    }
    public boolean areInSameClass(Integer i, Integer j) {
      if ( hasFailed(i) || hasFailed(j) )
        return false;
      return (behavioralClasses.get(i).equals(behavioralClasses.get(i)));
    }
  }

  private List<Interpreter.Factory> factories;
  private boolean graphLoaded = false;
  private Graph graph;
  private long timeout;

  public 
  InterpreterComparator 
  (Graph g, long timeout, Interpreter.Factory... factories) 
  {
    this(g, timeout, Arrays.asList(factories));
  }

  public 
  InterpreterComparator
  (Graph graph, long timeout, List<Interpreter.Factory> factories) 
  {
    this.factories = factories;
    this.graph = graph;
    this.timeout = timeout;
  }

  // First element is timeout, second element is Exception, then each list group
  // the factories with the same behavior;
  public 
  Comparison compare (Graph graph, Query query) 
  throws InterruptedException 
  {
    Comparison result = new Comparison(factories.size());
    List<Interpreter> interpreters = makeInterpreters();
    for (Interpreter interpreter : interpreters) {
        interpreter.inputGraph(graph);
        interpreter.inputQuery(query);
    }
    for (Interpreter interpreter : interpreters) {
      interpreter.run();
    }
    TimeUnit.MILLISECONDS.sleep(timeout); 
    //InterruptedException raised by `sleep` is propagated.
    for (Interpreter interpreter : interpreters) {
      interpreter.interrupt();
    }

    for (int i=0; i<interpreters.size(); i++) {
      Interpreter interpreter = interpreters.get(i);
      try {
        Table resultingTable = interpreter.outputTable();
        result.addSuccess(i, resultingTable);  
      }
      catch (ExecutionException e) {  
        result.addExceptionFails(i,e);  
      }
      catch (InterruptedException e) {
        result.addTimeoutFails(i);  
      }
      catch (ComputationOngoingException e) { 
        // This should not happen
        throw new UnreachableStatementError(); 
      }
      interpreter.close();
    }
    return result;
  }


  private List<Interpreter> makeInterpreters() {
    List<Interpreter> result 
      = new ArrayList<Interpreter> (factories.size());
    for (Interpreter.Factory factory : factories) {
      result.add(factory.make());
    }
    return result;
  }

  public int size() {
    return factories.size();
  }

  public void printHeader (PrintStream e) {
    String start = "";
    for (Interpreter.Factory factory : factories) {
      e.println(start + "\u250F\u2501\u2501\u2501 "
                      + factory.make().displayName() +".");
      start += "\u2503";
    }
    e.println(start);
  }
}
