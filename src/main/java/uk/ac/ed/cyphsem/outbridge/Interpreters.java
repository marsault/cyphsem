/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.outbridge;

import uk.ac.ed.cyphsem.exception.*;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.lang.InterruptedException;

import uk.ac.ed.cyphsem.datamodel.graph.Graph;
import uk.ac.ed.cyphsem.language.query.Query;

import uk.ac.ed.cyphsem.table.Table;

import java.util.concurrent.TimeoutException;
/** This enum registers instance of different {@link Intepreter}'s for easier use. 
  * Note that they won't inconditionally work; for instance, {@link #NEO4J} will only work if Neo4j service is correcly started and set up.
***/
public enum Interpreters implements Interpreter {

  CYPHSEM(new CyphSemInterpreter())
//  ,NEO4J(new NeoComInterpreter())
  ;

  Interpreter boxedInterpreter;

  private Interpreters(Interpreter i) { boxedInterpreter = i; }

  @Override
  public void inputGraph(Graph g) { boxedInterpreter.inputGraph(g); }

  @Override
  public void inputQuery(Query q) { boxedInterpreter.inputQuery(q); }

  @Override
  public Graph outputGraph()
  throws InterruptedException, ExecutionException, ComputationOngoingException  
  {
    return boxedInterpreter.outputGraph(); 
  }
  
  @Override
  public Table outputTable() 
  throws InterruptedException, ExecutionException, ComputationOngoingException  
  { return boxedInterpreter.outputTable(); }
 
  @Override
  public void interrupt()  { boxedInterpreter.interrupt(); }

  @Override
  public void run()  { boxedInterpreter.run(); }

  @Override
  public void close() { boxedInterpreter.close(); }

  @Override
  public String displayName()  { return boxedInterpreter.displayName(); }
}
