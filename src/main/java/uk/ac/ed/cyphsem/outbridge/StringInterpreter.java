/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.outbridge;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.utils.StringOf;
import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.query.Query;
import uk.ac.ed.cyphsem.language.parser.Parser;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.concurrent.ExecutionException;

import java.lang.InterruptedException ;

/** This abstract class is an helper to implement an {@link Interpreter} linking outside CyphSem.  Functions to implement manipulate strings instead of internal data types such as {@link #Graph}'s or {@link #Query}'s.
  * Note that no helper method is given for outputTable as there is no
***/
public abstract class StringInterpreter implements Interpreter {


  /** {@inheritDoc}
    * Implementation in {@link StringInterpreter} calls {@link #inputGraph(java.util.String} with {@code g.toCypher()} as parameter.
  ***/
  @Override
  public void inputGraph(Graph g) {
    inputGraph(g.toCypher());
  }

  abstract protected void inputGraph(String g);
  abstract protected void inputQuery(String q);
  abstract protected String outputGraphAsString() throws InterruptedException, ExecutionException, ComputationOngoingException;
  @Override public abstract Table outputTable() throws InterruptedException, ExecutionException, ComputationOngoingException;

  /** {@inheritDoc}
    * Implementation in {@link StringInterpreter} calls {@link #inputQuery(java.util.String)} with {@code q.toString()}.
  ***/
  @Override
  public void inputQuery(Query q) {
    inputQuery(q.toString());
  }

  /** {@inheritDoc}
    * Implementation in {@link StringInterpreter} calls {@link Parser#graphOf} with value returned by {@link #outputGraphAsString()}.
  ***/
  @Override
  public Graph outputGraph()
  throws InterruptedException, ExecutionException, ComputationOngoingException
  {
    return Parser.graphOf(outputGraphAsString());
  }

}


