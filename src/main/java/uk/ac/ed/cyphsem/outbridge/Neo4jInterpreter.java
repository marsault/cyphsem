/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.outbridge;

import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.types.*;
import static org.neo4j.driver.v1.Values.parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.File;
import java.io.IOException;

import java.lang.Long;
import java.lang.Number;
import java.lang.Double;
import java.lang.Boolean;
import java.lang.UnsupportedOperationException;
import java.lang.Thread.UncaughtExceptionHandler;

import java.lang.AutoCloseable;
import java.lang.Math;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;

import uk.ac.ed.cyphsem.exception.InterpretingException;
import uk.ac.ed.cyphsem.exception.UnreachableStatementError;
import uk.ac.ed.cyphsem.datamodel.graph.Graphs;

public class Neo4jInterpreter 
    implements Interpreter 
{

  //Fixme
  private static TypeSystem typeSystem
    = org.neo4j.driver.internal.types.InternalTypeSystem.TYPE_SYSTEM;

  //private uk.ac.ed.cyphsem.datamodel.graph.Graph graph;
  private boolean interrupted = false;
//   private String query;
  private Future<uk.ac.ed.cyphsem.language.query.QueryData> futureResult;
  private ExecutorService executor;
  protected Driver driver 
    = GraphDatabase.driver(
        "bolt://localhost:7687",
        Config.build().withLogging(Logging.none()).toConfig()
    );
  Optional<String> inputGraphAsString = Optional.empty();
  boolean graphChanged = false;
  Optional<String> inputQueryAsString = Optional.empty();

  @Override public
  void inputGraph(uk.ac.ed.cyphsem.datamodel.graph.Graph graph) 
  {
    uk.ac.ed.cyphsem.datamodel.graph.Graph graphCopy = graph.copy().first;
    Graphs.annotateWithCyphSemId(graphCopy);
    inputGraphAsString = Optional.of(graphCopy.toCypher());
    graphChanged = true;
  }

  @Override public
  void run() 
  {
    if (inputQueryAsString.isEmpty())
      throw new IllegalStateException("Input query is empty.");
    String query = inputQueryAsString.get();
    inputQueryAsString = Optional.empty();
    if (inputGraphAsString.isEmpty())
      throw new IllegalStateException("Input graph is empty.");
    update_graph();

    Session statementSession = driver.session();
    Callable<uk.ac.ed.cyphsem.language.query.QueryData> runnable 
      = new Callable<uk.ac.ed.cyphsem.language.query.QueryData> () 
      {
        public uk.ac.ed.cyphsem.language.query.QueryData call() 
        throws InterruptedException
        {
          List<Record> recordList = statementSession.run(query).list();
          uk.ac.ed.cyphsem.datamodel.graph.Graph graph = queryGraph();
          uk.ac.ed.cyphsem.table.Table table 
            = (new Converter(graph)).convert(recordList);
          return new uk.ac.ed.cyphsem.language.query.QueryData(graph,table);
        }
      };
    executor = Executors.newSingleThreadExecutor();
    futureResult = executor.submit(runnable);

//    while ( !interrupted ) {
//      try {
//        result = futureResult.get(50, TimeUnit.MILLISECONDS);
//        finishedProperly = true;
//        break;
//      } catch (TimeoutException e) {
//      } catch (InterruptedException e) {
//        interrupted = true;
//      } catch (ExecutionException e) {
//        exe.shutdownNow();
//        throw new InterpretingException(e.getCause());
//      }
//    }
//
//    exe.shutdownNow();
//    lastRunTime += System.nanoTime();
//    if (!finishedProperly) {
//      if (interrupted) {
////         statementSession.close();
//        cancelQuery(statement);
//        interrupted = false;
//        return null;
//      } else
//        throw new UnreachableStatementError();
//    } else
//      return result;
  }

  //this method is here to be conform to abtract class StringInterpreter but is actually useless.

//  @Override protected 
//  String outputGraphAsString() 
//  throws InterruptedException, ExecutionException, 
//         uk.ac.ed.cyphsem.exception.ComputationOngoingException
//  {
//    return outputGraph().toCypher();
//  }

  @Override public
  void inputQuery(uk.ac.ed.cyphsem.language.query.Query q)  
  { 
    inputQueryAsString = Optional.of(q.toString()); 
  }

  public Neo4jInterpreter() { }

  @Override public
  uk.ac.ed.cyphsem.language.query.QueryData output() 
  throws InterruptedException, ExecutionException, 
         uk.ac.ed.cyphsem.exception.ComputationOngoingException
  {
    if (futureResult.isDone()) {
      if (futureResult.isCancelled()) 
        throw new InterruptedException ();
      else
        return futureResult.get();
     }
    else  
      throw new uk.ac.ed.cyphsem.exception.ComputationOngoingException();
  }


  @Override public
  uk.ac.ed.cyphsem.table.Table outputTable () 
  throws InterruptedException, ExecutionException, 
         uk.ac.ed.cyphsem.exception.ComputationOngoingException
  {
    return output().table();
  }

  @Override public
  uk.ac.ed.cyphsem.datamodel.graph.Graph outputGraph() 
  throws InterruptedException, ExecutionException, 
         uk.ac.ed.cyphsem.exception.ComputationOngoingException
  {
    return output().graph();
  }

//  Optional<String> inputGraphAsString = Optional.empty();
//
//  @Override public 
//  void inputGraph(String str) 
//  {
//    inputGraphAsString = Optional.of(str);
//  }
//
//  @Override public
//  uk.ac.ed.cyphsem.datamodel.graph.Graph outputGraph() 
//  throws InterruptedException, ExecutionException, ComputationOngoingException
//  {
//     Table t = outputTable();
//     return get_graph();
//  }

//  //this method is here to be conform to abtract class StringInterpreter but is actually useless.
//  @Override protected 
//  String outputGraphAsString() 
//  {
//    outputGraph().toCypher();
//  }


//  //running two statement in the same interpreter is not safe;
//  @Override
//  public uk.ac.ed.cyphsem.table.Table run(String statement) 
//  throws InterpretingException, InterruptedException {
////     query = statement;
//    lastRunTime = -System.nanoTime();
////     final uk.ac.ed.cyphsem.table.Table[] t = new uk.ac.ed.cyphsem.table.Table[1];
//    uk.ac.ed.cyphsem.table.Table result = null;
//
//    Session statementSession = driver.session();
//    Callable<uk.ac.ed.cyphsem.table.Table> runnable 
//      = new Callable<uk.ac.ed.cyphsem.table.Table> () {
//      public uk.ac.ed.cyphsem.table.Table call() throws InterruptedException{
//        uk.ac.ed.cyphsem.table.Table table = convert(statementSession.run(statement));
//        return (table);
//      }
//    };
//    ExecutorService exe = Executors.newSingleThreadExecutor();
//    boolean finishedProperly = false;
//    Future<uk.ac.ed.cyphsem.table.Table> futureResult = exe.submit(runnable);
//
//    while ( !interrupted ) {
//      try {
//        result = futureResult.get(50, TimeUnit.MILLISECONDS);
//        finishedProperly = true;
//        break;
//      } catch (TimeoutException e) {
//      } catch (InterruptedException e) {
//        interrupted = true;
//      } catch (ExecutionException e) {
//        exe.shutdownNow();
//        throw new InterpretingException(e.getCause());
//      }
//    }
//
//    exe.shutdownNow();
//    lastRunTime += System.nanoTime();
//    if (!finishedProperly) {
//      if (interrupted) {
////         statementSession.close();
//        cancelQuery(statement);
//        interrupted = false;
//        return null;
//      } else
//        throw new UnreachableStatementError();
//    } else
//      return result;
//  }

  protected void cancelQuery(String statement) {
    Session cancelSession = driver.session();
    StatementResult cancelResult = cancelSession.run(
        "CALL dbms.listQueries() YIELD queryId, query");
    while (cancelResult.hasNext()) {
      Record record = cancelResult.next();
//       System.out.println(record);
      if (record.get("query").asString().equals(statement)) {
        String cancelQuery
          = "CALL dbms.killQuery(\""
          + record.get("queryId").asString()
          + "\")";
//         System.out.println(cancelQuery);
        cancelSession.run(cancelQuery);
//         cancelSession.close();
      }
    }
  }

  protected void update_graph() 
  throws IllegalStateException
  {
    if (inputGraphAsString.isEmpty())
      throw new IllegalStateException();
    if (!graphChanged)
      return;
    Session session = driver.session();
    clear_db(session);
    StatementResult result = session.run(inputGraphAsString.get());
    //make_graph(session);
    session.close();
    graphChanged=false;
  }


  protected void clear_db(Session session) {
    StatementResult result1
      = session.run("MATCH ()-[relationship]->() DELETE relationship");
    while ( result1.hasNext() ) { result1.next(); }
    StatementResult result2
      = session.run("MATCH (node) DELETE node");
    while ( result2.hasNext() ) { result2.next(); }
  }


  protected
  uk.ac.ed.cyphsem.datamodel.graph.Graph queryGraph () 
  {
    Session session = driver.session();
    List<Node> nodes = new LinkedList<Node>();
    StatementResult result1 = session.run( "MATCH (node) RETURN *" );
    while ( result1.hasNext() ) {
      Record record = result1.next();
      nodes.add( record.get("node").asNode() );
    }
    List<Relationship> relationships = new LinkedList<Relationship>();
    StatementResult result2
      = session.run( "MATCH ()-[relationship]->() RETURN *" );
    while (result2.hasNext()) {
      Record record = result2.next();
      relationships.add ( record.get("relationship").asRelationship() );
    }
    session.close();
    return new uk.ac.ed.cyphsem.datamodel.graph.ImmutableGraph 
        (new TemporaryGraph(nodes, relationships));
  }


  protected 
  class TemporaryGraph extends uk.ac.ed.cyphsem.datamodel.graph.Graph {
    public TemporaryGraph (List<Node> nodes, List<Relationship> rels) {
      for (Node node : nodes) {
        uk.ac.ed.cyphsem.datamodel.graph.Node n 
          = new uk.ac.ed.cyphsem.datamodel.graph.Node
          ( Math.toIntExact(node.id()) );
        addNode(n);
        for (String str : node.labels())
          n.addLabel(new uk.ac.ed.cyphsem.datamodel.graph.Label(str));
      }
      for (Relationship rel : rels) {
        uk.ac.ed.cyphsem.datamodel.graph.Node start 
          = getNodeById( Math.toIntExact(rel.startNodeId()) );
        uk.ac.ed.cyphsem.datamodel.graph.Node end 
          = getNodeById( Math.toIntExact(rel.endNodeId()) );
        uk.ac.ed.cyphsem.datamodel.graph.RelationType type
          = new uk.ac.ed.cyphsem.datamodel.graph.RelationType( rel.type() );
        uk.ac.ed.cyphsem.datamodel.graph.Relation r 
          = new uk.ac.ed.cyphsem.datamodel.graph.Relation (
            Math.toIntExact(rel.id()),
            type,
            start,
            end
          );
        addRelation(r);
      }
      Converter converter = new Converter(this);
      for (Node node : nodes) {
        for (String key : node.keys()) {
          getNodeById(Math.toIntExact(node.id())).overwriteProperty (
              new uk.ac.ed.cyphsem.datamodel.graph.PropertyKey(key),
              converter.convert(node.get(key))
          );
        }
      }
      for (Relationship rel : rels) {
        for (String key : rel.keys()) {
          getRelationById(Math.toIntExact(rel.id())).overwriteProperty (
            new uk.ac.ed.cyphsem.datamodel.graph.PropertyKey(key),
            converter.convert(rel.get(key))
          );
        }
      }
    }
  }

  private class Converter {

    uk.ac.ed.cyphsem.datamodel.graph.Graph graph;

    Converter(uk.ac.ed.cyphsem.datamodel.graph.Graph graph) {
      this.graph = graph;
    }

    public uk.ac.ed.cyphsem.datamodel.graph.Node convert(Node node)
    {
       return getNodeById(node.id());
    }
    
    uk.ac.ed.cyphsem.datamodel.graph.Node getNodeById (long i) 
    {
      uk.ac.ed.cyphsem.datamodel.graph.Node n 
          = graph.getNodeById(Math.toIntExact(i));
      if (n.hasPropertyKey(Graphs.cyphsemidKey)) {
         n = new uk.ac.ed.cyphsem.datamodel.graph.Node ( 
               n.getPropertyValue(Graphs.cyphsemidKey)
                .asInteger().intValue(),
               n.labels(), 
               n.propertyMap()
             );
       }
      return n;
    }
  
    public uk.ac.ed.cyphsem.datamodel.graph.Relation convert(Relationship rel)
    { 
      return getRelationById(rel.id(),rel.startNodeId(),rel.endNodeId());
    }

    uk.ac.ed.cyphsem.datamodel.graph.Relation getRelationById 
    (long relId, long startNodeId, long endNodeId)
    {
      uk.ac.ed.cyphsem.datamodel.graph.Relation r
        = graph.getRelationById(Math.toIntExact(relId));
      if (r.hasPropertyKey(Graphs.cyphsemidKey)) {
        r = new uk.ac.ed.cyphsem.datamodel.graph.Relation (
            r.getPropertyValue(Graphs.cyphsemidKey)
             .asInteger().intValue(),
            r.type(),
            getNodeById(startNodeId),
            getNodeById(endNodeId),
            r.propertyMap()
        );
      }
      return r;
  }
  
    public uk.ac.ed.cyphsem.datamodel.graph.Path convert(Path path) {
      int length = path.length();
      Iterable<Node> nodes_in = path.nodes();
      Iterable<Relationship> rels_in = path.relationships();
      List<uk.ac.ed.cyphsem.datamodel.graph.Node> nodes_out
          = new ArrayList<uk.ac.ed.cyphsem.datamodel.graph.Node>(length+1);
      List<uk.ac.ed.cyphsem.datamodel.graph.Relation> rels_out
        = new ArrayList<uk.ac.ed.cyphsem.datamodel.graph.Relation>(length);
      for(Node n : nodes_in)
        nodes_out.add(convert(n));
      for(Relationship r : rels_in)
        rels_out.add(convert(r));
      return new uk.ac.ed.cyphsem.datamodel.graph.Path(nodes_out, rels_out);
    }
  
  
    public
    uk.ac.ed.cyphsem.datamodel.value.Value
    convert (Value value) {
      Object o = value.asObject();
      if (o == null)
        return (new uk.ac.ed.cyphsem.datamodel.value.Value());
      if (o instanceof Long)
        return new uk.ac.ed.cyphsem.datamodel.value.Value(((Long) o).longValue());
      if (o instanceof Double)
        throw new Error("Convert value: cyphsem does not support floats.");
      if (o instanceof Number) //fixme ...
        return new uk.ac.ed.cyphsem.datamodel.value.Value(((Number) o).longValue());
      if (o instanceof String)
        return new uk.ac.ed.cyphsem.datamodel.value.Value((String) o);
      if (o instanceof Boolean)
        return new uk.ac.ed.cyphsem.datamodel.value.Value((Boolean) o);
      if (o instanceof Node)
        return new uk.ac.ed.cyphsem.datamodel.value.Value(convert((Node) o));
      if (o instanceof Path)
        return new uk.ac.ed.cyphsem.datamodel.value.Value(convert((Path) o));
      if (o instanceof Relationship)
        return new uk.ac.ed.cyphsem.datamodel.value.Value(convert((Relationship) o));
      if (o instanceof Map) {
        Iterable<String> keys = value.keys();
        Map<uk.ac.ed.cyphsem.datamodel.graph.PropertyKey,uk.ac.ed.cyphsem.datamodel.value.Value> map
          = new HashMap<uk.ac.ed.cyphsem.datamodel.graph.PropertyKey,uk.ac.ed.cyphsem.datamodel.value.Value>();
        for(String key : keys)
          map.put(new uk.ac.ed.cyphsem.datamodel.graph.PropertyKey(key), convert(value.get(key)));
        return new uk.ac.ed.cyphsem.datamodel.value.Value(map);
      }
  
      if (o instanceof List) {
        Iterable<Value> sub_vals = value.values();
        List<uk.ac.ed.cyphsem.datamodel.value.Value> list
          = new ArrayList<uk.ac.ed.cyphsem.datamodel.value.Value> (value.size());
        for (Value sub_val : sub_vals)
          list.add(convert(sub_val));
        return new uk.ac.ed.cyphsem.datamodel.value.Value(list);
      }
  
        throw new Error("Value has no type "+o);
    }
  
  
    uk.ac.ed.cyphsem.table.Record convert (Record neo_rec) 
    throws InterruptedException {
      if (interrupted)
        throw (new InterruptedException());
      uk.ac.ed.cyphsem.table.Record cyphsem_rec = new uk.ac.ed.cyphsem.table.Record();
      for(String key : neo_rec.keys())
        cyphsem_rec.bind(new uk.ac.ed.cyphsem.table.Name(key), convert(neo_rec.get(key)));
      return cyphsem_rec;
    }
  
  
    uk.ac.ed.cyphsem.table.Table convert (StatementResult table) 
    throws InterruptedException 
    {
      uk.ac.ed.cyphsem.table.Table result = new uk.ac.ed.cyphsem.table.Table();
      while (table.hasNext())
        result.add(convert(table.next()));
      return result;
    }

    uk.ac.ed.cyphsem.table.Table convert 
      (List<Record> recordList)
    throws InterruptedException
    {
      uk.ac.ed.cyphsem.table.Table result = new uk.ac.ed.cyphsem.table.Table();
      for(Record r: recordList)
        result.add(convert(r));
      return result;
    }
  }

  //@Override
  //public long getLastRunTime() {  return lastRunTime;  }


  @Override
  public void close() {
//     session.close();
    driver.close();
  }

  /** {@inheritDoc}
    * Implementation in {@link Neo4jInterpreter} {@link Future#cancel cancels} computation contained in {@link #futureResult}.
  ***/
  @Override public  void interrupt()  { futureResult.cancel(true); }

//  @Override
//  public void loadFrom(Interpreter i) { throw new UnsupportedOperationException(); }


  /**{@inheritDoc} ***/
  @Override public
  String displayName()  { return "Neo4j"; }

}
