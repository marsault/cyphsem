/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>WITH</tt> clause.
 * @see Sample#sampleTest1
 */
public class WithTest extends QueryTest
{

  /** Tests that <tt>WITH *</tt> does nothing.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("WITH *")
  public void withStar()
  {
    String query = "WITH *";
    Graph graph = catGraph;
    for (Table t : Immutable.listOf(
        unitTable,
        new Table( new Record(a,new Value("str")), new Record(a, new Value()) ),
        new Table(
            new Record(a,new Value("str"), b,new Value(1)),
            new Record(a,new Value(),      b,new Value(false))
          )
      )
    ){
      assertExecution("", t, graph, t, query);
    }
  }

  /** Tests that <tt>WITH *,c</tt> adds a column.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("WITH *,c")
  public void columnAddition()
  {
    String query;
    Graph graph = singleNodeGraph;
    Table in;
    Table out;

    { in = new Table(
          new Record(a,new Value("str")),
          new Record(a,new Value())
        );
      query = "WITH *, 1 AS c";
      out = new Table(
          new Record( a,new Value("str"), c,new Value(1) ),
          new Record( a,new Value(),      c,new Value(1) )
        );
      assertExecution("", out, graph, in, query);
    }
    { in = new Table(
          new Record(a,new Value(1)),
          new Record(a,new Value())
        );
      query = "WITH *, 1+a AS c";
      out = new Table(
          new Record( a,new Value(1), c,new Value(2) ),
          new Record( a,new Value(),      c,new Value() )
        );
      assertExecution("", out, graph, in, query);
    }
  }

  /** Tests <tt>WITH a,c</tt>, where a is bound and c is not.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("WITH c,d")
  public void newColumns()
  {
    String query;
    Graph graph = singleNodeGraph;
    Table in;
    Table out;

    { in = new Table(
          new Record(a,new Value("str")),
          new Record(a,new Value())
        );
      query = "WITH a, 1 AS c";
      out = new Table(
          new Record( a,new Value("str"), c,new Value(1) ),
          new Record( a,new Value(),      c,new Value(1) )
        );
      assertExecution("", out, graph, in, query);
    }
    { in = new Table(
          new Record( a,new Value(true),  b,new Value(false) ),
          new Record( a,new Value(false), b,new Value(false) ),
          new Record( a,new Value(),      b,new Value(true)  )
        );
      query = "WITH a, (a OR b) AS c";
      out = new Table(
          new Record( a,new Value(true),  c,new Value(true)  ),
          new Record( a,new Value(false), c,new Value(false) ),
          new Record( a,new Value(),      c,new Value(true)  )
        );
      assertExecution("", out, graph, in, query);
    }
  }

  /** Tests aggregation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("aggregation, count")
  public void aggregation1()
  {
    String query = "WITH a, count(b) AS c";
    Graph graph = singleNodeGraph;
    Table in;
    Table out;

    { in = new Table( new Record( a,new Value("str"), b,new Value(1) ) );
      out = new Table( new Record( a,new Value("str"), c,new Value(1)) );
      assertExecution("", out, graph, in, query);
    }
    { in = new Table(
          new Record( a,new Value("str"), b,new Value(1) ),
          new Record( a,new Value("str"), b,new Value(1) ),
          new Record( a,new Value("str"), b,new Value(2) ),
          new Record( a,new Value(),      b,new Value(1) ),
          new Record( a,new Value(),      b,new Value(2) )
        );
      out = new Table(
          new Record( a,new Value("str"), c,new Value(3) ),
          new Record( a,new Value(),      c,new Value(2) )
        );
      assertExecution("", out, graph, in, query);
    }
    { // null are not counted
      in = new Table( new Record( a,new Value("str"), b,new Value(1) ),
                      new Record( a,new Value("str"), b,new Value()  )  );
      out = new Table( new Record( a,new Value("str"), c,new Value(1)) );
      assertExecution("", out, graph, in, query);
    }
  }

  /** Tests DISTINCT aggregation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("aggregation, DISTINCT")
  public void aggregation2()
  {
    String query = "WITH a, count(DISTINCT b) AS c";
    Graph graph = singleNodeGraph;
    Table in;
    Table out;

    { in = new Table( new Record( a,new Value("str"), b,new Value(1) ) );
      out = new Table( new Record( a,new Value("str"), c,new Value(1)) );
      assertExecution("", out, graph, in, query);
    }
    { in = new Table(
          new Record( a,new Value("str"), b,new Value(1) ),
          new Record( a,new Value("str"), b,new Value(1) ),
          new Record( a,new Value("str"), b,new Value(2) ),
          new Record( a,new Value(),      b,new Value(1) ),
          new Record( a,new Value(),      b,new Value(2) )
        );
      out = new Table(
          new Record( a,new Value("str"), c,new Value(2) ),
          new Record( a,new Value(),      c,new Value(2) )
        );
      assertExecution("", out, graph, in, query);
    }
  }

  /** Tests aggregation with count(*).
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("aggregation, count(*)")
  public void aggregation3()
  {
    String query = "WITH a, count(*) AS c";
    Graph graph = singleNodeGraph;
    Table in;
    Table out;
    { // null are counted
      in = new Table( new Record( a,new Value("str"), b,new Value(1) ),
                      new Record( a,new Value("str"), b,new Value()  )  );
      out = new Table( new Record( a,new Value("str"), c,new Value(2)) );
      assertExecution("", out, graph, in, query);
    }
  }

  /** Tests behaviour when a <tt>WITH</tt> would produce two column with the same name.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Column name clash")
  public void columnNameClash()
  {
    Expression e = new FunctionalExpression(Functions.PLUS, a, b);
    Name a_plus_b = new Name(e); // Automatic name built from expression e
    Table in = new Table(
        new Record(
            a,new Value(1),  b,new Value(2),  a_plus_b ,new Value(3) )
      );
    List<String> queries = Immutable.listOf(
        "WITH 1 as c, 2 as c",
        "WITH *, 1 as a",
        "WITH *, a+b",
        "WITH a AS `COUNT(*)`, COUNT(*)"
      );
    for (String str : queries) {
      Query query = Parser.queryOf(str+" RETURN *");
      assertThrows(
          UnspecifiedBehaviourException.class,
          () -> System.out.println(query.apply( new QueryData(singleNodeGraph,in) )),
          "Query\n " + query + "\n on table "+in+" should have produced a name clash.\n"
        );
    }
  }


}









