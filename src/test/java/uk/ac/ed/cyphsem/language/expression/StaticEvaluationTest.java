/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.expression;

import static uk.ac.ed.cyphsem.language.expression.Functions.*;
import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.math.BigInteger;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Unit Test regarding evaluation of static expressions (that is, with no
  * reference to the driving {@link Table} or the {@link Graph}).
**/
public class StaticEvaluationTest {

  /** Graph used for created some {@link Kind.NODE NODE} {@link Value}. */
  protected Graph graph = new Graph();

  /** Sample node {@link Value}. **/
  private Node node1 = graph.addNode();

  /** Sample node {@link Value}. **/
  private Node node2 = graph.addNode();

  /** Relation used for value creation; **/
  private Relation rel1
    = graph.addRelation(new RelationType(""), node1, node2);


    Map<PropertyKey,Value> theMap = new HashMap<>();
    {
      theMap.put(new PropertyKey("a"), new Value("a"));
      theMap.put(new PropertyKey("1"), new Value(1));
    }

    Value nodev = new Value(node1);
    Value relv = new Value(rel1);
    Value intv = new Value(1);
    Value boolv = new Value(true);
    Value mapv = new Value(theMap);
    Value nullv = new Value();
    Value strv = new Value("a");
    Value pathv = new Value(new Path(node1,rel1,node2));
    Value listv = new Value(Arrays.asList(nodev,relv,intv));

    /** Relation used for value creation; **/
  private Relation rel2
    = graph.addRelation(new RelationType(""), node1, node2);

  /** List evaluation */
  @Test
  @DisplayName("Static evaluation, lists")
  public void list() {
    List<Pair<Expression,Value>> exp_value_list
      = Arrays.asList(
          new Pair<Expression,Value>( new ExplicitListExpression(), new Value(Arrays.asList()) ),
          new Pair<Expression,Value>(
              new ExplicitListExpression(new Value()),
              new Value(Arrays.asList(new Value()))               ),
          new Pair<Expression,Value>( new ExplicitListExpression( new Value(1),
                                                  new Value("abc"),
                                                  new Value(true)  ),
                      new Value(Arrays.asList(new Value(1),
                                              new Value("abc"),
                                              new Value(true)  ))),
          new Pair<Expression,Value>(
              new ExplicitListExpression(
                  new ExplicitListExpression(
                      new ExplicitListExpression(
                          new ExplicitListExpression() ))),
              new Value(Arrays.asList(
                  new Value(Arrays.asList(
                          new Value(Arrays.asList(
                              new Value(Arrays.asList()) )) )) ))
            )
        );

    for(Pair<Expression,Value> pair : exp_value_list)
      assertStaticEval(pair.second,pair.first);
  }

  /** Sublist evaluation */
  @Test
  @DisplayName("Static evaluation, sublists")
  public void sublist() {
    Value v1 = new Value(1),
          v2 = new Value(),
          v3 = new Value(true),
          v = new Value( Arrays.asList( v1, v2, v3));
    for (OptPair<Integer, Value> pair : Iterators.combine(
          Arrays.asList(0, 1, 2,  3, 4, 5,  -4,-3,-2,-1),
          Arrays.asList(v1,v2,v3, nullv, nullv, nullv, nullv,v1,v2,v3)
        ))
    {
      assertEquals(
          pair.right(),
          Expressions.staticEvaluation(
              new SubListExpression(
                  v,
                  SubListExpression.Span.UNIQUE,
                  new Value(pair.left())         )));
    }

    Value res1 = new Value(Arrays.asList(v1,v2));
    assertEquals (
        res1,
        Expressions.staticEvaluation (
            new SubListExpression ( v, SubListExpression.Span.UNTIL, new Value(2) ) ) );
    assertEquals (
        res1,
        Expressions.staticEvaluation (
            new SubListExpression ( v, new Value(0), new Value(2) )
          )
      );

    Value res2 = new Value(Arrays.asList(v3));
    assertEquals (
        res2,
        Expressions.staticEvaluation (
            new SubListExpression ( v, SubListExpression.Span.FROM, new Value(2) )
          )
      );
    assertEquals (
        res2,
        Expressions.staticEvaluation (
            new SubListExpression ( v, new Value(2), new Value(3) )
          )
      );

    List<Value> vals_to_nullv = Arrays.asList(new Value(-4),new Value(4),new Value());
    for (Value val : vals_to_nullv)
      for(SubListExpression.Span span: SubListExpression.Span.values())
        assertEquals (
            nullv,
            Expressions.staticEvaluation ( new SubListExpression ( v, span, val ) )
          );
    assertEquals (
          nullv,
          Expressions.staticEvaluation (
              new SubListExpression ( v, new Value(0), new Value(4) )
            )
        );
    assertEquals (
          nullv,
          Expressions.staticEvaluation (
              new SubListExpression ( v, new Value(-4), new Value(0) )
            )
        );

  }


  public void assertStaticEval(Value expected, Expression exp) {
    Value got = Expressions.staticEvaluation(exp);
    assertEquals( expected,
                  got,
                  "Expression " + exp + " statically evaluates to " + got
                    + " instead of "+expected+", as expected."            );
  }

  /** Map evaluation */
  @Test
  @DisplayName("Static evaluation, maps")
  public void map() {
    List<KeyExpPair> list = Arrays.asList(
        new KeyExpPair(new PropertyKey("aeece"), new Value(0)),
        new KeyExpPair(new PropertyKey("adfc"), new Value(1)),
        new KeyExpPair(new PropertyKey("cdb"), new Value(true)),
        new KeyExpPair(new PropertyKey("dada"), new Value("abc")),
        new KeyExpPair(new PropertyKey("abcd"), new Value(2)),
        new KeyExpPair(new PropertyKey("abcd"), new Value(3))
    );

    ExplicitMapExpression in = new ExplicitMapExpression();
    Map<PropertyKey,Value> out = new HashMap<>();
    assertStaticEval(new Value(out), in);
    for(KeyExpPair ke : list) {
      in.add(ke);
      out.put(ke.key, (Value) ke.exp);
      assertStaticEval(new Value(out), in);
    }
  }

  /** Function evaluations*/
  @Test
  @DisplayName("Static evaluation, functions")
  public void function() {
    List<List<Object>> directives = Arrays.asList(
                //func, operand 1, operand 2,..., result
      Arrays.asList(AND, true, false, false),
      Arrays.asList(AND, true, true, true),
      Arrays.asList(AND, true, null, null),
      Arrays.asList(AND, null, false, false),
      //
      Arrays.asList(IS_NOT_NULL, null, false),
      Arrays.asList(IS_NOT_NULL, true, true),
      Arrays.asList(IS_NOT_NULL, 2, true),
      Arrays.asList(IS_NOT_NULL, Arrays.asList(new Value()), true),
      //
      Arrays.asList(IS_NULL, null, true),
      Arrays.asList(IS_NULL, true, false),
      Arrays.asList(IS_NULL, 2, false),
      Arrays.asList(IS_NULL, Arrays.asList(new Value()), false),
      //
      Arrays.asList(CONTAINS, "abc", "cde", false),
      Arrays.asList(CONTAINS, "abc", "a", true),
      Arrays.asList(CONTAINS, "abc", "b", true),
      Arrays.asList(CONTAINS, "abc", "abc", true),
      Arrays.asList(CONTAINS, null, "abc", null),
      //
      Arrays.asList(EQ, "abc", "abc", true),
      Arrays.asList(EQ, "abc", "a", false),
      Arrays.asList(EQ, "abc", null, null),
      Arrays.asList(EQ, 1, 1, true),
      Arrays.asList(EQ, 1, -2, false),
      Arrays.asList(EQ, null, -10, null),
      Arrays.asList(EQ, Arrays.asList(), Arrays.asList(), true),
      Arrays.asList(EQ, Arrays.asList(), Arrays.asList(new Value()), false),
      Arrays.asList(EQ, node1, node1, true),
      Arrays.asList(EQ, node1, node2, false),
      Arrays.asList(EQ, rel1, rel1, true),
      Arrays.asList(EQ, rel1, rel2, false),
      Arrays.asList(EQ, new Path(node1), new Path(node1), true),
      Arrays.asList(EQ, new Path(node1,rel1,node2), new Path(node1,rel1,node2), true),
      Arrays.asList(EQ, new Path(node1,rel1,node2), new Path(node1,rel2,node2), false),
      Arrays.asList(EQ, new Path(node1,rel1,node2), new Path(node1,rel1,node1), false),
      Arrays.asList(EQ, new Path(node1), new Path(node1,rel1,node1), false),
      Arrays.asList(EQ, new HashMap<>(), new HashMap<>(), true),
      Arrays.asList(EQ, null, null, null),
      //
      Arrays.asList(ENDS_WITH, "abc", "cde", false),
      Arrays.asList(ENDS_WITH, "abc", "b" , false),
      Arrays.asList(ENDS_WITH, "abc", "abc" , true),
      Arrays.asList(ENDS_WITH, "abc", "bc" , true),
      Arrays.asList(ENDS_WITH, "abc", null , null),
      //
      Arrays.asList(MINUS, 3, 10, -7),
      Arrays.asList(MINUS, 0, 0, 0),
      Arrays.asList(MINUS, 30,27,3),
      Arrays.asList(MINUS, null,-27,null),
      Arrays.asList(MINUS, 28, null,null),
      //
      Arrays.asList(NOT, true, false),
      Arrays.asList(NOT, false, true),
      Arrays.asList(NOT, null, null),
      //
      Arrays.asList(OR, true, false, true),
      Arrays.asList(OR, false, false, false),
      Arrays.asList(OR, true, null, true),
      Arrays.asList(OR, null, false, null),
      //
      Arrays.asList(PLUS, 1, 1, 2),
      Arrays.asList(PLUS, 1, 0, 1),
      Arrays.asList(PLUS, -10, -10, -20),
      Arrays.asList(PLUS, null,-27,null),
      Arrays.asList(PLUS, 28, null,null),
      //
      Arrays.asList(STARTS_WITH, "abc", "cde", false),
      Arrays.asList(STARTS_WITH, "abc", "b" , false),
      Arrays.asList(STARTS_WITH, "abc", "abc" , true),
      Arrays.asList(STARTS_WITH, "abc", "ab" , true),
      Arrays.asList(STARTS_WITH, null, null , null),
      //
      Arrays.asList(IN, 1, Arrays.asList(new Value(1)), true),
      Arrays.asList(IN, 1, Arrays.asList(new Value()), null),
      Arrays.asList(IN, 1, Arrays.asList(new Value(),new Value(1)), true),
      Arrays.asList(IN, 1, Arrays.asList(new Value(2),new Value(3)), false),
      Arrays.asList(IN, 1, Arrays.asList(), false),
      Arrays.asList(IN, null, Arrays.asList(new Value()), null),
      Arrays.asList(IN, 1, null, null),
      //
      Arrays.asList(XOR, true, false, true),
      Arrays.asList(XOR, false, false, false),
      Arrays.asList(XOR, true, null, null),
      Arrays.asList(XOR, null, false, null)
      //
    );

    for (List<Object> d : directives)
      assertStaticEval(
          Values.of(d.get(d.size()-1)),
          new FunctionalExpression(
              (Functions) d.get(0),
              Utils.map(d.subList(1,d.size()-1), x -> Values.of(x))
            )
        );
    for (List<Object> d : directives) {
      assertThrows(
          UnspecifiedBehaviourException.class,
          ()->Expressions.staticEvaluation(
              new FunctionalExpression(
                  (Functions) d.get(0),
                  Utils.map(d.subList(1,d.size()-2), x -> Values.of(x))
        )));
      assertThrows(
          UnspecifiedBehaviourException.class,
          ()->Expressions.staticEvaluation(
              new FunctionalExpression(
                  (Functions) d.get(0),
                  Utils.map(d.subList(1,d.size()), x -> Values.of(x))
        )));
    }


    //Directive that should fail due to parameter type


    directives = Arrays.asList(
              //function, correct argument, list of incorrect arguments
      Arrays.asList(AND, true, nodev, relv, intv, mapv, strv, pathv, listv),
      Arrays.asList(OR, true, nodev, relv, intv, mapv, strv, pathv, listv),
      Arrays.asList(XOR, true, nodev, relv, intv, mapv, strv, pathv, listv),
      Arrays.asList(CONTAINS, "test", nodev, relv, intv, mapv, boolv, pathv, listv),
      Arrays.asList(STARTS_WITH, "test", nodev, relv, intv, mapv, boolv, pathv, listv),
      Arrays.asList(ENDS_WITH, "test", nodev, relv, intv, mapv, boolv, pathv, listv),
      Arrays.asList(PLUS, 1, nodev, relv, strv, mapv, boolv, pathv, listv),
      Arrays.asList(MINUS, 1, nodev, relv, strv, mapv, boolv, pathv, listv),
      Arrays.asList(IN, 1, nodev, relv, strv, mapv, boolv, pathv, intv)
    );
    for (List<Object> d : directives) {
      Value val_with_corr_kind = Values.of(d.get(1));
      for (Object o : d.subList(2,d.size())) {
        Value two = (Value) o;
        for (Value one : Arrays.asList(val_with_corr_kind, nullv)) {
          assertThrows(
            DynamicCastException.class,
            ()->Expressions.staticEvaluation(
                new FunctionalExpression( (Functions) d.get(0), one, two )));
          if (d.get(0) != IN)
            assertThrows(
              DynamicCastException.class,
              ()->Expressions.staticEvaluation(
                  new FunctionalExpression( (Functions) d.get(0), two, one )));
        }
      }
    }
  }
  /** Function evaluations*/
  @Test
  @DisplayName("Static evaluation, aggreg functions")
  public void aggFunction() {
    assertEquals(
        new Value(2),
        AggregatingFunction.COUNT.call(Arrays.asList(nullv, listv, nodev))
      );
    assertEquals(
        new Value(3),
        AggregatingFunction.COUNT.call(new Value(1), listv, nodev)
      );
    assertEquals(
      new Value(-2),
      AggregatingFunction.MIN.call(new Value(2), new Value(3), new Value(-2))
    );

    assertEquals(AggregatingFunction.COUNT, AggregatingFunction.of("COUNT"));
    assertEquals(AggregatingFunction.MIN, AggregatingFunction.of("MIN"));
    assertThrows(IllegalArgumentException.class, ()->AggregatingFunction.of("NAWAAAK"));
  }


  /** Tests equality of {@link Kind.MAP MAP} {@link Value}s with null. */
  @Test
  @DisplayName("Static evaluation, equality")
  public void equality_NullInMap() {
    Map<PropertyKey,Value> map1 = new HashMap<>();
    Map<PropertyKey,Value> map2 = new HashMap<>();
    Map<PropertyKey,Value> map3 = new HashMap<>();
    map1.put( new PropertyKey("a"), new Value("a") );
    map2.put( new PropertyKey("a"), new Value("a") );
    map3.put( new PropertyKey("a"), new Value(1)   );

    map1.put( new PropertyKey("b"), new Value(1) );
    map2.put( new PropertyKey("b"), new Value()  );
    map3.put( new PropertyKey("b"), new Value(1) );

    // Hence map3 != map1  ,   map2 != map3  , and  map2 is compatible but not
    // equal to map1.

    Value v1 = new Value(map1);
    Value v2 = new Value(map2);
    Value v3 = new Value(map3);

    List<Value> vals= Arrays.asList( v1, v2, nullv );
    for (Value one : vals)
      for (Value two : vals)
        if ( one != v1 || two != v1 )
          assertStaticEval(
                new Value(),
                new FunctionalExpression( Functions.EQ,
                                          one,
                                          two          ));

    vals = Arrays.asList(v1, v2);
    for (Value v : vals)
      assertStaticEval(
            new Value(false),
            new FunctionalExpression( Functions.EQ,
                                      v,
                                      v3             ));
  }

  /** Tests equality of {@link Kind.LIST LIST} {@link Value}s with nulls. */
  @Test
  @DisplayName("Static evaluation, equality")
  public void equality_NullInList() {
    List<Value> list1 = Arrays.asList(new Value("a"), new Value(1) );
    List<Value> list2 = Arrays.asList(new Value("a"), new Value( ) );
    List<Value> list3 = Arrays.asList(new Value("b"), new Value(1) );

    // Hence list1 != list3  ,   list2 != list3  , and list1 is compatible but not
    // equal to list1.

    Value v1 = new Value(list1);
    Value v2 = new Value(list2);
    Value v3 = new Value(list3);

    List<Value> vals= Arrays.asList( v1, v2, nullv );
    for (Value one : vals)
      for (Value two : vals)
        if ( one != v1 || two != v1 )
          assertStaticEval(
                new Value(),
                new FunctionalExpression( Functions.EQ,
                                          one,
                                          two          ));
    vals = Arrays.asList(v1, v2);
    for (Value v : vals)
      assertStaticEval(
            new Value(false),
            new FunctionalExpression( Functions.EQ,
                                      v,
                                      v3             ));
  }

}
