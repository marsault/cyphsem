/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>CREATE</tt> clause.
 * @see Sample#sampleTest1
 */
public class CreateTest extends QueryTest
{

  /** Tests the creation of one empty node.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, empty")
  public void creationNodeEmpty()
  {
    String query = "CREATE ()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(in);
    String changes = "createdNodes=1";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests the creation of one node
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, full")
  public void creationNodeFull()
  {
    String query = "CREATE (:xx:yy:zz{name:\"v\",age:20})";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(in);
    String changes = "createdNodes=1, addedLabels=3, modifiedProperties=2";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests the creation of one relation between two existing nodes.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, no-ext")
  public void creationRelationNoExt()
  {
    String query = "CREATE (a)-[:abcd {since:1}]->(b)";
    Graph graph = catGraph;
    Table in = new Table( new Record ( a,victor_v, b,nadime_v ),
                          new Record ( a,leonid_v, b,zelia_v  )  );
    Table out = in;
    String changes = "createdRelations=2, modifiedProperties=2";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests the creation of one relation between one existing node and one new node.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, one ext")
  public void creationRelationOneExt()
  {
    String query = "CREATE (a)-[:abcd {since:1}]->(:xx:yy{age:20})";
    Graph graph = catGraph;
    Table in = new Table( new Record ( a,victor_v, b,nadime_v ),
                          new Record ( a,leonid_v, b,zelia_v  )  );
    Table out = in;
    String changes = "createdNodes=2, addedLabels=4, createdRelations=2, modifiedProperties=4";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests the creation of one relation between two new nodes.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, one ext")
  public void creationRelationTwoExt()
  {
    String query = "CREATE (:yy)-[:abcd {since:1}]->(:xx:yy{age:20})";
    Graph graph = catGraph;
    Table in = unitTable;
    String changes = "createdNodes=2, addedLabels=3, createdRelations=1, modifiedProperties=2";
    QueryData qd = assertExecution(changes, graph, in,query);
  }

  /** Tests that the binding is correct during the creation of one relation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, binding, all unbound, one node")
  public void creationBindingAllUnboundNode()
  {
    String query = "CREATE (a:yy{age:21})";
    Graph graph = catGraph;
    Table in = unitTable;
    String changes = "createdNodes=1, addedLabels=1, modifiedProperties=1";
    QueryData qd = assertExecution(changes, graph, in,query);
    Table out = qd.table();
    assertEquals(1, out.size());
    Record r = out.anyOne();
    assertTrue(r.isBound(a));
    Node a_n = r.get(a).asNode();
    assertFalse(catGraph.nodes().contains(a_n));
    assertTrue(qd.graph().nodes().contains(a_n));
  }


  /** Tests that the binding is correct during the creation of one relation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, binding, all unbound, one relation")
  public void creationBindingAllUnboundRelation()
  {
    String query = "CREATE (a:yy)-[b:abcd {since:1}]->(c:xx:yy{age:20})";
    Graph graph = catGraph;
    Table in = unitTable;
    String changes = "createdNodes=2, addedLabels=3, createdRelations=1, modifiedProperties=2";
    QueryData qd = assertExecution(changes, graph, in,query);
    Table out = qd.table();
    assertEquals(1, out.size());
    Record r = out.anyOne();
    assertTrue(r.isBound(a));
    assertTrue(r.isBound(b));
    assertTrue(r.isBound(c));
    Node a_n = r.get(a).asNode();
    Relation b_n = r.get(b).asRelation();
    Node c_n = r.get(c).asNode();
    assertEquals(a_n, b_n.source());
    assertEquals(c_n, b_n.target());

    assertFalse(catGraph.nodes().contains(a_n));
    assertFalse(catGraph.relations().contains(b_n));
    assertFalse(catGraph.nodes().contains(c_n));
    assertTrue(qd.graph().nodes().contains(a_n));
    assertTrue(qd.graph().relations().contains(b_n));
    assertTrue(qd.graph().nodes().contains(c_n));
  }

  /** Tests that that creation binding is correct if source is bound.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, binding, some unbound, one relation")
  public void creationBindingOneBound()
  {
    String query = "CREATE (a)-[b:abcd]->(c:yy)";
    Graph graph = catGraph;
    Table in = new Table ( new Record( a,leonid_v ),
                           new Record( a,nadime_v )  );
    String changes = "createdNodes=2, addedLabels=2, createdRelations=2";
    QueryData qd = assertExecution(changes, graph, in,query);
    Table out = qd.table();
    assertEquals(2, out.size());
    Record r1 = null;
    Record r2 = null;
    for( Record r: out ) {
      assertEquals(3, r.domain().size());
      assertTrue(r.isBound(a));
      assertTrue(r.isBound(b));
      assertTrue(r.isBound(c));
      if (r.get(a).equals(leonid_v))
        r1 = r;
      if (r.get(a).equals(nadime_v))
        r2 = r;
    }
    assertNotNull(r1);
    assertNotNull(r2);

    { Relation b_n = r1.get(b).asRelation();
      Node c_n = r1.get(c).asNode();
      assertEquals(leonid, b_n.source());
      assertEquals(c_n, b_n.target());
    }
    { Relation b_n = r2.get(b).asRelation();
      Node c_n = r2.get(c).asNode();
      assertEquals(nadime, b_n.source());
      assertEquals(c_n, b_n.target());
    }
  }

  /** Tests that that creation binding is correct for path.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, binding, path")
  public void creationBindingPath()
  {
    String query = "CREATE f=(a)-[b:abcd]->(c:yy)-[d:abcd]->(e)";
    Graph graph = catGraph;
    Table in = unitTable;
    String changes = "createdNodes=3, addedLabels=1, createdRelations=2";
    QueryData qd = assertExecution(changes, graph, in,query);
    Table out = qd.table();
    assertEquals(1, out.size());
    Record r = out.anyOne();
    assertEquals(Immutable.setOf(a,b,c,d,e,f),r.domain());
    Node a_n = r.get(a).asNode();
    Relation b_r = r.get(b).asRelation();
    Node c_n = r.get(c).asNode();
    Relation d_r = r.get(d).asRelation();
    Node e_n = r.get(e).asNode();
    Path f_p = r.get(f).asPath();
    assertEquals(new Path(a_n,b_r,c_n,d_r,e_n), f_p);
  }
}









