/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Machinery for unit test of queries.
 */
public class QueryTest
{

  /**
   * Represents a simplication of the changes between two graphs.
   * Used to verify that the evaluation of a query induces the correct
   * modification of the data.
   */
  public static class GraphChange {
    public int createdNodes = 0;
    public int deletedNodes = 0;
    public int createdRelations = 0;
    public int deletedRelations = 0;
    public int addedLabels = 0;
    public int removedLabels = 0;
    public int modifiedProperties = 0;

    public boolean equals(Object o) {
      if (! (o instanceof GraphChange))
        return false;
      GraphChange that = (GraphChange) o;
      return (
             this.createdNodes == that.createdNodes
          && this.deletedNodes == that.deletedNodes
          && this.createdRelations == that.createdRelations
          && this.deletedRelations == that.deletedRelations
          && this.addedLabels == that.addedLabels
          && this.removedLabels == that.removedLabels
          && this.modifiedProperties == that.modifiedProperties
        );
    }

    public GraphChange(String s) {
      if (s.equals(""))
        return;
      String[] commands = s.split(",");
      for (String str : commands) {
        String[] command = str.split("=");
        if (command.length != 2)
          throw new Error("Too many =");
        int value = Integer.parseInt(command[1]);
        String key = command[0].replaceAll("\\W", "");
        switch (key) {
          case "createdNodes": createdNodes = value; break;
          case "deletedNodes": deletedNodes = value; break;
          case "createdRelations": createdRelations = value; break;
          case "deletedRelations": deletedRelations = value; break;
          case "addedLabels": addedLabels = value; break;
          case "removedLabels": removedLabels = value; break;
          case "modifiedProperties": modifiedProperties = value; break;
          default: throw new Error("Unknown GraphChange element.");
        }
      }
    }

    public GraphChange(Graph in, Graph out) {
      // Nodes
      Set<Node> all_nodes = new HashSet<>();
      all_nodes.addAll(in.nodes());
      all_nodes.addAll(out.nodes());
      for (Node n : all_nodes) {
        Integer id = n.content();
        boolean part_of_in = in.nodes().contains(n);
        boolean part_of_out = out.nodes().contains(n);
        // Counting nodes
        if (part_of_in && !part_of_out)
          this.deletedNodes++;
        if (part_of_out && !part_of_in)
          this.createdNodes++;
        // Finding (or creating empty) node
        Node node_in = part_of_in ? in.getNodeById(id) : new Node(id);
        Node node_out = part_of_out ? out.getNodeById(id) : new Node(id);
        // Counting modified labels
        Set<Label> labels_in = node_in.labels();
        Set<Label> labels_out = node_out.labels();
        for (Label label : labels_in)
          if(! labels_out.contains(label) )
            this.removedLabels++;
        for (Label label : labels_out)
          if(! labels_in.contains(label) )
            this.addedLabels++;
        // Counting modified properties
        Set<PropertyKey> keys = new HashSet<>();
        keys.addAll(node_in.propertyKeySet());
        keys.addAll(node_out.propertyKeySet());
        for (PropertyKey key : keys)
          if (! node_in.getPropertyValue(key).equals(node_out.getPropertyValue(key)) )
            modifiedProperties++;
      }

      // Relations
      Set<Relation> all_relations = new HashSet<>();
      all_relations.addAll(in.relations());
      all_relations.addAll(out.relations());
      Node dummy = new Node(-1);
      for (Relation r : all_relations) {
        Integer id = r.content();
        boolean part_of_in = in.relations().contains(r);
        boolean part_of_out = out.relations().contains(r);
        // Counting
        if (part_of_in && !part_of_out)
          this.deletedRelations++;
        if (part_of_out && !part_of_in)
          this.createdRelations++;
        // Finding (or creating empty) relation
        Relation relation_in
            = part_of_in ? in.getRelationById(id)
                         : new Relation(id, new RelationType(""), dummy, dummy);
        Relation relation_out
            = part_of_out ? out.getRelationById(id)
                          : new Relation(id, new RelationType(""), dummy, dummy);
        // Counting modified properties
        Set<PropertyKey> keys =  new HashSet<>();
        keys.addAll(relation_in.propertyKeySet());
        keys.addAll(relation_out.propertyKeySet());
        for (PropertyKey key : keys)
          if (!
                relation_in.getPropertyValue(key).equals(
                    relation_out.getPropertyValue(key))
          )
            modifiedProperties++;
      }
    }


    @Override
    public String toString() {
      List<String> items = new ArrayList<>();
      if (createdNodes != 0)
        items.add("createdNodes=" + createdNodes);
      if (deletedNodes != 0)
        items.add("deletedNodes=" + deletedNodes);
      if (createdRelations != 0)
        items.add("createdRelations=" + createdRelations);
      if (deletedRelations != 0)
        items.add("deletedRelations=" + deletedRelations);
      if (addedLabels != 0)
        items.add("addedLabels=" + addedLabels);
      if (removedLabels != 0)
        items.add("removedLabels=" + removedLabels);
      if (modifiedProperties != 0)
        items.add("modifiedProperties=" + modifiedProperties);
      return StringOf.iterable(items, "", "", ",");
    }

  }

  public void assertGraphChange(GraphChange target, Graph in, Graph out) {
    GraphChange got = new GraphChange(in, out);
    assertEquals(
      target,
      got,
      "The changes from first graph to second (below) is not as expected.\n"
      + "in:\n" + in.toCypher() + "\n"
      + "out:\n" + out.toCypher() + "\n"
    );
  }

  public void assertGraphChange(String str, Graph in, Graph out) {
    assertGraphChange(new GraphChange(str), in, out);
  }


  public QueryData assertExecution
    ( String changes, Table expectedTable, Graph inputGraph,
      Table inputTable, String query
    )
  {
    return assertExecution(changes, Optional.of(expectedTable), inputGraph, inputTable, query);
  }

  public QueryData assertExecution
    ( String changes, Graph inputGraph, Table inputTable, String query )
  {
    return assertExecution(changes, Optional.empty(), inputGraph, inputTable, query);
  }

  public QueryData assertExecution
    ( String changes, Optional<Table> expectedTable, Graph inputGraph,
      Table inputTable, String query
    )
  {
    String sanitizedQuery = query;
    if (! query.toUpperCase().contains("RETURN"));
      sanitizedQuery += " RETURN *";
    Query q = Parser.queryOf(sanitizedQuery);
    return assertExecution(changes, expectedTable,
      inputGraph, inputTable, q);
  }
  public QueryData assertExecution
    ( String changes, Optional<Table> expectedTable, Graph inputGraph,
      Table inputTable, Query q
    )
  {
    Triplet<Graph,Map<Node,Node>,Map<Relation,Relation>> triplet
      = inputGraph.copy();
    Table trueInputTable
      = inputTable
        .map( v -> v.isOfKind(Kind.NODEID)
                      ? Optional.of(new Value(triplet.second.get(v.asNode())))
                      : Optional.empty() )
        .map( v -> v.isOfKind(Kind.RELID)
                      ? Optional.of(new Value(triplet.third.get(v.asRelation())))
                      : Optional.empty() );
    QueryData qd = q.apply( new QueryData(triplet.first, trueInputTable) );
    assertGraphChange(changes, inputGraph, qd.graph());
    if ( expectedTable.isPresent() ) {
      assertEquals(
          expectedTable.get(),
          qd.table(),
          "Query evaluation did not produce the expected result.\nQuery:\n" + q
          + "\nGraph:\n" + ( (inputGraph == null)?"null":inputGraph.toCypher()
          + "\n"  )
        );
    }
    return qd;
  }

  public void assertExecutionThrows
    ( Class<? extends Exception> excClass, Graph inputGraph,Table inputTable, String query
    )
  {

    String sanitizedQuery = query;
    if (! query.toUpperCase().contains("RETURN"));
      sanitizedQuery += " RETURN *";
    Query q = Parser.queryOf(sanitizedQuery);
    Triplet<Graph,Map<Node,Node>,Map<Relation,Relation>> triplet
      = inputGraph.copy();
    Table trueInputTable
      = inputTable
        .map( v -> v.isOfKind(Kind.NODEID)
                      ? Optional.of(new Value(triplet.second.get(v.asNode())))
                      : Optional.empty() )
        .map( v -> v.isOfKind(Kind.RELID)
                      ? Optional.of(new Value(triplet.third.get(v.asRelation())))
                      : Optional.empty() );
    assertThrows(
      excClass,
      () -> q.apply( new QueryData(triplet.first, trueInputTable) )
    );
  }


}
