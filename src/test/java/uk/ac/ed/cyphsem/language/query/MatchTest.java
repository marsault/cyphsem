/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of MATCH clause.
 * @see Sample#sampleTest1
 */
public class MatchTest extends QueryTest
{

  /** Tests empty node-pattern matching.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching nodes, empty node patterns")
  public void emptyNodePattern()
  {
    String query;
    Graph graph = catGraph;
    Table in;
    Table out;
    { query = "MATCH ()";  // Empty node Pattern
      { in = unitTable; // Input table is the unit table.
        out = new Table();   // Output table has six empty records.
        for (int i = 0; i<6; i++ )
          out.add(new Record());
        assertExecution("", out, graph, in,query);
      }
      { Record r1 = new Record(a, new Value(0)); // Input table is:
        Record r2 = new Record(a, new Value());  //       a
        in = new Table();                        //      ------
        in.add(r1); in.add(r2);                  // r1->  0
                                                 // r2->  null

        out = new Table();   // Output table is six copies of r1 and r2
        for (int i = 0; i<6; i++ ) {
          out.add(new Record(r1));
          out.add(new Record(r2));
        }
        assertExecution("", out, graph, in,query);
      }
    }
    { query = "MATCH (b)";  // Empty node Pattern with variable b
      { in = unitTable; // Input table is the unit table.
        out = new Table();             // Output table has six records,
        for (Node n : graph.nodes()) { // one {a:n} for each node n in the
          Record r = new Record();     // graph.
          r.bind(b, new Value(n));
          out.add(r);
        }
        assertExecution("", out, graph, in,query);
      }
      { Record r1 = new Record(a, new Value(0)); // Input table is:
        Record r2 = new Record(a, new Value());  //       a
        in = new Table();                        //      ------
        in.add(r1); in.add(r2);                  // r1->  0
                                                 // r2->  null

        out = new Table(); // Output table has 12 records.
        for (Node n : graph.nodes()) {
          Record s1 = new Record(r1);
          s1.bind(b, new Value(n));
          out.add(s1);
          Record s2 = new Record(r2);
          s2.bind(b, new Value(n));
          out.add(s2);
        }
        assertExecution("", out, graph, in,query);
      }
    }

  }

  /** Tests node matching by labels.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching nodes, labels")
  public void labels()
  {
    String query;
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out;
    { query = "MATCH (a:cat)";  // Node pattern with one label
      out = new Table();
      for (Node n : Arrays.asList(leonidsCat,athanase,zelia)) {
          out.add(new Record(a,new Value(n)));
      }
      assertExecution("", out, graph, in,query);
    }
    { query = "MATCH (a:god)";  // Node pattern with nonexistent label
      out = new Table();
      assertExecution("", out, graph, in,query);
    }
    { query = "MATCH (a:cat:savvy)";  // Node pattern with two label
      out = new Table(  );
      out.add( new Record(a, athanase_v) );
      out.add( new Record(a, zelia_v) );
      assertExecution("", out, graph, in,query);
    }
    { query = "MATCH (a:cat:savvy:blue)";  // Node pattern with three label
      out = new Table( new Record(a, athanase_v) );
      assertExecution("", out, graph, in,query);
    }
    { query = "MATCH (a:blue:tabby)";  // Node pattern with bad combinaison
                                       // of existing labels
      out = new Table();
      assertExecution("", out, graph, in,query);
    }

    //Input table now contains each node
    in = new Table();
    for (Node n : graph.nodes())
      in.add( new Record(a, new Value(n)) );
    { query = "MATCH (a:human)"; //Tests filtering of input table
      out = new Table();
      out.add( new Record(a, victor_v) );
      out.add( new Record(a, nadime_v) );
      assertExecution("", out, graph, in,query);
    }
  }

  /** Tests node matching by properties: one property.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, one property")
  public void nodePropOne()
  {
    String query = "MATCH (a {name:\"Athanase\"})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table(new Record(a, athanase_v));
    assertExecution("", out, graph, in,query);
  }

  /** Tests node matching by properties: one property.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, two properties")
  public void nodePropTwo()
  {
    String query = "MATCH (a {name:\"Athanase\", breed:\"RussianBlue\"})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table(new Record(a, athanase_v));
    assertExecution("", out, graph, in,query);
  }

  /** Tests node matching by properties: inexistent property key.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, inexistent property key")
  public void nodePropBadKey()
  {
    String query = "MATCH (a {abcd:12})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }

  /** Tests node matching by properties: inexistent property value.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, inexistent property key")
  public void nodePropBadVal()
  {
    String query = "MATCH (a {name:\"Santa\"})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }

  /** Tests node matching by properties: inexistent property value.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, inexistent property")
  public void nodePropBadKeyValPair()
  {
    String query = "MATCH (a {name:\"RussianBlue\"})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }

  /** Tests node matching by properties:  inexistent combinaison of properties.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, inexistent combinaison of properties")
  public void nodePropBadCombinaison()
  {
    String query = "MATCH (a {name:\"Zelia\", breed:\"RussianBlue\"})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }

  /** Tests node matching by property+label..
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, property+label")
  public void nodeLabelPlusProp()
  {
    String query = "MATCH (a :cat {name:\"Zelia\"})";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table(new Record(a,zelia_v));
    assertExecution("", out, graph, in,query);
  }



  /** Tests empty relation-pattern matching.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching nodes, empty relation pattern 1")
  public void emptyRelPattern1()
  {
    String query = "MATCH ()-[]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();
    for (Relation r : graph.relations())
      out.add(new Record());
    assertExecution("", out, graph, in,query);
  }


  /** Tests anydirected empty relation-pattern matching with one end selected.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, empty pattern, from fixed source")
  public void emptyRelFixedSource()
  {
    String query = "MATCH (a)-[]-(b)";
    Graph graph = catGraph;
    Table in = new Table( new Record(a, zelia_v) );
    Table out = new Table(
        new Record(a,zelia_v, b,nadime_v),
        new Record(a,zelia_v, b,zelia_v)
      );
    assertExecution("", out, graph, in,query);
  }

  /** Tests empty relation-pattern matching with destination selected.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, empty pattern, from fixed target")
  public void emptyRelFixedTarget()
  {
    String query = "MATCH (b)-[]->(a)";
    Graph graph = catGraph;
    Table in = new Table( new Record(a, zelia_v) );
    Table out = new Table(
        new Record(a,zelia_v, b,nadime_v),
        new Record(a,zelia_v, b,zelia_v)
      );
    assertExecution("", out, graph, in,query);
  }


  /** Tests matching of relations by type.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relType()
  {
    String query = "MATCH ()-[a:has]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();
    for (Relation r  : Immutable.listOf(r1,r2,r3)) {
      out.add( new Record(a, new Value(r)) );
    }
    assertExecution("", out, graph, in,query);
  }

  /** Tests matching of relations by inexistent type.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, inexistent type")
  public void relInexistentType()
  {
    String query = "MATCH ()-[a:abcd]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }

  /** Tests matching of relations by property.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relProp()
  {
    String query = "MATCH ()-[a{since:2017}]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(new Record(a, new Value(r7)));
    assertExecution("", out, graph, in,query);
  }


  /**  Tests node matching by properties when property key is not in the graph.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relBadPropkey()
  {
    String query = "MATCH ()-[a{date:2017}]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }


  /**  Tests node matching by properties when property value is not in the graph.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relBadPropValue()
  {
    String query = "MATCH ()-[a{date:1234}]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }


  /** Tests node matching by properties when property is not in the graph.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relBadProp()
  {
    String query = "MATCH ()-[a{in:2014}]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();
    assertExecution("", out, graph, in,query);
  }

  /** Tests relation matching by properties+type.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relPropType()
  {
    String query = "MATCH ()-[a:has{since:2014}]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(new Record(a,new Value(r2)));;
    assertExecution("", out, graph, in,query);
  }

  /** Tests relation matching by properties+type when combinaison does not exist
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Matching relation, type")
  public void relBadPropType()
  {
    String query = "MATCH ()-[a:knows{since:2014}]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table();;
    assertExecution("", out, graph, in,query);
  }

  /** Tests matching of length-two fully-specified path.
    *
  **/
  @Test
  @DisplayName("Matching length-two path")
  public void lengthTwo()
  {

    Graph graph = catGraph;
    String query = "MATCH (a)-[:has]->()<-[:kept]-(b)";
    Table in = unitTable;
    Table out = new Table(
        new Record(a, victor_v, b, nadime_v),
        new Record(a, victor_v, b, nadime_v),
        new Record(a, leonid_v, b, nadime_v)
      );
    assertExecution("", out, graph, in, query);
  }

  /** Tests matching of length-two fully-specified path from given startpoint.
  **/
  @Test
  @DisplayName("Matching length-two path, fixed origin")
  public void lengthTwo2()
  {

    Graph graph = catGraph;
    String query = "MATCH (a)-[:has]->()<-[:kept]-(b)";
    Table in = new Table(new Record(a, leonid_v));
    Table out = new Table(
        new Record(a, leonid_v, b, nadime_v)
      );
    assertExecution("", out, graph, in, query);
  }

  /** Tests matching of variable bounded path.
  **/
  @Test
  @DisplayName("Matching bounded variable-length path.")
  public void pathBounded()
  {

    Graph graph = catGraph;
    String query = "MATCH (a)-[:knows*1..2]-(b)";
    Table in = new Table(new Record(a, leonid_v));
    Table out = new Table(
        new Record(a, leonid_v, b, nadime_v),
        new Record(a, leonid_v, b, victor_v),
        new Record(a, leonid_v, b, nadime_v),
        new Record(a, leonid_v, b, victor_v)
      );
    assertExecution("", out, graph, in, query);
  }


  /** Tests cypher-morphism with simple anydirected relation patterns.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Cyphermorphism, simple directed, no result")
  public void cypherMorphismSimple2()
  {
    Graph graph = catGraph;
    String query = "MATCH (a)<-[:has]-()-[:has]->(b)";
    Table in = unitTable;
    Table out = new Table();
    assertExecution("", out, graph, in, query);
  }

  /** Tests cypher-morphism with simple anydirected relation patterns.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Cyphermorphism, simple anydirected")
  public void cypherMorphismSimple3()
  {

    Graph graph = catGraph;
    String query = "MATCH (a)-[:knows]-()-[:knows]-(b)";
    Table in = unitTable;
    Table out = new Table(
        new Record(a, leonid_v, b, victor_v),
        new Record(a, leonid_v, b, nadime_v),
        new Record(a, victor_v, b, leonid_v),
        new Record(a, victor_v, b, nadime_v),
        new Record(a, nadime_v, b, leonid_v),
        new Record(a, nadime_v, b, victor_v)
      );
    assertExecution("", out, graph, in,query);
  }


  /** Tests path pattern requiring a post-condition, multi pattern.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, postconditions, fixed length path")
  public void postConditionsMulti()
  {
    String query = "MATCH (b{name:a.name})-[]->()-[]->()-[]->(a)";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table(
        new Record(a,victor_v, b,victor_v),
        new Record(a,nadime_v, b,nadime_v),
        new Record(a,leonid_v, b,leonid_v)
      );
    assertExecution("", out, graph, in,query);
  }


  /** Tests path pattern requiring a post-condition, one fixed-length relation pattern.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, postconditions, rigid")
  public void postConditionsFixed()
  {
    String query = "MATCH (b{name:a.name})-[*3]->(a)";
    Graph graph = catGraph;
    Table in = unitTable; // Input table is the unit table.
    Table out = new Table(
        new Record(a,victor_v, b,victor_v),
        new Record(a,nadime_v, b,nadime_v),
        new Record(a,leonid_v, b,leonid_v)
      );
    assertExecution("", out, graph, in,query);
  }


  /** Tests path pattern requiring a post-condition, one variable length relation pattern.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching nodes, postconditions, variable length")
  public void postConditionsVariable()
  {
    String query = "MATCH (b{name:a.name})-[*]->(a)";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(
        new Record(a,victor_v, b,victor_v),
        new Record(a,nadime_v, b,nadime_v),
        new Record(a,leonid_v, b,leonid_v),
        new Record(a,zelia_v,  b,zelia_v)
      );
    assertExecution("", out, graph, in,query);
  }

  /**  Tests path pattern requiring a post-condition, one variable length relation pattern.
   * @beware See class {@link Sample}, in particular {@link Sample#catGraph}, {@link Sample#a} and {@link Sample#b}.
  */
  @Test
  @DisplayName("Matching relations, postconditions")
  public void postConditionsRelations()
  {
    String query = "MATCH ()-[a:knows{since:b.in}]->(),()-[b:kept]->()";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(
        new Record(a,new Value(r7), b,new Value(r6)),
        new Record(a,new Value(r7), b,new Value(r4))
      );
    assertExecution("", out, graph, in,query);
  }



}









