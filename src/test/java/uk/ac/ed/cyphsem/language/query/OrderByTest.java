/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;
import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>DELETE</tt> clause.
 * @see Sample#sampleTest1
 */
public class OrderByTest extends QueryTest
{

  /** Tests simple ORDER BY .. SKIP .. clause.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, simple SKIP")
  public void skip()
  {
    String query = "WITH * ORDER BY a.name SKIP 1";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v ),
        new Record( a,zelia_v    )
      );
    Table out = new Table (
        new Record( a,zelia_v)
      );
    assertExecution("", out, graph, in, query);
  }

  /** Tests simple ORDER BY .. LIMIT .. clause.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, simple LIMIT")
  public void limit()
  {
    String query = "WITH * ORDER BY a.name LIMIT 1";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v ),
        new Record( a,zelia_v    )
      );
    Table out = new Table (
        new Record( a,athanase_v)
      );
    assertExecution("", out, graph, in, query);
  }

  /** Tests simple ORDER BY .. SKIP .. LIMIT .. clause.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, simple SKIP+LIMIT")
  public void skipLimit()
  {
    String query = "WITH * ORDER BY a.name SKIP 1 LIMIT 1";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v ),
        new Record( a,nadime_v   ),
        new Record( a,zelia_v    )
      );
    Table out = new Table (
        new Record( a,nadime_v )
      );
    assertExecution("", out, graph, in, query);
  }


  /** Tests ORDER BY with null.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, null")
  public void withNull()
  {
    String query = "WITH * ORDER BY a.name SKIP 1 LIMIT 1";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v   ),
        new Record( a,leonidsCat_v ),
        new Record( a,zelia_v      )
      );
    Table out = new Table (
        new Record( a,zelia_v )
      );
    assertExecution("", out, graph, in, query);
  }

  /** Tests that ORDER BY with a negative value in SKIP does nothing.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, negative SKIP")
  public void negativeSkip()
  {
    String query = "WITH * ORDER BY a.name SKIP -1";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v   ),
        new Record( a,leonidsCat_v ),
        new Record( a,zelia_v      )
      );
    Table out = new Table ( in );
    assertExecution("", out, graph, in, query);
  }

   /** Tests that ORDER BY with too great a value in LIMIT, does nothing.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, huge LIMIT")
  public void hugeLimit()
  {
    String query = "WITH * ORDER BY a.name LIMIT 2000";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v   ),
        new Record( a,leonidsCat_v ),
        new Record( a,zelia_v      )
      );
    Table out = new Table ( in );
    assertExecution("", out, graph, in, query);
  }

  /** Tests the DESC keyword.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("ORDER BY, DESC")
  public void desc()
  {
    String query = "WITH * ORDER BY a.name DESC LIMIT 2";
    Graph graph = catGraph;
    Table in = new Table (
        new Record( a,athanase_v   ),
        new Record( a,leonidsCat_v ),
        new Record( a,zelia_v      )
      );
    Table out = new Table (
        new Record( a,leonidsCat_v ),
        new Record( a,zelia_v      )
      );
    assertExecution("", out, graph, in, query);
  }

  /** Tests the nondeterminism in case of record equality.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */

  @Test
  @DisplayName("ORDER BY, DESC")
  public void recordEquality()
  {
    String query = "WITH * ORDER BY a.name SKIP 1";
    Graph graph = catGraph;
    Record r1 = new Record( a,athanase_v   , b,new Value(1)  );
    Record r2 = new Record( a,athanase_v   , b,new Value(2)  );
    Table in = new Table (r1, r2);
    Table out1 = new Table (r1);
    Table out2 = new Table (r2);
    QueryData qd = assertExecution("", graph, in, query);
    assertTrue(
        Immutable.listOf(out1,out2).contains(qd.table()),
        "Output table " + qd.table() + " is supposed to be either "
        + out1 + "or" + out2 + "."
      );
  }

}










