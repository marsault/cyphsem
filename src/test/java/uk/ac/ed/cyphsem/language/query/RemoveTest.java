/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import static uk.ac.ed.cyphsem.cli.Sample.*;
import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import uk.ac.ed.cyphsem.cli.*;


import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>REMOVE</tt> clause.
 * @see Sample#sampleTest1
 */
public class RemoveTest extends QueryTest
{

  /** Tests removal of one label.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove labels, one")
  public void removeOneLabel()
  {
    String query = "MATCH (a:cat) REMOVE a:cat";
    Graph graph = catGraph;
    String changes = "removedLabels=3";
    Table in = unitTable;

    QueryData qd = assertExecution(changes,graph,in,query);
    for(Node n : qd.graph().nodes())
      assertFalse(n.labels().contains(cat));
  }

  /** Tests removal of one label that may not exist.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove labels, one, laxist")
  public void removeOneLabelLaxist()
  {
    String query = "MATCH (a:cat) REMOVE a:tabby";
    Graph graph = catGraph;
    String changes = "removedLabels=1";
    Table in = unitTable;

    QueryData qd = assertExecution(changes,graph,in,query);
    for(Node n : qd.graph().nodes())
      assertFalse(n.labels().contains(tabby));
  }

  /** Tests removal of two labels.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove labels, multiple, laxist")
  public void removeMultipleLabels()
  {
    String query = "MATCH (a:cat) REMOVE a:cat:blue:human";
    Graph graph = catGraph;
    String changes = "removedLabels=4";
    Table in = unitTable;

    QueryData qd = assertExecution(changes,graph,in,query);
    for(Node n : qd.graph().nodes()) {
      assertFalse(n.labels().contains(cat));
      assertFalse(n.labels().contains(blue));
    }
  }

  /** Tests removal of one property.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove properties, one")
  public void removeOneProperty()
  {
    String query = "MATCH (a:cat)  WHERE a.name IS NOT NULL  REMOVE a.name";
    Graph graph = catGraph;
    String changes = "modifiedProperties=2";
    Table in = unitTable;

    QueryData qd = assertExecution(changes,graph,in,query);
    for(Node n : qd.graph().nodes())
      if(n.labels().contains(cat))
        assertEquals(new Value(), n.getPropertyValue(name));
  }

  /** Tests removal of one property that may not exist.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove properties, one, laxist")
  public void removeOnePropertyLaxist()
  {
    String query = "MATCH (a:cat) REMOVE a.breed";
    Graph graph = catGraph;
    String changes = "modifiedProperties=1";
    Table in = unitTable;

    QueryData qd = assertExecution(changes,graph,in,query);
    for(Node n : qd.graph().nodes())
      if(n.labels().contains(cat))
        assertEquals(new Value(), n.getPropertyValue(breed));
  }

  /** Tests removal of one property that may not exist.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove properties, multiple.")
  public void removeMultipleProperties()
  {
    String query = "MATCH ()-[a]->(b) REMOVE a.since.in,b.name";
    Graph graph = catGraph;
    String changes = "modifiedProperties=13";
    Table in = unitTable;

    QueryData qd = assertExecution(changes,graph,in,query);
    for(Node n : qd.graph().nodes())
        assertEquals(new Value(), n.getPropertyValue(name));
    for(Relation r : qd.graph().relations()) {
        assertEquals(new Value(), r.getPropertyValue(since));
        assertEquals(new Value(), r.getPropertyValue(Sample.in));
    }
  }

  /** Tests removal of one property that may not exist.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Remove, handMade")
  public void removeHandmade()
  {
    RemoveClause clause =
      new RemoveClause (
          Immutable.listOf(
              Pair.of(a,name),
              Pair.of(a,breed)
            ),
          Immutable.listOf(
              Pair.of(a,cat),
              Pair.of(a,human)
            )
        );
    QueryData qd = assertExecution("modifiedProperties=3,removedLabels=3",Optional.of(aToCats),catGraph,aToCats,
      new ClauseQueryBis(clause,new ReturnQueryBis()));
  }

}









