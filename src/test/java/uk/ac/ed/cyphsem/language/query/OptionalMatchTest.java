/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>OPTIONAL MATCH</tt> clause.
 * @see Sample#sampleTest1
 */
public class OptionalMatchTest extends QueryTest
{

  /** Tests working OPTIONAL MATCH.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Optional Match, working")
  public void optMatchWorking()
  {
    String query = "OPTIONAL MATCH (a:cat)";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table( new Record( a, zelia_v    ),
                           new Record( a, athanase_v ),
                           new Record( a, leonidsCat_v) );
    assertExecution("", out, graph, in,query);
  }

  /** Tests half-working OPTIONAL MATCH.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Optional Match, half working")
  public void optMatchHalfWorking()
  {
    String query = "OPTIONAL MATCH (b{name:a})";
    Graph graph = catGraph;
    Table in = new Table( new Record( a,new Value("Zelia") ),
                          new Record( a,new Value("God")   )  );
    Table out = new Table( new Record( a,new Value("Zelia"), b,zelia_v     ),
                           new Record( a,new Value("God"),   b,new Value() )  );
    assertExecution("", out, graph, in,query);
  }




  /** Tests OPTIONAL MATCH on inexistent pair of labels.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Optional Match, simple1")
  public void optMatchSimple1()
  {
    String query = "OPTIONAL MATCH (a:cat:human)";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(new Record(a, new Value()));
    assertExecution("", out, graph, in,query);
  }

  /** Tests OPTIONAL MATCH on inexistent label.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Optional Match, simple2")
  public void optMatchSimple2()
  {
    String query = "OPTIONAL MATCH (a:cat:inexistent)";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(new Record(a, new Value()));
    assertExecution("", out, graph, in,query);
  }

  /** Tests OPTIONAL MATCH with a failing WHERE subclause.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Optional Match, where false")
  public void optMatchWhereFalse()
  {
    String query = "OPTIONAL MATCH (a:cat) WHERE false";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(new Record(a, new Value()));
    assertExecution("", out, graph, in,query);
  }

  /** Tests OPTIONAL MATCH with a path variable.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Optional Match, path variable")
  public void optMatchPathVariable()
  {
    String query = "MATCH(a{name:\"Zelia\"}) OPTIONAL MATCH f=(a),(b:inexistent)";
    Graph graph = catGraph;
    Table in = unitTable;
    Table out = new Table(new Record(a,zelia_v, b,new Value(), f,new Value()));
    assertExecution("", out, graph, in,query);
  }

}









