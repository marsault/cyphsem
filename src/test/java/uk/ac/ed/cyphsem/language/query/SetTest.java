/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>SET</tt> clause.
 * @see Sample#sampleTest1
 */
public class SetTest extends QueryTest
{

  /** Tests SET simple label.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, simple label 1")
  public void setSimpleLabel1()
  {
    String query = " MATCH (a) SET a:label";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    String changes = "addedLabels=1";
    QueryData qd = assertExecution(changes, graph, in, query);
  }

  /** Tests SET simple label.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, simple label 2")
  public void setSimpleLabel2()
  {
    String query = " MATCH (a) SET a:label:label2";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    String changes = "addedLabels=2";
    QueryData qd = assertExecution(changes, graph, in, query);
  }

  /** Tests SET simple property.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, simple properties 1")
  public void setSimpleProperty1()
  {
    String query = " MATCH (a) SET a.name = \"x\"";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    String changes = "modifiedProperties=1";
    QueryData qd = assertExecution(changes, graph, in, query);
    Node n = qd.graph().nodes().iterator().next();
    assertEquals(new Value("x"), n.getPropertyValue(name));
  }

  /** Tests SET simple property.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, simple properties 2")
  public void setSimpleProperty2()
  {
    String query = " MATCH (a) SET a.name.firstname = \"x\"";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    String changes = "modifiedProperties=2";
    QueryData qd = assertExecution(changes, graph, in, query);
    Node n = qd.graph().nodes().iterator().next();
    assertEquals(new Value("x"), n.getPropertyValue(name));
    assertEquals(
        new Value("x"),
        n.getPropertyValue(new PropertyKey("firstname"))
      );
  }

  /** Tests SET +=.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, +=")
  public void setPlusEgal()
  {
    String query = "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a += b";
    Graph graph = catGraph;
    Table in = unitTable;
    String changes = "modifiedProperties=1";
    QueryData qd = assertExecution(changes, graph, in, query);
    Node z = qd.table().anyOne().get(a).asNode();
    assertEquals(new Value("Zelia"), z.getPropertyValue(name));
    assertEquals(new Value("RussianBlue"), z.getPropertyValue(breed));
  }

  /** Tests SET =.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, =")
  public void setEgal()
  {
    String query = "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a = b";
    Graph graph = catGraph;
    Table in = unitTable;
    String changes = "modifiedProperties=2";
    QueryData qd = assertExecution(changes, graph, in, query);
    Node z = qd.table().anyOne().get(a).asNode();
    assertEquals(new Value("Athanase"), z.getPropertyValue(name));
    assertEquals(new Value("RussianBlue"), z.getPropertyValue(breed));
  }

  /** Tests failure of set in case of non-determinism.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, nondeterminism failure")
  public void setNonDeterminism()
  {
    List<String> queries = Immutable.listOf(
        "MATCH (a{name:\"Zelia\"}) SET a.age=12,a.age=1",
        "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a=b,a.name=\"toto\"",
        "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a=b,a=a"
      );
    Graph graph = catGraph;
    Table in = unitTable;

    for (String query : queries)
      assertExecutionThrows(NonDeterminismException.class, graph, in, query);
  }

  /** Tests fhat SET clauses that sets twice the same key to the same value do not fail.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, non-nondeterminism")
  public void setNonNonDeterminism()
  {
    Graph graph = catGraph;
    Table in = unitTable;
    { String query = "MATCH (a{name:\"Zelia\"}) SET a.name = \"x\",a.name = \"x\"";
      String changes = "modifiedProperties=1";
      QueryData qd = assertExecution(changes, graph, in, query);
    }
    { String query = "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a=b,a.name = \"Athanase\"";
      String changes = "modifiedProperties=2";
      QueryData qd = assertExecution(changes, graph, in, query);
    }
    { String query = "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a+=b,a.breed = \"RussianBlue\"";
      String changes = "modifiedProperties=1";
      QueryData qd = assertExecution(changes, graph, in, query);
    }
  }

  /** Tests that SET clause semantics is simultaneous.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, simultaneity")
  public void setSimultaneous()
  {
    Graph graph = catGraph;
    Table in = unitTable;
    String query = "MATCH (a{name:\"Zelia\"}),(b{name:\"Athanase\"}) SET a.name=b.name,b.name = a.name";
    String changes = "modifiedProperties=2";
    QueryData qd = assertExecution(changes, graph, in, query);
    Record r = qd.table().anyOne();
    assertEquals(
        new Value("Athanase"),
        r.get(a).asNode().getPropertyValue(name)
      );
    assertEquals(
        new Value("Zelia"),
        r.get(b).asNode().getPropertyValue(name)
      );
  }


  /** Tests toString().
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Set, toString")
  public void setToString()
  {
    Graph graph = catGraph;
    Table in = unitTable;
    List<String> items = Immutable.listOf(
      "a+=b",         //1
      "a.name=12",    //2
      "a=b",          //3
      "a:xx:yy",      //4
      "a.year.age=1"  //5
    );
    List<List<String>> toFind = Immutable.listOf(
      Immutable.listOf("a","+=","b"),                    //1
      Immutable.listOf("a",".","name","12"),             //2
      Immutable.listOf("a","=","b"),                     //3
      Immutable.listOf("a",":","xx",":","yy"),           //4
      Immutable.listOf("a",".","year",".","age","=","1") //5
    );
    for (OptPair<String,List<String>> pair : Iterators.combine(items,toFind)) {
      List<String> list = pair.right();
      Clause clause = Parser.clauseOf("SET "+pair.left());
      String clauseToString = clause.toString();
      int index = 0;
      for(String str : list) {
        index = clauseToString.indexOf(str, index);
        assertNotEquals(-1,index);
      }
    }
  }
}

