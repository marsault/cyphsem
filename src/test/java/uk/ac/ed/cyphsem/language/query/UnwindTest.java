/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of UNWIND clause.
 * @see Sample#sampleTest1
 */
public class UnwindTest extends QueryTest
{

  /** Tests that UNWINDS works as intended on empty lists.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("UNWIND, emptylist")
  public void unwindEmptylist()
  {
    String query = "UNWIND [] AS a";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    Table out = emptyTable;
    assertExecution("", out, graph, in, query);
  }

  /** Tests that UNWINDS works as intended on non-empty lists.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("UNWIND, non-empty list")
  public void unwindNonemptylist()
  {
    String query = "UNWIND [[],2] AS a";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    Table out = new Table ( new Record( a,new Value(Immutable.listOf()) ),
                            new Record( a,new Value(2)                  )  );
    assertExecution("", out, graph, in, query);
  }

  /** Tests that UNWINDS works as intended on non-lists values.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("UNWIND, non-empty list")
  public void unwindSingleElement()
  {
    String query = "UNWIND \"abcd\" AS a";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    Table out = new Table ( new Record( a,new Value("abcd") ) );
    assertExecution("", out, graph, in, query);
  }


  /** Tests UNWIND chains
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("UNWIND, chain")
  public void unwindChain()
  {
    String query = "UNWIND [[\"abcd\",[],1],2,[{since:2}]] AS a UNWIND a AS b UNWIND b AS c WITH c";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    Table out = new Table ( new Record( c,new Value("abcd")               ),
                            new Record( c,new Value(1)                    ),
                            new Record( c,new Value(2)                    ),
                            new Record( c,new Value(Immutable.<PropertyKey,Value>mapOf(since,new Value(2))) )  );
    assertExecution("", out, graph, in, query);
  }

}
