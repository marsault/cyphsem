/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;


import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests whether patterns are accepted or not.
 */
public class PatternSyntaxTest
{

  /** Tests that statically-inconsistent reading-clause patterns are rejected at construction. */
  @Test
  @DisplayName("Malformed pattern, static, reading")
  public void badPatternReadingStatic()
  {
    List<String> strings = Immutable.listOf(
        "(a)-[a]->()",
        "(a),()-[a]->()",
        "(a)-[b*]->(),()-[a]->()",
        "(a),()-[a*]-()",
        "()-[a]-(),()-[a*]-()",
        "a=(a)",
        "a=()<-[a]-()",
        "a=()<-[a*]-()",
        "(a{test:\"\"})-[]-(a:l)"
      );

    for(String s : strings) {
      assertThrows (
          IllegalPatternException.class,
          () -> new CompiledPattern(false, Parser.patternsOf(s)),
          "Pattern " + s + " is supposed to be statically invalid in a MATCH clause."
        );
    }
  }

  /** Tests that statically-inconsistent update-clause patterns are rejected at construction. */
  @Test
  @DisplayName("Malformed pattern, static, update")
  public void badPatternUpdateStatic()
  {
    List<String> strings = Immutable.listOf(
        "()-[a :t *]->()", // Multiple relation are not allowed
        "()-[a :s|t|u]->()", // Multiple types are not allowed
        "()-[:t|s]->()", // idem
        "()-[]->()" //Type is mandatory in an anonymous relation patterns
      );

    for(String s : strings) {
      assertThrows (
          IllegalPatternException.class,
          () -> new CompiledPattern(true, Parser.patternsOf(s)),
          "Pattern " + s + " is supposed to be statically invalid in an update clause."
        );
    }
  }

  /** Tests that dynamically inconsistent patterns are rejected before evaluation. */
  @Test
  @DisplayName("Malformed pattern, dynamic, update")
  public void badPatternDynamicReading()
  {
    List<String> strings = Immutable.listOf(
        "()-[a]->()",
        "a=()-[:t]-()",
        "(a {k:\"str\"})",
        "(a)-[:t]->(a {k:\"str\"})"
      );

    List<Name> boundVariables = Immutable.listOf(new Name("a"));
    for(String s : strings) {
      CompiledPattern pattern =  new CompiledPattern(true, Parser.patternsOf(s));
      Name a = new Name("a");
      assertThrows(
          AssertionFailedException.class,
          () -> pattern.assertValidInContextOf(boundVariables),
          "Pattern " + s + " is supposed to invalid in an update clause if `a` is a bound variable."
        );
    }
  }




}
