/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>WHERE</tt> clause.
 * @see Sample#sampleTest1
 */
public class WhereTest extends QueryTest
{

  /** Tests that evaluation to {@code true} filters records out.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter in true")
  public void filterinTrue()
  {
    String query = "WITH * WHERE true";
    Graph graph = catGraph;
    Table in = new Table(
        new Record(a,victor_v),
        new Record(a,nadime_v),
        new Record(a,zelia_v)
      );
    Table out = new Table(in);
    assertExecution("", out, graph, in,query);
  }

  /** Tests that evaluation to {@code false} filters records out.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter out false")
  public void filterOutFalse()
  {
    String query = "WITH * WHERE false";
    Graph graph = catGraph;
    Table in = new Table(
        new Record(a,victor_v),
        new Record(a,nadime_v),
        new Record(a,zelia_v)
      );
    Table out = emptyTable;
    assertExecution("", out, graph, in,query);
  }


  /** Tests that evaluation to {@code null} filters records out.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter out null")
  public void filterOutNull()
  {
    String query = "WITH * WHERE null";
    Graph graph = catGraph;
    Table in = new Table(
        new Record(a,victor_v),
        new Record(a,nadime_v),
        new Record(a,zelia_v)
      );
    Table out = emptyTable;
    assertExecution("", out, graph, in,query);
  }

  /** Tests that expression is evaluated for filtering.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter depends on evaluation")
  public void filterEvaluation()
  {
    String query = "WITH * WHERE b";
    Graph graph = catGraph;
    Table in = new Table(
        new Record( a,victor_v, b,new Value(true)  ),
        new Record( a,nadime_v, b,new Value(false) ),
        new Record( a,zelia_v,  b,new Value()      )
      );
    Table out = new Table( new Record(a,victor_v, b,new Value(true)) );
    assertExecution("", out, graph, in,query);
  }

  /** Tests that WHERE fails if used with a list.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter fails if list")
  public void filterCastList()
  {
    String query = "WITH * WHERE []";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    assertExecutionThrows(
        DynamicCastException.class,
        graph,
        in,
        query
      );
  }

  /** Tests that WHERE fails if used with a map.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter fails if map")
  public void filterCastMap()
  {
    String query = "WITH * WHERE {text:\"Hello World!\"}";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    assertExecutionThrows(
        DynamicCastException.class,
        graph,
        in,
        query
      );
  }

  /** Tests that WHERE fails if used with an integer.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter fails if integer")
  public void filterCastInteger()
  {
    String query = "WITH * WHERE 12";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    assertExecutionThrows(
        DynamicCastException.class,
        graph,
        in,
        query
      );
  }

  /** Tests that WHERE fails if used with a String.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter fails if string")
  public void filterCastString()
  {
    String query = "WITH * WHERE \"abcd\"";
    Graph graph = singleNodeGraph;
    Table in = unitTable;
    assertExecutionThrows(
        DynamicCastException.class,
        graph,
        in,
        query
      );
  }

  /** Tests that WHERE fails if used with a node.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter fails if node")
  public void filterCastNode()
  {
    String query = "WITH * WHERE a";
    Graph graph = singleNodeGraph;
    Table in = new Table( new Record(a,new Value(n0)) );
    assertExecutionThrows(
        DynamicCastException.class,
        graph,
        in,
        query
      );
  }

  /** Tests that WHERE fails if used with a relation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. {@link Sample#singleNodeGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Filter fails if relation")
  public void filterCastRelation()
  {
    String query = "WITH * WHERE a";
    Graph graph = catGraph;
    Table in = new Table( new Record(a, new Value(r1)) );
    assertExecutionThrows(
        DynamicCastException.class,
        graph,
        in,
        query
      );
  }

}









