/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>MERGE</tt> clause.
 * @see Sample#sampleTest1
 */
public class MergeTest extends QueryTest
{
  /** Tests MERGE as MATCH with node.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Merging one node, existing")
  public void oneNodeExisting()
  {
    String query = "MERGE (a:cat)";
    Graph graph = catGraph;
    Table in =  unitTable;

    { Table out = aToCats;
      assertExecution( "",
          out,
          graph,
          in,
          query
        );
    }
  }

  /** Tests MERGE as CREATE with one node..
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Merging one node, absent")
  public void oneNodeAbsent()
  {
    String query = "MERGE (a:dog)";
    Graph graph = catGraph;
    Table in =  unitTable;

    {assertExecution( "createdNodes=1,addedLabels=1",
          graph,
          in,
          query
        );
    }
  }

  /** Tests MERGE as MATCH with relation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Merging one relation, existing")
  public void oneRelExisting()
  {
    String query = "MERGE ()-[:knows]->()";
    Graph graph = catGraph;
    Table in =  unitTable;

    {assertExecution( "",
          graph,
          in,
          query
        );
    }
  }
}
