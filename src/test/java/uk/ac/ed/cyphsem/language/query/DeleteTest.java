/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the behaviour of <tt>DELETE</tt> clause.
 * @see Sample#sampleTest1
 */
public class DeleteTest extends QueryTest
{

  /** Tests the DELETE clause on nodes.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, empty")
  public void deleteNode()
  {
    String query = "DELETE a";
    Graph graph = singleNodeGraph;
    Table in = new Table( new Record(a, new Value(n0)) );
    Table out = new Table(in);
    String changes = "deletedNodes=1";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests the DETACH DELETE clause on nodes.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, empty")
  public void detachDeleteNode()
  {
    String query = "DETACH DELETE a";
    Graph graph = catGraph;
    Table in = new Table( new Record(a, nadime_v) );
    Table out = new Table(in);
    String changes = "deletedNodes=1, deletedRelations=6, modifiedProperties=7, removedLabels=1";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests the DELETE clause on relation.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, empty")
  public void deleteRelation()
  {
    String query = "DELETE a";
    Graph graph = catGraph;
    Table in = new Table( new Record( a, new Value(r1) ),
                          new Record( a, new Value(r2) )  );
    Table out = new Table(in);
    String changes = "deletedRelations=2, modifiedProperties=1";
    QueryData qd = assertExecution(changes, out, graph, in,query);
  }

  /** Tests failure of deletion if one outgoing relation exists.
   * @beware See class {@link Sample}, where a lot of static fields used here are defined (e.g. @link Sample#catGraph}, {@link Sample#a}).
  */
  @Test
  @DisplayName("Creation, node, empty")
  public void deleteNodeFailure()
  {
    String query = "DELETE a";
    Graph graph = new Graph();
    Node n1 = graph.addNode();
    Node n2 = graph.addNode();
    Node n3 = graph.addNode();
    graph.addRelation(knows,n1,n2);
    graph.addRelation(knows,n3,n3);
    for (Node n: Immutable.listOf(n1,n2,n3)) {
      Table in = new Table( new Record( a, new Value(n) ) );
      assertExecutionThrows(UnspecifiedBehaviourException.class, graph, in, query);
    }
  }
}









