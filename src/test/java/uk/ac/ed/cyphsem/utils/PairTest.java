/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.language.query;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;



import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the utilitary class {@link Pair}
 */
public class PairTest
{


  /** Tests equality of Pair. */
  @Test
  @DisplayName("Pair equality")
  public void equality()
  {
    Integer r = new Integer(1);
    List<Pair<Integer,String>> pairs = new ArrayList<>();
    for (Integer x : Immutable.listOf(r, r, new Integer(1)))
      for (String y : Immutable.listOf("str","str"))
          pairs.add(Pair.of(x,y));
    for (int i = 0; i<pairs.size(); i++)
      for(int j = i; j<pairs.size(); j++) {
        assertTrue( pairs.get(i).equals(pairs.get(j)));
        assertTrue( pairs.get(j).equals(pairs.get(i)));
        assertTrue( pairs.get(i).hashCode() == pairs.get(j).hashCode());
      }
  }

  /** Tests inequality of Pair. */
  @Test
  @DisplayName("Pair inequality")
  public void inequality()
  {
    List<Pair<Integer,String>> pairs = new ArrayList<>();
    for(Integer x : Immutable.listOf(1,null,2))
      for(String y : Immutable.listOf("str","tus",null))
          pairs.add(Pair.of(x,y));

    pairs.add(
      new Pair<Integer,String>(null,null){}
    );
    pairs.add(null);
    for (int i = 0; i<pairs.size(); i++)
      for(int j = i+1; j<pairs.size(); j++)
        assertNotEquals(
          pairs.get(i),
          pairs.get(j),
          pairs.get(i)+" and "+pairs.get(j)+" are not supposed to be equal."
        );
  }

  /** Tests Pair mapping. */
  @Test
  @DisplayName("Pair inequality")
  public void map()
  {
    Pair<Integer,String> pair = Pair.of(1,"2");
    assertEquals( Pair.of(4,"2"), pair.mapFirst( i -> i+3 )  );
    assertEquals(
        Pair.of(3,"2"),
        pair.mapFirst( (i,s) -> i+Integer.parseInt(s) )
      );
    assertEquals(  Pair.of(1,"24"), pair.mapSecond( s -> s+"4" )  );
    assertEquals(  Pair.of(1,"21"), pair.mapSecond( (i,s) -> s+i )  );
  }
}
