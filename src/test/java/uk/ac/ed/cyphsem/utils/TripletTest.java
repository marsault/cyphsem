/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.utils;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Tests the utilitary class {@link Triplet}
 */
public class TripletTest
{


  /** Tests equality of Triplet. */
  @Test
  @DisplayName("Triplet equality")
  public void equality()
  {
    Integer r = new Integer(1);
    List<Integer> one = Immutable.listOf(r, r, new Integer(1));
    List<String> two = Immutable.listOf("str","str");
    List<Boolean> three = Immutable.listOf(true,true);
    List<Triplet<Integer,String,Boolean>> triplets = new ArrayList<>();
    for (Integer x : one)
      for (String y : two)
        for (Boolean z : three)
          triplets.add(Triplet.of(x,y,z));
    for (int i = 0; i<12; i++)
      for(int j = i; j<12; j++) {
        assertTrue( triplets.get(i).equals(triplets.get(j)));
        assertTrue( triplets.get(i).hashCode() == triplets.get(j).hashCode());
      }
  }

  /** Tests inequality of Triplet. */
  @Test
  @DisplayName("Triplet inequality")
  public void inequality()
  {
    LinkedList<Triplet<Integer,String,Boolean>> triplets = new LinkedList<>();
    for(Integer x : Immutable.listOf(1,null,2))
      for(String y : Immutable.listOf("str","tus",null))
        for(Boolean z : Immutable.listOf(true,false,null))
          triplets.add(Triplet.of(x,y,z));

    triplets.add(
      new Triplet<Integer,String,Boolean>(null,null,null){}
    );
    triplets.add(null);
    for (int i = 0; i<triplets.size(); i++)
      for(int j = i+1; j<triplets.size(); j++)
        assertNotEquals(
          triplets.get(i),
          triplets.get(j),
          triplets.get(i)+" and "+triplets.get(j)+" are not supposed to be equal."
        );

  }


}
