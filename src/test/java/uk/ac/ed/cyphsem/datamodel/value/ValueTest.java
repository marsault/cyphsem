/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.value;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.math.BigInteger;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/** Unit Test related to {@link Value}s, mostly testing. **/
public class ValueTest {

  /** */
  protected Graph graph = new Graph();

  /** Node used for value creation; **/
  private Node node1 = graph.addNode();

  /** Node used for value creation; **/
  private Node node2 = graph.addNode();

  /** Relation used for value creation; **/
  private Relation rel1
    = graph.addRelation(new RelationType(""), node1, node2);


  /** A list of {@link Value}s together with their {@link Kind Kind}.
    * All values are assumed to be different from each other and in increasing order. */
  public List< Pair<Value,Kind> > valueList = Arrays.asList(
      new Pair( new Value(new HashMap<>()), Kind.MAP ),
      new Pair( new Value(new HashMap<>()), Kind.MAP ),
      new Pair( new Value(new HashMap<>()), Kind.MAP ),
      new Pair( new Value(node1), Kind.NODEID ),
      new Pair( new Value(node2), Kind.NODEID ),
      new Pair( new Value(rel1), Kind.RELID ),
      new Pair( new Value(graph.addRelation(new RelationType(""),
                                            node1,
                                            node2)),
                Kind.RELID ),
      new Pair( new Value(graph.addRelation(new RelationType(""),
                                            node2,
                                            node1)),
                Kind.RELID ),
      new Pair( new Value( Arrays.asList() ), Kind.LIST ),
      new Pair( new Value( Arrays.asList(new Value(0), new Value(2))), Kind.LIST ),
      new Pair( new Value( Arrays.asList(new Value(0), new Value())), Kind.LIST ),
      new Pair( new Value( new Path (node1)), Kind.PATH ),
      new Pair( new Value( new Path (node1, rel1, node2)), Kind.PATH ),
      new Pair( new Value( new Path (node2, rel1, node1)), Kind.PATH ),
      new Pair( new Value ( "Test1" ), Kind.STRING ),
      new Pair( new Value ( "Test2" ), Kind.STRING ),
      new Pair( new Value ( "test3" ), Kind.STRING ),
      new Pair( new Value ( true ), Kind.BOOL ),
      new Pair( new Value ( new Boolean(false) ), Kind.BOOL ),
      new Pair( new Value(0) , Kind.INT),
      new Pair( new Value(1) , Kind.INT),
      new Pair( new Value(10) , Kind.INT),
      new Pair( new Value(100) , Kind.INT),
      new Pair( new Value(new BigInteger("74403ce44a13a053cf29",16)) , Kind.INT),
      new Pair( new Value(), Kind.NULL) );

  private Map<PropertyKey,Value> map1 = valueList.get(1).first.unsafeCastAsMap();
  private Map<PropertyKey,Value> map2 = valueList.get(2).first.unsafeCastAsMap();

  { //Code snippet that initializes the map values.
    map1.put(new PropertyKey("a"), new Value(1));
    map1.put(new PropertyKey("b"), new Value(node1));
    map2.put(new PropertyKey("a"), new Value(1));
    map2.put(new PropertyKey("b"), new Value(1));
  }

  /** Tests that {@link Value} construction guesses the correct {@link Kind Kind}.
  ***/
  @Test
  @DisplayName("Value Kind attribution")
  public void kindAttribution() {
    for (Pair<Value,Kind> pair : valueList)
      for (Kind k : Kind.values())
        if (k != pair.second)
          assertFalse(pair.first.isOfKind(k));
        else
          assertTrue(pair.first.isOfKind(k));
    assertThrows( AssertionFailedException.class,
                  () -> new Value( (String) null) );
  }

  /** Helper method that calls a "safe" {@link Value} casting depending on given {@link Kind Kind}.
    * Method called is of the form {@code Value#as[...]} or {@code asNullable[...]}, for instance {@link Value#asMap} or {@link Value#asNullableMap}.
    * @param v {@link Value to cast}
    * @param k {@link Kind Kind} indicating the type parameter {@code v} will be cast to.
    * @param allowNull whether to call the {@code as[...]} or {@code asNullable[...]} version of casting function.
  **/
  public void safeDynamicCast(Value v, Kind k, boolean allowNull)
  throws DynamicCastException
  {
    Object o;
    switch (k) {
      case MAP: o = allowNull ? v.asNullableMap() : v.asMap() ; break;
      case NODEID: o = allowNull ? v.asNullableNode() : v.asNode() ; break;
      case RELID:
        o = allowNull ? v.asNullableRelation() : v.asRelation() ;
        break;
      case LIST: o = allowNull ? v.asNullableList() : v.asList() ; break;
      case PATH: o = allowNull ? v.asNullablePath() : v.asPath() ; break;
      case STRING: o = allowNull ? v.asNullableString() : v.asString() ; break;
      case BOOL: o = allowNull ? v.asNullableBoolean() : v.asBoolean() ; break;
      case INT: o = allowNull ? v.asNullableInteger() : v.asInteger() ; break;
      case NULL: break;
    }
  }

  /** Helper method that calls an "unsafe" {@link Value} casting depending on given {@link Kind Kind}.
    * Method called is of the form {@code Value#unsafeCastAs[...]}, for instance {@link Value#unsafeCastAsMap}.
    * @param v {@link Value to cast}
    * @param k {@link Kind Kind} indicating the type parameter {@code v} will be cast to.
  **/
  public void unsafeDynamicCast(Value v, Kind k)
  throws DynamicCastException
  {
    Object o;
    switch (k) {
      case MAP: o = v.unsafeCastAsMap() ; break;
      case NODEID: o = v.unsafeCastAsNode() ; break;
      case RELID: o = v.unsafeCastAsRelation(); break;
      case LIST: o = v.unsafeCastAsList(); break;
      case PATH: o = v.unsafeCastAsPath(); break;
      case STRING: o = v.unsafeCastAsString(); break;
      case BOOL: o = v.unsafeCastAsBoolean(); break;
      case INT: o = v.unsafeCastAsInteger(); break;
      case NULL: break;
    }
  }

  /** Tests that {@link Value} safe casting is correct.
  ***/
  @Test
  @DisplayName("Value safe casting")
  public void casting()
  {
    for (Pair<Value,Kind> pair : valueList) {
      Value v = pair.first;
      if (pair.second != Kind.NULL) // Considered Value is not of Kind NULL
        for (Kind k : Kind.values()) {
          if (k != pair.second) {
            if (k != Kind.NULL) {
              assertThrows( DynamicCastException.class,
                            ()->safeDynamicCast(v,k,true) );
              assertThrows( DynamicCastException.class,
                            ()->safeDynamicCast(v,k,false) );
              assertThrows( ClassCastException.class,
                            ()->unsafeDynamicCast(v,k) );
            }
          }
          else {
            safeDynamicCast(v,k,true);
            safeDynamicCast(v,k,false);
            unsafeDynamicCast(v,k);
          }
        }
      else { // Considered Value is of Kind NULL
        for (Kind k : Kind.values())
          if (k != pair.second) {
            assertThrows( DynamicCastException.class,
              ()->safeDynamicCast(v,k,false));
            safeDynamicCast(v,k,true);
            unsafeDynamicCast(v,k);
          }
      }
    }
  }

  /** Tests {@link Value} equality and inequality. */
  @Test
  @DisplayName("Value equality/inequality")
  public void inequality()
  {
    for (Pair<Value,Kind> pair1 : valueList)
      for (Pair<Value,Kind> pair2 : valueList) {
        Value v1 = pair1.first;
        Value v2 = pair2.first;
        if (v1 == v2)
          assertTrue( v1.equals(v2) );
        else
          assertFalse( v1.equals(v2) );
      }

    for (Pair<Value,Kind> pair : valueList) {
      Value v1 = pair.first;
      Value v2 = Values.of(v1.content());
      assertTrue(v1.equals(v2));
    }
  }

  /** Returns the sign (-1, 0 or 1) of parameter */
  public int sign(int i) {
    if (i > 0)
      return 1;
    if (i < 0 )
      return -1;
    return 0;
  }

  /** Tests {@link Value} order (for ORDER BY subclauses). */
  @Test
  @DisplayName("Value ordering")
  public void order () {
    for (int i = 0; i<valueList.size(); i++)
      for (int j = 0; j<valueList.size(); j++) {
        int k = ( valueList.get(i).first ).compareTo( valueList.get(j).first );
        assertEquals( sign(i-j),
                      sign(k),
                      "The order of objects "
                        + valueList.get(i).first + " and "
                        + valueList.get(j).first + " is wrong. ");
      }
  }

  /** Tests {@link Value} storability. */
  @Test
  @DisplayName("Value storability")
  public void storable () {
    for (Value v : Arrays.asList(
                      new Value(),
                      new Value(1),
                      new Value(true),
                      new Value(false),
                      new Value("Hello World!"),
                      new Value(Arrays.asList(new Value(1),new Value(true))),
                      new Value(Arrays.asList(
                        new Value(Arrays.asList(
                          new Value(Arrays.asList()),
                          new Value(1)                )),
                        new Value(true)
                      )),
                      new Value(map2)
                    )
        )
    {
      assertTrue(v.isStorable(), "Value " + v + "is expected to be storable.");
      Values.assertStorable(v);
    }

    for (Value v : Arrays.asList(
                      new Value(node1),
                      new Value(rel1),
                      new Value(new Path()),
                      new Value(new Path(node1)),
                      new Value(Arrays.asList(new Value(new Path()))),
                      new Value(Arrays.asList(new Value(1), new Value(node1))),
                      new Value(Arrays.asList(
                          new Value(Arrays.asList(new Value(rel1))),
                          new Value(""))),
                      new Value(map1)
                    )
        )
    {
      assertFalse(v.isStorable(), "Value " + v + "is expected *not* to be storable.");
      assertThrows( DynamicCastException.class, ()->Values.assertStorable(v) );
    }

  }





}
