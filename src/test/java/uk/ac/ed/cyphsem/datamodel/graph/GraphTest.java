/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.datamodel.graph;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit tests for graph creation, modification and access.
 */
public class GraphTest
{

  /** Tests creations of graphs : nodes and relation addition/removal.
    */
  @Test
  @DisplayName("Graph creation : nodes/relations addition/removal.")
  public void creation()
  {
    final Graph g = new Graph();
    assertEquals(g.nodes(), new HashSet());
    assertEquals(g.relations(), new HashSet());
    Node n = null;
    List<Node> nodes= new ArrayList<Node>(20);
    for (int i = 1; i <= 20; i++) {
      if (i%2 == 0)
        n = g.addNode();
      else {
        n = g.makeNode(new ArrayList<>(),new HashMap<>());
        g.addNode(n);
      }
      assertTrue(g.nodes().contains(n));
      assertEquals(g.nodes().size(),i);
      g.assertExists(n);
      nodes.add(n);
    }
    assertTrue(g.nodes().containsAll(nodes));
    assertTrue(g.remove(n));
    assertFalse(g.remove(n));
    assertFalse(g.nodes.contains(n));
    final Node m = n;
    assertThrows(GraphException.class,() -> g.assertExists(m));


    List<Relation> relations= new ArrayList<>(100);
    Relation r = null;
    RelationType t = new RelationType("dummy");
    for (int j = 1; j <= 100; j++) {
      final Node source = nodes.get((j*13+3)%19);
      final Node target = nodes.get((j*29+7)%19);
      if (j%2 == 9)
        r = g.addRelation(t, source, target);
      else {
        r = g.makeRelation(t, source, target, new HashMap<>());
        assertFalse(g.relations().contains(r));
        g.addRelation(r);
      }
      assertTrue(g.relations().contains(r));
      assertEquals(g.relations().size(), j);
      final Relation p = r;
      assertThrows(GraphException.class,()->g.remove(source));
      assertThrows(GraphException.class,()->g.remove(target));
      g.assertExists(r);
      relations.add(r);
      if (source == target)
        assertTrue(source.loop.contains(r));
      else {
        assertTrue(source.out.contains(r));
        assertTrue(target.in.contains(r));
      }
    }
    assertTrue(g.relations().containsAll(relations));
    assertTrue(g.remove(r));
    assertFalse(g.remove(r));
    final Relation p = r;
    assertThrows(GraphException.class,() -> g.assertExists(p));
  }

  /** Tests the graph copy on random Graphs. **/
  @Test
  @DisplayName("Random graph copy")
  public void copy()
  {
    for (int i = 0; i<20; i++) {
      Graph graph = (new Random()).randomGraph();
      Triplet<Graph,Map<Node,Node>,Map<Relation,Relation>> triplet = graph.copy();
      Map<Node,Node> n_map = triplet.second;
      Map<Relation,Relation> r_map = triplet.third;
      Graph copy = triplet.first;

      assertEquals(graph.nodes(), copy.nodes());
      assertEquals(graph.nodes(), n_map.keySet());
      assertEquals(new HashSet(n_map.values()), copy.nodes());

      assertEquals(graph.relations(), copy.relations());
      assertEquals(graph.relations(), r_map.keySet());
      assertEquals(new HashSet(r_map.values()), copy.relations());

      for (Node n : graph.nodes()) {
        Node copy_of_n = n_map.get(n);
        assertEquals(n,copy_of_n); // this tests only that both Node's wraps the same id.
        assertEquals(n.labels(), copy_of_n.labels());
        assertEquals(n.propertyMap(), copy_of_n.propertyMap());
        assertEquals(n.out, copy_of_n.out);
        assertEquals(n.loop, copy_of_n.loop);
        assertEquals(n.in, copy_of_n.in);
      }

      for (Relation r : graph.relations()) {
        Relation copy_of_r = r_map.get(r);
        assertEquals(r, copy_of_r); // idem.
        assertEquals(r.type(), copy_of_r.type());
        assertEquals(r.propertyMap(), copy_of_r.propertyMap());
        assertEquals(r.source(), copy_of_r.source());
        assertEquals(r.target(), copy_of_r.target());
      }
    }
  }

  /** Tests label manipulation. */
  @Test
  @DisplayName("Label Manipulation")
  public void label() {
    int id = (new java.util.Random()).nextInt();
    final Node n = new Node(id);
    assertTrue(n.labels().isEmpty());

    Label l1 = new Label("abc");
    Label l2 = new Label("def");
    Label l3 = new Label("ghi");
    Label l4 = new Label("klm");
    Label l5 = new Label("nop");
    Label l6 = new Label("qrs");

    for (Label l : Arrays.asList(l1,l2,l3)) {
      assertFalse(n.labels.contains(l));
      n.addLabel(l);
      assertTrue(n.labels.contains(l));
    }

    assertTrue(n.removeLabel(l1));  //Effective removal returns true
    assertEquals(n.labels(), new HashSet<>(Arrays.asList(l2,l3)));
    assertFalse(n.removeLabel(l1)); //second removal does nothing and returns false
    for (Label l : Arrays.asList(l1,l3,l5))
      n.addLabel(l);
    assertEquals(n.labels(), new HashSet<>(Arrays.asList(l1,l2,l3,l5)));

    n.addLabels(Arrays.asList(l6,l5,l4,l3));
    assertEquals(n.labels(), new HashSet<>(Arrays.asList(l1,l2,l3,l4,l5,l6)));
  }

  /** Tests property maps manipulation. */
  @Test
  @DisplayName("Label Manipulation")
  public void properties() {
    final Id id = new Id(new java.util.Random().nextInt());
    assertTrue(id.propertyMap().isEmpty());
    assertTrue(id.propertyKeySet().isEmpty());

    PropertyKey k1 = new PropertyKey("abc");
    PropertyKey k2 = new PropertyKey("def");
    PropertyKey k3 = new PropertyKey("ghi");
    PropertyKey k4 = new PropertyKey("jkl");
    Value v1 = new Value("abc");
    Value v2 = new Value("def");
    Value v3 = new Value("ghi");
    Value v4 = new Value();

    assertTrue( id.setPropertyIfUnset(k1,v1) );
    assertFalse( id.setPropertyIfUnset(k1,v1) );
    assertEquals( id.getPropertyValue(k1), v1 );
    assertTrue( id.setPropertyIfUnset(k2,v2) );
    assertFalse( id.setPropertyIfUnset(k2,v2) );
    assertEquals( id.getPropertyValue(k2), v2 );

    id.overwriteProperty(k1, new Value());
    assertFalse(id.propertyKeySet().contains(k1));
    assertTrue( id.removeProperty(k2) );
    assertFalse( id.removeProperty(k2) );
    assertFalse( id.propertyKeySet().contains(k2) );

    Map<PropertyKey,Value> map = new HashMap<>();
    map.put(k1,v1);
    map.put(k2,v2);
    map.put(k4,v4);
    final Node n
        = new Node (new java.util.Random().nextInt(), new ArrayList<>(), map);
    final Relation r = new Relation (new java.util.Random().nextInt(), new RelationType("xyz"), n, n, map);
    assertEquals(n.propertyKeySet().size(),2); // NULL Value are not put in a property map.
    assertEquals(r.propertyKeySet().size(),2);
    map.put(k3,v3);
    assertEquals(n.propertyKeySet().size(),2); // Maps are copied when passed on to nodes/properties.
    assertEquals(r.propertyKeySet().size(),2);

    assertEquals(r.getPropertyValue(k1), v1);
    assertEquals(n.getPropertyValue(k2), v2);
    assertEquals(r.getPropertyValue(k4), v4);
    assertEquals(n.getPropertyValue(k4), v4);
    assertEquals(n.getPropertyValue(k3), v4); // k3 is not in the map, hence
    assertEquals(r.getPropertyValue(k3), v4); // these should return a NULL Value
  }

}
