/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit tests for {@link Table}s.
 */
public class TableTest
{

  /** Sample record in {@link Table} {@link #table}. **/
  Record r1 = new Record();

  /** Sample record in {@link Table} {@link #table}. **/
  Record r2 = new Record();

  /** Sample record not in {@link Table} {@link #table}. **/
  Record r3 = new Record();

  /** Sample record inuniform with {@link Table} {@link #table}. **/
  Record r4 = new Record();

  /** Sample name, part of domain of Table {@link #table}. **/
  Name name1 = new Name("a");

  /** Sample name, part of domain of Table {@link #table}. **/
  Name name2 = new Name("b");

  /** Sample name, not part of domain of Table {@link #table}. **/
  Name name3 = new Name("c");


  TableTest () {
    r1.bind(name1, new Value(0));
    r1.bind(name2, new Value(true));

    r2.bind(name1, new Value(1));
    r2.bind(name2, new Value(false));

    r3.bind(name1, new Value(2));
    r3.bind(name2, new Value("a"));

    r4.bind(name1, new Value(2));
    r4.bind(name2, new Value("a"));
    r4.bind(name3, new Value(true));
  }

    @Test
  @DisplayName("Table, add")
  void add()
  {
    Table t1 = new Table();
    t1.add(r1);
    assertTrue(
      Utils.equalAsMultiset(
          t1,
          Immutable.listOf(r1)),
          "The content of Table " + t1 + " is supposed to be the same as "
          + Immutable.listOf(r1)+"."
        );
    t1.add(r2);
    t1.add(r2);
    t1.add(r3);
    t1.add(r3);
    t1.add(r3);
    assertTrue( Utils.equalAsMultiset(t1,Immutable.listOf(r1, r2, r2, r3, r3, r3)) );
    assertThrows( IllegalArgumentException.class, ()->t1.add(r4) );
    //assertFalse(t1.add(r3,0));
  }

  @Test
  @DisplayName("Table, addAll")
  void addAll()
  {
    { Table t1 = new Table();
      t1.addAll(Immutable.listOf(r1, r2, r2));
      assertTrue( Utils.equalAsMultiset(t1 ,Immutable.listOf(r1, r2, r2)) );
      assertThrows( IllegalArgumentException.class, ()->t1.addAll(Immutable.listOf(r1, r3, r4) ));
    }
    { Table t2 = new Table();
      t2.addAll(Immutable.listOf(r1, r2, r2));
      Table s1 = new Table();
      s1.addAll(Immutable.listOf(r3,r3,r2));
      t2.addAll(s1);
      assertTrue( Utils.equalAsMultiset(t2 ,Immutable.listOf(r1, r2, r2, r2, r3, r3)) );
      Table s2 = new Table();
      s2.add(r4);
      assertThrows( IllegalArgumentException.class, ()->t2.addAll(s2) );
    }
    { Table t3 = new Table();
      t3.addAll(Immutable.listOf(r1, r2, r2));
      t3.addAll(Immutable.listOf(r2, r3), 2);
      assertTrue( Utils.equalAsMultiset(t3 , Immutable.listOf(r1, r2, r2, r2, r2, r3, r3)) );
      assertThrows( IllegalArgumentException.class, ()->t3.addAll(Immutable.listOf(r2, r4), 2) );
    }
    { Table t4 = new Table();
      t4.addAll(Immutable.listOf(r1, r2, r2));
      { Table s3 = new Table();
        s3.addAll(Immutable.listOf(r2, r3));
        t4.addAll(s3,2);
        assertTrue( Utils.equalAsMultiset(t4 , Immutable.listOf(r1, r2, r2, r2, r2, r3, r3)) );
      }
      { Table s4 = new Table();
        s4.add(r4); s4.add(r4);
        assertThrows( IllegalArgumentException.class, ()->t4.addAll(s4, 12) );
      }
    }
  }


  @Test
  @DisplayName("Table, domain")
  void domain() {
    Table t = new Table();
    t.add(r1);
    t.add(r2);
    assertEquals(Immutable.setOf(name1,name2),t.domain());
  }


  @Test
  @DisplayName("Table, domain and uniformity")
  void constructors()
  {
    Table t1 = new Table(Immutable.listOf(r1,r2));
    assertTrue( Utils.equalAsMultiset(t1, Immutable.listOf(r1,r2)) );

    Table t2 = new Table(Immutable.setOf(name1,name2));
    assertEquals(Immutable.setOf(name1,name2), t2.domain());
    assertThrows(IllegalArgumentException.class, ()->t2.add(r4));
    assertTrue( Utils.equalAsMultiset(t2, Immutable.emptyList()) );

    Table t3 = new Table(r1);
    assertTrue( Utils.equalAsMultiset(t3, Immutable.listOf(r1)) );

    Table t4 = new Table(r1,2);
    assertTrue( Utils.equalAsMultiset(t4, Immutable.listOf(r1,r1)) );
  }


  @Test
  @DisplayName("Table, multiplicity")
  void multiplicity() {
    Table t1 = new Table(Immutable.listOf(r1,r2,r2));
    assertEquals(1,t1.multiplicityOf(r1).intValue());
    assertEquals(2,t1.multiplicityOf(r2).intValue());
    assertEquals(0,t1.multiplicityOf(r3).intValue());
  }

  @Test
  @DisplayName("Table, size")
  void size() {
    Table t1 = new Table();
    assertEquals(0, t1.size());
    Table t2 = new Table(Immutable.listOf(r1,r2,r3));
    assertEquals(3, t2.size());
    Table t3 = new Table(Immutable.listOf(r1,r2,r2,r3,r3,r3));
    assertEquals(6, t3.size());


    t3.add(r1);
    assertEquals(7, t3.size());

    t3.add(r3, 13);
    assertEquals(20, t3.size());

    t3.addAll(t2);
    assertEquals(23, t3.size());

    t3.addAll(t2, 2);
    assertEquals(29, t3.size());

    t3.addAll(Immutable.listOf(r1,r2));
    assertEquals(31, t3.size());

    t3.addAll(Immutable.listOf(r1), 2);
    assertEquals(33, t3.size());
  }


  @Test
  @DisplayName("Table, unique")
  void unique() {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2, r3, r3, r3) );
    t1.unique();
    assertTrue(
        Utils.equalAsMultiset(t1, Immutable.listOf(r1, r2 ,r3))
      );
  }

  @Test
  @DisplayName("Table, remove")
  void remove() {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2, r3, r3, r3) );
    assertTrue(t1.remove(r1));
    assertTrue(
        Utils.equalAsMultiset(t1, Immutable.listOf(r2, r2, r3 , r3, r3))
      );
    assertTrue(t1.remove(r2));
    assertTrue(
        Utils.equalAsMultiset(t1, Immutable.listOf( r2, r3 , r3, r3))
      );
    assertEquals(2,t1.remove(r3,2));
    assertTrue(
        Utils.equalAsMultiset(t1, Immutable.listOf(r2, r3))
      );

    assertThrows(NullPointerException.class, ()->t1.remove(null));
    assertThrows(IllegalArgumentException.class, ()-> t1.remove("s"));
    assertEquals(1, t1.remove(r2,2));
    assertEquals(false, t1.remove(r2));
  }

  @Test
  @DisplayName("Table, equals")
  void equals() {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2) );
    Table t2 = new Table( Immutable.listOf(r1, r2, r2) );
    assertEquals(t1,t2);
    assertEquals(t1.hashCode(), t2.hashCode());

    Table t3 = new Table( Immutable.listOf(r2, r2) );
    assertFalse(t1.equals(t3));
    Table t4 = new Table( Immutable.listOf(r1, r2) );
    assertFalse(t1.equals(t4));
    Table t5 = new Table( Immutable.listOf(r1, r2, r2, r3) );
    assertFalse(t1.equals(t5));
    Table t6 = new Table( r4 );
    assertFalse(t1.equals(t6));

    assertFalse(t1.equals(null));
    assertFalse(t1.equals(r1));
    assertEquals(new Table(Immutable.setOf(name1)), new Table());
    assertFalse(t1.equals(new Table()));
    assertFalse((new Table()).equals(t1));
  }

  @Test
  @DisplayName("Table, copy")
  void copy() {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2) );
    Table t2 = new Table( t1 );
    assertNotSame(t1, t2);
    assertEquals(t1, t2);
  }

  @Test
  @DisplayName("Table, hasName")
  void hasName() {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2) );
    assertTrue(t1.hasName(name1));
    assertTrue(t1.hasName(name2));
    assertFalse(t1.hasName(name3));
  }


  @Test
  @DisplayName("Table, anyOne")
  void anyOne() {
    Table t1 = new Table( Immutable.listOf(r1) );
    assertEquals(r1, t1.anyOne());

    Table t2 = new Table( Immutable.listOf(r2,r2) );
    assertEquals(r2, t2.anyOne());

    Table t3 = new Table( Immutable.listOf() );
    assertThrows(NoSuchElementException.class, ()->t3.anyOne() );
  }


  @Test
  @DisplayName("Table, entries")
  void entries() {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2, r3, r3, r3) );
    assertTrue (
        Utils.equalAsMultiset(
          t1.entries(),
          Immutable.listOf( new Pair<Record,Integer>(r1,1),
                            new Pair<Record,Integer>(r2,2),
                            new Pair<Record,Integer>(r3,3))
        )
      );
  }


  @Test
  @DisplayName("Table, recordSet")
  void recordSet()
  {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2, r3, r3, r3) );
    assertEquals(t1.recordSet(), Immutable.setOf(r1,r2,r3));
  }


  @Test
  @DisplayName("Table, diff")
  void diff()
  {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2, r3, r3, r3) );
    Table t1_copy = new Table(t1);

    Table t2 = new Table( Immutable.listOf(r1, r3, r3) );
    Table t2_copy = new Table(t2);

    Map<Record,Integer> diff1 = Tables.diff(t1,t2);
    assertEquals( Immutable.mapOf(r2,2,r3,1), diff1 );

    Map<Record,Integer> diff2 = Tables.diff(t2,t1);
    assertEquals( Immutable.mapOf(r2,-2,r3,-1), diff2 );

    Table t3 = new Table( Immutable.listOf(r4,r4) );
    assertThrows( IllegalArgumentException.class, ()->Tables.diff(t1,t3) );

    assertEquals( Immutable.mapOf(r1,1,r2,2,r3,3), Tables.diff(t1,new Table()) );
    assertEquals( Immutable.mapOf(r1,-1,r3,-2), Tables.diff(new Table(),t2) );

    assertEquals(t2_copy, t2);
    assertEquals(t1_copy, t1);
  }

  @Test
  @DisplayName("Table, put")
  void put()
  {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2) );
    t1.put(r3,3);
    assertTrue (
        Utils.equalAsMultiset(
          t1,
          Immutable.listOf( r1, r2, r2, r3, r3, r3 )
        )
      );
    t1.put(r2,1);
    assertTrue (
        Utils.equalAsMultiset(
          t1,
          Immutable.listOf( r1, r2, r3, r3, r3 )
        )
      );
    assertThrows(IllegalArgumentException.class, ()-> t1.put(r1,-1));
  }

  @Test
  @DisplayName("Table, contains")
  void contains()
  {
    Table t1 = new Table( Immutable.listOf(r1, r2, r2) );
    assertTrue(t1.contains(r1));
    assertTrue(t1.contains(r2));

    assertThrows(NullPointerException.class, ()->t1.contains(null));
    assertThrows(IllegalArgumentException.class, ()->t1.contains("s"));
  }
}
