/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.table;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.language.expression.*;
import uk.ac.ed.cyphsem.datamodel.value.Value.Kind;
import uk.ac.ed.cyphsem.datamodel.graph.*;
import uk.ac.ed.cyphsem.datamodel.value.*;
import uk.ac.ed.cyphsem.outbridge.*;
import uk.ac.ed.cyphsem.language.parser.*;
import uk.ac.ed.cyphsem.language.pattern.*;
import uk.ac.ed.cyphsem.language.query.*;
import uk.ac.ed.cyphsem.language.query.items.*;
import uk.ac.ed.cyphsem.table.*;
import uk.ac.ed.cyphsem.utils.*;
import static uk.ac.ed.cyphsem.cli.Sample.*;

import uk.ac.ed.cyphsem.Utils;

import java.lang.Iterable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.BiFunction;

import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit tests for {@link Record}s.
 */
public class RecordTest
{

  /** {@link Name}s used in this test. */
  List<Name> names = Arrays.asList( new Name("a"),
                                    new Name("b"),
                                    new Name("c"),
                                    new Name("d")  );

  @Test
  void emptyRecord() {
    Record r = new Record();
    for (Name name : names) {
      assertFalse( r.isBound(name) );
      assertThrows( UndefinedNameException.class, ()->r.get(name) );
    }
    assertEquals(r.domain(), Immutable.emptySet());
    assertEquals(r.bindings(), Immutable.emptySet());
  }

  @Test
  @DisplayName("Record, simple binding")
  void binding() {
    Record r = new Record();

    Name name1 = names.get(0);
    Value value1 = new Value(1);
    r.bind(name1, value1);

    Name name2 = names.get(1);
    Value value2 = new Value("a");
    r.bind(name2, value2);

    Name name3 = names.get(2);
    Value value3 = new Value();
    r.bind(name3, value3);

    for (Name name : Arrays.asList(name1, name2, name3) )
      assertTrue(r.isBound(name));

    assertEquals( r.get(name1), value1 );
    assertEquals( r.get(name2), value2 );
    assertEquals( r.get(name3), value3 );


    assertEquals(r.bindings(), Immutable.mapOf( name1, value1,
                                                name2, value2,
                                                name3, value3 ).entrySet());



    // null parameters
    assertThrows( NullPointerException.class, ()->r.bind(null,value1) );
    assertThrows( NullPointerException.class, ()->r.bind(name1,null) );
  }

  @Test
  @DisplayName("Record, domain and uniformity")
  void uniformity()
  {
    Name name1 = names.get(0);
    Name name2 = names.get(1);
    Name name3 = names.get(2);
    Value value = new Value();

    Record r1 = new Record();
    r1.bind(name1, value);
    assertEquals(Immutable.setOf(name1), r1.domain());
    assertTrue(r1.isUniformWith(Immutable.setOf(name1)));

    Record r2 = new Record();
    r2.bind(name1, value);
    r2.bind(name2, value);
    assertEquals(Immutable.setOf(name1,name2), r2.domain());
    assertTrue(r2.isUniformWith(Immutable.setOf(name1,name2)));

    Record r3 = new Record();
    r3.bind(name1, value);
    r3.bind(name2, value);
    r3.bind(name3, value);
    assertEquals(Immutable.setOf(name1,name2,name3), r3.domain());
    assertTrue(r3.isUniformWith(Immutable.setOf(name1,name2,name3)));

    List<Record> list = Immutable.listOf(r1,r2,r3);
    for (Record one : list)
      for (Record two : list)
        if ( one != two ) {
          assertFalse(one.isUniformWith(two));
          assertFalse(one.equals(two));
        }

    Record r4 = new Record();
    r4.bind(name1, value);
    r4.bind(name2, value);
    r4.bind(name3, value);
    assertTrue(r3.isUniformWith(r4));
  }

  @Test
  @DisplayName("Record, copy")
  void copy() {
    Name name1 = names.get(0);
    Name name2 = names.get(1);
    Name name3 = names.get(2);
    Value value1 = new Value(1);
    Value value2 = new Value("a");
    Value value3 = new Value(true);

    Record r1 = new Record();
    r1.bind(name1, value1);
    r1.bind(name2, value2);

    Record r2 = new Record(r1);
    assertNotSame(r1,r2);
    assertEquals(r1,r2);
    assertEquals(value1, r2.get(name1));
    assertEquals(value2, r2.get(name2));

    r1.bind(name3, value3);
    assertFalse(r1.equals(r2));
    assertThrows(UndefinedNameException.class, () -> r2.get(name3));
  }

  @Test
  @DisplayName("Record, filling unbound")
  void fillingWithNull() {
    Name name1 = names.get(0);
    Value value1 = new Value(1);
    Record r = new Record();
    r.bind(name1, value1);
    r.bindAllUnboundToNull(names);
    assertEquals(r.domain(), new HashSet<Name>(names));
    assertTrue(r.isUniformWith(new HashSet<Name>(names)));
    for (Name name : names)
      if (name != name1)
        assertEquals(new Value(), r.get(name));
      else
        assertEquals(value1, r.get(name));
  }

  @Test
  @DisplayName("Record, filling unbound")
  void equality() {
    Name name1 = names.get(0);
    Name name2 = names.get(1);
    Name name3 = names.get(2);
    Value value1 = new Value();
    Value value2 = new Value(1);
    Value value3 = new Value("a");

    Record r = new Record();
    r.bind(name1, value1);
    r.bind(name2, value2);

    assertFalse(r.equals(null));
    assertFalse(r.equals(names));

    {
      Record equal_to_r_1 = new Record();
      equal_to_r_1.bind(name1, value1);
      equal_to_r_1.bind(name2, value2);
      assertTrue(r.equals(equal_to_r_1));
      assertTrue(equal_to_r_1.equals(r));
      assertTrue( r.hashCode() == equal_to_r_1.hashCode() );
    }
    {
      Record equal_to_r_2 = new Record(r);
      assertTrue(r.equals(equal_to_r_2));
      assertTrue(equal_to_r_2.equals(r));
      assertTrue( r.hashCode() == equal_to_r_2.hashCode() );
    }
    {
      Record inequal_to_r_due_to_domain = new Record();
      assertFalse( r.equals(inequal_to_r_due_to_domain) );
      assertFalse( inequal_to_r_due_to_domain.equals(r) );

      inequal_to_r_due_to_domain.bind(name1, value1);
      assertFalse( r.equals(inequal_to_r_due_to_domain) );
      assertFalse( inequal_to_r_due_to_domain.equals(r) );

      inequal_to_r_due_to_domain.bind(name2, value2);
      inequal_to_r_due_to_domain.bind(name3, value3);
      assertFalse( r.equals(inequal_to_r_due_to_domain) );
      assertFalse( inequal_to_r_due_to_domain.equals(r) );
    }
    {
      Record inequal_to_r_due_to_value = new Record();
      inequal_to_r_due_to_value.bind(name1, value1);
      inequal_to_r_due_to_value.bind(name2, value3);
      assertFalse( r.equals(inequal_to_r_due_to_value) );
      assertFalse( inequal_to_r_due_to_value.equals(r) );
    }


  }

  @Test
  @DisplayName("Record, unbinding")
  void unbinding() {
    Record r = new Record();
    Name name = names.get(0);
    Value value = new Value(1);
    r.bind(name,value);
    assertTrue(r.unbind(name));

    for (Name n : names) {
      assertFalse(r.isBound(n));
      assertFalse(r.unbind(n));
      assertThrows( UndefinedNameException.class, ()->r.get(n) );
    }

    // null parameter
    assertThrows( NullPointerException.class, ()->r.unbind(null) );
  }

  @Test
  @DisplayName("Record, rebinding")
  void rebinding() {
    Record r = new Record();
    r.bind(names.get(0), new Value(1));
    r.bind(names.get(0), new Value(1)); //silently ignored
    assertThrows( RecordInconsistencyException.class,
                  () -> r.bind(names.get(0), new Value(2)) );
  }

  @Test
  @DisplayName("Record, overbinding")
  void overbinding() {
    Record r = new Record();

    Name name1 = names.get(0);
    Value value1 = new Value(1);
    Value value2 = new Value("a");

    r.bind(name1, value1);
    r.overbind(name1, value2);
    assertEquals( r.get(name1), value2 );

    // null parameters
    assertThrows( NullPointerException.class, ()->r.overbind(null, value1) );
    assertThrows( NullPointerException.class, ()->r.overbind(name1, null) );
  }


  @Test
  @DisplayName("Record, ensureBinding")
  void ensuringBinding() {
    Name name1 = names.get(0);
    Name name2 = names.get(1);
    Name name3 = names.get(2);
    Value value1 = new Value();
    Value value2 = new Value(1);
    Value value3 = new Value("a");

    Record r1 = new Record();
    r1.bind(name1, value1);
    assertSame(r1, Record.ensureBinding(r1, name1, value1));
    assertThrows( RecordInconsistencyException.class,
                  () -> Record.ensureBinding(r1, name1, value2));

    Record copy_of_r1 = new Record(r1);
    Record r2 = Record.ensureBinding(r1, name2, value2);
    assertNotSame(r1, r2);
    assertEquals(copy_of_r1, r1);

    assertEquals(Immutable.setOf(name1,name2), r2.domain());
    assertEquals(value1, r2.get(name1));
    assertEquals(value2, r2.get(name2));
  }

}
