/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.outbridges.*;
import uk.ac.ed.cyphsem.outbridges.InterpreterComparator.*;
import uk.ac.ed.cyphsem.query.*;
import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;


import java.io.PrintWriter;


public class ComparisonTest {

  public static void main(String[] args) throws Exception {

    Random r = new Random();
    Graph g = r.randomGraph();
    PrintWriter gw = new PrintWriter("graph.log", "UTF-8");
    gw.println(g.toCypher());
    gw.flush();
//     System.out.println(g.nodes.toString());

    InterpreterComparator comparator
      = new InterpreterComparator(g, 1000,
            new InterpreterFactory() {
              @Override
              public Interpreter make (Graph g, boolean graphLoaded) {
                if (graphLoaded)
                  return new NeoComInterpreter(g);
                else
                  return new NeoComInterpreter(g.toCypher());
              }
            }
            ,
            new InterpreterFactory() {
              public @Override
              Interpreter make (Graph g, boolean graphAssumedLoaded) {
                return new CyphSemInterpreter(g);
              }
            }
      );

    PrintWriter writer = new PrintWriter("extras.log", "UTF-8");
//     writer.println("The second line");
//     writer.close();

//     System.out.println("\u250F\u2501\u2501\u2501 NEOCOM class (for fail, for timeout) ");
//     System.out.println("\u2503\u250F\u2501\u2501\u2501 CYPHSEM result class (for fail, for timeout)");
// //     System.out.println("\u2503\u2503\u250F\u2501\u2501\u2501 If Neither fails, are result equal ?");
//     System.out.println("\u2503\u2503\u2503 \n");

    comparator.printHeader(System.out);
    String indent = "";
    for (int o = 0; o <= comparator.size(); o++)
      indent += " ";
    for (int i = 0 ; i< 20000 ; i++) {
      System.out.println(i);
      Query q = r.randomQueryBis();
      String queryString = q.toString();


      writer.println("======================================================================");
      writer.println(q);
      writer.println("----------------------------------------------------------------------");
      writer.flush();
      Comparison c = comparator.compare(queryString);
      int o = 0;
      for ( Table t : c.tables ) {
        writer.print("Class n°"+(o++)+": { ");
        int max = 42;
        String sep = "";
        for(Record record: t.recordSet()) {
          if (--max == 0) {
            writer.print(" ... ");
            break;
          }
          writer.print(sep+record+"="+t.multiplicityOf(record));
          sep="  ";
        }
        writer.println(" }");
      }
      writer.flush();
      String str = "";

      for (int j = 0; j<comparator.size(); j++) {
        if(c.hasTimedOut(j))
          str += "\u231B";
        else if (c.hasFailedWithException(j)) {
          writer.println("Interpreter n°"+j+" failed with Exception: "+c.getException(j));
          writer.flush();
          str += "\u2718";
        }
        else
          str+= String.valueOf(c.behavioralClasses.get(j));
      }

    queryString = queryString.replace("\n","\n"+indent);
    System.out.println(str+" "+queryString);
    writer.println("----------------------------------------------------------------------");
    writer.println("Summary: "+str);

//       (neo_result.equals(cyphsem_result)?"=":"\u2260"
//
//
//       "\uD83D""\uDD50"
// //       boolean except = false;
// //       Table neo_result = null;
// //       Table cyphsem_result = null;
// //       try {
// //         neo_result = neocom.run(stt);
//         str += "\u00B7";
//       } catch (Exception e) {
//         except = true;
//         str += "\u2718";
//       }
//       try {
//         cyphsem_result = uk.ac.ed.cyphsem.run(stt);
//         str += "\u00B7";
//       } catch (Exception e) {
//         except =  true;
//         str += "\u2718";
//       }
//
//       if (!except)
//         str +=  (neo_result.equals(cyphsem_result)?"=":"\u2260");
//       else
//         str += "\u00B7";
//       System.out.println(str+"  "+stt);
    }


  }
}
