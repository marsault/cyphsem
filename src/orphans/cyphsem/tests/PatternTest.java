/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.*;

import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.expression.SubListExpression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.pattern.Iteration.*;
import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.query.*;
import uk.ac.ed.cyphsem.query.items.*;
import uk.ac.ed.cyphsem.pattern.RelationPattern.Direction;

import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;


public class PatternTest {

  public static void main(String[] args) throws Exception {

    Graph g = GraphTest.sample();

    Record r = new Record();

    PropertyKey since = new PropertyKey("since");
    PropertyKey first_name = new PropertyKey("first_name");

    RelationType knows = new RelationType("knows");
    RelationType has = new RelationType("has");
    RelationType kept = new RelationType("kept");

    Label human = new Label("human");
    Label savvy = new Label("savvy");
    Label black = new Label("black");
    Label cat = new Label("cat");

    ExplicitMapExpression e = new ExplicitMapExpression(new KeyExpPair(first_name,new Value("Victor")));
    UnnamedPathPattern pat1 = new NodePattern(new Name("x"),e);

//     System.out.println("Searching for " + pat1.toString());
//     for (Node n : g.nodes) {
//       List<AssignmentResult> l = pat1.computeAssignment ( new Path(n), new HashSet<Relation>(), g, r, new HashSet<Expression>());
//       System.out.println(l);
//     }
//
    List<Label> labels = Arrays.asList(human);
    NodePattern pat2 = new NodePattern(new Name("x"),labels);
//     System.out.println("\nSearching for "+pat2.toString());
//     for (Node n : g.nodes) {
//       List<AssignmentResult> l = pat2.computeAssignment ( new Path(n), new HashSet<Relation>(), g, r, new HashSet<Expression>());
//       System.out.println(l);
//     }


    List<Label> labels2 = Arrays.asList(new Label("savvy"));
    NodePattern pat3 = new NodePattern(new Name("x"),labels2);
    System.out.println("\nSearching for "+pat3.toString());
    System.out.println(computeAssignment (pat3,g));


    NodePattern pat4 = new NodePattern(new Name("y"),labels);
    UnnamedPathPattern pat5 =
      new SimpleRelationPattern(pat2, pat4, new RelationType("knows"));
    // (x :human) -:knows- (y :human)
    System.out.println("\nSearching for "+pat5.toString());
    System.out.println(computeAssignment (pat5,g));

    UnnamedPathPattern pat6 =
      new SimpleRelationPattern(pat2, new NodePattern(new Name("y")), new Name("z"), new RelationType("knows"), new RelationType("has"));
    // (x :human) -:knows- (y :human)
    System.out.println("\nSearching for "+pat6.toString());
    System.out.println(computeAssignment(pat6,g));


    UnnamedPathPattern pat7 =
      new SimpleRelationPattern(
        new NodePattern(new Name("x")),
        new SimpleRelationPattern(new NodePattern(),
                                  new NodePattern(),
                                  Direction.FORWARD,
                                  new Name("z"),
                                  new KeyExpPair(since,
                                    new UnaryMapExpression(new Name("y"), since))),
        Direction.FORWARD,
        new Name("y"),
        new KeyExpPair(since,
                        new UnaryMapExpression(new Name("z"), since)));
    System.out.println("\nSearching for "+pat7.toString());
    System.out.println(computeAssignment(pat7,g));



  List<Iteration> its = Arrays.asList(new Between(2,3), new Star(), new Until (2), new From (2), new Fixed(1));

  for (Iteration it : its) {
    UnnamedPathPattern patx =
        new MultipleRelationPattern(
          it, new NodePattern(Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
          Direction.FORWARD,
          new Name("z"), knows, kept
        );
    Query q = new ClauseQueryBis(new MatchClause(Arrays.asList(patx)), new ReturnQueryBis());
    System.out.println("\nSearching for "+patx.toString());
//     ReadingClause.Result result =
//     Table t = result.table());
    System.out.println(q.execute(g).table());
  }

  UnnamedPathPattern pat9 =
      new MultipleRelationPattern(
        new Between(2,3), new NodePattern(Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
        Direction.FORWARD,
        new Name("z"), knows, kept
      );
  UnnamedPathPattern pat10 =
      new MultipleRelationPattern(
        new Between(3,4), new NodePattern(Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
        Direction.FORWARD,
        new Name("z"), knows, kept
      );
  Query q = new UnionQuery(
    new ClauseQueryBis(new MatchClause(Arrays.asList(pat9)), new ReturnQueryBis()),
    new ClauseQueryBis(new MatchClause(Arrays.asList(pat10)), new ReturnQueryBis()),
    true
    );
  System.out.println("\nQuerying for  "+q.toString());
//   ReadingClause.Result result = q.execute(g);
//   Table t = result.table());
  System.out.println(q.execute(g).table());


  System.out.println("\nFree variables: for  "+pat6);
  System.out.println(pat6.freeVariables());

  Query q2 = new UnionQuery(
    new ClauseQueryBis(new MatchClause(new NamedPathPattern(new Name("p"),pat9)), new ReturnQueryBis()),
    new ClauseQueryBis(new MatchClause(new NamedPathPattern(new Name("p"),pat10)), new ReturnQueryBis()),
    true
    );
  System.out.println("\nQuerying for  "+q2.toString());
  System.out.println(q2.execute(g).table());



  UnnamedPathPattern pat11 =
      new MultipleRelationPattern(
        new Between(3,4), new NodePattern(new Name("x"),Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
        Direction.FORWARD,
        new Name("z"), knows, kept
      );
  Query q3 = new ClauseQueryBis (
    new MatchClause(pat9),
    new ClauseQueryBis( new OptionalMatchClause(pat11),
                        new ReturnQueryBis() )
  );
  System.out.println("\nQuerying for  "+q3.toString());
  System.out.println(q3.execute(g).table());

  UnnamedPathPattern pat12 =
    new MultipleRelationPattern(
      new Between(1,2), new NodePattern(new Name("x"),Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
      Direction.FORWARD,
      new Name("z"), knows, kept
    );

  Query q4 = new ClauseQueryBis (
    new MatchClause(pat9),
    new ClauseQueryBis( new OptionalMatchClause(pat11,pat12),
                        new ReturnQueryBis() )
  );
  System.out.println("\nQuerying for  "+q4.toString());
  System.out.println(q4.execute(g).table());



  QueryBis q5 = new ClauseQueryBis(Arrays.asList(new MatchClause(pat9,pat12),
    new WithClause(true,Arrays.asList(new WithItem(new Name("y"), new SubListExpression(new Name("z"),Span.UNIQUE, new Value(1)))))), new ReturnQueryBis());
  System.out.println("\nQuerying for  "+q5.toString());
  System.out.println(q5.execute(g).table());

  QueryBis q6 = new ClauseQueryBis(Arrays.asList(
      new MatchClause(new MultipleRelationPattern(
          new From(0), new NodePattern(Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
          Direction.FORWARD,
          new Name("z"), knows, kept
        )),
      new  UnwindClause(new Name("z"), new Name("a")),
      new  UnwindClause(new Name("a"), new Name("b"))),
    new ReturnQueryBis());
  System.out.println("\nQuerying for  "+q6.toString());
  System.out.println(q6.execute(g).table());

  QueryBis q7 = new ClauseQueryBis(Arrays.asList(
    new MatchClause(new MultipleRelationPattern(
          new From(0), new NodePattern(Arrays.asList(human)), new NodePattern(Arrays.asList(savvy)),
          Direction.FORWARD,
          new Name("z"), knows, kept
        ))), new ReturnQueryBis());
  System.out.println("\nQuerying for  "+q7.toString());
  System.out.println(q7.execute(g).table());



  }


// Moved from PathPattern
    public static List<AssignmentResult> computeAssignment (PathPattern pat, Graph graph) {
    List<AssignmentResult> result = new LinkedList<AssignmentResult>();

    Record r = new Record();
    Set<Relation> used = new HashSet<Relation>();
    Set<Expression> postConditions = new HashSet<Expression>();
    for (Node n : graph.nodes()) {
      List<AssignmentResult> part_result
        = pat.computeAssignment(graph,
            new AssignmentResult(new Path(n), r,used,postConditions) );
      for (AssignmentResult assignment : part_result)
        if (assignment.filterPostConditions(graph))
          result.add(assignment);
    }
    return result;
  }

}




