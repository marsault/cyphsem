/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.outbridges.*;
import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.parser.Parser;

import uk.ac.ed.cyphsem.query.Query;


import java.nio.charset.StandardCharsets;

import java.util.Arrays;
import java.util.List;




public class CopyTest {

  public static void main (String[] args) throws Exception {
    Random r = new Random();
    Graph g1 = r.randomGraph();
    Graph g2 = g1.copy().first;

    g1.nodes().iterator().next().addLabel(new Label("extralabel"));
    g1.relations().iterator().next().overwriteProperty(new PropertyKey("extrakey"), new Value("ExtraValue"));
    Node n = g2.addNode();
    g2.addRelation(new RelationType("extratype"), n, g2.nodes().iterator().next());
    System.out.println(g1.toCypher());
    System.out.println();
    System.out.println(g2.toCypher());
  }

}
