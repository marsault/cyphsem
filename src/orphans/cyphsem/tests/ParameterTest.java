/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.outbridges.*;
import uk.ac.ed.cyphsem.outbridges.InterpreterComparator.*;
import uk.ac.ed.cyphsem.query.*;
import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.*;

import java.util.*;

public class ParameterTest {


  public static void main (String[] args) throws Exception {

    for (String opt : args) {
      String[] strs = opt.split("=");
      if (strs.length != 2)
        throw new RuntimeException("not two args");
      String key = strs[0].substring(2);
      String value = strs[1];
      Parameters.global.setParameter(key,value);
    }


    System.out.println(Parameters.global);
  }


}
