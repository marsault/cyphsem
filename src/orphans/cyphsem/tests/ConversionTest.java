/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.expression.SubListExpression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.parser.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.pattern.Iteration.*;
import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.query.*;
import uk.ac.ed.cyphsem.pattern.RelationPattern.Direction;
import uk.ac.ed.cyphsem.outbridges.*;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import java.io.File;

public class ConversionTest {

  public static void main(String[] args) throws Exception {
    Interpreter neocom = Interpreters.NEOCOM;
    neocom.load(GraphTest.sample().toCypher());
    Interpreter cyphsem = new CyphSemInterpreter(neocom);

    if (args.length == 0) {
      sampleTest(neocom,cyphsem);
      return;
    }

    String statement = "";
    for (int i = 0; i< args.length; i++ )
      statement += args[i] + " " ;

    Table t1 = null, t2 = null;

    System.out.println("Statement: "+statement);
    System.out.println("\n==============================| Neo4j-community |===============================");
    try {
      t1 = neocom.run(statement);
      System.out.println(t1);
      System.out.println("Run time: "+neocom.getLastRunTime());
    } catch (Exception e) {  e.printStackTrace();  }

    System.out.println("\n==================================| Cyphsem |===================================");
    try {
      t2 = cyphsem.run(statement);
//       System.out.println(Parser.queryOf(statement));
      System.out.println(t2);
      System.out.println("Run time: "+cyphsem.getLastRunTime());
    } catch (Exception e) {  e.printStackTrace();  }

    System.out.println("\n=============================| Are result equals? |=============================");
    if ((t1 != null) && (t2 != null)) {
      boolean b = t1.equals(t2);
      System.out.println(b);
      if (!b)
        System.out.println("Difference:\n"+Tables.diff(t1,t2));
    } else
      System.out.println("irrelevant");
  }


  public static void sampleTest(Interpreter neocom, Interpreter cyphsem) throws Exception {

    neocom.load(GraphTest.sample().toCypher());
    cyphsem.loadFrom(neocom);

    List<String> statements = Arrays.asList(
        "MATCH (a)-[r *2..3]-(a) UNWIND r as b Return a,b"
      , "RETURN *"
      , "WITH 2 as a UNWIND a as b RETURN b"
      , "UNWIND 2 as b RETURN b"
      , "RETURN 2[\"foo\"] as a"
      , "RETURN null[\"foo\"] as a"
      , "RETURN null[null+1..2] as a"
      , "RETURN null[null..\"bar\"] as a"
      , "RETURN null[null..null] as a"
      , "RETURN null[true..] as a"
      , "RETURN null[..[]] as a"
      , "MATCH (a) -[b]-> (c) WITH *, a.name IS NOT NULL AND c.name IS NOT NULL as x"+
                                ", (a.birthyear + c.birthyear) IS NOT NULL as y"+
                            " WHERE x and y and a.birthyear > c.birthyear return a.name,c.name"
      , "OPTIONAL MATCH (a) WHERE false RETURN *"
      , "OPTIONAL MATCH (a:nonexistent_label) RETURN *"
      , "MATCH (a:nonexistent_label {what:nonexistent_variable}) RETURN *"
      , "MATCH (a:human {what:nonexistent_variable}) RETURN *"
      , "MATCH (a {schooled_at:b.schooled_at}),(b) RETURN *"
      , "MATCH ()-[a *1]->() WHERE a RETURN *"
      , "UNWIND null as x RETURN *"
      , "MATCH (n) OPTIONAL MATCH p=(n), q=(n)-[r]->(n), (m:unexistent_label) RETURN *"
      );


    System.out.println("\u250F\u2501\u2501\u2501 Does NEOCOM fails ?");
    System.out.println("\u2503\u250F\u2501\u2501\u2501 Does CYPHSEM fails ?");
    System.out.println("\u2503\u2503\u250F\u2501\u2501\u2501 If Neither fails, are result equal ?");
    System.out.println("\u2503\u2503\u2503 \n");
    for (String stt : statements) {
      String str = "";
      boolean except = false;
      Table neo_result = null;
      Table cyphsem_result = null;
      try {
        neo_result = neocom.run(stt);
        str += "\u00B7";
      } catch (Exception e) {
        except = true;
        str += "\u2718";
      }
      try {
        cyphsem_result = cyphsem.run(stt);
        str += "\u00B7";
      } catch (Exception e) {
        except =  true;
        str += "\u2718";
      }

      if (!except)
        str +=  (neo_result.equals(cyphsem_result)?"=":"\u2260");
      else
        str += "\u00B7";
      System.out.println(str+"  "+stt);
    }
//     System.out.println(Parser.queryOf(statements.get(statements.size()-1)));
//     System.out.println(cyphsem.run(statements.get(statements.size()-1)));


  }

}
