/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.outbridges.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class GraphTest {

  public static void main(String[] args) throws InterpretingException {

//     Random r= new Random();
//     String g = r.randomGraph().toCypher();
//     System.out.println(g);
//     uk.ac.ed.cyphsem.outbridges.Interpreters.NEOCOM.load(g);

    System.out.println(sample().toCypher());
  }


  public static Graph sample() {
    Graph g = new Graph();
    Node n1 = g.addNode(new HashSet<>(), new HashMap<>());
    Node n2 = g.addNode(new HashSet<>(), new HashMap<>());
    Node n3 = g.addNode(new HashSet<>(), new HashMap<>());
    Node n4 = g.addNode(new HashSet<>(), new HashMap<>());

    RelationType knows = new RelationType("knows");
    RelationType kept = new RelationType("kept");
    RelationType has = new RelationType("has");

    Label human = new Label("human");
    Label cat = new Label("cat");
    Label black = new Label("black");
    Label savvy = new Label("savvy");

    PropertyKey firstname = new PropertyKey("first_name");
    PropertyKey school = new PropertyKey("schooled_at");
    PropertyKey since = new PropertyKey("since");
    PropertyKey color = new PropertyKey("color");

    n1.addLabel(human);
    n1.overwriteProperty(firstname, new Value("Paolo"));

    n3.addLabel(human);
    n3.addLabel(black);
    n3.addLabel(savvy);
    n3.overwriteProperty(firstname, new Value("Nadime"));
    n3.overwriteProperty(school, new Value("ENS Cachan"));

    n4.addLabel(cat);
    n4.addLabel(savvy);
    n4.overwriteProperty(firstname, new Value("Athanase"));
    n4.overwriteProperty(color, new Value("blue"));

    n2.addLabel(human);
    n2.overwriteProperty(firstname, new Value("Victor"));
    n2.overwriteProperty(school, new Value("ENS Cachan"));

    Relation r1 = g.addRelation(knows, n2, n3, new HashMap<>());
    Relation r2 = g.addRelation(knows, n3, n2, new HashMap<>());
    Relation r3 = g.addRelation(has, n2, n4, new HashMap<>());
    Relation r4 = g.addRelation(kept, n3, n4, new HashMap<>());
    Relation r5 = g.addRelation(knows, n1, n2, new HashMap<>());
    Relation r6 = g.addRelation(knows, n1, n1, new HashMap<>());

    r1.overwriteProperty(since,new Value(2008));
    r2.overwriteProperty(since,new Value(2008));
    r3.overwriteProperty(since,new Value(2015));

    return g;
  }

}
