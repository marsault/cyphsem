/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.graph.Graph;

import uk.ac.ed.cyphsem.parser.Parser;

import uk.ac.ed.cyphsem.query.Query;


import java.nio.charset.StandardCharsets;

import java.util.Arrays;
import java.util.List;




public class EchoTest {

  public static void main (String[] args) throws Exception {

    if (args.length!= 1) {
      System.out.println("Usage: EchoTest <query>");
      System.exit(1);
    }
    Query query = Parser.queryOf(args[0]);
    System.out.println(query);
  }

}
