/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.expression.*;


public class HashTest {

  public static void main(String[] args) {
    Name n11 = new Name("toto");
    Name n21 = new Name("titi");
    Name n12 = new Name("toto");
    Name n22 = new Name("titi");

    System.out.println(new Value().hashCode() == (new Value()).hashCode());
    {
      Record r1 = new Record();
      Record r2 = new Record();

      r1.bind(n11,new Value());
      r2.bind(n11,new Value());

      System.out.println(r1.equals(r2));
      System.out.println(r1.hashCode()+" "+r2.hashCode());
    }

    {
      Record r1 = new Record();
      Record r2 = new Record();

      r1.bind(n11,new Value());
      r2.bind(n12,new Value());

      System.out.println(r1.equals(r2));
      System.out.println(r1.hashCode()+" "+r2.hashCode());
    }

    {
      Record r1 = new Record();
      Record r2 = new Record();

      r1.bind(n11,new Value());
      r2.bind(n12,new Value());

      r1.bind(n21,new Value(191));
      r2.bind(n22,new Value(191));

      System.out.println(r1.equals(r2));
      System.out.println(r1.hashCode()+" "+r2.hashCode());
    }

    {
      Record r1 = new Record();
      Record r2 = new Record();

      r1.bind(n11,new Value());
      r1.bind(n21,new Value(191));
      r2.bind(n22,new Value(191));
      r2.bind(n12,new Value());

      System.out.println(r1.equals(r2));
      System.out.println(r1.hashCode()+" "+r2.hashCode());
    }


  }
}
