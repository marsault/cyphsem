/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.expression.*;
// import uk.ac.ed.cyphsem.expression.ValueToValueFunction.FunctionName;

import uk.ac.ed.cyphsem.graph.PropertyKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ExpressionTest {

  public static List<Expression> makeList(Expression... exps) {
    List<Expression> l = new ArrayList<Expression>(exps.length);
    for (Expression e : exps)
      l.add(e);
    return l;

  }

  public static ExplicitMapExpression makeMapE(Object... os) {
    List<PropertyKey> l1 = new ArrayList<PropertyKey>(os.length/2);
    List<Expression> l2 = new ArrayList<Expression>(os.length/2);
    for (int i=0; i< os.length ; i++) {
      l1.add((PropertyKey) os[i]);
      i++;
      l2.add((Expression) os[i]);
    }
    return new ExplicitMapExpression(l1,l2);
  }

//   public static Map<FunctionName,ValueToValueFunction> functions = ValueToValueFunction.functions;

  public static void main (String[] args) {

    PropertyKey name = new PropertyKey("name");
    PropertyKey age = new PropertyKey("age");
    Expression e1 = new UnaryMapExpression(
                        makeMapE( name, new Value("Victor"),
                                  age, new Value(28)),
                          age);
    Expression e2 =
      new FunctionalExpression( Functions.PLUS,
                new Value(2),
                e1);

    Expression e3= new FunctionalExpression( Functions.XOR,
        new FunctionalExpression( Functions.IS_NULL, new Value()),
        new FunctionalExpression( Functions.LE,
            new Value(30),
            e2
        )
    );

    Expression e4= new FunctionalExpression( Functions.XOR,
      new FunctionalExpression( Functions.IS_NULL, new Value()),
      new FunctionalExpression( Functions.LEQ,
          new Value(30),
            e2
        )
    );

    Expression e= new ExplicitListExpression(new Value("Hello"), new Value("World"));
    Expression f= new ExplicitListExpression(new Value(), new Value("World"));

    Expression e_eq_f = new FunctionalExpression(Functions.EQ, e, f);
    Expression e_in_f = new FunctionalExpression(Functions.IN, e, new ExplicitListExpression(f));


    System.out.println("e= "+e);
    System.out.println("f= "+f);
    System.out.println(e_eq_f+"   evaluates to   "+e_eq_f.evaluate(null,null));
    System.out.println(e_in_f+"   evaluates to   "+e_in_f.evaluate(null,null));



  }

}
