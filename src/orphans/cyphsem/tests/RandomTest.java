/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.exception.*;
import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.outbridges.*;
import uk.ac.ed.cyphsem.query.*;
import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;

public class RandomTest {

  public static void main(String[] args) throws Exception {

    Random r = new Random();
    Graph g = r.randomGraph();

    Interpreters.NEOCOM.load(g.toCypher());

    HashMap<Integer,Integer> results = new HashMap<Integer,Integer>();
    results.put(-2, 0);
    results.put(-1, 0);
    for (int i = 0; i<1000; i++) {
        if (i%100 == 0)
          System.out.println(i);
        Interpreter interpreter = new NeoComInterpreter(g);

        Query q = r.randomQueryBis();
//         System.out.println("=============================");
//         System.out.println(q);

        final Object[] t = new Object[1];
    uk.ac.ed.cyphsem.record.Table result = null;

    Callable<uk.ac.ed.cyphsem.record.Table> runnable = new Callable<uk.ac.ed.cyphsem.record.Table> () {
      public uk.ac.ed.cyphsem.record.Table call() throws InterpretingException,InterruptedException {
        return interpreter.run(q.toString());
      }
    };
    long start = System.nanoTime();
    ExecutorService exe = Executors.newSingleThreadExecutor();
    boolean finishedProperly = false;
    Future<uk.ac.ed.cyphsem.record.Table> futureResult = exe.submit(runnable);
    exe.shutdown();

    try {
      result = futureResult.get(1, TimeUnit.SECONDS);
      Integer str = result.recordSet().size();
      Integer prev = results.get(str);
      results.put(str, (prev==null)?1:(prev+1));
//       System.out.println(str);
    } catch (TimeoutException e) {
//       futureResult.cancel(true);
//       System.out.println("-2");
      interpreter.interrupt();
      results.put(-2, results.get(-2)+1);
    } catch (InterruptedException e) { throw new RuntimeException(e);
    } catch (ExecutionException e) {
      results.put(-1, results.get(-1)+1);
//       System.out.println("-1: "+e);
    }
//     exe.shutdownNow();


//         ExecutorService exe = Executors.newSingleThreadExecutor();
//         boolean finishedProperly;
//         try {
//           exe.execute(thread);
//           exe.shutdown();
//           finishedProperly = exe.awaitTermination(1, TimeUnit.SECONDS);
//         } catch (InterruptedException e) { finishedProperly = false; }
//         if (!finishedProperly) {
//
//         } else {
// //           exe.shutdownNow();
//           if (t[0] instanceof Table){
//             Integer str = ((Table) t[0]).keySet().size();
//             Integer prev = results.get(str);
//             results.put(str, (prev==null)?1:(prev+1));
//           } else if (t[0] instanceof Exception)
//           else
//             System.out.println(t[0]);
//       }
      exe.shutdown();
      interpreter.close();
    }
    System.out.println(results);
  }
}
