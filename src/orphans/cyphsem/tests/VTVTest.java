/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.exception.UnspecifiedBehaviourException;
import uk.ac.ed.cyphsem.expression.Value;
import uk.ac.ed.cyphsem.expression.ValueToValueFunction;
import uk.ac.ed.cyphsem.expression.Functions;

import uk.ac.ed.cyphsem.graph.PropertyKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


class VTVTest {


  public static List<Value> makeList(Value... vals) {
    List<Value> l = new ArrayList<Value>(vals.length);

    for (Value v: vals) {
      l.add(v);
    }
    return l;
  }

  public static Value makeListV(Value... vals) {
    return new Value(makeList(vals));
  }

  public static Value makeMap(Object... vals) {
    int max = vals.length;
    HashMap<PropertyKey,Value> m = new HashMap<PropertyKey,Value>();

    for (int i=0; i< max; i++) {
      PropertyKey k = (PropertyKey) vals[i];
      i++;
      Value v=  (Value) vals[i];
      m.put(k,v);
    }
    return new Value(m);
  }


//   public static Map<FunctionName,ValueToValueFunction> functions = ValueToValueFunction.functions;

  public static void main (String[] args) {

    System.out.println("\n1. Building and displaying Values");

    Value bool_1 =  new Value(true);
    Value bool_1bis = new Value(true);
    Value bool_2 =  new Value(false);

    System.out.println("\t"+bool_1);
    System.out.println("\t"+bool_2);

    Value int_1 =  new Value(3);
    Value int_1bis = new Value(3);
    Value int_2 =  new Value(42);
    Value int_3 =  new Value(19876);

    System.out.println("\t"+int_1);
    System.out.println("\t"+int_2);
    System.out.println("\t"+int_3);

    Value null_1 = new Value();
    Value null_2 = new Value();

    System.out.println("\t"+null_1);
    System.out.println("\t"+null_2);


    Value str_1 = new Value("Hello");
    Value str_1bis = new Value("Hello");
    Value str_2 = new Value("World");
    Value str_3 = new Value("Hello World !");

    System.out.println("\t"+str_1);
    System.out.println("\t"+str_2);
    System.out.println("\t"+str_3);

    Value list_1 = makeListV(int_1, int_2, new Value("Test"));
    Value list_1bis = makeListV(new Value(3), int_2, new Value("Test"));
    Value list_1ter = makeListV(new Value(3), new Value(), new Value("Test"));
    Value list_2 = makeListV(new Value(3), int_3, new Value("Test2"));
    Value list_4 = makeListV(new Value(3));
    Value list_3 = makeListV(list_1, null_2, int_3);

    System.out.println("\t"+list_1);
    System.out.println("\t"+list_2);
    System.out.println("\t"+list_3);

    PropertyKey k_1 =  new PropertyKey("FirstName");
    PropertyKey k_2 =  new PropertyKey("LastName");
    PropertyKey k_3 =  new PropertyKey("Age");
    PropertyKey k_4 =  new PropertyKey("From");

    Value map_1 = makeMap(k_1, list_1, k_2, str_1, k_3, int_1bis);
    Value map_1bis = makeMap(k_1, list_1bis, new PropertyKey("LastName"), str_1, k_3, int_1bis);
    Value map_1ter = makeMap(k_1, list_1ter, new PropertyKey("LastName"), new Value(), k_3, int_1bis);
    Value map_2 = makeMap(k_1, list_1bis, k_2, str_1);
    Value map_3 = makeMap();
    Value map_4 = makeMap();

    System.out.println("\t"+map_1);
    System.out.println("\t"+map_2);

    System.out.println("\n2.1 Binary String, expected false");
    callAndCatchUBE(Functions.STARTS_WITH,makeList(str_1,str_3));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(str_1,str_2));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(str_2,str_1));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(new Value("!"),str_3));
    callAndCatchUBE(Functions.CONTAINS,makeList(str_3,new Value("toto")));

    System.out.println("\n2.2 Binary String, expected false");
    callAndCatchUBE(Functions.STARTS_WITH,makeList(str_3,str_1));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(str_3,new Value("!")));
    callAndCatchUBE(Functions.CONTAINS,makeList(str_3,str_2));

    System.out.println("\n2.3 Binary String, expected UnspecfiedBehaviourException");
    callAndCatchUBE(Functions.STARTS_WITH,makeList(map_1,list_2,list_3));
    callAndCatchUBE(Functions.STARTS_WITH,makeList(map_1,str_1));
    callAndCatchUBE(Functions.STARTS_WITH,makeList(str_1,list_1));
    callAndCatchUBE(Functions.STARTS_WITH,makeList(str_1));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(map_1,list_2,list_3));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(list_1,str_1));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(str_1,bool_1));
    callAndCatchUBE(Functions.ENDS_WITH,makeList(str_1));
    callAndCatchUBE(Functions.CONTAINS,makeList(map_1,list_2,list_3));
    callAndCatchUBE(Functions.CONTAINS,makeList(list_1,str_1));
    callAndCatchUBE(Functions.CONTAINS,makeList(str_1,int_1));
    callAndCatchUBE(Functions.CONTAINS,makeList(str_1));

    System.out.println("\n3.1 Equality, expected true");
    callAndCatchUBE(Functions.EQ,makeList(bool_1,bool_1bis));
    callAndCatchUBE(Functions.EQ,makeList(str_1,str_1bis));
    callAndCatchUBE(Functions.EQ,makeList(int_1,int_1bis));
    callAndCatchUBE(Functions.EQ,makeList(list_1,list_1bis));
    callAndCatchUBE(Functions.EQ,makeList(makeListV(),makeListV()));
    callAndCatchUBE(Functions.EQ,makeList(map_1,map_1bis));
    callAndCatchUBE(Functions.EQ,makeList(makeMap(),makeMap()));
    callAndCatchUBE(Functions.EQ,makeList(map_3,map_4));


    System.out.println("\n3.2 Equality, expected null");
    callAndCatchUBE(Functions.EQ,makeList(null_1,null_2));
    callAndCatchUBE(Functions.EQ,makeList(null_1,bool_1));
    callAndCatchUBE(Functions.EQ,makeList(null_1,int_1));
    callAndCatchUBE(Functions.EQ,makeList(null_1,str_1));
    callAndCatchUBE(Functions.EQ,makeList(null_1,list_1));
    callAndCatchUBE(Functions.EQ,makeList(null_1,map_1));
    callAndCatchUBE(Functions.EQ,makeList(list_1,list_1ter));
    callAndCatchUBE(Functions.EQ,makeList(map_1,map_1ter));

    System.out.println("\n3.3 Equality, expected false");
    callAndCatchUBE(Functions.EQ,makeList(bool_1,bool_2));
    callAndCatchUBE(Functions.EQ,makeList(int_1,int_2));
    callAndCatchUBE(Functions.EQ,makeList(str_1,str_2));
    callAndCatchUBE(Functions.EQ,makeList(list_1,list_2));
    callAndCatchUBE(Functions.EQ,makeList(map_1,map_2));
    callAndCatchUBE(Functions.EQ,makeList(map_1,map_3));
    callAndCatchUBE(Functions.EQ,makeList(str_1,map_2));
    callAndCatchUBE(Functions.EQ,makeList(list_1,bool_2));
    callAndCatchUBE(Functions.EQ,makeList(map_1,list_2));
    callAndCatchUBE(Functions.EQ,makeList(list_1,list_4));

    System.out.println("\n3.4 Equality, expected UnspecifiedBehaviourException");
    callAndCatchUBE(Functions.EQ,makeList(map_1));
    callAndCatchUBE(Functions.EQ,makeList(bool_1,bool_2,bool_2));
    callAndCatchUBE(Functions.EQ,makeList(bool_1,int_2));
    callAndCatchUBE(Functions.EQ,makeList(str_1,int_2));
    callAndCatchUBE(Functions.EQ,makeList(str_1,bool_2));

    System.out.println("\n4.1 Inequality, expected false");
    callAndCatchUBE(Functions.NEQ,makeList(bool_1,bool_1bis));
    callAndCatchUBE(Functions.NEQ,makeList(str_1,str_1bis));
    callAndCatchUBE(Functions.NEQ,makeList(int_1,int_1bis));
    callAndCatchUBE(Functions.NEQ,makeList(list_1,list_1bis));
    callAndCatchUBE(Functions.NEQ,makeList(makeListV(),makeListV()));
    callAndCatchUBE(Functions.NEQ,makeList(map_1,map_1bis));
    callAndCatchUBE(Functions.NEQ,makeList(makeMap(),makeMap()));


    System.out.println("\n4.2 Inequality, expected null");
    callAndCatchUBE(Functions.NEQ,makeList(null_1,null_2));
    callAndCatchUBE(Functions.NEQ,makeList(null_1,bool_1));
    callAndCatchUBE(Functions.NEQ,makeList(null_1,int_1));
    callAndCatchUBE(Functions.NEQ,makeList(null_1,str_1));
    callAndCatchUBE(Functions.NEQ,makeList(null_1,list_1));
    callAndCatchUBE(Functions.NEQ,makeList(null_1,map_1));
    callAndCatchUBE(Functions.NEQ,makeList(list_1,list_1ter));
    callAndCatchUBE(Functions.NEQ,makeList(map_1,map_1ter));

    System.out.println("\n4.3 Inequality, expected true");
    callAndCatchUBE(Functions.NEQ,makeList(bool_1,bool_2));
    callAndCatchUBE(Functions.NEQ,makeList(int_1,int_2));
    callAndCatchUBE(Functions.NEQ,makeList(str_1,str_2));
    callAndCatchUBE(Functions.NEQ,makeList(list_1,list_2));
    callAndCatchUBE(Functions.NEQ,makeList(map_1,map_2));
    callAndCatchUBE(Functions.NEQ,makeList(str_1,map_2));
    callAndCatchUBE(Functions.NEQ,makeList(list_1,bool_2));
    callAndCatchUBE(Functions.NEQ,makeList(map_1,list_2));

    System.out.println("\n4.4 Inequality, expected UnspecfiedBehaviourException");
    callAndCatchUBE(Functions.NEQ,makeList(map_1));
    callAndCatchUBE(Functions.NEQ,makeList(bool_1,bool_2,bool_2));
  }

  public static void callAndCatchUBE (Functions name, List<Value> args) {
    try {
      Value v = name.call(args);
      System.out.println("\t"+v);
    } catch (UnspecifiedBehaviourException e) {
      System.out.println("\t[E] Caught UnspecifiedBehaviourException:");
      System.out.println("\t\t"+e.getMessage());
    }
  }


}
