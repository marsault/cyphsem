/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.Utils;
import uk.ac.ed.cyphsem.graph.Graph;

import uk.ac.ed.cyphsem.parser.Parser;


import java.nio.charset.StandardCharsets;

import java.util.Arrays;
import java.util.List;




public class ParserTest {

  public static void main (String[] args) throws Exception {

    if (args.length == 1) {
      System.out.println(Parser.queryOf(args[0]));
      System.exit(0);
    }

//     System.out.println("============ Expressions ============");
//     List<String> exps = Arrays.asList(
//       "2+0xA2",
//       "[false and (b or a ) and true[2..][..4] STARTS WITH 2[2..true]]",
//       "[false AND 2+1<3-4 XoR false,true,{toto:2}]",
//       "{k:false, A:2+0, totally_a_name:[\"test\"]}",
//       "[a,b,c][..0x10000000000000000-18446744073709551615]" // 2**64-(2**64-1)
//       );
//      for (String str : exps) {
//       System.out.println(str);
//       System.out.println(Main.parseExpression(str));
//       System.out.println();
//     }

    System.out.println("============ Queries ============");
    List<String> queries = Arrays.asList(
      "Return *, NOT true as c, [e.test] as f"
      , "OPTIOnAL MATch (a :human)-[:knows]->(b) RETuRn * UNIoN MaTCH (:human)-[a ]->(b) RETURN *"
      , "WItH *, [a,b] as c UNWiND c as d REtURN *"
      , "MatcH p= ()-[]->(a),q = (a {name:c.name})-[b]->(c {name:a.name}) return a,c,p1"
      , "MATCH (n:suricate) -[:plays]-> (m:videogame) RETURN m"
      );
     for (String str : queries) {
      System.out.println(str);
      System.out.println(Parser.queryOf(str));
      System.out.println();
    }

    String yago = Utils.readFile("sample-graphs/yago.cypher");
    Graph g = Parser.graphOf(yago);
    System.out.println(g.toCypher());
  }

}
