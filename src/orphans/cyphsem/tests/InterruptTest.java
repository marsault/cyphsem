/*  CyphSem 
    Copyright (c) 2018,2019 Victor Marsault

    This file is part of CyphSem.

    CyphSem is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CyphSem is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CyphSem. If not, see <https://www.gnu.org/licenses/>.          */

package uk.ac.ed.cyphsem.tests;

import uk.ac.ed.cyphsem.expression.*;
import uk.ac.ed.cyphsem.graph.*;
import uk.ac.ed.cyphsem.pattern.*;
import uk.ac.ed.cyphsem.outbridges.*;
import uk.ac.ed.cyphsem.query.*;
import uk.ac.ed.cyphsem.record.*;
import uk.ac.ed.cyphsem.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit.*;
import java.util.HashMap;
import java.util.Map;



public class InterruptTest {

  public static void main (String[] args) throws Exception {

//     Interpreters.NEOCOM.make_graph();
    Runnable runnable = new Runnable() {
      public void run() {
        try {
          Interpreters.NEOCOM.run("MATCH ()-[a *]-() RETURN *");
        } catch (Exception e) { System.out.println(e); e.printStackTrace(); }
      }
    };
    ExecutorService exe = Executors.newSingleThreadExecutor();
    boolean finishedProperly;
    try {
      exe.execute(runnable);
      exe.shutdown();
      finishedProperly = exe.awaitTermination(2, TimeUnit.SECONDS);
    } catch (InterruptedException e) { finishedProperly = false; }
    if (!finishedProperly)
      Interpreters.NEOCOM.interrupt();

    System.out.println("end:"+finishedProperly);
    exe.shutdownNow();
  }

}
