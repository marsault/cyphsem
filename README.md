# cyphsem

Requirement :
 - Java 9
 - Maven 3.5.2+

Compile with:
$ mvn compile

Launch test suite with:
$ mvn test

Produce doc with:
$ mvn javadoc:javadoc

